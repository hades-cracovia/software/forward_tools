#!/bin/env python3

import argparse
import glob
import sys
from online_helpers import *

WD=os.path.dirname(os.path.realpath(__file__))
config = parse_shell_config(make_abs('config.sh', WD))

ABS_DATA_DIR=make_abs(config.DATA_DIR, WD)
ABS_LUMI_STATUS_LIST=make_abs(config.LUMI_STATUS_LIST, ABS_DATA_DIR)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--foo', help='foo help')

    args, opts = parser.parse_known_args()

    if len(opts) > 1:
        print("Only one directory allowed")
        sys.exit(1)

#    file_filter = opts[0] + "/*.lumi" if len(opts) == 1 else "*.lumi"
    #print(f"Use filter {file_filter}")
#    fl = sorted(glob.glob(file_filter))

    print(f"Read inputs from {ABS_LUMI_STATUS_LIST}", file=sys.stderr)
    inputs = get_status_list_from_file(ABS_LUMI_STATUS_LIST)

    file_duration = {}
    n_prots = {}
    n_protv = {}
    n_evts = {}
    n_elas = {}
    lumi_hh = {}
    lumi_hf = {}

    broken_hld = open('broken_list_hld.tx', 'w+')
    broken_dst = open('broken_list_dst.txt', 'w+')

    counter = 0
    accepted = 0
    skipped = 0
    for f in inputs:
        counter = counter + 1
        print(f"({counter}) Read {f} ({counter})", file=sys.stderr, end='\r')

        if f[1] is not 0:
            skipped = skipped + 1
            continue
        accepted = accepted + 1

        with open(f[0]) as file:
            line = file.readline()
            sl = [float(x) for x in line.split()[0:8]]
            if sl[0] == 0:
                continue
            k=sl[0]  # k - key, a timestamp
            d=sl[1]
            ps=sl[3]
            pv=sl[4]
            e=sl[5]
            s=sl[6]
            lhh=sl[7]

            if ps > 1e10 or pv > 1e10 or ps == 0 or pv == 0:
                broken_hld.write(f[2].split('_dst_')[0] + '\n')
                broken_dst.write(f[2].split('_dst_')[0] + '\n')

            if not k in n_prots:
                file_duration[k] = 0
                n_prots[k] = 0
                n_protv[k] = 0
                n_evts[k] = 0
                n_elas[k] = 0
                lumi_hh[k] = 0

            if d > file_duration[k]:
                file_duration[k] = d
            n_prots[k] = n_prots[k] + ps
            n_protv[k] = n_protv[k] + pv
            n_evts[k] = n_evts[k] + e
            n_elas[k] = n_elas[k] + s
            lumi_hh[k] = lumi_hh[k] + lhh

    print(f"\n", file=sys.stderr)

    start_t = 0
    last_t = 0
    # accumulated values
    acc_ps = 0  # no. protons in START
    acc_pv = 0  # -//- in VETO
    acc_e = 0   # no. events
    acc_s = 0   # no. elastics
    tot_d = 0   # total duration
    tot_lhh = 0 # total lumi hh

    # previous values
    prev_t = 0
    n = 0
    for k, v in n_prots.items():
        n = n + 1

        if start_t == 0:
            start_t = k
        else:
            print(f"{prev_t}  {acc_ps}  {acc_pv}  {acc_s}  {tot_d}  {acc_e}  {tot_lhh}")

        prev_t = k

        acc_ps = acc_ps + v
        acc_pv = acc_pv + n_protv[k]
        acc_e = acc_e + n_evts[k]
        acc_s = acc_s + n_elas[k]
        tot_d = tot_d + file_duration[k]
        tot_lhh = tot_lhh + lumi_hh[k]

        last_t = k

    avg_t = tot_d/n if n > 0 else 0
    print(f"{last_t}  {acc_ps}  {acc_pv}  {acc_s}  {tot_d + avg_t}  {acc_e}  {tot_lhh}")

    print(f"Summary: checked: {counter}  accepted: {accepted}  skipped: {skipped}", file=sys.stderr)
