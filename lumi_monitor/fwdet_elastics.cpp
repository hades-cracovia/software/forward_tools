#include "forward_tools.h"
#include "ft_config.h"

#include <hades.h>
#include <hloop.h>
#include <htaskset.h>

#include <hcategory.h>
#include <hcategorymanager.h>
#include <hrecevent.h>
#include <hreconstructor.h>
#include <hruntimedb.h>

//--------category definitions---------
#include <hgeantdef.h>
#include <hparticledef.h>
#include <hpiontrackerdef.h>
#include <hstartdef.h>
//-------------------------------------

//-------objects-----------------------
#include <heventheader.h>
#include <hgeantkine.h>
#include <hparticlecand.h>
#include <hparticlecandsim.h>
#include <hparticleevtinfo.h>
#include <hparticletracksorter.h>
#include <hpiontrackertrack.h>
#include <hstart2hit.h>

#include <hstart2cal.h>
//-------------------------------------
#include <hparticletool.h>
#include <hphysicsconstants.h>

#include <henergylosscorrpar.h>

#include <forwarddef.h>
#include <hdst.h>
#include <hforwardcand.h>
#include <hfrpccal.h>
#include <hfrpccalpar.h>
#include <hfrpccluster.h>
#include <hfrpcdigipar.h>
#include <hfrpcgeompar.h>
#include <hfrpchit.h>
#include <hfrpcraw.h>
#include <hgeantfrpc.h>
#include <hgeantsts.h>
#include <hgeomcompositevolume.h>
#include <hgeomvector.h>
#include <hgeomvolume.h>
#include <hparasciifileio.h>
#include <hparticlecand.h>
#include <hrpccal.h>
#include <hrpccalpar.h>
#include <hrpccluster.h>
#include <hrpcdigipar.h>
#include <hrpcgeompar.h>
#include <hrpchit.h>
#include <hrpcraw.h>
#include <hspectrometer.h>
#include <hstart2hit.h>
#include <hstscal.h>
#include <hstsgeompar.h>
#include <hstsraw.h>
#include <htofhit.h>
//#include <htofraw.h>
#include <rpcdef.h>
#include <tofdef.h>

#include <TCanvas.h>
#include <TColor.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1I.h>
#include <TH2F.h>
#include <TH2I.h>
#include <TH3I.h>
#include <THStack.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TMatrixD.h>
#include <TMultiGraph.h>
#include <TROOT.h>
#include <TStopwatch.h>
#include <TString.h>
#include <TStyle.h>
#include <TTree.h>
#include <TVector3.h>

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include <vector>
#include <math.h>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

using namespace std;
using namespace ConfigBeamtime;

//##############################################################################################
// Usefull functions and definitions
//##############################################################################################

//----------------------------------------------------------------------------------------------
// Defining parameters
//----------------------------------------------------------------------------------------------

typedef void (*pad_callback)(TVirtualPad* pad);

struct AnaParameters
{
    TString outfile;
    TString outpath;
    TString paramfile;
    std::string label;
    int events;
    int start;
    int sim;
    int verbose;
    bool show_fakes;
    int cal_frpc_stage1{0};
    int cal_frpc_stage2{0};
    int cal_frpc_stage3{0};
    int cal_frpc_stage4{0};
    int cal_frpc_stage5{0};
    int pt1{0};
    int pt2{0};
    int pt3{0};
    int no_t0{0};
    int alt_ToF{0};
};

const Float_t avg_beam_rate = 400e3; // 400 kHz

int no_fStart2Hit_cnt = 0;
int rejected_no_T0_cnt = 0;

TString format_pic_name(TString path, TString name)
{
    return path + "PICTURES/QA_TEST/" + name + ".png";
}

struct tm decodeHadesTimeAndDate(UInt_t date, UInt_t time, bool verbose = false)
{
    time_t rawtime;
    std::time(&rawtime);
    struct tm t = *localtime(&rawtime);

    t.tm_mday = (date & 0xFF);
    t.tm_mon = 1 + ((date >> 8) & 0xFF) - 1;
    t.tm_year = ((date >> 16) & 0xFF);

    t.tm_hour = ((time >> 16) & 0xFF) + 1;
    t.tm_min = (time >> 8) & 0xFF;
    t.tm_sec = time & 0xFF;

    if (verbose)
    {
        printf("Date:  %#x   Time:  %#x\n", date, time);
        printf("Decoded  %4d-%02d-%02d %02d:%02d:%02d\n", 1900 + t.tm_year, t.tm_mon + 1, t.tm_mday,
               t.tm_hour, t.tm_min, t.tm_sec);
    }

    return t;
}

//##############################################################################################
// fwdet_tests function
//##############################################################################################

Int_t fwdet_tests(HLoop* loop, const AnaParameters& anapars)
{
    // Check if loop was properly initialized
    // if (!loop->setInput(""))
    if (!loop->setInput(
            "-*,+HParticleCand,+HForwardCand,+HFRpcRaw,+HFRpcCal,+HStsRaw")) //,+HStart2Hit,+HRpcCal,+HTofRaw"))
                                                                    //// ,+HFRpcHit,+HFRpcClus
    { // reading file structure
        std::cerr << "READBACK: ERROR : cannot read input !\n";
        std::exit(EXIT_FAILURE);
    }

    // Timer for checking analysis time
    TStopwatch timer;
    timer.Reset(); // Reset timer
    timer.Start(); // Start timer (T0)

    //////////////////////////////////////////////////////////////////////////////
    //      Fast tree builder for creating of ntuples                            //
    //////////////////////////////////////////////////////////////////////////////

    // Hades *myHades = new Hades;
    // myHades->setTreeBufferSize(8000);
    // HSpectrometer* spec = gHades->getSetup();

    //----------------------------------------------------------------------------------------------
    // Note on how to save terminal output to a txt file
    //----------------------------------------------------------------------------------------------

    // Add " | tee fRPC_progress.txt " to the end of the command executing the program to save all
    // the output to screen into a txt file Alternatively " 2>&1 | tee fRPC_progress.txt " to
    // capture also error output

    // Using files by Lukas
    // ./bin/fwdet_elastics /lustre/hades/dst/feb22/online/035/root/be2203513* -p
    // ../../feb22_dst_params.txt -o output_el_test.root -d ./frpc_cal_output/ -e 5000000 --exp 2>&1
    // | tee ./frpc_cal_output/fRPC_el_test_progress.txt

    // Using files by Rafal
    // ./bin/fwdet_elastics
    // /lustre/nyx/hades/user/rlalik/hades/feb22/tools/data/out/22035/be2203513* -p
    // ../../feb22_dst_params.txt -o output_el_Rafal.root -d ./frpc_cal_output/ -e 5000000 --exp
    // 2>&1 | tee ./frpc_cal_output/fRPC_el_Rafal_progress.txt

    //----------------------------------------------------------------------------------------------
    // Input parameters file
    //----------------------------------------------------------------------------------------------

    cout << "Input parameters file ...\n";

    Int_t mdcMods[6][4] = {{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1},
                           {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};
    HDst::setupSpectrometer("feb22", mdcMods, "sts,frpc");

    TString asciiParFile = anapars.paramfile;  // File containing all parameters of the detector
    HRuntimeDb* rtdb = HRuntimeDb::instance(); // myHades -> getRuntimeDb();

    // Checking if parameters file was opened properly
    if (!asciiParFile.IsNull())
    {
        HParAsciiFileIo* input1 = new HParAsciiFileIo;
        input1->open((Text_t*)asciiParFile.Data(), "in");
        if (!input1->check())
        {
            std::cerr << "Param file " << asciiParFile << " not open!\n";
            abort();
        }
        rtdb->setFirstInput(input1);
    }

    // Initializing the parameter container for geometry
    HStsGeomPar* pStrawGeomPar = (HStsGeomPar*)gHades->getRuntimeDb()->getContainer("StsGeomPar");
    // Checking if parameters container for geometry was created properly
    if (!pStrawGeomPar)
    {
        Error("HStsDigitizer::init()", "Parameter container for geometry not created");
        return kFALSE;
    }

    // Initializing the parameter container for geometry
    HFRpcGeomPar* pFRpcGeomPar = (HFRpcGeomPar*)gHades->getRuntimeDb()->getContainer("FRpcGeomPar");
    // Checking if parameters container for geometry was created properly
    if (!pFRpcGeomPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for geometry not created");
        return kFALSE;
    }

    HFRpcDigiPar* pFRpcDigiPar = (HFRpcDigiPar*)gHades->getRuntimeDb()->getContainer("FRpcDigiPar");
    if (!pFRpcDigiPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for HFRpcDigiPar not created");
        return kFALSE;
    }

    HFRpcCalPar* pFRpcCalPar = (HFRpcCalPar*)gHades->getRuntimeDb()->getContainer("FRpcCalPar");
    if (!pFRpcCalPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for FRpcCalPar not created");
        return kFALSE;
    }

    rtdb->initContainers(0);
    rtdb->print();
    // pFRpcCalPar->printParams();

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s\n";

    //----------------------------------------------------------------------------------------------
    // Output parameters file
    //----------------------------------------------------------------------------------------------

    cout << "Output parameters file ...\n";

    HParAsciiFileIo* f_param_output = new HParAsciiFileIo;

    // Print detector parameters on screen
    if (anapars.verbose)
    {
        printf("PARAMETERS LOADING\n");
        pStrawGeomPar->print();
        pFRpcGeomPar->print();
    }

    /*
        // Array of indices for short straws
        //Int_t short_index[STS_MAX_MODULES];

        // Float_t sina[STS_MAX_MODULES][FWDET_STRAW_MAX_LAYERS];
        // Float_t cosa[STS_MAX_MODULES][FWDET_STRAW_MAX_LAYERS];

        for (Int_t m = 0; m < STS_MAX_MODULES; ++m)
        {
            short_index[m] = 0;//2 * pStrawGeomPar->getShortIndex(m);    FIXME short index


            for (Int_t l = 0; l < FWDET_STRAW_MAX_LAYERS; ++l)
            {
                Float_t a = pStrawGeomPar->getLayerRotation(m, l) * TMath::DegToRad();
                cosa[m][l] = TMath::Cos(a);
                sina[m][l] = TMath::Sin(a);
            }
        }
        */
    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s\n";

    //----------------------------------------------------------------------------------------------
    // Accessing categories inside input file
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Accessing categories inside input file ...\n";

    // Hades Particle Candidates
    HCategory* fParticleCand =
        HCategoryManager::getCategory(catParticleCand, kTRUE, "catParticleCand");
    if (!fParticleCand) { cout << "No catParticleCand!\n"; }

    // Forward Detector Candidates
    HCategory* fForwardCand =
        HCategoryManager::getCategory(catForwardCand, kTRUE, "catForwardCand");
    if (!fForwardCand) { cout << "No catForwardCand!\n"; }

    // Forward Detector RPC Raw data
    HCategory* fFRpcRaw = HCategoryManager::getCategory(catFRpcRaw, kTRUE, "catFRpcRaw");
    if (!fFRpcRaw) { cout << "No catfFRpcRaw!\n"; }

    // Forward Detector RPC Calibrated data
    HCategory* fFRpcCal = HCategoryManager::getCategory(catFRpcCal, kTRUE, "catFRpcCal");
    if (!fFRpcCal) { cout << "No catFRpcCal!\n"; }

    // hstart2hit
    HCategory* fStart2Hit = HCategoryManager::getCategory(catStart2Hit, kTRUE, "catStart2Hit");
    if (!fStart2Hit) { cout << "No catStart2Hit!\n"; }

    // Categories for PT1 trigger
    HCategory* rpcCal = nullptr;
    rpcCal = HCategoryManager::getCategory(catRpcCal, kTRUE, "catRpcCal");
    if (!rpcCal) { cout << "No catRpcCal!\n"; }

    // Forward Detector Raw data
    HCategory* fStsRaw = HCategoryManager::getCategory(catStsRaw, kTRUE, "catStsRaw");
    if (!fStsRaw) { cout << "No fStsRaw!" << endl; }

    /*
    HCategory * tofRaw= nullptr;
    tofRaw = HCategoryManager::getCategory(catTofRaw, kTRUE, "catTofRaw");
    if(!tofRaw){ cout<< "No catTofRaw!"<<endl; }

    // Forward Detector Calibrated data
    HCategory* fStsCal = HCategoryManager::getCategory(catStsCal, kTRUE, "catStsCal");
    if (!fStsCal) { cout << "No catStsCal!" << endl; }

    // Forward Detector RPC Cluster
    HCategory* fFRpcClus = HCategoryManager::getCategory(catFRpcClus, kTRUE, "catFRpcCluster");
    if (!fFRpcClus) { cout << "No catFRpcClus!" << endl; }

    // Forward Detector RPC Hits
    HCategory* fFRpcHit = HCategoryManager::getCategory(catFRpcHit, kTRUE, "catFRpcHit");
    if (!fFRpcHit) { cout << "No catFRpcHit!" << endl; }

    // RPC Raw data
    HCategory* fRpcRaw = HCategoryManager::getCategory(catRpcRaw, kTRUE, "catRpcRaw");
    if (!fRpcRaw) { cout << "No catfRpcRaw!" << endl; }

    // RPC Calibrated data
    HCategory* fRpcCal = HCategoryManager::getCategory(catRpcCal, kTRUE, "catRpcCal");
    if (!fRpcCal) { cout << "No catRpcCal!" << endl; }

    // RPC Cluster
    HCategory* fRpcClus = HCategoryManager::getCategory(catRpcClus, kTRUE, "catRpcCluster");
    if (!fRpcClus) { cout << "No catRpcClus!" << endl; }

    // RPC Hits
    HCategory* fRpcHit = HCategoryManager::getCategory(catRpcHit, kTRUE, "catRpcHit");
    if (!fRpcHit) { cout << "No catRpcHit!" << endl; }
    */

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s\n";

    //----------------------------------------------------------------------------------------------
    // Setting parameters for loop over events
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Setting parameters for loop over events ...\n";

    Int_t entries = loop->getEntries(); // Number of entries in loop
    int limit_sta = anapars.start;      // Limit START - Where to start the loop
    int limit_sto = 0;                  // Limit STOP - Where to stop the loop

    if (anapars.events >= 0)
        limit_sto = limit_sta + anapars.events;
    else
        limit_sto = entries;

    if (limit_sto > entries) limit_sto = entries;

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s\n";

    //----------------------------------------------------------------------------------------------
    // Output file and directories definition
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Output file and directories definition ...\n";

    TFile* f_root_output = TFile::Open(anapars.outpath + anapars.outfile, "RECREATE");
    TDirectory* dir_Elastics = f_root_output->mkdir("Elastics");
    TDirectory* dir_Sectors = f_root_output->mkdir("Sectors");
    TDirectory* dir_Mass = f_root_output->mkdir("Mass");
    TDirectory* dir_STS_Mult = f_root_output->mkdir("STS_Mult");
    TDirectory* dir_Vertex_Rec = f_root_output->mkdir("Vertex_Rec");

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s\n";

    //------------------------------------------------------------------------------------------------------
    // Important variables definition
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Important variables definition ...\n";

    char buff1[1000];
    char buff2[1000];

    Int_t particle_cand_cnt = 0;
    Int_t forward_cand_cnt = 0;

    Bool_t check_mom = false;
    Bool_t check_theta = false;

    Int_t t_PT1, t_PT2, t_PT3;
    Int_t t_ToF_Rec;
    Int_t t_Sector_P1, t_Sector_P2;
    Int_t t_mass_P1, t_mass_P2;
    Float_t t_z_start_P1, t_r_start_P1;                   //, t_track_length_P1;
    Float_t t_z_start_P2, t_r_start_P2;                   //, t_track_length_P2;
    Float_t t_theta_P1, t_phi_P1, t_mom_P1, t_mom_theor_P1 ,t_beta_P1; //, t_tof_P1, t_tof_theor_P1, t_mom_P1, t_mom_theor_P1, t_beta_P1, t_beta_theor_P1, t_E_P1, t_E_theor_P1;
    Float_t t_theta_P2, t_phi_P2, t_mom_P2, t_mom_theor_P2 ,t_beta_P2; //, t_tof_P2, t_tof_theor_P2, t_mom_P2, t_mom_theor_P2,t_beta_P2, t_beta_theor_P2, t_E_P2, t_E_theor_P2;
    Float_t t_phi_diff, t_tan_theta_product;
    Float_t t_dist; // Elyptical distance (with correction for distribution sigma values) from the theorethical centre of elastics distribution on a 2D tan_theta_prod VS phi_diff distribution


    // HGeomVectors for vertex reconstruction in elastic events
    HGeomVector base_P1, base_P2;
    HGeomVector dir_P1, dir_P2;

    HGeomVector el_base_P1, el_base_P2;
    HGeomVector el_dir_P1, el_dir_P2;

    Float_t t_zPrime_P1, t_zPrime_P2;
    Float_t t_rPrime_P1, t_rPrime_P2;
    Float_t el_zPrime_P1, el_zPrime_P2;
    Float_t el_rPrime_P1, el_rPrime_P2;

    // Temp values to make sure there's only one elastic reaction per event - if the values are not
    // zero (an elastic reaction has already been found in this event) that means the event should
    // be skipped without filling el. histograms
    Int_t el_Sector_P1, el_Sector_P2;
    Int_t el_mass_P1, el_mass_P2;
    Float_t el_z_start_P1, el_r_start_P1;
    Float_t el_z_start_P2, el_r_start_P2;
    Float_t el_theta_P1, el_phi_P1, el_mom_P1, el_mom_theor_P1 ,el_beta_P1;
    Float_t el_theta_P2, el_phi_P2, el_mom_P2, el_mom_theor_P2 ,el_beta_P2;
    Float_t el_phi_diff, el_tan_theta_product;

    // Temp values for background reference
    Int_t bkg_Sector_P1, bkg_Sector_P2;
    Float_t bkg_z_start_P1;
    Float_t bkg_z_start_P2;
    Float_t bkg_theta_P1, bkg_phi_P1, bkg_mom_P1;
    Float_t bkg_theta_P2, bkg_phi_P2, bkg_mom_P2;
    Float_t bkg_phi_diff, bkg_tan_theta_product;

    // Theta1 vs theta2 for elastic proton scattering function
    TF1 *f_theta_vs_theta = new TF1("f_theta_vs_theta","(180.0/TMath::Pi())*atan(0.3088/tan((TMath::Pi()/180.0)*x))",0,90);
    f_theta_vs_theta->SetLineColor(kGreen);

    // Mom vs theta theoretical function (for elastic scattering)
    TF1 *f_mom_vs_theta = new TF1("f_mom_vs_theta","[0]/( (cos((TMath::Pi()/180.0)*x))*( 1 + pow( tan((TMath::Pi()/180.0)*x) * [1] , 2 ) ) )",0,90);
    f_mom_vs_theta->SetParameter(0,beam_p01);
    f_mom_vs_theta->SetParameter(1,beam_gamma_cm);
    f_mom_vs_theta->SetLineColor(kBlue);

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s\n";

    //----------------------------------------------------------------------------------------------
    // Histograms definition
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Histograms definition ...\n";

    //----------------------------------------------------------------------------------------------
    // Miscellaneous histograms
    //----------------------------------------------------------------------------------------------

    cout << " -> Miscellaneous histograms ...\n";

    //----------------------------------------------------------------------------------------------
    // Particle ID
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    // HH
    TH1D* h_HH_mass_P1 = new TH1D("h_HH_mass_P1", "Mass HH Particle 1;Mass;counts", 500, 0, 2000);
    TCanvas* c_HH_mass_P1 = new TCanvas("c_HH_mass_P1", "c_HH_mass_P1", 800, 800);
    h_HH_mass_P1->SetMinimum(0.0);

    TH1D* h_HH_mass_P2 = new TH1D("h_HH_mass_P2", "Mass HH Particle 2;Mass;counts", 500, 0, 2000);
    TCanvas* c_HH_mass_P2 = new TCanvas("c_HH_mass_P2", "c_HH_mass_P2", 800, 800);
    h_HH_mass_P2->SetMinimum(0.0);

    TH1D* h_HH_mass_P1_el = new TH1D("h_HH_mass_P1_el", "Mass HH Particle 1 (Elastic);Mass;counts", 500, 0, 2000);
    TCanvas* c_HH_mass_P1_el = new TCanvas("c_HH_mass_P1_el", "c_HH_mass_P1_el", 800, 800);
    h_HH_mass_P1_el->SetMinimum(0.0);

    TH1D* h_HH_mass_P2_el = new TH1D("h_HH_mass_P2_el", "Mass HH Particle 2 (Elastic);Mass;counts", 500, 0, 2000);
    TCanvas* c_HH_mass_P2_el = new TCanvas("c_HH_mass_P2_el", "c_HH_mass_P2_el", 800, 800);
    h_HH_mass_P2_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // HF
    TH1D* h_HF_mass_P1 = new TH1D("h_HF_mass_P1", "Mass HF Particle 1;Mass;counts", 500, 0, 2000);
    TCanvas* c_HF_mass_P1 = new TCanvas("c_HF_mass_P1", "c_HF_mass_P1", 800, 800);
    h_HF_mass_P1->SetMinimum(0.0);

    TH1D* h_HF_mass_P1_el = new TH1D("h_HF_mass_P1_el", "Mass HF Particle 1 (Elastic);Mass;counts", 500, 0, 2000);
    TCanvas* c_HF_mass_P1_el = new TCanvas("c_HF_mass_P1_el", "c_HF_mass_P1_el", 800, 800);
    h_HF_mass_P1_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // H
    TH1D* h_H_mass = new TH1D("h_H_mass", "Mass H;Mass;counts", 500, 0, 2000);
    TCanvas* c_H_mass = new TCanvas("c_H_mass", "c_H_mass", 800, 800);
    h_H_mass->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // STS Multiplicity
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    // Global
    TH1D* h_STS_multiplicity_ALL = new TH1D("h_STS_multiplicity_ALL", "STS Multiplicity;Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_ALL = new TCanvas("c_STS_multiplicity_ALL", "c_STS_multiplicity_ALL", 800, 800);
    h_STS_multiplicity_ALL->SetMinimum(0.0);

    TH1D* h_STS_multiplicity_STS1 = new TH1D("h_STS_multiplicity_STS1", "STS Multiplicity STS1;Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_STS1 = new TCanvas("c_STS_multiplicity_STS1", "c_STS_multiplicity_STS1", 800, 800);
    h_STS_multiplicity_STS1->SetMinimum(0.0);

    TH1D* h_STS_multiplicity_STS2 = new TH1D("h_STS_multiplicity_STS2", "STS Multiplicity STS2;Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_STS2 = new TCanvas("c_STS_multiplicity_STS2", "c_STS_multiplicity_STS2", 800, 800);
    h_STS_multiplicity_STS2->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // For one HADES track selection
    TH1D* h_STS_multiplicity_ALL_H = new TH1D("h_STS_multiplicity_ALL_H", "STS Multiplicity H;Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_ALL_H = new TCanvas("c_STS_multiplicity_ALL_H", "c_STS_multiplicity_ALL_H", 800, 800);
    h_STS_multiplicity_ALL_H->SetMinimum(0.0);

    TH1D* h_STS_multiplicity_STS1_H = new TH1D("h_STS_multiplicity_STS1_H", "STS Multiplicity STS1 H;Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_STS1_H = new TCanvas("c_STS_multiplicity_STS1_H", "c_STS_multiplicity_STS1_H", 800, 800);
    h_STS_multiplicity_STS1->SetMinimum(0.0);

    TH1D* h_STS_multiplicity_STS2_H = new TH1D("h_STS_multiplicity_STS2_H", "STS Multiplicity STS2 H;Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_STS2_H = new TCanvas("c_STS_multiplicity_STS2_H", "c_STS_multiplicity_STS2_H", 800, 800);
    h_STS_multiplicity_STS2_H->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // HF elastic cases
    TH1D* h_STS_multiplicity_ALL_HF_el = new TH1D("h_STS_multiplicity_ALL_HF_el", "STS Multiplicity HF (Elastic);Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_ALL_HF_el = new TCanvas("c_STS_multiplicity_ALL_HF_el", "c_STS_multiplicity_ALL_HF_el", 800, 800);
    h_STS_multiplicity_ALL_HF_el->SetMinimum(0.0);

    TH1D* h_STS_multiplicity_STS1_HF_el = new TH1D("h_STS_multiplicity_STS1_HF_el", "STS Multiplicity STS1 HF (Elastic);Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_STS1_HF_el = new TCanvas("c_STS_multiplicity_STS1_HF_el", "c_STS_multiplicity_STS1_HF_el", 800, 800);
    h_STS_multiplicity_STS1->SetMinimum(0.0);

    TH1D* h_STS_multiplicity_STS2_HF_el = new TH1D("h_STS_multiplicity_STS2_HF_el", "STS Multiplicity STS2 HF (Elastic);Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_STS2_HF_el = new TCanvas("c_STS_multiplicity_STS2_HF_el", "c_STS_multiplicity_STS2_HF_el", 800, 800);
    h_STS_multiplicity_STS2_HF_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // HH elastic cases
    TH1D* h_STS_multiplicity_ALL_HH_el = new TH1D("h_STS_multiplicity_ALL_HH_el", "STS Multiplicity HH (Elastic);Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_ALL_HH_el = new TCanvas("c_STS_multiplicity_ALL_HH_el", "c_STS_multiplicity_ALL_HH_el", 800, 800);
    h_STS_multiplicity_ALL_HH_el->SetMinimum(0.0);

    TH1D* h_STS_multiplicity_STS1_HH_el = new TH1D("h_STS_multiplicity_STS1_HH_el", "STS Multiplicity STS1 HH (Elastic);Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_STS1_HH_el = new TCanvas("c_STS_multiplicity_STS1_HH_el", "c_STS_multiplicity_STS1_HH_el", 800, 800);
    h_STS_multiplicity_STS1->SetMinimum(0.0);

    TH1D* h_STS_multiplicity_STS2_HH_el = new TH1D("h_STS_multiplicity_STS2_HH_el", "STS Multiplicity STS2 HH (Elastic);Multiplicity;counts", 50, 0, 50);
    TCanvas* c_STS_multiplicity_STS2_HH_el = new TCanvas("c_STS_multiplicity_STS2_HH_el", "c_STS_multiplicity_STS2_HH_el", 800, 800);
    h_STS_multiplicity_STS2_HH_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Vertex Reconstruction
    //----------------------------------------------------------------------------------------------

    // Each track pairing
    TH1D* h_vertex_X_Pair = new TH1D("h_vertex_X_Pair", "Vertex X (Global, Pair);X [mm];counts", 400, -100, 100);
    TCanvas* c_vertex_X_Pair = new TCanvas("c_vertex_X_Pair", "c_vertex_X_Pair", 800, 800);
    h_vertex_X_Pair->SetMinimum(0.0);

    TH1D* h_vertex_Y_Pair = new TH1D("h_vertex_Y_Pair", "Vertex Y (Global, Pair);Y [mm];counts", 400, -100, 100);
    TCanvas* c_vertex_Y_Pair = new TCanvas("c_vertex_Y_Pair", "c_vertex_Y_Pair", 800, 800);
    h_vertex_Y_Pair->SetMinimum(0.0);

    TH1D* h_vertex_Z_Pair = new TH1D("h_vertex_Z_Pair", "Vertex Z (Global, Pair);Z [mm];counts", 4000, -1000, 1000);
    TCanvas* c_vertex_Z_Pair = new TCanvas("c_vertex_Z_Pair", "c_vertex_Z_Pair", 800, 800);
    h_vertex_Z_Pair->SetMinimum(0.0);

    TH1D* h_vertex_R_Pair = new TH1D("h_vertex_R_Pair", "Vertex R (Global, Pair);R [mm];counts", 200, 0, 100);
    TCanvas* c_vertex_R_Pair = new TCanvas("c_vertex_R_Pair", "c_vertex_R_Pair", 800, 800);
    h_vertex_R_Pair->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Global (zPrime and FwDet tracks crossign this plane)
    TH2F* h_vertex_R_vs_Z_Plane = new TH2F("h_vertex_R_vs_Z_Plane", "Vertex reconstruction (Global, Plane);Z [mm];R [mm]", 4000, -1000, 1000, 200, 0, 100);
    TCanvas* c_vertex_R_vs_Z_Plane = new TCanvas("c_vertex_R_vs_Z_Plane", "c_vertex_R_vs_Z_Plane", 800, 800);
    h_vertex_R_vs_Z_Plane->SetMinimum(0.0);

    TH1D* h_vertex_Z_Plane = new TH1D("h_vertex_Z_Plane", "Vertex Z (Global, Plane);Z [mm];counts", 4000, -1000, 1000);
    TCanvas* c_vertex_Z_Plane = new TCanvas("c_vertex_Z_Plane", "c_vertex_Z_Plane", 800, 800);
    h_vertex_Z_Plane->SetMinimum(0.0);

    TH1D* h_vertex_R_Plane = new TH1D("h_vertex_R_Plane", "Vertex R (Global, Plane);R [mm];counts", 200, 0, 100);
    TCanvas* c_vertex_R_Plane = new TCanvas("c_vertex_R_Plane", "c_vertex_R_Plane", 800, 800);
    h_vertex_R_Plane->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Global (pairing tracks)
    TH2F* h_vertex_R_vs_Z = new TH2F("h_vertex_R_vs_Z", "Vertex reconstruction (Global);Z [mm];R [mm]", 4000, -1000, 1000, 200, 0, 100);
    TCanvas* c_vertex_R_vs_Z = new TCanvas("c_vertex_R_vs_Z", "c_vertex_R_vs_Z", 800, 800);
    h_vertex_R_vs_Z->SetMinimum(0.0);

    TH1D* h_vertex_Z = new TH1D("h_vertex_Z", "Vertex Z (Global);Z [mm];counts", 4000, -1000, 1000);
    TCanvas* c_vertex_Z = new TCanvas("c_vertex_Z", "c_vertex_Z", 800, 800);
    h_vertex_Z->SetMinimum(0.0);

    TH1D* h_vertex_R = new TH1D("h_vertex_R", "Vertex R (Global);R [mm];counts", 200, 0, 100);
    TCanvas* c_vertex_R = new TCanvas("c_vertex_R", "c_vertex_R", 800, 800);
    h_vertex_R->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // HF elastic (pairing tracks)
    TH2F* h_vertex_R_vs_Z_HF_el = new TH2F("h_vertex_R_vs_Z_HF_el", "Vertex reconstruction HF (Elastic);Z [mm];R [mm]", 4000, -1000, 1000, 200, 0, 100);
    TCanvas* c_vertex_R_vs_Z_HF_el = new TCanvas("c_vertex_R_vs_Z_HF_el", "c_vertex_R_vs_Z_HF_el", 800, 800);
    h_vertex_R_vs_Z_HF_el->SetMinimum(0.0);

    TH1D* h_vertex_Z_HF_el = new TH1D("h_vertex_Z_HF_el", "Vertex Z HF (Elastic);Z [mm];counts", 4000, -1000, 1000);
    TCanvas* c_vertex_Z_HF_el = new TCanvas("c_vertex_Z_HF_el", "c_vertex_Z_HF_el", 800, 800);
    h_vertex_Z_HF_el->SetMinimum(0.0);

    TH1D* h_vertex_R_HF_el = new TH1D("h_vertex_R_HF_el", "Vertex R HF (Elastic);R [mm];counts", 200, 0, 100);
    TCanvas* c_vertex_R_HF_el = new TCanvas("c_vertex_R_HF_el", "c_vertex_R_HF_el", 800, 800);
    h_vertex_R_HF_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // HH elastic (pairing tracks)
    TH2F* h_vertex_R_vs_Z_HH_el = new TH2F("h_vertex_R_vs_Z_HH_el", "Vertex reconstruction HH (Elastic);Z [mm];R [mm]", 4000, -1000, 1000, 200, 0, 100);
    TCanvas* c_vertex_R_vs_Z_HH_el = new TCanvas("c_vertex_R_vs_Z_HH_el", "c_vertex_R_vs_Z_HH_el", 800, 800);
    h_vertex_R_vs_Z_HH_el->SetMinimum(0.0);

    TH1D* h_vertex_Z_HH_el = new TH1D("h_vertex_Z_HH_el", "Vertex Z HH (Elastic);Z [mm];counts", 4000, -1000, 1000);
    TCanvas* c_vertex_Z_HH_el = new TCanvas("c_vertex_Z_HH_el", "c_vertex_Z_HH_el", 800, 800);
    h_vertex_Z_HH_el->SetMinimum(0.0);

    TH1D* h_vertex_R_HH_el = new TH1D("h_vertex_R_HH_el", "Vertex R HH (Elastic);R [mm];counts", 200, 0, 100);
    TCanvas* c_vertex_R_HH_el = new TCanvas("c_vertex_R_HH_el", "c_vertex_R_HH_el", 800, 800);
    h_vertex_R_HH_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Single HADES track
    TH2F* h_vertex_R_vs_Z_H = new TH2F("h_vertex_R_vs_Z_H", "Vertex R vs Z (from ParticleCand);Z [mm];R [mm]", 4000, -1000, 1000, 200, 0, 100);
    TCanvas* c_vertex_R_vs_Z_H = new TCanvas("c_vertex_R_vs_Z_H", "c_vertex_R_vs_Z_H", 800, 800);
    h_vertex_R_vs_Z_H->SetMinimum(0.0);

    TH1D* h_vertex_Z_H = new TH1D("h_vertex_Z_H", "Vertex Z (from ParticleCand);Z [mm];counts", 4000, -1000, 1000);
    TCanvas* c_vertex_Z_H = new TCanvas("c_vertex_Z_H", "c_vertex_Z_H", 800, 800);
    h_vertex_Z_H->SetMinimum(0.0);

    TH1D* h_vertex_R_H = new TH1D("h_vertex_R_H", "Vertex R (from ParticleCand);Z [mm];counts", 400, -100, 100);
    TCanvas* c_vertex_R_H = new TCanvas("c_vertex_R_H", "c_vertex_R_H", 800, 800);
    h_vertex_R_H->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Single HADES track (elastic)
    TH2F* h_vertex_R_vs_Z_H_el = new TH2F("h_vertex_R_vs_Z_H_el", "Vertex R vs Z (from ParticleCand);Z [mm];R [mm]", 4000, -1000, 1000, 200, 0, 100);
    TCanvas* c_vertex_R_vs_Z_H_el = new TCanvas("c_vertex_R_vs_Z_H_el", "c_vertex_R_vs_Z_H_el", 800, 800);
    h_vertex_R_vs_Z_H_el->SetMinimum(0.0);

    TH1D* h_vertex_Z_H_el = new TH1D("h_vertex_Z_H_el", "Vertex Z (from ParticleCand - Elastic);Z [mm];counts", 4000, -1000, 1000);
    TCanvas* c_vertex_Z_H_el = new TCanvas("c_vertex_Z_H_el", "c_vertex_Z_H_el", 800, 800);
    h_vertex_Z_H_el->SetMinimum(0.0);

    TH1D* h_vertex_R_H_el = new TH1D("h_vertex_R_H_el", "Vertex R (from ParticleCand - Elastic);Z [mm];counts", 400, -100, 100);
    TCanvas* c_vertex_R_H_el = new TCanvas("c_vertex_R_H_el", "c_vertex_R_H_el", 800, 800);
    h_vertex_R_H_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Elastic candidates per event

    TH1D* h_HF_el_per_event_count = new TH1D( "h_HF_el_per_event_count", "Elastics per event HF;El_per_event_counts;counts", 20, 0, 20);
    TCanvas* c_HF_el_per_event_count = new TCanvas("c_HF_el_per_event_count", "c_HF_el_per_event_count", 800, 800);
    h_HF_el_per_event_count->SetMinimum(0.0);

    TH1D* h_HH_el_per_event_count = new TH1D( "h_HH_el_per_event_count", "Elastics per event HH;El_per_event_counts;counts", 20, 0, 20);
    TCanvas* c_HH_el_per_event_count = new TCanvas("c_HH_el_per_event_count", "c_HH_el_per_event_count", 800, 800);
    h_HH_el_per_event_count->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // HADES track theta

    TH1D* h_H_theta = new TH1D("h_H_theta", "Theta;#theta [deg];counts", 180, 0, 90);
    TCanvas* c_H_theta = new TCanvas("c_H_theta", "c_H_theta", 800, 800);
    h_H_theta->SetMinimum(0.0);

    TH2F* h_H_mom_vs_theta = new TH2F("h_H_mom_vs_theta", "Mom vs Theta HADES Particle;#theta_{1} [deg];P [MeV]", 360, 0, 90, 350, 0, 7000);
    TCanvas* c_H_mom_vs_theta = new TCanvas("c_H_mom_vs_theta", "c_H_mom_vs_theta", 800, 800);
    h_H_mom_vs_theta->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Theta

    TH1D* h_HF_theta = new TH1D("h_HF_theta", "Theta HF;#theta [deg];counts", 180, 0, 90);
    TCanvas* c_HF_theta = new TCanvas("c_HF_theta", "c_HF_theta", 800, 800);
    h_HF_theta->SetMinimum(0.0);

    TH1D* h_HH_theta = new TH1D("h_HH_theta", "Theta HH;#theta [deg];counts", 180, 0, 90);
    TCanvas* c_HH_theta = new TCanvas("c_HH_theta", "c_HH_theta", 800, 800);
    h_HH_theta->SetMinimum(0.0);

    TH1D* h_HF_theta_el = new TH1D("h_HF_theta_el", "Theta HF (ELASTIC);#theta [deg];counts", 180, 0, 90);
    TCanvas* c_HF_theta_el = new TCanvas("c_HF_theta_el", "c_HF_theta_el", 800, 800);
    h_HF_theta_el->SetMinimum(0.0);

    TH1D* h_HH_theta_el = new TH1D("h_HH_theta_el", "Theta HH (ELASTIC);#theta [deg];counts", 180, 0, 90);
    TCanvas* c_HH_theta_el = new TCanvas("c_HH_theta_el", "c_HH_theta_el", 800, 800);
    h_HH_theta_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Phi diff

    TH1D* h_HF_phi_diff = new TH1D("h_HF_phi_diff", "Phi angle difference HF;#Delta #phi [deg];counts", 720, 0, 360);
    TCanvas* c_HF_phi_diff = new TCanvas("c_HF_phi_diff", "c_HF_phi_diff", 800, 800);
    h_HF_phi_diff->SetMinimum(0.0);

    TH1D* h_HH_phi_diff = new TH1D("h_HH_phi_diff", "Phi angle difference HH;#Delta #phi [deg];counts", 720, 0, 360);
    TCanvas* c_HH_phi_diff = new TCanvas("c_HH_phi_diff", "c_HH_phi_diff", 800, 800);
    h_HH_phi_diff->SetMinimum(0.0);

    TH1D* h_HF_phi_diff_cut = new TH1D( "h_HF_phi_diff_cut", "Phi angle difference HF (CUT);#Delta #phi [deg];counts", 720, 0, 360);
    TCanvas* c_HF_phi_diff_cut = new TCanvas("c_HF_phi_diff_cut", "c_HF_phi_diff_cut", 800, 800);
    h_HF_phi_diff_cut->SetMinimum(0.0);

    TH1D* h_HH_phi_diff_cut = new TH1D( "h_HH_phi_diff_cut", "Phi angle difference HH (CUT);#Delta #phi [deg];counts", 720, 0, 360);
    TCanvas* c_HH_phi_diff_cut = new TCanvas("c_HH_phi_diff_cut", "c_HH_phi_diff_cut", 800, 800);
    h_HH_phi_diff_cut->SetMinimum(0.0);

    TH1D* h_HF_phi_diff_el = new TH1D("h_HF_phi_diff_el", "Phi angle difference HF (ELASTIC);#Delta #phi [deg];counts", 720, 0, 360);
    h_HF_phi_diff_el->SetLineColor(kGreen);
    TH1D* h_HH_phi_diff_el = new TH1D("h_HH_phi_diff_el", "Phi angle difference HH (ELASTIC);#Delta #phi [deg];counts", 720, 0, 360);
    h_HH_phi_diff_el->SetLineColor(kGreen);

    TH1D* h_HF_phi_diff_bkg = new TH1D("h_HF_phi_diff_bkg", "Phi angle difference HF (BACKGROUND);#Delta #phi [deg];counts", 720, 0, 360);
    h_HF_phi_diff_bkg->SetLineColor(kRed);
    TH1D* h_HH_phi_diff_bkg = new TH1D("h_HH_phi_diff_bkg", "Phi angle difference HH (BACKGROUND);#Delta #phi [deg];counts", 720, 0, 360);
    h_HH_phi_diff_bkg->SetLineColor(kRed);

    //----------------------------------------------------------------------------------------------
    // Tan theta product

    TH1D* h_HF_tan_theta_prod = new TH1D( "h_HF_tan_theta_prod", "Tan theta product HF;Tan #theta product;counts", 100, 0, 1);
    TCanvas* c_HF_tan_theta_prod = new TCanvas("c_HF_tan_theta_prod", "c_HF_tan_theta_prod", 800, 800);
    h_HF_tan_theta_prod->SetMinimum(0.0);

    TH1D* h_HH_tan_theta_prod = new TH1D( "h_HH_tan_theta_prod", "Tan theta product HH;Tan #theta product;counts", 100, 0, 1);
    TCanvas* c_HH_tan_theta_prod = new TCanvas("c_HH_tan_theta_prod", "c_HH_tan_theta_prod", 800, 800);
    h_HH_tan_theta_prod->SetMinimum(0.0);

    TH1D* h_HF_tan_theta_prod_cut = new TH1D("h_HF_tan_theta_prod_cut", "Tan theta product HF (CUT);Tan #theta product;counts", 100, 0, 1);
    TCanvas* c_HF_tan_theta_prod_cut = new TCanvas("c_HF_tan_theta_prod_cut", "c_HF_tan_theta_prod_cut", 800, 800);
    h_HF_tan_theta_prod_cut->SetMinimum(0.0);

    TH1D* h_HH_tan_theta_prod_cut = new TH1D("h_HH_tan_theta_prod_cut", "Tan theta product HH (CUT);Tan #theta product;counts", 100, 0, 1);
    TCanvas* c_HH_tan_theta_prod_cut = new TCanvas("c_HH_tan_theta_prod_cut", "c_HH_tan_theta_prod_cut", 800, 800);
    h_HH_tan_theta_prod_cut->SetMinimum(0.0);

    TH1D* h_HF_tan_theta_prod_el = new TH1D("h_HF_tan_theta_prod_el", "Tan theta product HF (ELASTIC);Tan #theta product;counts", 100, 0, 1);
    h_HF_tan_theta_prod_el->SetLineColor(kGreen);

    TH1D* h_HH_tan_theta_prod_el = new TH1D("h_HH_tan_theta_prod_el", "Tan theta product HH (ELASTIC);Tan #theta product;counts", 100, 0, 1);
    h_HH_tan_theta_prod_el->SetLineColor(kGreen);

    TH1D* h_HF_tan_theta_prod_bkg = new TH1D("h_HF_tan_theta_prod_bkg", "Tan theta product HF (BACKGROUND);Tan #theta product;counts", 100, 0, 1);
    h_HF_tan_theta_prod_bkg->SetLineColor(kGreen);

    TH1D* h_HH_tan_theta_prod_bkg = new TH1D("h_HH_tan_theta_prod_bkg", "Tan theta product HH (BACKGROUND);Tan #theta product;counts", 100, 0, 1);
    h_HH_tan_theta_prod_bkg->SetLineColor(kGreen);

    TH1D* h_track_start_Z = new TH1D("h_track_start_Z", "Vertex Reconstruction (Z);Z [mm];counts", 600, -3000, 3000);
    TCanvas* c_track_start_Z = new TCanvas("c_track_start_Z", "c_track_start_Z", 800, 800);
    h_track_start_Z->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // 2D
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    // Mom vs Theta
    TH2F* h_HH_and_HF_mom_vs_theta_ALL = new TH2F( "h_HH_and_HF_mom_vs_theta_ALL", "Mom vs Theta - HH and HF - ALL SECTORS;#theta_{1} [deg];P [MeV]", 360, 0, 90, 350, 0, 7000);
    TCanvas* c_HH_and_HF_mom_vs_theta_ALL = new TCanvas("c_HH_and_HF_mom_vs_theta_ALL", "c_HH_and_HF_mom_vs_theta_ALL", 800, 800);
    h_HH_and_HF_mom_vs_theta_ALL->SetMinimum(0.0);

    TH2F* h_HH_and_HF_mom_vs_theta_ALL_el = new TH2F("h_HH_and_HF_mom_vs_theta_ALL_el", "Mom vs Theta - HH and HF - ALL SECTORS (Elastic);#theta_{1} [deg];P [MeV]", 360, 0, 90, 350, 0, 7000);
    TCanvas* c_HH_and_HF_mom_vs_theta_ALL_el = new TCanvas("c_HH_and_HF_mom_vs_theta_ALL_el", "c_HH_and_HF_mom_vs_theta_ALL_el", 800, 800);
    h_HH_and_HF_mom_vs_theta_ALL_el->SetMinimum(0.0);

    // Delta mom vs mom
    TH2F* h_HH_and_HF_delta_mom_vs_mom = new TH2F( "h_HH_and_HF_delta_mom_vs_mom", "Delta Mom vs Mom - HH and HF;P [MeV];#Delta P [MeV]", 350, 0, 7000, 350, -3500, 3500);
    TCanvas* c_HH_and_HF_delta_mom_vs_mom = new TCanvas("c_HH_and_HF_delta_mom_vs_mom", "c_HH_and_HF_delta_mom_vs_mom", 800, 800);
    h_HH_and_HF_delta_mom_vs_mom->SetMinimum(0.0);

    TH2F* h_HH_and_HF_delta_mom_vs_mom_cut = new TH2F("h_HH_and_HF_delta_mom_vs_mom_cut", "Delta Mom vs Mom - HH and HF (CUT);P [MeV];#Delta P [MeV]", 350, 0, 7000, 350, -3500, 3500);
    TCanvas* c_HH_and_HF_delta_mom_vs_mom_cut = new TCanvas("c_HH_and_HF_delta_mom_vs_mom_cut", "c_HH_and_HF_delta_mom_vs_mom_cut", 800, 800);
    h_HH_and_HF_delta_mom_vs_mom_cut->SetMinimum(0.0);

    TH2F* h_HH_and_HF_delta_mom_vs_mom_el = new TH2F("h_HH_and_HF_delta_mom_vs_mom_el", "Delta Mom vs Mom - HH and HF (Elastic);P [MeV];#Delta P [MeV]", 350, 0, 7000, 350, -3500, 3500);
    TCanvas* c_HH_and_HF_delta_mom_vs_mom_el = new TCanvas("c_HH_and_HF_delta_mom_vs_mom_el", "c_HH_and_HF_delta_mom_vs_mom_el", 800, 800);
    h_HH_and_HF_delta_mom_vs_mom_el->SetMinimum(0.0);

    // Mom vs Beta
    TH2F* h_HH_and_HF_mom_vs_beta = new TH2F( "h_HH_and_HF_mom_vs_beta", "Mom vs Beta - HH and HF;#beta [ ]; P [MeV]", 220, 0, 1.1, 350, 0, 7000);
    TCanvas* c_HH_and_HF_mom_vs_beta = new TCanvas("c_HH_and_HF_mom_vs_beta", "c_HH_and_HF_mom_vs_beta", 800, 800);
    h_HH_and_HF_mom_vs_beta->SetMinimum(0.0);

    TH2F* h_HH_and_HF_mom_vs_beta_el = new TH2F("h_HH_and_HF_mom_vs_beta_el", "Mom vs Beta - HH and HF (Elastic);#beta [ ]; P [MeV]", 220, 0, 1.1, 350, 0, 7000);
    TCanvas* c_HH_and_HF_mom_vs_beta_el = new TCanvas("c_HH_and_HF_mom_vs_beta_el", "c_HH_and_HF_mom_vs_beta_el", 800, 800);
    h_HH_and_HF_mom_vs_beta_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Tan theta product VS Phi diff
    TH2F* h_HH_tan_theta_prod_VS_phi_diff = new TH2F("h_HH_tan_theta_prod_VS_phi_diff", "Tan theta product VS Phi diff HH;#Delta #phi [deg];Tan #theta product", 720, 0, 360, 100, 0, 1);
    TCanvas* c_HH_tan_theta_prod_VS_phi_diff = new TCanvas("c_HH_tan_theta_prod_VS_phi_diff", "c_HH_tan_theta_prod_VS_phi_diff", 800, 800);
    h_HH_tan_theta_prod_VS_phi_diff->SetMinimum(0.0);

    TH2F* h_HF_tan_theta_prod_VS_phi_diff = new TH2F("h_HF_tan_theta_prod_VS_phi_diff", "Tan theta product VS Phi diff HF;#Delta #phi [deg];Tan #theta product", 720, 0, 360, 100, 0, 1);
    TCanvas* c_HF_tan_theta_prod_VS_phi_diff = new TCanvas("c_HF_tan_theta_prod_VS_phi_diff", "c_HF_tan_theta_prod_VS_phi_diff", 800, 800);
    h_HF_tan_theta_prod_VS_phi_diff->SetMinimum(0.0);

    TH2F* h_HH_tan_theta_prod_VS_phi_diff_el = new TH2F("h_HH_tan_theta_prod_VS_phi_diff_el", "Tan theta product VS Phi diff HH (ELASTIC);#Delta #phi [deg];Tan #theta product", 720, 0, 360, 100, 0, 1);
    TCanvas* c_HH_tan_theta_prod_VS_phi_diff_el = new TCanvas( "c_HH_tan_theta_prod_VS_phi_diff_el", "c_HH_tan_theta_prod_VS_phi_diff_el", 800, 800);
    h_HH_tan_theta_prod_VS_phi_diff_el->SetMinimum(0.0);

    TH2F* h_HF_tan_theta_prod_VS_phi_diff_el = new TH2F("h_HF_tan_theta_prod_VS_phi_diff_el", "Tan theta product VS Phi diff HF (ELASTIC);#Delta #phi [deg];Tan #theta product", 720, 0, 360, 100, 0, 1);

    TH2F* h_HH_tan_theta_prod_VS_phi_diff_bkg = new TH2F("h_HH_tan_theta_prod_VS_phi_diff_bkg", "Tan theta product VS Phi diff HH (BACKGROUND);#Delta #phi [deg];Tan #theta product", 720, 0, 360, 100, 0, 1);
    TCanvas* c_HH_tan_theta_prod_VS_phi_diff_bkg = new TCanvas( "c_HH_tan_theta_prod_VS_phi_diff_bkg", "c_HH_tan_theta_prod_VS_phi_diff_bkg", 800, 800);
    h_HH_tan_theta_prod_VS_phi_diff_bkg->SetMinimum(0.0);

    TH2F* h_HF_tan_theta_prod_VS_phi_diff_bkg = new TH2F("h_HF_tan_theta_prod_VS_phi_diff_bkg", "Tan theta product VS Phi diff HF (BACKGROUND);#Delta #phi [deg];Tan #theta product", 720, 0, 360, 100, 0, 1);
    TCanvas* c_HF_tan_theta_prod_VS_phi_diff_bkg = new TCanvas( "c_HF_tan_theta_prod_VS_phi_diff_bkg", "c_HF_tan_theta_prod_VS_phi_diff_bkg", 800, 800);
    h_HF_tan_theta_prod_VS_phi_diff_bkg->SetMinimum(0.0);

    TCanvas* c_HF_tan_theta_prod_VS_phi_diff_el = new TCanvas( "c_HF_tan_theta_prod_VS_phi_diff_el", "c_HF_tan_theta_prod_VS_phi_diff_el", 800, 800);
    h_HF_tan_theta_prod_VS_phi_diff_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Phi diff VS Phi P1
    TH2F* h_HH_phi_diff_VS_phi_P1 = new TH2F("h_HH_phi_diff_VS_phi_P1", "Phi diff VS Phi P1 HH;#Delta #phi [deg];#phi_{P1} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HH_phi_diff_VS_phi_P1 = new TCanvas("c_HH_phi_diff_VS_phi_P1", "c_HH_phi_diff_VS_phi_P1", 800, 800);
    h_HH_phi_diff_VS_phi_P1->SetMinimum(0.0);

    TH2F* h_HF_phi_diff_VS_phi_P1 = new TH2F("h_HF_phi_diff_VS_phi_P1", "Phi diff VS Phi P1 HF;#Delta #phi [deg];#phi_{P1} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HF_phi_diff_VS_phi_P1 = new TCanvas("c_HF_phi_diff_VS_phi_P1", "c_HF_phi_diff_VS_phi_P1", 800, 800);
    h_HF_phi_diff_VS_phi_P1->SetMinimum(0.0);

    TH2F* h_HH_phi_diff_VS_phi_P1_cut = new TH2F("h_HH_phi_diff_VS_phi_P1_cut", "Phi diff VS Phi P1 HH (CUT);#Delta #phi [deg];#phi_{P1} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HH_phi_diff_VS_phi_P1_cut = new TCanvas( "c_HH_phi_diff_VS_phi_P1_cut", "c_HH_phi_diff_VS_phi_P1_cut", 800, 800);
    h_HH_phi_diff_VS_phi_P1_cut->SetMinimum(0.0);

    TH2F* h_HF_phi_diff_VS_phi_P1_cut = new TH2F("h_HF_phi_diff_VS_phi_P1_cut", "Phi diff VS Phi P1 HF (CUT);#Delta #phi [deg];#phi_{P1} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HF_phi_diff_VS_phi_P1_cut = new TCanvas( "c_HF_phi_diff_VS_phi_P1_cut", "c_HF_phi_diff_VS_phi_P1_cut", 800, 800);
    h_HF_phi_diff_VS_phi_P1_cut->SetMinimum(0.0);

    TH2F* h_HH_phi_diff_VS_phi_P1_el = new TH2F("h_HH_phi_diff_VS_phi_P1_el", "Phi diff VS Phi P1 HH (ELASTIC);#Delta #phi [deg];#phi_{P1} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HH_phi_diff_VS_phi_P1_el = new TCanvas( "c_HH_phi_diff_VS_phi_P1_el", "c_HH_phi_diff_VS_phi_P1_el", 800, 800);
    h_HH_phi_diff_VS_phi_P1_el->SetMinimum(0.0);

    TH2F* h_HF_phi_diff_VS_phi_P1_el = new TH2F("h_HF_phi_diff_VS_phi_P1_el", "Phi diff VS Phi P1 HF (ELASTIC);#Delta #phi [deg];#phi_{P1} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HF_phi_diff_VS_phi_P1_el = new TCanvas( "c_HF_phi_diff_VS_phi_P1_el", "c_HF_phi_diff_VS_phi_P1_el", 800, 800);
    h_HF_phi_diff_VS_phi_P1_el->SetMinimum(0.0);

    // Phi diff VS Phi P1
    TH2F* h_HH_phi_diff_VS_phi_P2 = new TH2F("h_HH_phi_diff_VS_phi_P2", "Phi diff VS Phi P1 HH;#Delta #phi [deg];#phi_{P2} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HH_phi_diff_VS_phi_P2 = new TCanvas("c_HH_phi_diff_VS_phi_P2", "c_HH_phi_diff_VS_phi_P2", 800, 800);
    h_HH_phi_diff_VS_phi_P2->SetMinimum(0.0);

    TH2F* h_HF_phi_diff_VS_phi_P2 = new TH2F("h_HF_phi_diff_VS_phi_P2", "Phi diff VS Phi P1 HF;#Delta #phi [deg];#phi_{P2} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HF_phi_diff_VS_phi_P2 = new TCanvas("c_HF_phi_diff_VS_phi_P2", "c_HF_phi_diff_VS_phi_P2", 800, 800);
    h_HF_phi_diff_VS_phi_P2->SetMinimum(0.0);

    TH2F* h_HH_phi_diff_VS_phi_P2_cut = new TH2F("h_HH_phi_diff_VS_phi_P2_cut", "Phi diff VS Phi P1 HH (CUT);#Delta #phi [deg];#phi_{P2} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HH_phi_diff_VS_phi_P2_cut = new TCanvas( "c_HH_phi_diff_VS_phi_P2_cut", "c_HH_phi_diff_VS_phi_P2_cut", 800, 800);
    h_HH_phi_diff_VS_phi_P2_cut->SetMinimum(0.0);

    TH2F* h_HF_phi_diff_VS_phi_P2_cut = new TH2F("h_HF_phi_diff_VS_phi_P2_cut", "Phi diff VS Phi P1 HF (CUT);#Delta #phi [deg];#phi_{P2} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HF_phi_diff_VS_phi_P2_cut = new TCanvas( "c_HF_phi_diff_VS_phi_P2_cut", "c_HF_phi_diff_VS_phi_P2_cut", 800, 800);
    h_HF_phi_diff_VS_phi_P2_cut->SetMinimum(0.0);

    TH2F* h_HH_phi_diff_VS_phi_P2_el = new TH2F("h_HH_phi_diff_VS_phi_P2_el", "Phi diff VS Phi P1 HH (ELASTIC);#Delta #phi [deg];#phi_{P2} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HH_phi_diff_VS_phi_P2_el = new TCanvas( "c_HH_phi_diff_VS_phi_P2_el", "c_HH_phi_diff_VS_phi_P2_el", 800, 800);
    h_HH_phi_diff_VS_phi_P2_el->SetMinimum(0.0);

    TH2F* h_HF_phi_diff_VS_phi_P2_el = new TH2F("h_HF_phi_diff_VS_phi_P2_el", "Phi diff VS Phi P1 HF (ELASTIC);#Delta #phi [deg];#phi_{P2} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HF_phi_diff_VS_phi_P2_el = new TCanvas( "c_HF_phi_diff_VS_phi_P2_el", "c_HF_phi_diff_VS_phi_P2_el", 800, 800);
    h_HF_phi_diff_VS_phi_P2_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Theta P1 vs Theta P2
    TH2F* h_HH_and_HF_theta_P1_vs_theta_P2 = new TH2F( "h_HH_and_HF_theta_P1_vs_theta_P2", "Theta P1 vs Theta P2 HH and HF;#theta_{2} [deg];#theta_{1} [deg]", 360, 0, 90, 360, 0, 90);
    TCanvas* c_HH_and_HF_theta_P1_vs_theta_P2 = new TCanvas( "c_HH_and_HF_theta_P1_vs_theta_P2", "c_HH_and_HF_theta_P1_vs_theta_P2", 800, 800);
    h_HH_and_HF_theta_P1_vs_theta_P2->SetMinimum(0.0);

    TH2F* h_HH_and_HF_theta_P1_vs_theta_P2_el = new TH2F("h_HH_and_HF_theta_P1_vs_theta_P2_el", "Theta P1 vs Theta P2 HH and HF (Elastic);#theta_{2} [deg];#theta_{1} [deg]", 360, 0, 90, 360, 0, 90);
    TCanvas* c_HH_and_HF_theta_P1_vs_theta_P2_el = new TCanvas( "c_HH_and_HF_theta_P1_vs_theta_P2_el", "c_HH_and_HF_theta_P1_vs_theta_P2_el", 800, 800);
    h_HH_and_HF_theta_P1_vs_theta_P2_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Theta P1 vs Theta P2
    TH2F* h_HH_phi_P1_vs_phi_P2 = new TH2F("h_HH_phi_P1_vs_phi_P2", "Phi P1 vs Phi P2 HH;#theta_{2} [deg];#theta_{1} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HH_phi_P1_vs_phi_P2 = new TCanvas("c_HH_phi_P1_vs_phi_P2", "c_HH_phi_P1_vs_phi_P2", 800, 800);
    h_HH_phi_P1_vs_phi_P2->SetMinimum(0.0);

    TH2F* h_HF_phi_P1_vs_phi_P2 = new TH2F("h_HF_phi_P1_vs_phi_P2", "Phi P1 vs Phi P2 HF;#theta_{2} [deg];#theta_{1} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HF_phi_P1_vs_phi_P2 = new TCanvas("c_HF_phi_P1_vs_phi_P2", "c_HF_phi_P1_vs_phi_P2", 800, 800);
    h_HF_phi_P1_vs_phi_P2->SetMinimum(0.0);

    TH2F* h_HH_phi_P1_vs_phi_P2_el = new TH2F("h_HH_phi_P1_vs_phi_P2_el", "Phi P1 vs Phi P2 HH (Elastic);#theta_{2} [deg];#theta_{1} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HH_phi_P1_vs_phi_P2_el = new TCanvas("c_HH_phi_P1_vs_phi_P2_el", "c_HH_phi_P1_vs_phi_P2_el", 800, 800);
    h_HH_phi_P1_vs_phi_P2_el->SetMinimum(0.0);

    TH2F* h_HF_phi_P1_vs_phi_P2_el = new TH2F("h_HF_phi_P1_vs_phi_P2_el", "Phi P1 vs Phi P2 HF (Elastic);#theta_{2} [deg];#theta_{1} [deg]", 720, 0, 360, 720, 0, 360);
    TCanvas* c_HF_phi_P1_vs_phi_P2_el = new TCanvas("c_HF_phi_P1_vs_phi_P2_el", "c_HF_phi_P1_vs_phi_P2_el", 800, 800);
    h_HF_phi_P1_vs_phi_P2_el->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Theta

    TH1D* h_HF_theta_sec[6];
    TCanvas* c_HF_theta_sec[6];

    TH1D* h_HH_theta_sec[6];
    TCanvas* c_HH_theta_sec[6];

    TH1D* h_HF_theta_sec_el[6];
    TCanvas* c_HF_theta_sec_el[6];

    TH1D* h_HH_theta_sec_el[6];
    TCanvas* c_HH_theta_sec_el[6];

    //----------------------------------------------------------------------------------------------
    // Mom vs theta

    TH2F* h_HH_and_HF_mom_vs_theta[6];
    TCanvas* c_HH_and_HF_mom_vs_theta[6];

    TH2F* h_HH_and_HF_mom_vs_theta_el[6];
    TCanvas* c_HH_and_HF_mom_vs_theta_el[6];

    //----------------------------------------------------------------------------------------------
    // Mom vs theta histograms
    //----------------------------------------------------------------------------------------------

    for (Int_t i = 0; i < 6; ++i)
    {
        //----------------------------------------------------------------------------------------------
        // Theta histograms
        //----------------------------------------------------------------------------------------------

        // h_HF_theta_sec
        sprintf(buff1, "h_HF_theta_Sec_%d", i + 1);
        sprintf(buff2, "Theta HF - Mod_%d;#theta [deg];counts", i + 1);
        h_HF_theta_sec[i] = new TH1D(buff1, buff2, 180, 0, 90);
        h_HF_theta_sec[i]->SetMinimum(0.0);

        sprintf(buff1, "c_HF_theta_Sec_%d", i + 1);
        c_HF_theta_sec[i] = new TCanvas(buff1, buff1, 800, 800);

        // h_HH_theta_sec
        sprintf(buff1, "h_HH_theta_Sec_%d", i + 1);
        sprintf(buff2, "Theta HH - Mod_%d;#theta [deg];counts", i + 1);
        h_HH_theta_sec[i] = new TH1D(buff1, buff2, 180, 0, 90);
        h_HH_theta_sec[i]->SetMinimum(0.0);

        sprintf(buff1, "c_HH_theta_Sec_%d", i + 1);
        c_HH_theta_sec[i] = new TCanvas(buff1, buff1, 800, 800);

        // h_HF_theta_sec_el
        sprintf(buff1, "h_HF_theta_Sec_%d_EL", i + 1);
        sprintf(buff2, "Theta HF - Mod_%d (ELASTIC);#theta [deg];counts", i + 1);
        h_HF_theta_sec_el[i] = new TH1D(buff1, buff2, 180, 0, 90);
        h_HF_theta_sec_el[i]->SetMinimum(0.0);

        sprintf(buff1, "c_HF_theta_Sec_%d_EL", i + 1);
        c_HF_theta_sec_el[i] = new TCanvas(buff1, buff1, 800, 800);

        // h_HH_theta_sec_el
        sprintf(buff1, "h_HH_theta_Sec_%d_EL", i + 1);
        sprintf(buff2, "Theta HH - Mod_%d (ELASTIC);#theta [deg];counts", i + 1);
        h_HH_theta_sec_el[i] = new TH1D(buff1, buff2, 180, 0, 90);
        h_HH_theta_sec_el[i]->SetMinimum(0.0);

        sprintf(buff1, "c_HH_theta_Sec_%d_EL", i + 1);
        c_HH_theta_sec_el[i] = new TCanvas(buff1, buff1, 800, 800);

        //----------------------------------------------------------------------------------------------
        // Mom vs theta histograms
        //----------------------------------------------------------------------------------------------

        // h_HH_and_HF_mom_vs_theta
        sprintf(buff1, "h_HH_and_HF_mom_vs_theta_Mod_%d", i + 1);
        sprintf(buff2, "Mom vs Theta - HH and HF - Mod_%d;#theta [deg];P [MeV]", i + 1);
        h_HH_and_HF_mom_vs_theta[i] = new TH2F(buff1, buff2, 180, 0, 90, 350, 0, 7000);
        h_HH_and_HF_mom_vs_theta[i]->SetMinimum(0.0);

        sprintf(buff1, "c_HH_and_HF_mom_vs_theta_Mod_%d", i + 1);
        c_HH_and_HF_mom_vs_theta[i] = new TCanvas(buff1, buff1, 800, 800);

        // h_HH_and_HF_mom_vs_theta_el
        sprintf(buff1, "h_HH_and_HF_mom_vs_theta_Mod_%d_el", i + 1);
        sprintf(buff2, "Mom vs Theta - HH and HF - Mod_%d (Elastic);#theta [deg];P [MeV]", i + 1);
        h_HH_and_HF_mom_vs_theta_el[i] = new TH2F(buff1, buff2, 180, 0, 90, 350, 0, 7000);
        h_HH_and_HF_mom_vs_theta_el[i]->SetMinimum(0.0);

        sprintf(buff1, "c_HH_and_HF_mom_vs_theta_Mod_%d_el", i + 1);
        c_HH_and_HF_mom_vs_theta_el[i] = new TCanvas(buff1, buff1, 800, 800);
    }

    TH2F* h_track_start_Y_vs_X = new TH2F("h_track_start_Y_vs_X", "Vertex Reconstruction (Y vs X);X [mm];Y [mm]", 200, -100, 100, 200, -100, 100);
    TCanvas* c_track_start_Y_vs_X = new TCanvas("c_track_start_Y_vs_X", "c_track_start_Y_vs_X", 800, 800);
    h_track_start_Y_vs_X->SetMinimum(0.0);

    TH2F* h_track_start_R_vs_Z = new TH2F("h_track_start_R_vs_Z", "Vertex Reconstruction (R vs Z);R [mm];Z [mm]", 200, -100, 100, 200, -100, 100);
    TCanvas* c_track_start_R_vs_Z = new TCanvas("c_track_start_R_vs_Z", "c_track_start_R_vs_Z", 800, 800);
    h_track_start_R_vs_Z->SetMinimum(0.0);

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s\n";

    //----------------------------------------------------------------------------------------------
    // Loop over events in input file
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "//---------------------------------------------------------------------------------\n";
    cout << "Loop over events in input file ...\n";
    cout << "//---------------------------------------------------------------------------------\n";

    // Total number of events
    Int_t num_Events_Total = 0;
    Int_t num_Events_Total_Elastic = 0;
    Int_t num_Events_Total_Bkg_Ref = 0;
    Int_t num_Events_HH = 0;
    Int_t num_Events_HF = 0;
    Int_t num_Events_HH_Elastic = 0;
    Int_t num_Events_HF_Elastic = 0;
    Int_t num_Events_HH_Bkg_Ref = 0;
    Int_t num_Events_HF_Bkg_Ref = 0;
    Int_t num_Events_HH_Elastic_Bkg_Corr = 0;
    Int_t num_Events_HF_Elastic_Bkg_Corr = 0;

    Int_t num_steps = 10;
    Int_t step_size = (limit_sto - limit_sta)/num_steps;
    Float_t num_Events_table[10] = {};
    Float_t num_Events_HH_Elastic_table[10] = {};
    Float_t num_Events_HF_Elastic_table[10] = {};
    Float_t num_Events_HH_Bkg_Ref_table[10] = {};
    Float_t num_Events_HF_Bkg_Ref_table[10] = {};
    Float_t num_Events_HH_Elastic_Bkg_Corr_table[10] = {};
    Float_t num_Events_HF_Elastic_Bkg_Corr_table[10] = {};

    // Number of events with specific triggers
    Int_t num_Events_PT1 = 0;
    Int_t num_Events_PT2 = 0;
    Int_t num_Events_PT3 = 0;
    Int_t num_HH_Events_PT1 = 0;
    Int_t num_HH_Events_PT2 = 0;
    Int_t num_HH_Events_PT3 = 0;
    Int_t num_HF_Events_PT1 = 0;
    Int_t num_HF_Events_PT2 = 0;
    Int_t num_HF_Events_PT3 = 0;
    Int_t num_HH_Events_Elastic_PT1 = 0;
    Int_t num_HH_Events_Elastic_PT2 = 0;
    Int_t num_HH_Events_Elastic_PT3 = 0;
    Int_t num_HF_Events_Elastic_PT1 = 0;
    Int_t num_HF_Events_Elastic_PT2 = 0;
    Int_t num_HF_Events_Elastic_PT3 = 0;

    // Number of entires in specific cases
    Int_t num_HH_Entries = 0;
    Int_t num_HF_Entries = 0;
    Int_t num_HH_Elastic_Entries = 0;
    Int_t num_HF_Elastic_Entries = 0;

    // Number of entires in specific cases (PT1 trigger)
    Int_t num_HH_Entries_PT1 = 0;
    Int_t num_HF_Entries_PT1 = 0;
    Int_t num_HH_Elastic_Entries_PT1 = 0;
    Int_t num_HF_Elastic_Entries_PT1 = 0;

    // Number of entires in specific cases (PT2 trigger)
    Int_t num_HH_Entries_PT2 = 0;
    Int_t num_HF_Entries_PT2 = 0;
    Int_t num_HH_Elastic_Entries_PT2 = 0;
    Int_t num_HF_Elastic_Entries_PT2 = 0;

    // Number of entires in specific cases (PT3 trigger)
    Int_t num_HH_Entries_PT3 = 0;
    Int_t num_HF_Entries_PT3 = 0;
    Int_t num_HH_Elastic_Entries_PT3 = 0;
    Int_t num_HF_Elastic_Entries_PT3 = 0;

    // Number of elastic pairs found per event (Shouldn't ever be higher than 1, but the experiment
    // isn't perfect)
    Int_t num_HH_El_per_Event = 0;
    Int_t num_HF_El_per_Event = 0;
    Int_t num_HH_Events_Mult_El_Pairs = 0;
    Int_t num_HF_Events_Mult_El_Pairs = 0;

    // Number of events in h_H_theta
    Int_t num_H_part = 0;

    // Check if event contains HH or HF pair
    bool has_HH = false;
    bool has_HF = false;

    // Check if event contains an elastic pair
    bool has_HH_elastic = false;
    bool has_HF_elastic = false;

    // Check if event a background reference pair
    bool has_HH_bkg_ref = false;
    bool has_HF_bkg_ref = false;

    // Timestamps definition
    time_t start_t = 0;
    time_t stop_t = 0;
    std::string timestamp;
    regex str_expr(".*/?[a-z]{2}([0-9]{11})[0-9]{2}.+");

    HParticleTrackSorter sorter;
    sorter.init();
    sorter.cleanUp();
    sorter.resetFlags(kTRUE, kTRUE, kTRUE, kTRUE);
    Int_t nCandHad = sorter.fill(HParticleTrackSorter::selectHadrons);
    Int_t nCandHadBest = sorter.selectBest(Particle::kIsBestRKSorter, Particle::kIsHadronSorter);

    printf("Total events = %d  Start = %d  To analyze = %d\n", entries, anapars.start, anapars.events);

    //----------------------------------------------------------------------------------------------
    // Beam position finding loop

    Double_t xBeam = 1.866; // TODO this needs to be changed as soon as we have beam and alignment. Otherwise calculating z-Prime does not works
    Double_t yBeam = -1.438;

    /*
    Int_t beam_pos_event_limit;
    if ((limit_sto - limit_sta) < 1000 ) beam_pos_event_limit = (limit_sto - limit_sta);
    else beam_pos_event_limit = 1000;

    cout<<beam_pos_event_limit<<endl;

    for (Int_t i = limit_sta; i < limit_sta + beam_pos_event_limit; i++)
    {

        HEventHeader* header  = gHades->getCurrentEvent()->getHeader();
        HVertex vertex      = header->getVertexReco();

        xBeam = xBeam + vertex.getX();
        yBeam = yBeam + vertex.getY();

        if(i%100 == 0) cout<<vertex.getX()<<" ; "<<vertex.getY()<<endl;
    }

    xBeam = xBeam / (Double_t)beam_pos_event_limit;
    yBeam = yBeam /(Double_t)beam_pos_event_limit;
    */

    cout<<"xBeam = "<<xBeam<<" ; yBeam = "<<yBeam<<endl;


    //----------------------------------------------------------------------------------------------
    // Event loop
    for (Int_t i = limit_sta; i < limit_sto; i++)
    {
        if (i % 10000 == 0)
        {
            timer.Stop();
            // printf("Event nr.: %d, progress: %.2f%%, time: %f s\n", i, (double)(i - limit_sta) /
            // (limit_sto - limit_sta) * 100., timer.RealTime());
            printf("Event nr.: %d, Progress: %.2f%%, ", i,
                   (double)(i - limit_sta) / (limit_sto - limit_sta) * 100.);
            timer.Print();
            // cout<<endl;
            timer.Start(kFALSE);
        }

        if ((i+1)%step_size == 0)
        {
            num_Events_table[i/step_size] = i/step_size;
        }

        //----------------------------------------------------------------------------------------------
        // Get next event. Categories will be cleared before
        loop->nextEvent(i);

        num_Events_Total++;

        HEventHeader* event_header = NULL;
        if (!(event_header = gHades->getCurrentEvent()->getHeader())) continue;

        if (num_Events_Total == 1)
        {
            auto t = decodeHadesTimeAndDate(event_header->getDate(), event_header->getTime(), true);
            start_t = std::mktime(&t);
        }
        else
        {
            auto t = decodeHadesTimeAndDate(event_header->getDate(), event_header->getTime());
            auto _start_t = std::mktime(&t);
            if (_start_t < start_t)
                start_t < _start_t;

            t = decodeHadesTimeAndDate(event_header->getDate(), event_header->getTime());
            auto _stop_t = std::mktime(&t);
            if (_stop_t > stop_t)
                stop_t = _stop_t;
        }

        // Timestamp pierwszego pliku:
        TString new_file;
        auto r = loop->isNewFile(new_file);
        if (r and num_Events_Total == 1)
        {
            auto last = new_file.Last('/');
            if (last != kNPOS) {}
            std::smatch capture;
            string nf = new_file.Data();
            if (regex_match(nf, capture, str_expr))
            {
                char buf[255];

                struct tm tm;
                memset(&tm, 0, sizeof(tm));

                strptime(capture[1].str().c_str(), "%y%j%H%M%S", &tm);
                strftime(buf, sizeof(buf), "%s", &tm);
                timestamp = buf;
            }
        }

        num_HH_El_per_Event = 0;
        num_HF_El_per_Event = 0;
        has_HH = false;
        has_HF = false;
        has_HH_elastic = false;
        has_HF_elastic = false;
        has_HH_bkg_ref = false;
        has_HF_bkg_ref = false;

        // Setting temporary values to 0, to ensure there's only one elastic pair in an event
        el_Sector_P1 = 0;
        el_Sector_P2 = 0;
        el_mass_P1 = 0;
        el_mass_P2 = 0;
        el_z_start_P1 = 0.0;
        el_z_start_P2 = 0.0;
        el_r_start_P1 = 0.0;
        el_r_start_P2 = 0.0;
        el_theta_P1 = 0.0;
        el_phi_P1 = 0.0;
        el_mom_P1 = 0.0;
        el_theta_P2 = 0.0;
        el_phi_P2 = 0.0;
        el_mom_P2 = 0.0;
        el_phi_diff = 0.0;
        el_tan_theta_product = 0.0;

        // Setting temporary values to 0, to ensure there's only one background reference pair in
        // event
        bkg_Sector_P1 = 0;
        bkg_Sector_P2 = 0;
        bkg_z_start_P1 = 0.0;
        bkg_z_start_P2 = 0.0;
        bkg_theta_P1 = 0.0;
        bkg_phi_P1 = 0.0;
        bkg_mom_P1 = 0.0;
        bkg_theta_P2 = 0.0;
        bkg_phi_P2 = 0.0;
        bkg_mom_P2 = 0.0;
        bkg_phi_diff = 0.0;
        bkg_tan_theta_product = 0.0;

        Int_t TBit = (Int_t)event_header->getTBit();

        //----------------------------------------------------------------------------------------------
        // Checking trigger
        bool is_pt1 = false;
        bool is_pt2 = false;
        bool is_pt3 = false;

        // Binary flags for each trigger type
        t_PT1 = 0;
        t_PT2 = 0;
        t_PT3 = 0;

        if ((TBit & 2048) == 2048)
        {
            // bit=1;//PT1
            num_Events_PT1++;
            is_pt1 = true;
            t_PT1 = 1;
        }
        if ((TBit & 4096) == 4096)
        {
            // bit=2;//PT2
            num_Events_PT2++;
            is_pt2 = true;
            t_PT2 = 1;
        }
        if ((TBit & 8192) == 8192)
        {
            // bit=3;//PT3
            num_Events_PT3++;
            is_pt3 = true;
            t_PT3 = 1;
        }

        /*
        //----------------------------------------------------------------------------------------------
        // Elastic events checks
        //----------------------------------------------------------------------------------------------

        auto has_HH_elastic = ForwardTools::Elastics::check_elastics_hh(fParticleCand,
                                                                        cut_HH_phi_diff_min,
                                                                        cut_HH_phi_diff_max,
                                                                        cut_HH_tan_theta_min,
                                                                        cut_HH_tan_theta_max);

        auto has_HF_elastic = ForwardTools::Elastics::check_elastics_hf(fParticleCand, fForwardCand,
                                                                        cut_HF_phi_diff_min,
                                                                        cut_HF_phi_diff_max,
                                                                        cut_HF_tan_theta_min,
                                                                        cut_HF_tan_theta_max);

        if(has_HH_elastic || has_HF_elastic) num_Events_Total_Elastic = num_Events_Total_Elastic +
        1;

        if(has_HH_elastic)
        {
            num_Events_HH_Elastic = num_Events_HH_Elastic + 1;
            if(t_PT1 == 1) num_HH_Events_Elastic_PT1 = num_HH_Events_Elastic_PT1 + 1;
            if(t_PT2 == 1) num_HH_Events_Elastic_PT2 = num_HH_Events_Elastic_PT2 + 1;
            if(t_PT3 == 1) num_HH_Events_Elastic_PT3 = num_HH_Events_Elastic_PT3 + 1;
        }

        if(has_HF_elastic)
        {
            num_Events_HF_Elastic = num_Events_HF_Elastic + 1;
            if(t_PT1 == 1) num_HF_Events_Elastic_PT1 = num_HF_Events_Elastic_PT1 + 1;
            if(t_PT2 == 1) num_HF_Events_Elastic_PT2 = num_HF_Events_Elastic_PT2 + 1;
            if(t_PT3 == 1) num_HF_Events_Elastic_PT3 = num_HF_Events_Elastic_PT3 + 1;
        }
        */

        if (fForwardCand && fParticleCand && t_PT2 == 1)
        {
            particle_cand_cnt = fParticleCand->getEntries();
            forward_cand_cnt = fForwardCand->getEntries();

            /*

            //----------------------------------------------------------------------------------------------
            // Calculate hit position (X or Y) in fRPC from intersection of track with fRPC plane
            Float_t ForwardTools::Miscellaneous::calc_hit_pos(Float_t x1, Float_t x2, Float_t z1, Float_t z2,
                                                            Float_t z_strip)
            {
                Float_t slope = (x2 - x1) / (z2 - z1); // Track slope parameter
                Float_t intercept = x2 - slope * z2;   // Track intercept parameter
                return slope * z_strip + intercept;    // Hit position
            }

            */

            //----------------------------------------------------------------------------------------------
            // Vertex reconstruction - zPrime and FwDet tracks crossing this plane
            //----------------------------------------------------------------------------------------------

            //Double_t xBeam = 1.788; // TODO this needs to be changed as soon as we have beam and alignment. Otherwise calculating z-Prime does not works
            //Double_t yBeam = -1.135;
            //HParticleCand *cand = 0;

            Float_t num_H_tracks = 0.0;
            Float_t zPlane_avg = 0.0;
            Float_t xPlane_avg = 0.0;
            Float_t yPlane_avg = 0.0;

            if(particle_cand_cnt >= 1 && forward_cand_cnt >= 1)
            {

                // Loop over particle candidate tracks - searching for average zPrime in the event
                for (Int_t i = 0; i < particle_cand_cnt; i++)
                {
                    HParticleCand* fparticlecand = 0;
                    fparticlecand = HCategoryManager::getObject(fparticlecand, fParticleCand, i);
                    if (fparticlecand && fparticlecand->getChi2() > 0 && fparticlecand->getChi2() < 1000)
                    {
                        //Double_t weight = (Double_t)1 / particle_cand_cnt;
                        Float_t rPlane = fparticlecand->getRprime(xBeam, yBeam);
                        Float_t zPlane = fparticlecand->getZprime(xBeam, yBeam, rPlane);

                        num_H_tracks = num_H_tracks + 1.0;
                        zPlane_avg = zPlane_avg + zPlane;
                    }
                }
                zPlane_avg = zPlane_avg / num_H_tracks;

                // Loop over forward candidate tracks - searching points of crossing the zPrime plane
                for (Int_t i = 0; i < forward_cand_cnt; i++)
                {
                    HForwardCand* fwdetcand = 0;
                    fwdetcand = HCategoryManager::getObject(fwdetcand, fForwardCand, i);

                    HGeomVector base;
                    HGeomVector end;
                    HGeomVector dir;

                    // Track extraction
                    HParticleTool::calcSegVector(fwdetcand->getZ(), fwdetcand->getR(), fwdetcand->Phi(), fwdetcand->Theta(), base, dir);
                    end = base + dir;

                    Float_t xPlane = ForwardTools::Miscellaneous::calc_hit_pos(base.getX(), end.getX(), base.getZ(), end.getZ(), zPlane_avg);
                    Float_t yPlane = ForwardTools::Miscellaneous::calc_hit_pos(base.getY(), end.getY(), base.getZ(), end.getZ(), zPlane_avg);

                    xPlane_avg = xPlane_avg + xPlane;
                    yPlane_avg = yPlane_avg + yPlane;
                }
                xPlane_avg = xPlane_avg / forward_cand_cnt;
                yPlane_avg = yPlane_avg / forward_cand_cnt;

                if (zPlane_avg != 0.0 && xPlane_avg != 0.0 && yPlane_avg != 0.0)
                {
                    h_vertex_R_vs_Z_Plane->Fill(zPlane_avg,TMath::Sqrt(xPlane_avg*xPlane_avg + yPlane_avg*yPlane_avg));
                    h_vertex_R_Plane->Fill(TMath::Sqrt(xPlane_avg*xPlane_avg + yPlane_avg*yPlane_avg));
                    h_vertex_Z_Plane->Fill(zPlane_avg);
                }
            }

            //----------------------------------------------------------------------------------------------
            // Vertex reconstruction - zPrime from HADES tracks (Z coordinate) and pairing tracks (XY components)
            //----------------------------------------------------------------------------------------------

            /*

            if(particle_cand_cnt >= 2 && forward_cand_cnt >= 2)
            {
                HGeomVector av_PCA_fcand, av_PCA_pcand;
                av_PCA_fcand.setXYZ(0.0,0.0,0.0);
                av_PCA_pcand.setXYZ(0.0,0.0,0.0);

                // Vector of "HGeomVector" paris defining tracks
                std::vector<std::pair<HGeomVector, HGeomVector>> tracks_pcand;
                std::vector<std::pair<HGeomVector, HGeomVector>> tracks_fcand;

                //----------------------------------------------------------------------------------------------
                // FwDet Particles
                for (Int_t i = 0; i < forward_cand_cnt; i++)
                {
                    HForwardCand* fwdetcand = 0;
                    fwdetcand = HCategoryManager::getObject(fwdetcand, fForwardCand, i);

                    HGeomVector base;
                    HGeomVector dir;

                    // Track extraction
                    HParticleTool::calcSegVector(fwdetcand->getZ(), fwdetcand->getR(), fwdetcand->Phi(), fwdetcand->Theta(), base, dir);
                    tracks_fcand.push_back({base, dir}); // Adding track to vector
                }

                //----------------------------------------------------------------------------------------------
                // HADES Particles
                for (Int_t i = 0; i < particle_cand_cnt; i++)
                {
                    HParticleCand* fparticlecand = 0;
                    fparticlecand = HCategoryManager::getObject(fparticlecand, fParticleCand, i);
                    if(!fparticlecand->isFlagBit(kIsUsed)) continue;

                    HGeomVector base;
                    HGeomVector dir;

                    // Track extraction
                    HParticleTool::calcSegVector(fparticlecand->getZ(), fparticlecand->getR(), fparticlecand->Phi(), fparticlecand->Theta(), base, dir);
                    tracks_pcand.push_back({base, dir}); // Adding track to vector
                }

                //----------------------------------------------------------------------------------------------
                // Calculating closest approach point - average from pairing tracks with each other in ForwardCand

                // Number of tracks
                auto size_fcand = (Int_t)tracks_fcand.size();
                Double_t pairs_num_fcand = 0.0;

                // Loop over "i"
                for (Int_t i = 0; i < size_fcand; i++)
                {
                    // Loop over "j" - starting from "i+1" to avoid pairing same tracks twice and pairing a track with itself
                    for (Int_t j = i + 1; j < size_fcand; j++)
                    {
                        //auto [base1, dir1] = tracks[i]; c++17 only
                        //auto [base2, dir2] = tracks[j];
                        auto base1 = tracks_fcand[i].first;
                        auto dir1 = tracks_fcand[i].second;
                        auto base2 = tracks_fcand[j].first;
                        auto dir2 = tracks_fcand[j].second;

                        // Calculating the point of closest approach between the two tracks
                        HGeomVector PCA_fcand ;
                        PCA_fcand.setXYZ(-2000.0,-2000.0,-2000.0);
                        if(dir1==dir2 && dir1.getZ()==1) // track same direction / along z axis
                        {
                            //cout<<"parallel fw"<<endl;
                        }
                        else
                        {
                            PCA_fcand = HParticleTool::calculatePointOfClosestApproach(base1, dir1, base2, dir2);

                            // Calculating average PCA
                            av_PCA_fcand += PCA_fcand;
                            pairs_num_fcand = pairs_num_fcand + 1.0;
                        }
                    }
                }

                // Number of tracks
                auto size_pcand = (Int_t)tracks_pcand.size();
                Double_t pairs_num_pcand = 0.0;

                // Loop over "i"
                for (Int_t i = 0; i < size_pcand; i++)
                {
                    // Loop over "j" - starting from "i+1" to avoid pairing same tracks twice and pairing a track with itself
                    for (Int_t j = i + 1; j < size_pcand; j++)
                    {
                        //auto [base1, dir1] = tracks[i]; c++17 only
                        //auto [base2, dir2] = tracks[j];
                        auto base1 = tracks_pcand[i].first;
                        auto dir1 = tracks_pcand[i].second;
                        auto base2 = tracks_pcand[j].first;
                        auto dir2 = tracks_pcand[j].second;

                        // Calculating the point of closest approach between the two tracks
                        HGeomVector PCA_pcand ;
                        PCA_pcand.setXYZ(-2000.0,-2000.0,-2000.0);
                        if(dir1==dir2) // trach same direction
                        {
                            //cout<<"parallel"<<endl;
                        }
                        else
                        {
                            PCA_pcand = HParticleTool::calculatePointOfClosestApproach(base1, dir1, base2, dir2);

                            // Calculating average PCA
                            av_PCA_pcand += PCA_pcand;
                            pairs_num_pcand = pairs_num_pcand + 1.0;
                        }
                    }
                }

                // Calculating average PCAs
                av_PCA_fcand /= pairs_num_fcand;
                av_PCA_pcand /= pairs_num_pcand;
                Float_t av_PCA_R = TMath::Sqrt(av_PCA_fcand.getX()*av_PCA_fcand.getX() + av_PCA_fcand.getY()*av_PCA_fcand.getY() );
                if (av_PCA_pcand.getZ() != -2000.0 && av_PCA_fcand.getX() != -2000.0 && av_PCA_fcand.getY() != -2000.0)
                {
                    h_vertex_R_vs_Z->Fill(av_PCA_pcand.getZ(),av_PCA_R);
                    //if (av_PCA_R <= 20.0)
                        h_vertex_Z->Fill(av_PCA_pcand.getZ());
                    //if (av_PCA_pcand.getZ() >= -300.0 && av_PCA_pcand.getZ() <= 100.0)
                        h_vertex_R->Fill(av_PCA_R);
                }
            }
            else if (forward_cand_cnt + particle_cand_cnt >= 2)
            {

                //----------------------------------------------------------------------------------------------
                // Loops over all tracks
                //----------------------------------------------------------------------------------------------

                HGeomVector av_PCA;
                av_PCA.setXYZ(0.0,0.0,0.0);

                // Vector of "HGeomVector" paris defining tracks
                std::vector<std::pair<HGeomVector, HGeomVector>> tracks;

                //----------------------------------------------------------------------------------------------
                // FwDet Particles
                for (Int_t i = 0; i < forward_cand_cnt; i++)
                {
                    HForwardCand* fwdetcand = 0;
                    fwdetcand = HCategoryManager::getObject(fwdetcand, fForwardCand, i);

                    HGeomVector base;
                    HGeomVector dir;

                    // Track extraction
                    HParticleTool::calcSegVector(fwdetcand->getZ(), fwdetcand->getR(), fwdetcand->Phi(), fwdetcand->Theta(), base, dir);
                    tracks.push_back({base, dir}); // Adding track to vector
                }

                //----------------------------------------------------------------------------------------------
                // HADES Particles
                for (Int_t i = 0; i < particle_cand_cnt; i++)
                {
                    HParticleCand* fparticlecand = 0;
                    fparticlecand = HCategoryManager::getObject(fparticlecand, fParticleCand, i);
                    if(!fparticlecand->isFlagBit(kIsUsed)) continue;

                    HGeomVector base;
                    HGeomVector dir;

                    // Track extraction
                    HParticleTool::calcSegVector(fparticlecand->getZ(), fparticlecand->getR(), fparticlecand->Phi(), fparticlecand->Theta(), base, dir);
                    tracks.push_back({base, dir}); // Adding track to vector
                }

                //----------------------------------------------------------------------------------------------
                // Calculating closest approach point - average from pairing tracks with each other

                // Number of tracks
                auto s = (Int_t)tracks.size();
                Double_t pairs_num = 0.0;

                // Loop over "i"
                for (Int_t i = 0; i < s; i++)
                {
                    // Loop over "j" - starting from "i+1" to avoid pairing same tracks twice and pairing a track with itself
                    for (Int_t j = i + 1; j < s; j++)
                    {
                        //auto [base1, dir1] = tracks[i]; c++17 only
                        //auto [base2, dir2] = tracks[j];
                        auto base1 = tracks[i].first;
                        auto dir1 = tracks[i].second;
                        auto base2 = tracks[j].first;
                        auto dir2 = tracks[j].second;

                        // Calculating the point of closest approach between the two tracks
                        HGeomVector PCA;
                        PCA.setXYZ(-2000,-2000,-2000);
                        if(dir1==dir2) // trach same direction
                        {
                            //cout<<"parallel"<<endl;
                        }
                        else
                        {
                            PCA = HParticleTool::calculatePointOfClosestApproach(base1, dir1, base2, dir2);

                            h_vertex_X_Pair->Fill(PCA.getX());
                            h_vertex_Y_Pair->Fill(PCA.getY());
                            h_vertex_Z_Pair->Fill(PCA.getZ());
                            h_vertex_R_Pair->Fill(TMath::Sqrt(PCA.getX()*PCA.getX() + PCA.getY()*PCA.getY() ));

                            // Calculating average PCA
                            av_PCA += PCA;
                            pairs_num = pairs_num + 1.0;
                        }
                    }
                }

                // Calculating average PCA
                av_PCA /= pairs_num;

                Float_t av_PCA_R = TMath::Sqrt(av_PCA.getX()*av_PCA.getX() + av_PCA.getY()*av_PCA.getY() );
                if (av_PCA.getZ() != -2000.0 && av_PCA.getX() != -2000.0 && av_PCA.getY() != -2000.0)
                {
                    h_vertex_R_vs_Z->Fill(av_PCA.getZ(),av_PCA_R);
                    if (av_PCA_R <= 20.0) h_vertex_Z->Fill(av_PCA.getZ());
                    if (av_PCA.getZ() >= -300.0 && av_PCA.getZ() <= 100.0) h_vertex_R->Fill(av_PCA_R);
                }
            }

            */

            if(particle_cand_cnt >= 1 && forward_cand_cnt >= 2)
            {
                HGeomVector av_PCA_fcand;
                av_PCA_fcand.setXYZ(0.0,0.0,0.0);
                Float_t av_PCA_zPrime = 0.0;
                Float_t num_pcand_zPrime = 0.0;

                // Vector of "HGeomVector" paris defining tracks
                std::vector<std::pair<HGeomVector, HGeomVector>> tracks_pcand;
                std::vector<std::pair<HGeomVector, HGeomVector>> tracks_fcand;

                //----------------------------------------------------------------------------------------------
                // FwDet Particles
                for (Int_t i = 0; i < forward_cand_cnt; i++)
                {
                    HForwardCand* fwdetcand = 0;
                    fwdetcand = HCategoryManager::getObject(fwdetcand, fForwardCand, i);

                    HGeomVector base;
                    HGeomVector dir;

                    // Track extraction
                    HParticleTool::calcSegVector(fwdetcand->getZ(), fwdetcand->getR(), fwdetcand->Phi(), fwdetcand->Theta(), base, dir);
                    tracks_fcand.push_back({base, dir}); // Adding track to vector
                }

                //----------------------------------------------------------------------------------------------
                // HADES Particles - extracting zPrime
                for (Int_t i = 0; i < particle_cand_cnt; i++)
                {
                    HParticleCand* fparticlecand = 0;
                    fparticlecand = HCategoryManager::getObject(fparticlecand, fParticleCand, i);
                    if(!fparticlecand->isFlagBit(kIsUsed)) continue;

                    Float_t rPrime = fparticlecand->getRprime(xBeam, yBeam);
                    Float_t zPrime = fparticlecand->getZprime(xBeam, yBeam, rPrime);

                    num_pcand_zPrime = num_pcand_zPrime + 1.0;
                    av_PCA_zPrime = av_PCA_zPrime + zPrime;
                }

                av_PCA_zPrime = av_PCA_zPrime/num_pcand_zPrime;

                //----------------------------------------------------------------------------------------------
                // Calculating closest approach point - average from pairing tracks with each other in ForwardCand

                // Number of tracks
                auto size_fcand = (Int_t)tracks_fcand.size();
                Double_t pairs_num_fcand = 0.0;

                // Loop over "i"
                for (Int_t i = 0; i < size_fcand; i++)
                {
                    // Loop over "j" - starting from "i+1" to avoid pairing same tracks twice and pairing a track with itself
                    for (Int_t j = i + 1; j < size_fcand; j++)
                    {
                        //auto [base1, dir1] = tracks[i]; c++17 only
                        //auto [base2, dir2] = tracks[j];
                        auto base1 = tracks_fcand[i].first;
                        auto dir1 = tracks_fcand[i].second;
                        auto base2 = tracks_fcand[j].first;
                        auto dir2 = tracks_fcand[j].second;

                        // Calculating the point of closest approach between the two tracks
                        HGeomVector PCA_fcand ;
                        PCA_fcand.setXYZ(-2000.0,-2000.0,-2000.0);
                        if(dir1==dir2 && dir1.getZ()==1) // track same direction / along z axis
                        {
                            //cout<<"parallel fw"<<endl;
                        }
                        else
                        {
                            PCA_fcand = HParticleTool::calculatePointOfClosestApproach(base1, dir1, base2, dir2);

                            // Calculating average PCA
                            av_PCA_fcand += PCA_fcand;
                            pairs_num_fcand = pairs_num_fcand + 1.0;
                        }
                    }
                }

                // Calculating average PCAs
                av_PCA_fcand /= pairs_num_fcand;
                Float_t av_PCA_R = TMath::Sqrt(av_PCA_fcand.getX()*av_PCA_fcand.getX() + av_PCA_fcand.getY()*av_PCA_fcand.getY() );
                if (av_PCA_zPrime != -2000.0 && av_PCA_fcand.getX() != -2000.0 && av_PCA_fcand.getY() != -2000.0)
                {
                    h_vertex_R_vs_Z->Fill(av_PCA_zPrime,av_PCA_R);
                    if (av_PCA_R <= 20.0) h_vertex_Z->Fill(av_PCA_zPrime);
                    if (av_PCA_zPrime >= -300.0 && av_PCA_zPrime <= 100.0) h_vertex_R->Fill(av_PCA_R);
                }
            }
            else if (forward_cand_cnt + particle_cand_cnt >= 2)
            {

                //----------------------------------------------------------------------------------------------
                // Loops over all tracks
                //----------------------------------------------------------------------------------------------

                HGeomVector av_PCA;
                av_PCA.setXYZ(0.0,0.0,0.0);
                Float_t av_PCA_zPrime = 0.0;
                Float_t num_pcand_zPrime = 0.0;

                // Vector of "HGeomVector" paris defining tracks
                std::vector<std::pair<HGeomVector, HGeomVector>> tracks;

                //----------------------------------------------------------------------------------------------
                // FwDet Particles
                for (Int_t i = 0; i < forward_cand_cnt; i++)
                {
                    HForwardCand* fwdetcand = 0;
                    fwdetcand = HCategoryManager::getObject(fwdetcand, fForwardCand, i);

                    HGeomVector base;
                    HGeomVector dir;

                    // Track extraction
                    HParticleTool::calcSegVector(fwdetcand->getZ(), fwdetcand->getR(), fwdetcand->Phi(), fwdetcand->Theta(), base, dir);
                    tracks.push_back({base, dir}); // Adding track to vector
                }

                //----------------------------------------------------------------------------------------------
                // HADES Particles
                for (Int_t i = 0; i < particle_cand_cnt; i++)
                {
                    HParticleCand* fparticlecand = 0;
                    fparticlecand = HCategoryManager::getObject(fparticlecand, fParticleCand, i);
                    if(!fparticlecand->isFlagBit(kIsUsed)) continue;

                    HGeomVector base;
                    HGeomVector dir;

                    // Track extraction
                    HParticleTool::calcSegVector(fparticlecand->getZ(), fparticlecand->getR(), fparticlecand->Phi(), fparticlecand->Theta(), base, dir);
                    tracks.push_back({base, dir}); // Adding track to vector

                    Float_t rPrime = fparticlecand->getRprime(xBeam, yBeam);
                    Float_t zPrime = fparticlecand->getZprime(xBeam, yBeam, rPrime);

                    num_pcand_zPrime = num_pcand_zPrime + 1.0;
                    av_PCA_zPrime = av_PCA_zPrime + zPrime;
                }

                av_PCA_zPrime = av_PCA_zPrime/num_pcand_zPrime;

                //----------------------------------------------------------------------------------------------
                // Calculating closest approach point - average from pairing tracks with each other

                // Number of tracks
                auto s = (Int_t)tracks.size();
                Double_t pairs_num = 0.0;

                // Loop over "i"
                for (Int_t i = 0; i < s; i++)
                {
                    // Loop over "j" - starting from "i+1" to avoid pairing same tracks twice and pairing a track with itself
                    for (Int_t j = i + 1; j < s; j++)
                    {
                        //auto [base1, dir1] = tracks[i]; c++17 only
                        //auto [base2, dir2] = tracks[j];
                        auto base1 = tracks[i].first;
                        auto dir1 = tracks[i].second;
                        auto base2 = tracks[j].first;
                        auto dir2 = tracks[j].second;

                        // Calculating the point of closest approach between the two tracks
                        HGeomVector PCA;
                        PCA.setXYZ(-2000,-2000,-2000);
                        if(dir1==dir2) // trach same direction
                        {
                            //cout<<"parallel"<<endl;
                        }
                        else
                        {
                            PCA = HParticleTool::calculatePointOfClosestApproach(base1, dir1, base2, dir2);

                            h_vertex_X_Pair->Fill(PCA.getX());
                            h_vertex_Y_Pair->Fill(PCA.getY());
                            h_vertex_Z_Pair->Fill(PCA.getZ());
                            h_vertex_R_Pair->Fill(TMath::Sqrt(PCA.getX()*PCA.getX() + PCA.getY()*PCA.getY() ));

                            // Calculating average PCA
                            av_PCA += PCA;
                            pairs_num = pairs_num + 1.0;
                        }
                    }
                }

                // Calculating average PCA
                av_PCA /= pairs_num;
                Float_t av_PCA_R = TMath::Sqrt(av_PCA.getX()*av_PCA.getX() + av_PCA.getY()*av_PCA.getY() );
                if (av_PCA_zPrime != -2000.0 && av_PCA.getX() != -2000.0 && av_PCA.getY() != -2000.0)
                {
                    h_vertex_R_vs_Z->Fill(av_PCA_zPrime,av_PCA_R);
                    if (av_PCA_R <= 20.0) h_vertex_Z->Fill(av_PCA_zPrime);
                    if (av_PCA_zPrime >= -300.0 && av_PCA_zPrime <= 100.0) h_vertex_R->Fill(av_PCA_R);
                }
            }

            //----------------------------------------------------------------------------------------------
            // Global STS multiplicity
            //----------------------------------------------------------------------------------------------

            if(fStsRaw)
            {
                HStsRaw * stsstraw; // Initializing HStsRaw pointer

                Int_t multiplicity_ALL = fStsRaw->getEntries(); // Get number of entries (number of signals registered by straws) in one event
                Int_t multiplicity_STS1 = 0;
                Int_t multiplicity_STS2 = 0;

                for(Int_t i = 0; i < fStsRaw->getEntries(); i ++) // Loop over entries inside each event
                {
                    stsstraw = (HStsRaw*)fStsRaw->getObject(i); // Get fStsRaw object

                    if(stsstraw) // Check if "stsstraw" object was accessed properly
                    {
                        // Initializing extracted variables
                        Char_t mod, lay, ud; // mod - Module, lay - Layer, ud - upper/lower (short) or long straw
                        Int_t straw; // Straw index

                        // Getting values of each variable in this entry
                        stsstraw->getAddress(mod, lay, straw, ud);

                        // Filling multiplicity for STS1 and STS2
                        if ( mod == 0 ) multiplicity_STS1 = multiplicity_STS1 + 1;
                        if ( mod == 1 ) multiplicity_STS2 = multiplicity_STS2 + 1;
                    }
                }
                // Filling multiplicity histograms
                h_STS_multiplicity_ALL->Fill(multiplicity_ALL);
                h_STS_multiplicity_STS1->Fill(multiplicity_STS1);
                h_STS_multiplicity_STS2->Fill(multiplicity_STS2);
            }

            //----------------------------------------------------------------------------------------------
            // HADES particles
            //----------------------------------------------------------------------------------------------

            if (particle_cand_cnt >= 1)
            {
                // Loop over ParticleCand
                for (Int_t j = 0; j < particle_cand_cnt; j++)
                {
                    //----------------------------------------------------------------------------------------------
                    // HADES particle
                    HParticleCand* fparticlecand = 0;
                    fparticlecand = HCategoryManager::getObject(fparticlecand, fParticleCand, j);

                    if (fparticlecand->isFlagBit(kIsUsed) && fparticlecand->getTofdEdx() > 1.0 && fparticlecand->getSystem() == 1)
                    {
                        num_H_part++;
                        //if (fparticlecand->getMass() < 400.0) continue;
                        h_H_mass->Fill(fparticlecand->getMass());
                        h_H_theta->Fill(fparticlecand->getTheta()); // [deg]
                        h_H_mom_vs_theta->Fill(fparticlecand->getTheta(), fparticlecand->getMomentum());

                        // if hit in range 70 deg to 80 deg --> look for STS1 and STS2 multiplicity
                        if(fStsRaw && fparticlecand->getTheta() >= 70.0 && fparticlecand->getTheta() < 80.0)
                        {
                            HStsRaw * stsstraw; // Initializing HStsRaw pointer

                            Int_t multiplicity_ALL = fStsRaw->getEntries(); // Get number of entries (number of signals registered by straws) in one event
                            Int_t multiplicity_STS1 = 0;
                            Int_t multiplicity_STS2 = 0;

                            for(Int_t i = 0; i < fStsRaw->getEntries(); i ++) // Loop over entries inside each event
                            {
                                stsstraw = (HStsRaw*)fStsRaw->getObject(i); // Get fStsRaw object

                                if(stsstraw) // Check if "stsstraw" object was accessed properly
                                {
                                    // Initializing extracted variables
                                    Char_t mod, lay, ud; // mod - Module, lay - Layer, ud - upper/lower (short) or long straw
                                    Int_t straw; // Straw index

                                    // Getting values of each variable in this entry
                                    stsstraw->getAddress(mod, lay, straw, ud);

                                    // Filling multiplicity for STS1 and STS2
                                    if ( mod == 0 ) multiplicity_STS1 = multiplicity_STS1 + 1;
                                    if ( mod == 1 ) multiplicity_STS2 = multiplicity_STS2 + 1;
                                }
                            }
                            // Filling multiplicity histograms
                            h_STS_multiplicity_ALL_H->Fill(multiplicity_ALL);
                            h_STS_multiplicity_STS1_H->Fill(multiplicity_STS1);
                            h_STS_multiplicity_STS2_H->Fill(multiplicity_STS2);
                        }

                        Float_t rPrime = fparticlecand->getRprime(xBeam, yBeam);
                        Float_t zPrime = fparticlecand->getZprime(xBeam, yBeam, rPrime);

                        h_vertex_R_vs_Z_H->Fill(zPrime,rPrime);
                        h_vertex_Z_H->Fill(zPrime);
                        h_vertex_R_H->Fill(rPrime);

                        break; // If one track in event found, stop looking for further ones
                    }
                }
            }

            /*

            HParticleTrackSorter sorter;
            sorter.init();
            sorter.cleanUp();
            sorter.resetFlags(kTRUE,kTRUE,kTRUE,kTRUE);
            Int_t nCandHad = sorter.fill(HParticleTrackSorter::selectHadrons);
            Int_t nCandHadBest =
            sorter.selectBest(Particle::kIsBestRKSorter,Particle::kIsHadronSorter);

            this must be above particle cand loop

            then inside the loop

            if(!cand->isFlagBit(kIsUsed)) continue;

            */

            //----------------------------------------------------------------------------------------------
            // HADES - HADES case --> Looking for elastic candidates
            //----------------------------------------------------------------------------------------------

            Float_t peak_width = cut_HH_phi_diff_max - cut_HH_phi_diff_min;
            Float_t integration_offset = 0.5 * peak_width;

            Float_t bkg_low_min = (cut_HH_phi_diff_min - 0.5 * peak_width - integration_offset);
            Float_t bkg_low_max = (cut_HH_phi_diff_min - integration_offset);
            Float_t bkg_high_min = (cut_HH_phi_diff_max + integration_offset);
            Float_t bkg_high_max = (cut_HH_phi_diff_max + 0.5 * peak_width + integration_offset);

            if (particle_cand_cnt >= 2)
            {
                // Loop over ParticleCand
                for (Int_t j = 0; j < particle_cand_cnt; j++)
                {
                    // Break if an elastic candidate already found in this event
                    // if (has_HH_elastic == true) continue;

                    // Loop over ParticleCand - starting from "j+1" to avoid pairing same tracks
                    // twice and pairing a track with itself
                    for (Int_t k = j + 1; k < particle_cand_cnt; k++)
                    {
                        check_mom = false;
                        check_theta = false;

                        //----------------------------------------------------------------------------------------------
                        // Particle 1 - HADES particle
                        HParticleCand* fparticlecand = 0;
                        fparticlecand = HCategoryManager::getObject(fparticlecand, fParticleCand, j);

                        if (!((fparticlecand->getTofdEdx() > 1.0 || fparticlecand->getMdcdEdx() > 1.0) && fparticlecand->isFlagBit(kIsUsed) )) continue;
                        //if (fparticlecand->getMass() < 400.0) continue;

                        t_Sector_P1 = (Int_t)fparticlecand->getSector();
                        t_mass_P1 = fparticlecand->getMass();
                        t_r_start_P1 = fparticlecand->getRprime(xBeam, yBeam); // [mm]
                        t_z_start_P1 = fparticlecand->getZprime(xBeam, yBeam, t_r_start_P1); // [mm]
                        t_phi_P1 = fparticlecand->getPhi();      // [deg]
                        t_theta_P1 = fparticlecand->getTheta();  // [deg]
                        t_mom_P1 = fparticlecand->getMomentum(); // [MeV/c]
                        t_mom_theor_P1 = f_mom_vs_theta->Eval(t_theta_P1,0.0,0.0); // [MeV/c]
                        t_beta_P1 = fparticlecand->getBeta();

                        // Track extraction
                        HParticleTool::calcSegVector(fparticlecand->getZ(), fparticlecand->getR(), fparticlecand->Phi(), fparticlecand->Theta(), base_P1, dir_P1);
                        t_rPrime_P1 = fparticlecand->getRprime(xBeam, yBeam);
                        t_zPrime_P1 = fparticlecand->getZprime(xBeam, yBeam, t_rPrime_P1);

                        //----------------------------------------------------------------------------------------------
                        // Particle 2 - HADES particle
                        fparticlecand = HCategoryManager::getObject(fparticlecand, fParticleCand, k);

                        if (!((fparticlecand->getTofdEdx() > 1.0 || fparticlecand->getMdcdEdx() > 1.0) && fparticlecand->isFlagBit(kIsUsed) )) continue;
                        //if (fparticlecand->getMass() < 400.0) continue;

                        t_Sector_P2 = (Int_t)fparticlecand->getSector();
                        t_mass_P2 = fparticlecand->getMass();
                        t_r_start_P2 = fparticlecand->getRprime(xBeam, yBeam); // [mm]
                        t_z_start_P2 = fparticlecand->getZprime(xBeam, yBeam, t_r_start_P2); // [mm]
                        t_phi_P2 = fparticlecand->getPhi();      // [deg]
                        t_theta_P2 = fparticlecand->getTheta();  // [deg]
                        t_mom_P2 = fparticlecand->getMomentum(); // [MeV/c]
                        t_mom_theor_P2 = f_mom_vs_theta->Eval(t_theta_P2,0.0,0.0); // [MeV/c]
                        t_beta_P2 = fparticlecand->getBeta();

                        // Track extraction
                        HParticleTool::calcSegVector(fparticlecand->getZ(), fparticlecand->getR(), fparticlecand->Phi(), fparticlecand->Theta(), base_P2, dir_P2);
                        t_rPrime_P2 = fparticlecand->getRprime(xBeam, yBeam);
                        t_zPrime_P2 = fparticlecand->getZprime(xBeam, yBeam, t_rPrime_P2);

                        //----------------------------------------------------------------------------------------------
                        // Two Partices Variables
                        //t_phi_diff = TMath::Abs(t_phi_P1 - t_phi_P2); // [deg]
                        t_phi_diff = fmod( (t_phi_P1 + 360.0 - t_phi_P2) , 360.0); // [deg]
                        t_tan_theta_product = TMath::Tan(TMath::DegToRad() * t_theta_P1) *
                                              TMath::Tan(TMath::DegToRad() * t_theta_P2); // [ ]
                        //t_dist = TMath::Sqrt( pow( ((t_phi_diff - 180.0) / ( HH_phi_diff_sigma )), 2) + pow( ((t_tan_theta_product - inverse_gamma2) / ( HH_tan_theta_prod_sigma )), 2) );

                        if ( (t_mom_P1 -t_mom_theor_P1 >= -500.0 && t_mom_P1 -t_mom_theor_P1 <= 500.0 ) && (t_mom_P2 -t_mom_theor_P2 >= -500.0 && t_mom_P2 -t_mom_theor_P2 <= 500.0) ) check_mom = true;
                        check_theta = true;

                        //----------------------------------------------------------------------------------------------
                        // Entries counting

                        has_HH = true;
                        num_HH_Entries++;
                        if (t_PT1 == 1) num_HH_Entries_PT1++;
                        if (t_PT2 == 1) num_HH_Entries_PT2++;
                        if (t_PT3 == 1) num_HH_Entries_PT3++;

                        //----------------------------------------------------------------------------------------------
                        // Histograms filling
                        h_HH_mass_P1->Fill(t_mass_P1);
                        h_HH_mass_P2->Fill(t_mass_P2);
                        h_HH_theta->Fill(t_theta_P1);
                        h_HH_theta->Fill(t_theta_P2);
                        h_HH_theta_sec[t_Sector_P1]->Fill(t_theta_P1);
                        h_HH_theta_sec[t_Sector_P2]->Fill(t_theta_P2);
                        h_HH_phi_diff->Fill(t_phi_diff);
                        h_HH_tan_theta_prod->Fill(t_tan_theta_product);
                        h_HH_tan_theta_prod_VS_phi_diff->Fill(t_phi_diff, t_tan_theta_product);
                        h_HH_phi_diff_VS_phi_P1->Fill(t_phi_diff, t_phi_P1);
                        h_HH_phi_diff_VS_phi_P2->Fill(t_phi_diff, t_phi_P2);
                        h_HH_and_HF_theta_P1_vs_theta_P2->Fill(t_theta_P1, t_theta_P2);
                        h_HH_phi_P1_vs_phi_P2->Fill(t_phi_P1, t_phi_P2);
                        h_HH_and_HF_mom_vs_theta_ALL->Fill(t_theta_P1, t_mom_P1);
                        h_HH_and_HF_mom_vs_theta_ALL->Fill(t_theta_P2, t_mom_P2);
                        h_HH_and_HF_mom_vs_theta[t_Sector_P1]->Fill(t_theta_P1, t_mom_P1);
                        h_HH_and_HF_mom_vs_theta[t_Sector_P2]->Fill(t_theta_P2, t_mom_P2);
                        h_HH_and_HF_delta_mom_vs_mom->Fill(t_mom_P1, t_mom_P1 - t_mom_theor_P1);
                        h_HH_and_HF_delta_mom_vs_mom->Fill(t_mom_P2, t_mom_P2 - t_mom_theor_P2);
                        h_HH_and_HF_mom_vs_beta->Fill(t_beta_P1, t_mom_P1);
                        h_HH_and_HF_mom_vs_beta->Fill(t_beta_P2, t_mom_P2);

                        if (t_phi_diff >= cut_HH_phi_diff_min && t_phi_diff <= cut_HH_phi_diff_max && check_mom == true)
                        {
                            h_HH_tan_theta_prod_cut->Fill(t_tan_theta_product);
                        }
                        if (t_tan_theta_product >= cut_HH_tan_theta_min && t_tan_theta_product <= cut_HH_tan_theta_max && check_mom == true)
                        {
                            h_HH_phi_diff_cut->Fill(t_phi_diff);
                            h_HH_phi_diff_VS_phi_P1_cut->Fill(t_phi_diff, t_phi_P1);
                            h_HH_phi_diff_VS_phi_P2_cut->Fill(t_phi_diff, t_phi_P2);
                        }
                        if ((t_phi_diff >= cut_HH_phi_diff_min &&
                             t_phi_diff <= cut_HH_phi_diff_max) &&
                            (t_tan_theta_product >= cut_HH_tan_theta_min &&
                             t_tan_theta_product <= cut_HH_tan_theta_max))
                        {
                            h_HH_and_HF_delta_mom_vs_mom_cut->Fill(t_mom_P1, t_mom_P1 - t_mom_theor_P1);
                            h_HH_and_HF_delta_mom_vs_mom_cut->Fill(t_mom_P2, t_mom_P2 - t_mom_theor_P2);
                        }

                        // Background reference selection

                        if (((t_phi_diff >= bkg_low_min && t_phi_diff <= bkg_low_max) ||
                             (t_phi_diff >= bkg_high_min && t_phi_diff <= bkg_high_max)) &&
                            (t_tan_theta_product >= cut_HH_tan_theta_min &&
                             t_tan_theta_product <= cut_HH_tan_theta_max) && check_mom == true)
                        //if ( t_dist > cut_BKG_dist_min && t_dist <= cut_BKG_dist_max)
                        {
                            // If it's the first background reference pair found in this event
                            if (has_HH_bkg_ref == false)
                            {
                                has_HH_bkg_ref = true;
                                bkg_phi_diff = t_phi_diff;
                                bkg_tan_theta_product = t_tan_theta_product;
                            }
                        }

                        // Elastic scattering selection

                        if ((t_phi_diff >= cut_HH_phi_diff_min &&
                             t_phi_diff <= cut_HH_phi_diff_max) &&
                            (t_tan_theta_product >= cut_HH_tan_theta_min &&
                             t_tan_theta_product <= cut_HH_tan_theta_max) && check_mom == true)
                        //if ( t_dist <= cut_EL_dist )
                        {
                            num_HH_El_per_Event++;

                            // If it's the first elastic candidate found in this event
                            if (has_HH_elastic == false)
                            {
                                has_HH_elastic = true;

                                //----------------------------------------------------------------------------------------------
                                // Candidate particle 1 - HADES particle
                                el_Sector_P1 = t_Sector_P1;
                                el_mass_P1 = t_mass_P1;
                                el_z_start_P1 = t_z_start_P1;
                                el_r_start_P1 = t_r_start_P1;
                                el_phi_P1 = t_phi_P1;
                                el_theta_P1 = t_theta_P1;
                                el_mom_P1 = t_mom_P1;
                                el_mom_theor_P1 = t_mom_theor_P1;
                                el_base_P1 = t_beta_P1;

                                el_base_P1 = base_P1;
                                el_dir_P1 = dir_P1;
                                el_zPrime_P1 = t_zPrime_P1;
                                el_rPrime_P1 = t_rPrime_P1;

                                //----------------------------------------------------------------------------------------------
                                // Candidate particle 2 - HADES particle
                                el_Sector_P2 = t_Sector_P2;
                                el_mass_P2 = t_mass_P2;
                                el_z_start_P2 = t_z_start_P2;
                                el_r_start_P2 = t_r_start_P2;
                                el_phi_P2 = t_phi_P2;
                                el_theta_P2 = t_theta_P2;
                                el_mom_P2 = t_mom_P2;
                                el_mom_theor_P2 = t_mom_theor_P2;
                                el_base_P2 = t_beta_P2;

                                el_base_P2 = base_P2;
                                el_dir_P2 = dir_P2;
                                el_zPrime_P2 = t_zPrime_P2;
                                el_rPrime_P2 = t_rPrime_P2;

                                el_phi_diff = t_phi_diff;
                                el_tan_theta_product = t_tan_theta_product;
                            }
                        }
                    }
                }
            }

            //----------------------------------------------------------------------------------------------
            // HADES - FwDet case --> Looking for elastic candidates
            //----------------------------------------------------------------------------------------------

            peak_width = cut_HF_phi_diff_max - cut_HF_phi_diff_min;
            integration_offset = 0.5 * peak_width;

            bkg_low_min = (cut_HF_phi_diff_min - 0.5 * peak_width - integration_offset);
            bkg_low_max = (cut_HF_phi_diff_min - integration_offset);
            bkg_high_min = (cut_HF_phi_diff_max + integration_offset);
            bkg_high_max = (cut_HF_phi_diff_max + 0.5 * peak_width + integration_offset);

            if (particle_cand_cnt >= 1 && forward_cand_cnt >= 1)
            {
                // Loop over ParticleCand
                for (Int_t j = 0; j < particle_cand_cnt; j++)
                {
                    // Break if more than one elastic candidate found in this event
                    // if (has_HF_elastic == true && has_HH_elastic == true) continue;

                    // Loop over ForwardCand
                    for (Int_t k = 0; k < forward_cand_cnt; k++)
                    {
                        check_mom = false;
                        check_theta = false;
                        //----------------------------------------------------------------------------------------------
                        // Particle 1 - HADES particle
                        HParticleCand* fparticlecand = 0;
                        fparticlecand = HCategoryManager::getObject(fparticlecand, fParticleCand, j);

                        if (!(fparticlecand->getTofdEdx() > 1.0 && fparticlecand->isFlagBit(kIsUsed) )) continue;
                        //if (fparticlecand->getMass() < 400.0) continue;

                        t_Sector_P1 = (Int_t)fparticlecand->getSector();
                        t_mass_P1 = fparticlecand->getMass();
                        t_r_start_P1 = fparticlecand->getRprime(xBeam, yBeam); // [mm]
                        t_z_start_P1 = fparticlecand->getZprime(xBeam, yBeam, t_r_start_P1); // [mm]
                        // t_track_length_P1 = fparticlecand->getDistanceToMetaHit(); // [mm]
                        t_phi_P1 = fparticlecand->getPhi();      // [deg]
                        t_theta_P1 = fparticlecand->getTheta();  // [deg]
                        t_mom_P1 = fparticlecand->getMomentum(); // [MeV/c]
                        t_mom_theor_P1 = f_mom_vs_theta->Eval(t_theta_P1,0.0,0.0); // [MeV/c]
                        t_beta_P1 = fparticlecand->getBeta();

                        // Track extraction
                        HParticleTool::calcSegVector(fparticlecand->getZ(), fparticlecand->getR(), fparticlecand->Phi(), fparticlecand->Theta(), base_P1, dir_P1);
                        t_rPrime_P1 = fparticlecand->getRprime(xBeam, yBeam);
                        t_zPrime_P1 = fparticlecand->getZprime(xBeam, yBeam, t_rPrime_P1);

                        //----------------------------------------------------------------------------------------------
                        // Particle 2 - FwDet Particle
                        HForwardCand* fwdetcand =
                            HCategoryManager::getObject(fwdetcand, fForwardCand, k);

                        // if(fwdetcand->getTofRec() <= 0) t_ToF_Rec = 0;
                        // else if (fwdetcand->getTofRec() > 0) t_ToF_Rec = 1;

                        t_Sector_P2 = t_Sector_P1;
                        t_mass_P2 = t_mass_P1;

                        t_r_start_P2 = fparticlecand->getRprime(xBeam, yBeam); // [mm]
                        t_z_start_P2 = fparticlecand->getZprime(xBeam, yBeam, t_r_start_P2); // [mm]

                        // Correction for track start position (Z_START)
                        fwdetcand->setStartXYZ(0.0, 0.0, t_z_start_P2); // We take the track start position from the reconstructed HADES track paired with FwDet track
                        fwdetcand->calcPoints(); // Recalculating ToF and Distance to account for correction
                        fwdetcand->calc4vectorProperties( proton_mass); // Calculating 4 vector properties assuming particles are protons

                        t_phi_P2 = fwdetcand->getPhi();     // [deg]
                        t_theta_P2 = fwdetcand->getTheta(); // [deg]
                        t_beta_P2 = fwdetcand->getBeta();

                        // Track extraction
                        HParticleTool::calcSegVector(fwdetcand->getZ(), fwdetcand->getR(), fwdetcand->Phi(), fwdetcand->Theta(), base_P2, dir_P2);
                        t_rPrime_P2 = t_rPrime_P1;
                        t_zPrime_P2 = t_zPrime_P2;

                        //----------------------------------------------------------------------------------------------
                        // Two Partices Variables
                        //t_phi_diff = TMath::Abs(t_phi_P1 - t_phi_P2); // [deg]
                        t_phi_diff = fmod( (t_phi_P1 + 360.0 - t_phi_P2) , 360.0); // [deg]
                        t_tan_theta_product = TMath::Tan(TMath::DegToRad() * t_theta_P1) *
                                              TMath::Tan(TMath::DegToRad() * t_theta_P2); // [ ]

                        //t_dist = TMath::Sqrt( pow( ((t_phi_diff - 180.0) / ( HF_phi_diff_sigma )), 2) + pow( ((t_tan_theta_product - inverse_gamma2) / ( HF_tan_theta_prod_sigma )), 2) );

                        if (t_mom_P1 -t_mom_theor_P1 >= -500.0 && t_mom_P1 -t_mom_theor_P1 <= 500.0)  check_mom = true;
                        if ( (t_theta_P1 >= 70.0 && t_theta_P1 <= 80.0) && (t_theta_P2 >= 3.5 && t_theta_P2 <= 6.0) ) check_theta = true;

                        //----------------------------------------------------------------------------------------------
                        // Event counting

                        has_HF = true;
                        num_HF_Entries++;
                        if (t_PT1 == 1) num_HF_Entries_PT1++;
                        if (t_PT2 == 1) num_HF_Entries_PT2++;
                        if (t_PT3 == 1) num_HF_Entries_PT3++;

                        //----------------------------------------------------------------------------------------------
                        // Histograms filling
                        h_HF_mass_P1->Fill(t_mass_P1);
                        h_HF_theta->Fill(t_theta_P1);
                        h_HF_theta->Fill(t_theta_P2);
                        h_HF_theta_sec[t_Sector_P1]->Fill(t_theta_P1);
                        h_HF_theta_sec[t_Sector_P1]->Fill(t_theta_P2);
                        h_HF_phi_diff->Fill(t_phi_diff);
                        h_HF_tan_theta_prod->Fill(t_tan_theta_product);
                        h_HF_tan_theta_prod_VS_phi_diff->Fill(t_phi_diff, t_tan_theta_product);
                        h_HF_phi_diff_VS_phi_P1->Fill(t_phi_diff, t_phi_P1);
                        h_HF_phi_diff_VS_phi_P2->Fill(t_phi_diff, t_phi_P2);
                        h_HH_and_HF_theta_P1_vs_theta_P2->Fill(t_theta_P1, t_theta_P2);
                        h_HF_phi_P1_vs_phi_P2->Fill(t_phi_P1, t_phi_P2);
                        h_HH_and_HF_mom_vs_theta_ALL->Fill(t_theta_P1, t_mom_P1);
                        h_HH_and_HF_mom_vs_theta[t_Sector_P1]->Fill(t_theta_P1, t_mom_P1);
                        h_HH_and_HF_delta_mom_vs_mom->Fill(t_mom_P1, t_mom_P1 - t_mom_theor_P1);
                        h_HH_and_HF_mom_vs_beta->Fill(t_beta_P1, t_mom_P1);

                        if (t_phi_diff >= cut_HF_phi_diff_min && t_phi_diff <= cut_HF_phi_diff_max && check_mom == true && check_theta == true)
                        {
                            h_HF_tan_theta_prod_cut->Fill(t_tan_theta_product);
                        }
                        if (t_tan_theta_product >= cut_HF_tan_theta_min && t_tan_theta_product <= cut_HF_tan_theta_max && check_mom == true && check_theta == true)
                        {
                            h_HF_phi_diff_cut->Fill(t_phi_diff);
                            h_HF_phi_diff_VS_phi_P1_cut->Fill(t_phi_diff, t_phi_P1);
                            h_HF_phi_diff_VS_phi_P2_cut->Fill(t_phi_diff, t_phi_P2);
                        }
                         if ((t_phi_diff >= cut_HF_phi_diff_min &&
                             t_phi_diff <= cut_HF_phi_diff_max) &&
                            (t_tan_theta_product >= cut_HF_tan_theta_min &&
                             t_tan_theta_product <= cut_HF_tan_theta_max))
                        {
                            h_HH_and_HF_delta_mom_vs_mom_cut->Fill(t_mom_P1, t_mom_P1 - t_mom_theor_P1);
                        }

                        // Background reference selection

                        if (((t_phi_diff >= bkg_low_min && t_phi_diff <= bkg_low_max) ||
                             (t_phi_diff >= bkg_high_min && t_phi_diff <= bkg_high_max)) &&
                            (t_tan_theta_product >= cut_HF_tan_theta_min &&
                             t_tan_theta_product <= cut_HF_tan_theta_max) && check_mom == true && check_theta == true)
                        //if ( t_dist > cut_BKG_dist_min && t_dist <= cut_BKG_dist_max)
                        {
                            // If it's the first background reference pair found in this event
                            if (has_HF_bkg_ref == false)
                            {
                                has_HF_bkg_ref = true;
                                bkg_phi_diff = t_phi_diff;
                                bkg_tan_theta_product = t_tan_theta_product;
                            }
                        }

                        // Elastic scattering selection

                        if ((t_phi_diff >= cut_HF_phi_diff_min &&
                             t_phi_diff <= cut_HF_phi_diff_max) &&
                            (t_tan_theta_product >= cut_HF_tan_theta_min &&
                             t_tan_theta_product <= cut_HF_tan_theta_max) && check_mom == true && check_theta == true)
                        //if ( t_dist <= cut_EL_dist )
                        {
                            num_HF_El_per_Event++;

                            // If it's the first elastic candidate found in this event
                            if (has_HF_elastic == false)
                            {
                                has_HF_elastic = true;
                                //----------------------------------------------------------------------------------------------
                                // Candidate particle 1 - HADES particle
                                el_Sector_P1 = t_Sector_P1;
                                el_mass_P1 = t_mass_P1;
                                el_z_start_P1 = t_z_start_P1;
                                el_r_start_P1 = t_r_start_P2;
                                el_phi_P1 = t_phi_P1;
                                el_theta_P1 = t_theta_P1;
                                el_mom_P1 = t_mom_P1;
                                el_mom_theor_P1 = t_mom_theor_P1;
                                el_beta_P1 = t_beta_P1;

                                el_base_P1 = base_P1;
                                el_dir_P1 = dir_P1;
                                el_zPrime_P1 = t_zPrime_P1;
                                el_rPrime_P1 = t_rPrime_P1;

                                //----------------------------------------------------------------------------------------------
                                // Candidate particle 2 - FwDet particle
                                el_Sector_P2 = t_Sector_P2;
                                el_mass_P2 = t_mass_P2;
                                el_z_start_P2 = t_z_start_P1;
                                el_r_start_P2 = t_r_start_P2;
                                el_phi_P2 = t_phi_P2;
                                el_theta_P2 = t_theta_P2;
                                el_mom_P2 = t_mom_P2;
                                el_beta_P2 = t_beta_P2;

                                el_base_P2 = base_P2;
                                el_dir_P2 = dir_P2;
                                el_zPrime_P2 = t_zPrime_P2;
                                el_rPrime_P2 = t_rPrime_P2;

                                el_phi_diff = t_phi_diff;
                                el_tan_theta_product = t_tan_theta_product;
                            }
                        }
                    }
                }
            }

            //----------------------------------------------------------------------------------------------
            // HADES - HADES case - evaluating candidate event
            h_HH_el_per_event_count->Fill(num_HH_El_per_Event);
            if (num_HH_El_per_Event >= 2) num_HH_Events_Mult_El_Pairs++;

            // Counting events with HH case
            if (has_HH)
            {
                num_Events_HH++;
                if (t_PT1 == 1) num_HH_Events_PT1++;
                if (t_PT2 == 1) num_HH_Events_PT2++;
                if (t_PT3 == 1) num_HH_Events_PT3++;

                // Filling HH background reference histograms
                if (has_HH_bkg_ref == true && has_HF_bkg_ref == false)
                {
                    h_HH_phi_diff_bkg->Fill(bkg_phi_diff);
                    h_HH_tan_theta_prod_bkg->Fill(bkg_tan_theta_product);
                    h_HH_tan_theta_prod_VS_phi_diff_bkg->Fill(bkg_phi_diff, bkg_tan_theta_product);
                    num_Events_HH_Bkg_Ref++;
                    num_Events_HH_Bkg_Ref_table[i/step_size]++;
                    num_Events_Total_Bkg_Ref++;
                }

                // Filling HH elastic histograms
                if (has_HH_elastic == true && has_HF_elastic == false)
                {
                    h_HH_mass_P1_el->Fill(el_mass_P1);
                    h_HH_mass_P2_el->Fill(el_mass_P2);
                    h_HH_theta_el->Fill(el_theta_P1);
                    h_HH_theta_el->Fill(el_theta_P2);
                    h_HH_theta_sec_el[el_Sector_P1]->Fill(el_theta_P1);
                    h_HH_theta_sec_el[el_Sector_P2]->Fill(el_theta_P2);
                    h_HH_phi_diff_el->Fill(el_phi_diff);
                    h_HH_tan_theta_prod_el->Fill(el_tan_theta_product);
                    h_HH_tan_theta_prod_VS_phi_diff_el->Fill(el_phi_diff, el_tan_theta_product);
                    h_HH_phi_diff_VS_phi_P1_el->Fill(t_phi_diff, t_phi_P1);
                    h_HH_phi_diff_VS_phi_P2_el->Fill(t_phi_diff, t_phi_P2);
                    h_HH_and_HF_theta_P1_vs_theta_P2_el->Fill(el_theta_P1, el_theta_P2);
                    h_HH_phi_P1_vs_phi_P2_el->Fill(el_phi_P1, el_phi_P2);
                    h_HH_and_HF_mom_vs_theta_ALL_el->Fill(el_theta_P1, el_mom_P1);
                    h_HH_and_HF_mom_vs_theta_ALL_el->Fill(el_theta_P2, el_mom_P2);
                    h_HH_and_HF_mom_vs_theta_el[el_Sector_P1]->Fill(el_theta_P1, el_mom_P1);
                    h_HH_and_HF_mom_vs_theta_el[el_Sector_P2]->Fill(el_theta_P2, el_mom_P2);
                    h_HH_and_HF_delta_mom_vs_mom_el->Fill(el_mom_P1, el_mom_P1 - el_mom_theor_P1);
                    h_HH_and_HF_delta_mom_vs_mom_el->Fill(el_mom_P2, el_mom_P2 - el_mom_theor_P2);
                    h_HH_and_HF_mom_vs_beta_el->Fill(el_beta_P1, el_mom_P1);
                    h_HH_and_HF_mom_vs_beta_el->Fill(el_beta_P2, el_mom_P2);

                    // STS multiplicity - HF elastic events
                    if(fStsRaw)
                    {
                        HStsRaw * stsstraw; // Initializing HStsRaw pointer

                        Int_t multiplicity_ALL = fStsRaw->getEntries(); // Get number of entries (number of signals registered by straws) in one event
                        Int_t multiplicity_STS1 = 0;
                        Int_t multiplicity_STS2 = 0;

                        for(Int_t i = 0; i < fStsRaw->getEntries(); i ++) // Loop over entries inside each event
                        {
                            stsstraw = (HStsRaw*)fStsRaw->getObject(i); // Get fStsRaw object

                            if(stsstraw) // Check if "stsstraw" object was accessed properly
                            {
                                // Initializing extracted variables
                                Char_t mod, lay, ud; // mod - Module, lay - Layer, ud - upper/lower (short) or long straw
                                Int_t straw; // Straw index

                                // Getting values of each variable in this entry
                                stsstraw->getAddress(mod, lay, straw, ud);

                                // Filling multiplicity for STS1 and STS2
                                if ( mod == 0 ) multiplicity_STS1 = multiplicity_STS1 + 1;
                                if ( mod == 1 ) multiplicity_STS2 = multiplicity_STS2 + 1;
                            }
                        }
                        // Filling multiplicity histograms
                        h_STS_multiplicity_ALL_HF_el->Fill(multiplicity_ALL);
                        h_STS_multiplicity_STS1_HF_el->Fill(multiplicity_STS1);
                        h_STS_multiplicity_STS2_HF_el->Fill(multiplicity_STS2);
                    }

                    // Calculating the point of closest approach between the two tracks
                    HGeomVector PCA;
                    PCA.setXYZ(-2000,-2000,-2000);
                    if(el_dir_P1 == el_dir_P2 ) // trach same direction
                    {
                        //cout<<"parallel"<<endl;
                    }
                    else
                    {
                        PCA = HParticleTool::calculatePointOfClosestApproach(el_base_P1, el_dir_P1, el_base_P2, el_dir_P2);
                        h_vertex_R_vs_Z_HH_el->Fill(el_zPrime_P1,TMath::Sqrt(PCA.getX()*PCA.getX() + PCA.getY()*PCA.getY() ));
                        h_vertex_Z_HH_el->Fill(el_zPrime_P1);
                        h_vertex_R_HH_el->Fill(TMath::Sqrt(PCA.getX()*PCA.getX() + PCA.getY()*PCA.getY() ));

                        h_vertex_R_vs_Z_H_el->Fill(el_z_start_P1,el_r_start_P1);
                        h_vertex_R_vs_Z_H_el->Fill(el_z_start_P2,el_r_start_P2);
                        h_vertex_Z_H_el->Fill(el_z_start_P1);
                        h_vertex_Z_H_el->Fill(el_z_start_P2);
                        h_vertex_R_H_el->Fill(el_r_start_P1);
                        h_vertex_R_H_el->Fill(el_r_start_P2);
                    }

                    num_Events_HH_Elastic++;
                    num_Events_HH_Elastic_table[i/step_size]++;
                    num_Events_Total_Elastic++;
                    if (t_PT1 == 1) num_HH_Events_Elastic_PT1++;
                    if (t_PT2 == 1) num_HH_Events_Elastic_PT2++;
                    if (t_PT3 == 1) num_HH_Events_Elastic_PT3++;
                }
            }

            //----------------------------------------------------------------------------------------------
            // HADES - FwDet case - evaluating candidate event
            h_HF_el_per_event_count->Fill(num_HF_El_per_Event);
            if (num_HF_El_per_Event >= 2) num_HF_Events_Mult_El_Pairs++;

            // Counting events with HF case
            if (has_HF)
            {
                num_Events_HF++;
                if (t_PT1 == 1) num_HF_Events_PT1++;
                if (t_PT2 == 1) num_HF_Events_PT2++;
                if (t_PT3 == 1) num_HF_Events_PT3++;

                // Filling HH background reference histograms
                if (has_HF_bkg_ref == true && has_HH_bkg_ref == false)
                {
                    h_HF_phi_diff_bkg->Fill(bkg_phi_diff);
                    h_HF_tan_theta_prod_bkg->Fill(bkg_tan_theta_product);
                    h_HF_tan_theta_prod_VS_phi_diff_bkg->Fill(bkg_phi_diff, bkg_tan_theta_product);
                    num_Events_HF_Bkg_Ref++;
                    num_Events_HF_Bkg_Ref_table[i/step_size]++;
                    num_Events_Total_Bkg_Ref++;
                }

                // Filling HF elastic histograms
                if (has_HF_elastic == true && has_HH_elastic == false)
                {
                    h_HF_mass_P1_el->Fill(el_mass_P1);
                    h_HF_theta_el->Fill(el_theta_P1);
                    h_HF_theta_el->Fill(el_theta_P2);
                    h_HF_theta_sec_el[el_Sector_P1]->Fill(el_theta_P1);
                    h_HF_theta_sec_el[el_Sector_P1]->Fill(el_theta_P2);
                    h_HF_phi_diff_el->Fill(el_phi_diff);
                    h_HF_tan_theta_prod_el->Fill(el_tan_theta_product);
                    h_HF_tan_theta_prod_VS_phi_diff_el->Fill(el_phi_diff, el_tan_theta_product);
                    h_HF_phi_diff_VS_phi_P1_el->Fill(t_phi_diff, t_phi_P1);
                    h_HF_phi_diff_VS_phi_P2_el->Fill(t_phi_diff, t_phi_P2);
                    h_HH_and_HF_theta_P1_vs_theta_P2_el->Fill(el_theta_P1, el_theta_P2);
                    h_HF_phi_P1_vs_phi_P2_el->Fill(el_phi_P1, el_phi_P2);
                    h_HH_and_HF_mom_vs_theta_ALL_el->Fill(el_theta_P1, el_mom_P1);
                    h_HH_and_HF_mom_vs_theta_el[el_Sector_P1]->Fill(el_theta_P1, el_mom_P1);
                    h_HH_and_HF_delta_mom_vs_mom_el->Fill(el_mom_P1, el_mom_P1 - el_mom_theor_P1);
                    h_HH_and_HF_mom_vs_beta_el->Fill(el_beta_P1, el_mom_P1);

                    // STS multiplicity - HF elastic events
                    if(fStsRaw)
                    {
                        HStsRaw * stsstraw; // Initializing HStsRaw pointer

                        Int_t multiplicity_ALL = fStsRaw->getEntries(); // Get number of entries (number of signals registered by straws) in one event
                        Int_t multiplicity_STS1 = 0;
                        Int_t multiplicity_STS2 = 0;

                        for(Int_t i = 0; i < fStsRaw->getEntries(); i ++) // Loop over entries inside each event
                        {
                            stsstraw = (HStsRaw*)fStsRaw->getObject(i); // Get fStsRaw object

                            if(stsstraw) // Check if "stsstraw" object was accessed properly
                            {
                                // Initializing extracted variables
                                Char_t mod, lay, ud; // mod - Module, lay - Layer, ud - upper/lower (short) or long straw
                                Int_t straw; // Straw index

                                // Getting values of each variable in this entry
                                stsstraw->getAddress(mod, lay, straw, ud);

                                // Filling multiplicity for STS1 and STS2
                                if ( mod == 0 ) multiplicity_STS1 = multiplicity_STS1 + 1;
                                if ( mod == 1 ) multiplicity_STS2 = multiplicity_STS2 + 1;
                            }
                        }
                        // Filling multiplicity histograms
                        h_STS_multiplicity_ALL_HH_el->Fill(multiplicity_ALL);
                        h_STS_multiplicity_STS1_HH_el->Fill(multiplicity_STS1);
                        h_STS_multiplicity_STS2_HH_el->Fill(multiplicity_STS2);
                    }

                    // Calculating the point of closest approach between the two tracks
                    HGeomVector PCA;
                    PCA.setXYZ(-2000,-2000,-2000);
                    if(el_dir_P1 == el_dir_P2 ) // trach same direction
                    {
                        //cout<<"parallel"<<endl;
                    }
                    else
                    {
                        PCA = HParticleTool::calculatePointOfClosestApproach(el_base_P1, el_dir_P1, el_base_P2, el_dir_P2);

                        h_vertex_R_vs_Z_HF_el->Fill(el_zPrime_P1,TMath::Sqrt(PCA.getX()*PCA.getX() + PCA.getY()*PCA.getY() ));
                        h_vertex_Z_HF_el->Fill(el_zPrime_P1);
                        h_vertex_R_HF_el->Fill(TMath::Sqrt(PCA.getX()*PCA.getX() + PCA.getY()*PCA.getY() ));

                        h_vertex_R_vs_Z_H_el->Fill(el_z_start_P1,el_r_start_P1);
                        h_vertex_Z_H_el->Fill(el_z_start_P1);
                        h_vertex_R_H_el->Fill(el_r_start_P1);
                    }

                    num_Events_HF_Elastic++;
                    num_Events_HF_Elastic_table[i/step_size]++;
                    num_Events_Total_Elastic++;
                    if (t_PT1 == 1) num_HF_Events_Elastic_PT1++;
                    if (t_PT2 == 1) num_HF_Events_Elastic_PT2++;
                    if (t_PT3 == 1) num_HF_Events_Elastic_PT3++;
                }
            }
        }
    } // end eventloop

    //----------------------------------------------------------------------------------------------
    // Counting elastic events
    //----------------------------------------------------------------------------------------------
    dir_Elastics->cd();
    // Elastic events number estimated as integral over peak area on Phi_diff_cut histogram minus
    // integral over two background areas on both sides of the peak, each 1/2 of peak width wide A
    // bit like sideband analysis

    //----------------------------------------------------------------------------------------------
    // HADES - HADES case - evaluating candidate event
    Float_t peak_width = cut_HH_phi_diff_max - cut_HH_phi_diff_min;
    Float_t integration_offset = 0.5 * peak_width;
    num_Events_HH_Elastic_Bkg_Corr = num_Events_HH_Elastic - num_Events_HH_Bkg_Ref;

    ForwardTools::Drawing::draw_write_and_save_picture(c_HH_phi_diff, h_HH_phi_diff, dir_Elastics, 180.0, format_pic_name(anapars.outpath, c_HH_phi_diff->GetName()));

    c_HH_phi_diff_cut->cd();    h_HH_phi_diff_cut->Draw();    h_HH_phi_diff_el->Draw("same");    h_HH_phi_diff_bkg->Draw("same");
    ForwardTools::Drawing::draw_vertical_line_color(gPad, h_HH_phi_diff_cut, 180.0, 2); // Blue
    c_HH_phi_diff_cut->Write();    h_HH_phi_diff_cut->Write();    h_HH_phi_diff_el->Write();    h_HH_phi_diff_bkg->Write();
    c_HH_phi_diff_cut->SaveAs(format_pic_name(anapars.outpath, c_HH_phi_diff_cut->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_HH_tan_theta_prod, h_HH_tan_theta_prod, dir_Elastics, inverse_gamma2, format_pic_name(anapars.outpath, c_HH_tan_theta_prod->GetName()));

    c_HH_tan_theta_prod_cut->cd();    h_HH_tan_theta_prod_cut->Draw();    h_HH_tan_theta_prod_el->Draw("same");    h_HH_tan_theta_prod_bkg->Draw("same");
    ForwardTools::Drawing::draw_vertical_line_color(gPad, h_HH_tan_theta_prod_cut, inverse_gamma2, 2); // Blue
    c_HH_tan_theta_prod_cut->Write();    h_HH_tan_theta_prod_cut->Write();    h_HH_tan_theta_prod_el->Write();    h_HH_tan_theta_prod_bkg->Write();
    c_HH_tan_theta_prod_cut->SaveAs(format_pic_name(anapars.outpath, c_HH_tan_theta_prod_cut->GetName()));

    //----------------------------------------------------------------------------------------------
    // HADES - FwDet case - evaluating candidate event
    peak_width = cut_HF_phi_diff_max - cut_HF_phi_diff_min;
    integration_offset = 0.5 * peak_width;
    num_Events_HF_Elastic_Bkg_Corr = num_Events_HF_Elastic - num_Events_HF_Bkg_Ref;

    ForwardTools::Drawing::draw_write_and_save_picture(c_HF_phi_diff, h_HF_phi_diff, dir_Elastics, 180.0, format_pic_name(anapars.outpath, c_HF_phi_diff->GetName()));

    c_HF_phi_diff_cut->cd();    h_HF_phi_diff_cut->Draw();    h_HF_phi_diff_el->Draw("same");    h_HF_phi_diff_bkg->Draw("same");
    ForwardTools::Drawing::draw_vertical_line_color(gPad, h_HF_phi_diff_cut, 180.0, 4); // Blue
    c_HF_phi_diff_cut->Write();    h_HF_phi_diff_cut->Write();    h_HF_phi_diff_el->Write();    h_HF_phi_diff_bkg->Write();
    c_HF_phi_diff_cut->SaveAs(format_pic_name(anapars.outpath, c_HF_phi_diff_cut->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_HF_tan_theta_prod, h_HF_tan_theta_prod, dir_Elastics, inverse_gamma2, format_pic_name(anapars.outpath, c_HF_tan_theta_prod->GetName()));

    c_HF_tan_theta_prod_cut->cd();    h_HF_tan_theta_prod_cut->Draw();    h_HF_tan_theta_prod_el->Draw("same");    h_HF_tan_theta_prod_bkg->Draw("same");
    ForwardTools::Drawing::draw_vertical_line_color(gPad, h_HF_tan_theta_prod_cut, inverse_gamma2, 2); // Blue
    c_HF_tan_theta_prod_cut->Write();    h_HF_tan_theta_prod_cut->Write();    h_HF_tan_theta_prod_el->Write();    h_HF_tan_theta_prod_bkg->Write();
    c_HF_tan_theta_prod_cut->SaveAs(format_pic_name(anapars.outpath, c_HF_tan_theta_prod_cut->GetName()));

    for (Int_t i = 0; i<num_steps; i++)
    {
        num_Events_HH_Elastic_Bkg_Corr_table[i] = num_Events_HH_Elastic_table[i] - num_Events_HH_Bkg_Ref_table[i];
        num_Events_HF_Elastic_Bkg_Corr_table[i] = num_Events_HF_Elastic_table[i] - num_Events_HF_Bkg_Ref_table[i];
    }

    dir_Elastics->cd();

    TGraph* g_HF_Elastics_Graph = new TGraph(num_steps,num_Events_table,num_Events_HF_Elastic_Bkg_Corr_table);
    TCanvas* c_HF_Elastics_Graph = new TCanvas("c_HF_Elastics_Graph", "c_HF_Elastics_Graph", 800, 800);
    g_HF_Elastics_Graph->SetMinimum(0);
    c_HF_Elastics_Graph->cd();    g_HF_Elastics_Graph->Draw("AC*");
    g_HF_Elastics_Graph->Write();   c_HF_Elastics_Graph->Write();

    TGraph* g_HH_Elastics_Graph = new TGraph(num_steps,num_Events_table,num_Events_HH_Elastic_Bkg_Corr_table);
    TCanvas* c_HH_Elastics_Graph = new TCanvas("c_HH_Elastics_Graph", "c_HH_Elastics_Graph", 800, 800);
    g_HH_Elastics_Graph->SetMinimum(0);
    c_HH_Elastics_Graph->cd();    g_HH_Elastics_Graph->Draw("AC*");
    g_HH_Elastics_Graph->Write();   c_HH_Elastics_Graph->Write();





    //----------------------------------------------------------------------------------------------
    // End of eventloop
    //----------------------------------------------------------------------------------------------

    timer.Stop();
    cout << "//---------------------------------------------------------------------------------\n";
    cout << "End of eventloop, T = " << timer.RealTime() << " s\n";
    cout << "//---------------------------------------------------------------------------------\n";

    // Mom vs theta theoretical function (for elastic scattering)
    TF1* f_mom_vs_theta_temp = new TF1(
        "f_mom_vs_theta",
        "[0]/( (cos((TMath::Pi()/180.0)*x))*( 1 + pow( tan((TMath::Pi()/180.0)*x) * [1] , 2 ) ) )",
        0, 90);
    f_mom_vs_theta_temp->SetParameter(0, beam_p01);
    f_mom_vs_theta_temp->SetParameter(1, beam_gamma_cm);

    // Theta1 vs theta2 for elastic proton scattering function
    TF1* f_theta1_vs_theta2_temp = new TF1(
        "f_theta1_vs_theta2", "(180.0/TMath::Pi())*atan(0.3088/tan((TMath::Pi()/180.0)*x))", 0, 90);

    //----------------------------------------------------------------------------------------------
    // Drawing, writing and saving (as pictures) histograms and canvases
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Drawing, writing and saving (as pictures) histograms and canvases ...\n";

    //----------------------------------------------------------------------------------------------
    // Mass
    dir_Mass->cd();

    ForwardTools::Drawing::draw_write_and_save_picture(c_HH_mass_P1, h_HH_mass_P1, dir_Mass, -9999.9, format_pic_name(anapars.outpath, c_HH_mass_P1->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_HH_mass_P2, h_HH_mass_P2, dir_Mass, -9999.9, format_pic_name(anapars.outpath, c_HH_mass_P2->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_HH_mass_P1_el, h_HH_mass_P1_el, dir_Mass, -9999.9, format_pic_name(anapars.outpath, c_HH_mass_P1_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_HH_mass_P2_el, h_HH_mass_P2_el, dir_Mass, -9999.9, format_pic_name(anapars.outpath, c_HH_mass_P2_el->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_HF_mass_P1, h_HF_mass_P1, dir_Mass, -9999.9, format_pic_name(anapars.outpath, c_HF_mass_P1->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_HF_mass_P1_el, h_HF_mass_P1_el, dir_Mass, -9999.9, format_pic_name(anapars.outpath, c_HF_mass_P1_el->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_H_mass, h_H_mass, dir_Mass, -9999.9, format_pic_name(anapars.outpath, c_H_mass->GetName()));

    //----------------------------------------------------------------------------------------------
    // STS Multiplicity
    dir_STS_Mult->cd();
    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_ALL, h_STS_multiplicity_ALL, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_ALL->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_STS1, h_STS_multiplicity_STS1, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_STS1->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_STS2, h_STS_multiplicity_STS2, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_STS2->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_ALL_H, h_STS_multiplicity_ALL_H, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_ALL_H->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_STS1_H, h_STS_multiplicity_STS1_H, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_STS1_H->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_STS2_H, h_STS_multiplicity_STS2_H, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_STS2_H->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_ALL_HF_el, h_STS_multiplicity_ALL_HF_el, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_ALL_HF_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_STS1_HF_el, h_STS_multiplicity_STS1_HF_el, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_STS1_HF_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_STS2_HF_el, h_STS_multiplicity_STS2_HF_el, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_STS2_HF_el->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_ALL_HH_el, h_STS_multiplicity_ALL_HH_el, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_ALL_HH_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_STS1_HH_el, h_STS_multiplicity_STS1_HH_el, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_STS1_HH_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_STS_multiplicity_STS2_HH_el, h_STS_multiplicity_STS2_HH_el, dir_STS_Mult, -9999.9, format_pic_name(anapars.outpath, c_STS_multiplicity_STS2_HH_el->GetName()));

    //----------------------------------------------------------------------------------------------
    // Vertex reconstruction
    dir_Vertex_Rec->cd();

    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_X_Pair, h_vertex_X_Pair, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_X_Pair->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_Y_Pair, h_vertex_Y_Pair, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_Y_Pair->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_Z_Pair, h_vertex_Z_Pair, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_Z_Pair->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_Pair, h_vertex_R_Pair, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_Pair->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_vs_Z_Plane, h_vertex_R_vs_Z_Plane, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_vs_Z_Plane->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_Z_Plane, h_vertex_Z_Plane, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_Z_Plane->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_Plane, h_vertex_R_Plane, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_Plane->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_vs_Z, h_vertex_R_vs_Z, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_vs_Z->GetName()));
    //ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_Z, h_vertex_Z, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_Z->GetName()));
    //ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R, h_vertex_R, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R->GetName()));

    c_vertex_Z->cd();
    h_vertex_Z->SetLineColor(kRed); h_vertex_Z->Draw();
    h_vertex_Z_HF_el->SetLineColor(kGreen); h_vertex_Z_HF_el->Draw("same");
    //ForwardTools::Drawing::draw_vertical_line(gPad, h_vertex_Z, -300.0);
    //ForwardTools::Drawing::draw_vertical_line(gPad, h_vertex_Z, 100.0);
    c_vertex_Z->Write();   h_vertex_Z->Write(); c_vertex_Z->SaveAs(format_pic_name(anapars.outpath, c_vertex_Z->GetName()));

    c_vertex_R->cd();
    h_vertex_R->SetLineColor(kRed); h_vertex_R->Draw();
    h_vertex_R_HF_el->SetLineColor(kGreen); h_vertex_R_HF_el->Draw("same");
    //ForwardTools::Drawing::draw_vertical_line(gPad, h_vertex_R, 20.0);
    c_vertex_R->Write();   h_vertex_R->Write(); c_vertex_R->SaveAs(format_pic_name(anapars.outpath, c_vertex_R->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_vs_Z_HF_el, h_vertex_R_vs_Z_HF_el, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_vs_Z_HF_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_Z_HF_el, h_vertex_Z_HF_el, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_Z_HF_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_HF_el, h_vertex_R_HF_el, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_HF_el->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_vs_Z_HH_el, h_vertex_R_vs_Z_HH_el, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_vs_Z_HH_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_Z_HH_el, h_vertex_Z_HH_el, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_Z_HH_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_HH_el, h_vertex_R_HH_el, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_HH_el->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_vs_Z_H, h_vertex_R_vs_Z_H, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_vs_Z_H->GetName()));
    //ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_Z_H, h_vertex_Z_H, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_Z_H->GetName()));
    //ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_H, h_vertex_R_H, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_H->GetName()));

    c_vertex_Z_H->cd();
    h_vertex_Z_H->SetLineColor(kRed); h_vertex_Z_H->Draw();
    h_vertex_Z_HF_el->SetLineColor(kGreen); h_vertex_Z_HF_el->Draw("same");
    c_vertex_Z_H->Write();   h_vertex_Z_H->Write(); c_vertex_Z_H->SaveAs(format_pic_name(anapars.outpath, c_vertex_Z_H->GetName()));

    c_vertex_R_H->cd();
    h_vertex_R_H->SetLineColor(kRed); h_vertex_R_H->Draw();
    h_vertex_R_HF_el->SetLineColor(kGreen); h_vertex_R_HF_el->Draw("same");
    c_vertex_R_H->Write();   h_vertex_R_H->Write(); c_vertex_R_H->SaveAs(format_pic_name(anapars.outpath, c_vertex_R_H->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_vs_Z_H_el, h_vertex_R_vs_Z_H_el, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_vs_Z_H_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_Z_H_el, h_vertex_Z_H_el, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_Z_H_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_vertex_R_H_el, h_vertex_R_H_el, dir_Vertex_Rec, -9999.9, format_pic_name(anapars.outpath, c_vertex_R_H_el->GetName()));

    //----------------------------------------------------------------------------------------------
    // Elastics reconstruction
    dir_Elastics->cd();
    ForwardTools::Drawing::draw_write_and_save_picture(c_H_theta, h_H_theta, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_H_theta->GetName()));

    // 2D hist drawing function test
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_H_mom_vs_theta, h_H_mom_vs_theta, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_H_mom_vs_theta->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_HH_el_per_event_count, h_HH_el_per_event_count, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_el_per_event_count->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_HF_el_per_event_count, h_HF_el_per_event_count, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_el_per_event_count->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture(c_HF_theta, h_HF_theta, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_theta->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_HF_theta_el, h_HF_theta_el, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_theta_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_HH_theta, h_HH_theta, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_theta->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture(c_HH_theta_el, h_HH_theta_el, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_theta_el->GetName()));

    dir_Elastics->cd();

    // Mom vs theta - 2D histograms (all sectors)
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_and_HF_mom_vs_theta_ALL, h_HH_and_HF_mom_vs_theta_ALL, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_and_HF_mom_vs_theta_ALL->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_and_HF_mom_vs_theta_ALL_el, h_HH_and_HF_mom_vs_theta_ALL_el, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_and_HF_mom_vs_theta_ALL_el->GetName()));

    for (Int_t i = 0; i < 6; ++i)
    {
        // Theta
        ForwardTools::Drawing::draw_write_and_save_picture(c_HF_theta_sec[i], h_HF_theta_sec[i], dir_Sectors, -9999.9, format_pic_name(anapars.outpath, c_HF_theta_sec[i]->GetName()));
        ForwardTools::Drawing::draw_write_and_save_picture(c_HH_theta_sec[i], h_HH_theta_sec[i], dir_Sectors, -9999.9, format_pic_name(anapars.outpath, c_HH_theta_sec[i]->GetName()));

        ForwardTools::Drawing::draw_write_and_save_picture(c_HF_theta_sec_el[i], h_HF_theta_sec_el[i], dir_Sectors, -9999.9, format_pic_name(anapars.outpath, c_HF_theta_sec_el[i]->GetName()));
        ForwardTools::Drawing::draw_write_and_save_picture(c_HH_theta_sec_el[i], h_HH_theta_sec_el[i], dir_Sectors, -9999.9, format_pic_name(anapars.outpath, c_HH_theta_sec_el[i]->GetName()));

        // Mom vs theta
        ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_and_HF_mom_vs_theta[i], h_HH_and_HF_mom_vs_theta[i], dir_Sectors, -9999.9, format_pic_name(anapars.outpath, c_HH_and_HF_mom_vs_theta[i]->GetName()));
        ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_and_HF_mom_vs_theta_el[i], h_HH_and_HF_mom_vs_theta_el[i], dir_Sectors, -9999.9,format_pic_name(anapars.outpath, c_HH_and_HF_mom_vs_theta_el[i]->GetName()));
    }

    // Delta Mom vs Mom - 2D histograms (all sectors)
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_and_HF_delta_mom_vs_mom, h_HH_and_HF_delta_mom_vs_mom, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_and_HF_delta_mom_vs_mom->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_and_HF_delta_mom_vs_mom_cut, h_HH_and_HF_delta_mom_vs_mom_cut, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_and_HF_delta_mom_vs_mom_cut->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_and_HF_delta_mom_vs_mom_el, h_HH_and_HF_delta_mom_vs_mom_el, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_and_HF_delta_mom_vs_mom_el->GetName()));

    // Mom vs Beta - 2D histograms (all sectors)
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_and_HF_mom_vs_beta, h_HH_and_HF_mom_vs_beta, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_and_HF_mom_vs_beta->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_and_HF_mom_vs_beta_el, h_HH_and_HF_mom_vs_beta_el, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_and_HF_mom_vs_beta_el->GetName()));

    //------------------------------------------------------------------------------------------------------
    // Phi diff VS Phi
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_phi_diff_VS_phi_P1, h_HH_phi_diff_VS_phi_P1, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_phi_diff_VS_phi_P1->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_phi_diff_VS_phi_P2, h_HH_phi_diff_VS_phi_P2, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_phi_diff_VS_phi_P2->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HF_phi_diff_VS_phi_P1, h_HF_phi_diff_VS_phi_P1, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_phi_diff_VS_phi_P1->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HF_phi_diff_VS_phi_P2, h_HF_phi_diff_VS_phi_P2, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_phi_diff_VS_phi_P2->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_phi_diff_VS_phi_P1_cut, h_HH_phi_diff_VS_phi_P1_cut, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_phi_diff_VS_phi_P1_cut->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_phi_diff_VS_phi_P2_cut, h_HH_phi_diff_VS_phi_P2_cut, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_phi_diff_VS_phi_P2_cut->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HF_phi_diff_VS_phi_P1_cut, h_HF_phi_diff_VS_phi_P1_cut, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_phi_diff_VS_phi_P1_cut->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HF_phi_diff_VS_phi_P2_cut, h_HF_phi_diff_VS_phi_P2_cut, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_phi_diff_VS_phi_P2_cut->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_phi_diff_VS_phi_P1_el, h_HH_phi_diff_VS_phi_P1_el, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_phi_diff_VS_phi_P1_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_phi_diff_VS_phi_P2_el, h_HH_phi_diff_VS_phi_P2_el, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_phi_diff_VS_phi_P2_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HF_phi_diff_VS_phi_P1_el, h_HF_phi_diff_VS_phi_P1_el, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_phi_diff_VS_phi_P1_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HF_phi_diff_VS_phi_P2_el, h_HF_phi_diff_VS_phi_P2_el, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_phi_diff_VS_phi_P2_el->GetName()));

    //------------------------------------------------------------------------------------------------------
    // Tan theta product VS Phi diff
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_tan_theta_prod_VS_phi_diff, h_HH_tan_theta_prod_VS_phi_diff, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_tan_theta_prod_VS_phi_diff->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HF_tan_theta_prod_VS_phi_diff, h_HF_tan_theta_prod_VS_phi_diff, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_tan_theta_prod_VS_phi_diff->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_tan_theta_prod_VS_phi_diff_el, h_HH_tan_theta_prod_VS_phi_diff_el, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_tan_theta_prod_VS_phi_diff_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HF_tan_theta_prod_VS_phi_diff_el, h_HF_tan_theta_prod_VS_phi_diff_el, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_tan_theta_prod_VS_phi_diff_el->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HH_tan_theta_prod_VS_phi_diff_bkg, h_HH_tan_theta_prod_VS_phi_diff_bkg, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HH_tan_theta_prod_VS_phi_diff_bkg->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(c_HF_tan_theta_prod_VS_phi_diff_bkg, h_HF_tan_theta_prod_VS_phi_diff_bkg, dir_Elastics, -9999.9, format_pic_name(anapars.outpath, c_HF_tan_theta_prod_VS_phi_diff_bkg->GetName()));

    //------------------------------------------------------------------------------------------------------
    // Theta P1 vs Theta P2
    ForwardTools::Drawing::draw_write_and_save_picture_2D(
        c_HH_and_HF_theta_P1_vs_theta_P2, h_HH_and_HF_theta_P1_vs_theta_P2, dir_Elastics, -9999.9,
        format_pic_name(anapars.outpath, c_HH_and_HF_theta_P1_vs_theta_P2->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(
        c_HH_and_HF_theta_P1_vs_theta_P2_el, h_HH_and_HF_theta_P1_vs_theta_P2_el, dir_Elastics, -9999.9,
        format_pic_name(anapars.outpath, c_HH_and_HF_theta_P1_vs_theta_P2_el->GetName()));

    //------------------------------------------------------------------------------------------------------
    // Phi P1 vs Phi P2
    ForwardTools::Drawing::draw_write_and_save_picture_2D(
        c_HH_phi_P1_vs_phi_P2, h_HH_phi_P1_vs_phi_P2, dir_Elastics, -9999.9,
        format_pic_name(anapars.outpath, c_HH_phi_P1_vs_phi_P2->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(
        c_HF_phi_P1_vs_phi_P2, h_HF_phi_P1_vs_phi_P2, dir_Elastics, -9999.9,
        format_pic_name(anapars.outpath, c_HF_phi_P1_vs_phi_P2->GetName()));

    ForwardTools::Drawing::draw_write_and_save_picture_2D(
        c_HH_phi_P1_vs_phi_P2_el, h_HH_phi_P1_vs_phi_P2_el, dir_Elastics, -9999.9,
        format_pic_name(anapars.outpath, c_HH_phi_P1_vs_phi_P2_el->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(
        c_HF_phi_P1_vs_phi_P2_el, h_HF_phi_P1_vs_phi_P2_el, dir_Elastics, -9999.9,
        format_pic_name(anapars.outpath, c_HF_phi_P1_vs_phi_P2_el->GetName()));

    ForwardTools::Drawing::draw_and_write(c_track_start_Z, h_track_start_Z, dir_Elastics, -9999.9);
    ForwardTools::Drawing::draw_write_and_save_picture_2D(
        c_track_start_Y_vs_X, h_track_start_Y_vs_X, dir_Elastics, -9999.9,
        format_pic_name(anapars.outpath, c_track_start_Y_vs_X->GetName()));
    ForwardTools::Drawing::draw_write_and_save_picture_2D(
        c_track_start_R_vs_Z, h_track_start_R_vs_Z, dir_Elastics, -9999.9,
        format_pic_name(anapars.outpath, c_track_start_R_vs_Z->GetName()));

    //------------------------------------------------------------------------------------------------------
    // Writing calibration parameters to file and statistics to screen

    cout << "Writing calibration parameters to file and statistics to screen ...\n";

    cout << "//---------------------------------------------------------------------------------\n";
    cout << "Events counts:\n";

    cout << "Number of elastic ALL events = " << num_Events_Total_Elastic << " out of "
         << num_Events_Total << " events\n";
    cout << "Number of elastic HH events = " << num_Events_HH_Elastic << " out of " << num_Events_HH
         << " HH events\n";
    cout << "Number of elastic HF events = " << num_Events_HF_Elastic << " out of " << num_Events_HF
         << " HF events\n";
    cout << "Number of elastic HH events minus background = " << num_Events_HH_Elastic_Bkg_Corr
         << " out of " << num_Events_HH << " HH events\n";
    cout << "Number of elastic HF events minus background = " << num_Events_HF_Elastic_Bkg_Corr
         << " out of " << num_Events_HF << " HF events\n";

    cout << "Number of elastic HH events (PT1) = " << num_HH_Events_Elastic_PT1 << " out of "
         << num_HH_Events_PT1 << " HH events (PT1); out of " << num_Events_PT1 << " events (PT1)\n";
    cout << "Number of elastic HH events (PT2) = " << num_HH_Events_Elastic_PT2 << " out of "
         << num_HH_Events_PT2 << " HH events (PT2); out of " << num_Events_PT2 << " events (PT2)\n";
    cout << "Number of elastic HH events (PT3) = " << num_HH_Events_Elastic_PT3 << " out of "
         << num_HH_Events_PT3 << " HH events (PT3); out of " << num_Events_PT3 << " events (PT3)\n";

    cout << "Number of elastic HF events (PT1) = " << num_HF_Events_Elastic_PT1 << " out of "
         << num_HF_Events_PT1 << " HF events (PT1); out of " << num_Events_PT1 << " events (PT1)\n";
    cout << "Number of elastic HF events (PT2) = " << num_HF_Events_Elastic_PT2 << " out of "
         << num_HF_Events_PT2 << " HF events (PT2); out of " << num_Events_PT2 << " events (PT2)\n";
    cout << "Number of elastic HF events (PT3) = " << num_HF_Events_Elastic_PT3 << " out of "
         << num_HF_Events_PT3 << " HF events (PT3); out of " << num_Events_PT3 << " events (PT3)\n";

    cout << "Number of ALL events with multiple elastic pairs = "
         << num_HH_Events_Mult_El_Pairs + num_HF_Events_Mult_El_Pairs << " out of "
         << num_Events_Total << " events\n";
    cout << "Number of HH events with multiple elastic pairs = " << num_HH_Events_Mult_El_Pairs
         << " out of " << num_Events_HH << " HH events\n";
    cout << "Number of HF events with multiple elastic pairs = " << num_HF_Events_Mult_El_Pairs
         << " out of " << num_Events_HF << " HF events\n";

    cout << "//---------------------------------------------------------------------------------\n";

    cout << "Number of elastic ALL entries = " << num_HH_Elastic_Entries + num_HF_Elastic_Entries
         << " out of " << num_HH_Entries + num_HF_Entries << " entries\n";
    cout << "Number of elastic HH entries = " << num_HH_Elastic_Entries << "; out of "
         << num_HH_Entries << " HH entries\n";
    cout << "Number of elastic HF entries = " << num_HF_Elastic_Entries << "; out of "
         << num_HF_Entries << " HF entries\n";
    cout << "Number of elastic HH entries (PT1) = " << num_HH_Elastic_Entries_PT1 << "; out of "
         << num_HH_Entries_PT1 << " HH entries (PT1)\n";
    cout << "Number of elastic HH entries (PT2) = " << num_HH_Elastic_Entries_PT2 << "; out of "
         << num_HH_Entries_PT2 << " HH entries (PT2)\n";
    cout << "Number of elastic HH entries (PT3) = " << num_HH_Elastic_Entries_PT3 << "; out of "
         << num_HH_Entries_PT3 << " HH entries (PT3)\n";

    cout << "Number of elastic HF entries (PT1) = " << num_HF_Elastic_Entries_PT1 << "; out of "
         << num_HF_Entries_PT1 << " HF entries (PT1)\n";
    cout << "Number of elastic HF entries (PT2) = " << num_HF_Elastic_Entries_PT2 << "; out of "
         << num_HF_Entries_PT2 << " HF entries (PT2)\n";
    cout << "Number of elastic HF entries (PT3) = " << num_HF_Elastic_Entries_PT3 << "; out of "
         << num_HF_Entries_PT3 << " HF entries (PT3)\n";

    timer.Stop();
    cout << "//---------------------------------------------------------------------------------\n";
    cout << "End of program:\n";
    timer.Print();
    cout << "//---------------------------------------------------------------------------------\n";

    cout << "TF1 *f_theta_vs_theta = new TF1(\"f_theta_vs_theta\",\"(180.0/TMath::Pi())*atan("
         << inverse_gamma2 << "/tan((TMath::Pi()/180.0)*x))\",0,90);\n";
    cout << "TF1 *f_mom_vs_theta = new TF1(\"f_mom_vs_theta\",\"" << beam_p01
         << "/( (cos((TMath::Pi()/180.0)*x))*( 1 + pow( tan((TMath::Pi()/180.0)*x) * "
         << beam_gamma_cm << " , 2 ) ) )\",0,90);\n";

    // Write line to .txt file with "start_time stop_time num_Events_Total nElastic luminosity"
    cout << std::setw(16) << "Timestamp" << std::setw(10) << "Duration" << std::setw(16)
         << "#Events" << std::setw(16) << "#PT2Events" << std::setw(16) << "#Elast. HH"
         << std::setw(16) << "Elastic HF" << std::setw(16) << "Elastic Sum\n";

    std::stringstream ss;
    ss << std::setw(16) << timestamp << std::setw(10) << stop_t - start_t << std::setw(16)
       << num_Events_Total << std::setw(16) << num_Events_PT2 << std::setw(16)
       << num_Events_HH_Elastic_Bkg_Corr << std::setw(16) << num_Events_HF_Elastic_Bkg_Corr
       << std::setw(16) << num_Events_HH_Elastic_Bkg_Corr + num_Events_HF_Elastic_Bkg_Corr
       << std::endl;

    TString lumi_out = anapars.outpath + anapars.outfile;
    if (lumi_out.EndsWith(".root")) lumi_out.ReplaceAll(".root", ".lumi");
    cout << ss.str();
    ofstream ofs(lumi_out.Data());
    ofs << ss.str();

    return 0;
}

//##############################################################################################
// Main fRPC calibration function
//##############################################################################################

int main(int argc, char** argv)
{
    TROOT Analysis("Analysis", "compiled analysis macro");
    gStyle->SetOptStat(0);

    int c;

    AnaParameters anapars;
    anapars.start = 0;
    anapars.events = -1;
    anapars.show_fakes = 0;
    anapars.cal_frpc_stage1 = 0;
    anapars.cal_frpc_stage2 = 0;
    anapars.cal_frpc_stage3 = 0;
    anapars.cal_frpc_stage4 = 0;
    anapars.cal_frpc_stage5 = 0;
    anapars.alt_ToF = 0;
    anapars.sim = 0;
    anapars.outpath = "";
    anapars.outfile = "output.root";
    anapars.paramfile = "feb22_dst_params.txt";

    while (1)
    {
        static struct option long_options[] = {
            /* These options set a flag. */
            {"verbose", no_argument, &anapars.verbose, 1},
            {"brief", no_argument, &anapars.verbose, 0},
            {"sim", no_argument, &anapars.sim, 1},
            {"exp", no_argument, &anapars.sim, 0},
            {"cal-frpc-stage1", no_argument, &anapars.cal_frpc_stage1, 1},
            {"cal-frpc-stage2", no_argument, &anapars.cal_frpc_stage2, 1},
            {"cal-frpc-stage3", no_argument, &anapars.cal_frpc_stage3, 1},
            {"cal-frpc-stage4", no_argument, &anapars.cal_frpc_stage4, 1},
            {"cal-frpc-stage5", no_argument, &anapars.cal_frpc_stage5, 1},
            {"pt1", no_argument, &anapars.pt1, 1},
            {"pt2", no_argument, &anapars.pt2, 1},
            {"pt3", no_argument, &anapars.pt3, 1},
            {"no_t0", no_argument, &anapars.no_t0, 1},
            {"alt_ToF", no_argument, &anapars.alt_ToF, 1},

            /* These options don’t set a flag.
             *              We distinguish them by their indices. */
            {"output", required_argument, 0, 'o'},
            {"dir", required_argument, 0, 'd'},
            {"param", required_argument, 0, 'p'},
            {"events", required_argument, 0, 'e'},
            {"start", required_argument, 0, 's'},
            {"label", required_argument, 0, 'l'},
            {0, 0, 0, 0}};
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long(argc, argv, "o:d:p:e:s:l:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1) break;

        switch (c)
        {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0) break;
                printf("option %s", long_options[option_index].name);
                if (optarg) printf(" with arg %s", optarg);
                printf("\n");
                break;

            case 'o':
                anapars.outfile = optarg;
                break;

            case 'd':
                anapars.outpath = optarg;
                break;

            case 'p':
                anapars.paramfile = optarg;
                break;

            case 'e':
                anapars.events = atol(optarg);
                break;

            case 's':
                anapars.start = atol(optarg);
                break;

            case 'l':
                anapars.label = optarg;
                break;

            case '?':
                /* getopt_long already printed an error message. */
                break;

            default:
                abort();
        }
    }

    /* Instead of reporting ‘--verbose’
     *      and ‘--brief’ as they are encountered,
     *           we report the final status resulting from them. */
    if (anapars.verbose) puts("verbose flag is set");

    HLoop* loop = new HLoop(kTRUE);

    /* Print any remaining command line arguments (not options). */
    if (optind < argc)
    {
        Bool_t ret;
        // 		printf ("non-option ARGV-elements: ");
        while (optind < argc)
        {
            TString infile = argv[optind++];
            if (infile.Contains(","))
                ret = loop->addMultFiles(infile);
            else if (infile.Contains(".root"))
                ret = loop->addFiles(infile);
            else
                ret = loop->addFilesList(infile);

            if (!ret)
            {
                std::cerr << "READBACK: ERROR : cannot find inputfiles : " << infile.Data() << endl;
                std::exit(EXIT_FAILURE);
            }
        }
    }

    if (!anapars.pt1 && !anapars.pt2 && !anapars.pt3)
    {
        anapars.pt1 = 1;
        anapars.pt2 = 1;
        anapars.pt3 = 1;
    }
    printf("PT config %d %d %d\n", anapars.pt1, anapars.pt2, anapars.pt3);
    if (!anapars.outpath.IsNull()) anapars.outpath += "/";

    fwdet_tests(loop, anapars);

    exit(0);
}
