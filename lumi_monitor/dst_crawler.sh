#!/bin/bash

# directories and files
WD="$(readlink -e $(dirname "$0"))"     # working dir
DD="${DD:-$WD/../data}"                 # datadir

[ -e $WD/user.sh ] && source $WD/user.sh

DSTOUT=${DSTOUT:-$DD/out}
DSTSTAMPDIR=${DSTSTAMPDIR:-$DD/stamps_dst}
HLDMASK=${HLDMASK:-be*}
LUMIOUT=${LUMIOUT:-$DD/lumi}

DSTLISTFILE=${DSTLISTFILE:-$DD/dstlist.txt}
LUMISTATUSFILE=${LUMISTATUSFILE:-$DD/lumistatus.txt}

COOLDOWNTIME=600

MARKEXT=.found

# tool to convert hades filename into normal time
HADESTIMECONV=$WD/hadestime

# root checking tool
LUMITOOL=$WD/lumi_monitor

[ ! -d $DD ] && mkdir -p $DD
[ ! -d $DSTSTAMPDIR ] && mkdir -p $DSTSTAMPDIR
[ ! -d $LUMIOUT ] && mkdir -p $LUMIOUT

allfiles=$(find ${DSTOUT} -name "${HLDMASK}.hld_*root" -print)
for f in $allfiles; do
    ct=$(date +%s)          # current time, update each loop
    mt=$(stat -c %Y $f)     # file modification time
                            # while still being copied
                            # mt will be equal to current
                            # we need to be sure that file was fully copied
                            # ask for 2 minutes difference from ct
    tdiff=$(($ct - $mt))

    if [ $tdiff -gt $COOLDOWNTIME ]; then
        fn=$(basename $f .root) #get just filename
        if [ ! -f $DSTSTAMPDIR/${fn}$MARKEXT ]; then
            date > $DSTSTAMPDIR/${fn}$MARKEXT    # store date of processing
            if ! grep -q $f $DSTLISTFILE; then
                echo $f >> $DSTLISTFILE         # add dst file to list only if not add before
            fi

            # run lumi monitor tool here
            echo $WD/submit_lumi_job.sh $WD $DD $f $LUMIOUT $LUMITOOL $LUMISTATUSFILE
            exec $WD/submit_lumi_job.sh $WD $DD $f $LUMIOUT $LUMITOOL $LUMISTATUSFILE &
        fi
    fi
done
