#!/usr/bin/gnuplot

#set style func linespoints

set title "Total luminosity Feb22"

TimeIntervalInSeconds = 3600*1
ct = time(0)

set timefmt "%Y-%m-%dT%H:%M:%SZ%z"
set xdata time

set yrange [ 0 : ]
set xdata time

set xrange [ : ct+TimeIntervalInSeconds ]
#set xrange [ "2021-12-21T16:00:00+01:00":"2022-03-05T08:00:00+01:00" ]
set ylabel "Total luminosity [μb]"
set format x "%d/%m\n%H:%M"
set grid
set key left

trend(x) = a*(x-c) + b
a = 1.0
b = 1e-8
c = 1e10

trend_last(x) = al*(x-cl) + bl
al = 1.0
bl = 1e-8
cl = 1e10

fit trend(x) 'test.txt' u 1:3 via a, b, c
fit trend_last(x) '<(tail -n 5 test.txt)' u 1:3 via al, bl, cl

set timestamp "%d/%m/%y %H:%M"

plot 'test.txt' u 1:3 w lp pt 4 lw 2 t "All files", \
    trend(x) lw 2 dt 3 t "Total trend"
replot '<(tail -n 5 test.txt)' u 1:3 w lp pt 5 t "Last 5 files", \
    trend_last(x) lw 2 dt 2 t "Last 5 files trend"

pause -1
