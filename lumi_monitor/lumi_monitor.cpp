#include "forward_tools.h"
#include "ft_config.h"

#include <hades.h>
#include <hcategory.h>
#include <hcategorymanager.h>
#include <hforwardcand.h>
#include <hforwardcandsim.h>
#include <hgeantkine.h>
#include <hiterator.h>
#include <hlinearcategory.h>
#include <hloop.h>
#include <hparticlecand.h>
#include <hparticlecandsim.h>
#include <hparticledef.h>
#include <hparticlegeant.h>
#include <hparticlegeantdecay.h>
#include <hparticlegeantevent.h>
#include <hrpccal.h>
#include <hrpccalpar.h>
#include <hrpccluster.h>
#include <hrpcdigipar.h>
#include <hrpcgeompar.h>
#include <hrpchit.h>
#include <hrpcraw.h>
#include <hstartdef.h>
#include <htboxchan.h>
#include <htofhit.h>
#include <htofraw.h>
#include <htool.h>
#include <rpcdef.h>
#include <tofdef.h>

#include <TFile.h>
#include <TROOT.h>
#include <TTree.h>

#include <regex>
#include <sstream>

#include <getopt.h>

using namespace Particle;
using namespace ConfigBeamtime;

using LumiTiple = std::tuple<long, long, long, double, double, double, double>;
using LumiMap = std::map<long, LumiTiple>;

//----------------------------------------------------------------------------------------------
// Main function
//----------------------------------------------------------------------------------------------

struct tm decodeHadesTimeAndDate(UInt_t date, UInt_t time, bool verbose = false)
{
    time_t rawtime;
    std::time(&rawtime);
    struct tm t = *localtime(&rawtime);

    t.tm_mday = (date & 0xFF);
    t.tm_mon = 1 + ((date >> 8) & 0xFF) - 1;
    t.tm_year = ((date >> 16) & 0xFF);

    t.tm_hour = ((time >> 16) & 0xFF) + 1;
    t.tm_min = (time >> 8) & 0xFF;
    t.tm_sec = time & 0xFF;

    if (verbose)
    {
        printf("Date:  %#x   Time:  %#x\n", date, time);
        printf("Decoded  %4d-%02d-%02d %02d:%02d:%02d\n", 1900 + t.tm_year, t.tm_mon + 1, t.tm_mday,
               t.tm_hour, t.tm_min, t.tm_sec);
    }

    return t;
}

// Int_t lumiMonitor(TString lumiPar, TString infileList, Int_t nEvents=-1){
Int_t lumiMonitor(HLoop* loop, std::string outfile)
{
    // Int_t lumiMonitor(HLoop* loop, const AnaParameters& anapars){

    Int_t nEvents = -1;

    TStopwatch timer;
    timer.Start();

    double deg2rad = TMath::DegToRad();

    // -----------------------------------------------------------------------
    // define output file and some histograms
    // -----------------------------------------------------------------------
    // set ouput file

    // TFile *said_lab = new
    // TFile("/lustre/hades/user/jrieger/pp_elastic/lumiMonitor/pp_elastic_4200_SAID_lab.root",
    // "read"); TH1F *h_said_cm = (TH1F *)said_lab->Get("hangle_cm");

    TH1F* h_lab_rec_elcut = new TH1F("h_lab_rec_elcut", "#theta_{p}", 120, 0, 90);
    h_lab_rec_elcut->SetXTitle("#theta in deg");
    h_lab_rec_elcut->GetXaxis()->SetTitleSize(0.05);
    h_lab_rec_elcut->GetXaxis()->SetLabelSize(0.05);
    h_lab_rec_elcut->GetYaxis()->SetTitleSize(0.05);
    h_lab_rec_elcut->GetYaxis()->SetLabelSize(0.05);
    TH1F* h_lab_rec_elcut_trig = (TH1F*)h_lab_rec_elcut->Clone("h_lab_rec_elcut_trig");

    TH1F* h_lab_rec_elcut_hf = (TH1F*)h_lab_rec_elcut->Clone("h_lab_rec_elcut_hf");
    h_lab_rec_elcut_hf->GetXaxis()->Set(50, 2, 7);

    TH1F* h_lab_rec_elcut_hf_trig_data =
        (TH1F*)h_lab_rec_elcut_hf->Clone("h_lab_rec_elcut_hf_trig_data");

    TH1F* h_lab_rec_elcut_hh = (TH1F*)h_lab_rec_elcut->Clone("h_lab_rec_elcut_hh");

    TH1F* h_lab_rec_elcut_hh_trig_data =
        (TH1F*)h_lab_rec_elcut_hh->Clone("h_lab_rec_elcut_hh_trig_data");

    TH1F* h_lab_gen = (TH1F*)h_lab_rec_elcut->Clone("h_lab_gen");
    TH1F* h_lab_gen_hf = (TH1F*)h_lab_gen->Clone("h_lab_gen_hf");
    h_lab_gen_hf->SetBins(50, 2, 7);

    TFile* efficiencies = new TFile(eff_file, "read");
    if (!efficiencies) abort();

    TH1F* h_eff_hh = (TH1F*)efficiencies->Get("h_eff_hh");
    TH1F* h_eff_hf = (TH1F*)efficiencies->Get("h_eff_hf");
    TH1F* h_eff_hh_trig = (TH1F*)efficiencies->Get("h_eff_hh_trig");
    TH1F* h_eff_hf_trig = (TH1F*)efficiencies->Get("h_eff_hf_trig");

    // output file
    std::ofstream outtxt;

    // -----------------------------------------------------------------------

    // select categories here
    if (!loop->setInput("-*,+HParticleCand,+HForwardCand,+HTBoxChan"))
    {
        cout << "READBACK: ERROR : cannot read input !" << endl;
        exit(1);
    } // read all categories

    loop->printCategories();
    loop->printChain();

    HCategory* catParticle = loop->getCategory("HParticleCand");
    if (!catParticle)
    {
        std::cout << "No particleCat in input!" << std::endl;
        exit(1);
    }
    HCategory* catFwParticle = loop->getCategory("HForwardCand");
    if (!catFwParticle)
    {
        std::cout << "No FWparticleCat in input!" << std::endl;
        exit(1);
    }

    HCategory* fCatTBoxChan = HCategoryManager::getCategory(catTBoxChan, kTRUE, "catTBoxChan");
    if (!fCatTBoxChan)
    {
        cout << "No fCatTBoxChan!" << endl;
        abort();
    }
    HIterator* iter = (HIterator*)fCatTBoxChan->MakeIterator("native");

    // Variable definitions
    Int_t pp_elastic_counter_hf_trig_data = 0;
    Int_t pp_elastic_counter_hh_trig_data = 0;

    Int_t entries = loop->getEntries();
    if (nEvents > entries || nEvents <= 0) nEvents = entries;

    cout << "events: " << nEvents << endl;

    Double_t mp = 938.2720813;
    TLorentzVector ppSystem(0, 0, 5051.8794, 2 * mp + 4500);

    std::string timestamp;

    regex str_expr(".*/?[a-z]{2}([0-9]{11})[0-9]{2}.+");

    time_t start_t = 0;
    time_t stop_t = 0;

    ULong64_t total_protons_start = 0;
    ULong64_t total_protons_veto = 0;
    ULong64_t time_in_spill = 0.0f;

    // start of the event loop -- data events counting
    // nEvents = 10000;
    for (Int_t i = 1; i < nEvents; i++)
    {
        // cout<<"Loop started "<<endl;
        //----------break if last event is reached-------------
        if (loop->nextEvent(i) <= 0)
        {
            cout << " end recieved " << endl;
            break;
        } // last event reached
        HTool::printProgress(i, nEvents, 1, "Analysing evt# :");

        auto header = gHades->getCurrentEvent()->getHeader();
        Int_t eventid = header->getId();

        // scalers
        iter->Reset();
        HTBoxChan* tbox = nullptr;
        if (eventid == 14)
            while ((tbox = (HTBoxChan*)iter->Next()) != 0)
            {
                Int_t chan = 0;
                UInt_t scaler1 = 0;
                tbox->getScalerData(chan, scaler1);

                if (chan == 0) time_in_spill += scaler1;
                if (chan == 85) total_protons_start += scaler1;
                if (chan == 86) total_protons_veto += scaler1;
            }
        // end of scalers calculations

        if (i == 1)
        {
            auto t = decodeHadesTimeAndDate(header->getDate(), header->getTime(), true);
            start_t = std::mktime(&t);
        }
        else
        {
            auto t = decodeHadesTimeAndDate(header->getDate(), header->getTime());
            auto _stop_t = std::mktime(&t);
            if (_stop_t > stop_t)
                stop_t = _stop_t;
        }

        TString new_file;
        auto r = loop->isNewFile(new_file);
        if (r)
        {
            auto last = new_file.Last('/');
            if (last != kNPOS) {}
            std::smatch capture;
            string nf = new_file.Data();
            if (regex_match(nf, capture, str_expr))
            {
                char buf[255];

                struct tm tm;
                memset(&tm, 0, sizeof(tm));

                strptime(capture[1].str().c_str(), "%y%j%H%M%S", &tm);
                strftime(buf, sizeof(buf), "%s", &tm);
                if (!timestamp.length()) { timestamp = buf; }
                else if (timestamp != buf)
                {
                    std::cerr << "Cannot mix different timestamp files\n";
                    abort();
                }
            }
        }

        // select only PT2 trigger
        Int_t TBit = (Int_t)header->getTBit();
        if ((TBit & 4096) != 4096) { continue; }

        auto has_hh_elastic = ForwardTools::Elastics::check_elastics_hh(
            catParticle, cut_HH_phi_diff_min, cut_HH_phi_diff_max, cut_HH_tan_theta_min,
            cut_HH_tan_theta_max);

        auto has_hf_elastic = ForwardTools::Elastics::check_elastics_hf(
            catParticle, catFwParticle, cut_HF_phi_diff_min, cut_HF_phi_diff_max,
            cut_HF_tan_theta_min, cut_HF_tan_theta_max);

        if (has_hh_elastic)
        {
            pp_elastic_counter_hh_trig_data++;

            // Particle 1 - HADES particle
            HParticleCand* cand1 = HCategoryManager::getObject(cand1, catParticle, 0);
            // Particle 2 - HADES particle
            HParticleCand* cand2 = HCategoryManager::getObject(cand2, catParticle, 1);

            h_lab_rec_elcut_hh_trig_data->Fill(cand1->getTheta());
            h_lab_rec_elcut_hh_trig_data->Fill(cand2->getTheta());
        }

        if (has_hf_elastic)
        {
            pp_elastic_counter_hf_trig_data++;

            // Particle 1 - HADES particle
            HParticleCand* cand1 = HCategoryManager::getObject(cand1, catParticle, 0);
            // Particle 2 - FwDet Particle
            HForwardCand* cand2 = HCategoryManager::getObject(cand2, catFwParticle, 0);
            Double_t t_z_start_P2 = cand2->getZ(); // [mm]
            // Correction for track start position (Z_START)
            cand2->setStartXYZ(0.0, 0.0,
                               t_z_start_P2); // We take the track start position from the
                                              // reconstructed HADES track paired with FwDet track
            cand2->calcPoints(); // Recalculating ToF and Distance to account for correction
            cand2->calc4vectorProperties(
                proton_mass); // Calculating 4 vector properties assuming particles are protons

            h_lab_rec_elcut_hf_trig_data->Fill(cand2->getTheta());
        }

    } // end of event loop

    // h_lab_rec_elcut->Draw();
    // h_lab_rec_elcut->Divide(h_lumiPar);
    // h_lab_rec_elcut->Divide(h_eff);
    h_lab_rec_elcut_hf_trig_data->Divide(h_eff_hf_trig);
    h_lab_rec_elcut_hh_trig_data->Divide(h_eff_hh_trig);

    Double_t nel_hf_trig =
        h_lab_rec_elcut_hf_trig_data->Integral(h_eff_hf->FindBin(3), h_eff_hf->FindBin(5.5));
    Double_t nel_hh_trig =
        h_lab_rec_elcut_hh_trig_data->Integral(h_eff_hh->FindBin(19), h_eff_hh->FindBin(44));

    Double_t sigma_hf = 5.11004; // integrated from SAID data
    Double_t sigma_hh = 0.0829282;

    Double_t lumi_hf_trig = nel_hf_trig / sigma_hf;
    Double_t lumi_hh_trig = nel_hh_trig / sigma_hh;

    cout << "2p in Hades, trigger: " << pp_elastic_counter_hh_trig_data << endl;
    cout << "1p in Hades, 1 in FW, trigger: " << pp_elastic_counter_hf_trig_data << endl;
    cout << "N HF, trigger: " << nel_hf_trig << endl;
    cout << "N HH, trigger: " << nel_hh_trig << endl;
    cout << "Lumi HF, trigger: " << lumi_hf_trig << endl;
    cout << "Lumi HH, trigger: " << lumi_hh_trig << endl;

    // Write line to .txt file with "start_time stop_time nEvents nElastic luminosity"
    cout << std::setw(16) << "Timestamp" << std::setw(10) << "Duration" << std::setw(16)
         << "TimeInSpill" << std::setw(16) << "Scalers START" << std::setw(16) << "Scalers VETO"
         << std::setw(10) << "#Events" << std::setw(10) << "#Elast." << std::setw(16) << "Lumi. HH"
         << std::setw(16) << "Lumi. HF" << std::setw(16) << "Lumi. Avg" << std::setw(16)
         << "Lumi. Sum" << std::endl;

    std::stringstream ss;
    ss << std::setw(16) << timestamp << std::setw(10) << stop_t - start_t << std::setw(16)
       << time_in_spill * 5e-9 << std::setw(16) << total_protons_start << std::setw(16)
       << total_protons_veto << std::setw(10) << nEvents << std::setw(10)
       << pp_elastic_counter_hh_trig_data + pp_elastic_counter_hf_trig_data << std::setw(16)
       << lumi_hh_trig << std::setw(16) << lumi_hf_trig << std::setw(16)
       << (lumi_hf_trig + lumi_hh_trig) / 2. << std::setw(16) << lumi_hf_trig + lumi_hh_trig
       << std::endl;

    cout << ss.str();
    ofstream ofs(outfile);
    ofs << ss.str();

    TFile* file = TFile::Open((outfile + ".root").c_str(), "RECREATE");
    file->cd();
    h_lab_rec_elcut_hh_trig_data->Write();
    h_lab_rec_elcut_hf_trig_data->Write();
    file->Write();
    file->Close();

    return 0;
}

int main(int argc, char** argv)
{
    TROOT LumiMonitor("LumiMonitor", "compiled lumiMonitor macro");

    if (argc < 2)
    {
        cout << "usage : " << argv[0] << " inputFile [ inputFile ... ]  [ -o outputFile ]" << endl;
        return 0;
    }

    std::string outfile = "out.txt";

    while (1)
    {
        static struct option long_options[] = {/* These options set a flag. */
                                               {"output", required_argument, 0, 'o'},
                                               {0, 0, 0, 0}};
        /* getopt_long stores the option index here. */
        int option_index = 0;

        char c = getopt_long(argc, argv, "o:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1) break;

        switch (c)
        {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0) break;
                printf("option %s", long_options[option_index].name);
                if (optarg) printf(" with arg %s", optarg);
                printf("\n");
                break;

            case 'o':
                outfile = optarg;
                break;

            default:
                abort();
        }
    }

    HLoop* loop = new HLoop(kTRUE);

    if (optind < argc)
    {
        while (optind < argc)
        {
            TString infile = argv[optind++];
            Bool_t ret;
            if (infile.Contains(","))
                ret = loop->addMultFiles(infile);
            else if (infile.Contains(".root"))
                ret = loop->addFiles(infile);
            else
                ret = loop->addFilesList(infile);

            if (!ret)
            {
                std::cerr << "READBACK: ERROR : cannot find inputfiles : " << infile.Data() << endl;
                std::exit(EXIT_FAILURE);
            }
        }
    }

    return lumiMonitor(loop, outfile); // Loop
}
