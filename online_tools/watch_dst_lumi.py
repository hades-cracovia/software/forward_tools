#!/usr/bin/env python3

import argparse
import glob
import pyinotify
import random
#import functools

from online_helpers import *

# global variables
LUMI_WATCH_PID_FILE="~/.dst_lumi_watcher.pid"
LUMI_WATCH_LOG_FILE="~/.dst_lumi_watcher.log"


WD=os.path.dirname(os.path.realpath(__file__))
config = parse_shell_config(make_abs('config.sh', WD))


ABS_DATA_DIR=make_abs(config.DATA_DIR, WD)
ABS_DST_DIR=make_abs(config.DST_DIR, ABS_DATA_DIR)
ABS_LUMI_DIR=make_abs(config.LUMI_DIR, ABS_DATA_DIR)

#full_name = make_abs(config.DST_MASK, ABS_DST_DIR)
#re_full_name = wildcard_to_regex(full_name, decorate=True)
#p_full_name = re.compile(re_full_name)

ABS_HLD_LIST_FILE=make_abs(config.HLD_LIST_FILE, ABS_DATA_DIR)
ABS_LUMI_DST_LIST=make_abs(config.LUMI_DST_LIST, ABS_DATA_DIR)
ABS_LUMI_SUBMIT_SCRIPT=make_abs(config.LUMI_SUBMIT_SCRIPT, WD)
ABS_LUMI_SUBMIT_LIST=make_abs(config.LUMI_SUBMIT_LIST, ABS_DATA_DIR)
ABS_LUMI_STATUS_LIST=make_abs(config.LUMI_STATUS_LIST, ABS_LUMI_DIR)

class EventHandler(pyinotify.ProcessEvent):
    """
    Capture events and process. Add file to the the list file
    on IN_CLOSE_WRITE event.
    """
    #def process_IN_MODIFY(self, event):
        #print("Modifying", event.pathname)
    def process_IN_CLOSE_WRITE(self, event):
        if p_full_name.match(event.pathname):
            add_file_to_list(event.pathname, ABS_HLD_LIST_FILE)


def on_loop(notifier):
    pass


# TODO make also for other critical files, as function
dirname = os.path.dirname(ABS_LUMI_DST_LIST)
try:
    os.mkdir(dirname)
except FileExistsError:
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--daemon', help='daemon mode', action='store_true')
    parser.add_argument('-a', '--all', help='list and send all files', action='store_true')
    parser.add_argument('-l', '--list', help='list and send new files', action='store_true')
    parser.add_argument('-r', '--resend', help='resend invalid or modified files', action='store_true')
    parser.add_argument('-c', '--check', help='check files', action='store_true')
    #parser.add_argument('-s', '--submit', help='submit pending files', action='store_true')

    args, opts = parser.parse_known_args()

    if args.all:
        dst_list = get_list_from_file(ABS_LUMI_DST_LIST) # current hld list content
        days_list, day_files = get_days_from_list(dst_list) # current list of days

        selected_days = filter_days(opts, days_list) # take days from list or from cmdline

        if len(selected_days):
            for sd in selected_days:
                submit_list = day_files[sd]
                random.shuffle(submit_list)
                prefix='.lumi_all_'
                tmp_files = make_temp_lists(submit_list, dir=ABS_DATA_DIR, prefix=prefix, suffix='.txt')
                for f in tmp_files:
                    print(f"Created new list for submitting: {f}")
                    submit_file([ABS_LUMI_SUBMIT_SCRIPT, f])
        else:
            print("No files to submit")

    elif args.check or args.resend:
        """
        Check selected files. First read all hld files and find all possible days.
        Run check for given or all days.
        """
        files_list = get_list_from_file(ABS_HLD_LIST_FILE) # current hld list content
        days_list, day_files = get_days_from_list(files_list) # current list of days

        selected_days = filter_days(opts, days_list) # take days from list or from cmdline

        if len(selected_days):
            files_to_check = []
            for sd in selected_days:

                day_status_file = ABS_LUMI_STATUS_LIST.replace("DAY", sd)
                current_list = get_status_list_from_file(day_status_file)
                rerun_list = get_list_of_rerun_needed(current_list)

                files_to_check = files_to_check + day_files[sd]
                print(f"Check day {sd}")

            random.shuffle(files_to_check)
            prefix='.lumi_check_' if args.check else '.lumi_resend_'
            tmp_files = make_temp_lists(files_to_check, dir=ABS_DATA_DIR, prefix=prefix, suffix='.txt')
            for f in tmp_files:
                print(f"Created new list for submission: {f}")
                submit_file([ABS_LUMI_SUBMIT_SCRIPT, f])
        else:
            print("No files to submit")

    elif args.list:
        files_list = get_list_from_file(ABS_LUMI_DST_LIST) # current list content
        new_files_list = [] # keeps new files

        dst_list = get_list_from_file(ABS_HLD_LIST_FILE) # current hld list content
        days_list, day_files = get_days_from_list(files_list) # current list of days

        selected_days = filter_days(opts, days_list) # take days from list or from cmdline

        if len(selected_days):
            for sd in selected_days:
                full_name = make_abs(config.DST_MASK, f"{ABS_DST_DIR}/{sd}/")
                re_full_name = wildcard_to_regex(full_name, decorate=True)
                p_full_name = re.compile(re_full_name)

                r = glob.glob(full_name)
                add_file_to_list(r, files_list, new_files_list)

            print(f"Update list of all files: {ABS_LUMI_DST_LIST}")
            write_list(ABS_LUMI_DST_LIST, new_files_list)

            if len(new_files_list):
                tmp_files = make_temp_lists(new_files_list, dir=ABS_DATA_DIR, prefix='.lumi_tmp_', suffix='.txt')
                for f in tmp_files:
                    print(f"Created new list for submission: {f}")
                    submit_file([ABS_LUMI_SUBMIT_SCRIPT, f])
        else:
            print("No new files to submit")

    elif args.resend:
        current_list = get_status_list_from_file(ABS_LUMI_STATUS_LIST)
        rerun_list = get_list_of_rerun_needed(current_list)

        if len(rerun_list):
            tmp_files = make_temp_lists(rerun_list, dir=ABS_DATA_DIR, prefix='.lumi_tmp_', suffix='.txt')
            for f in tmp_files:
                print(f"Created new list for submission: {f}")
                submit_file([ABS_LUMI_SUBMIT_SCRIPT, f])
        else:
            print("No new files to submit")

    else:
        wm = pyinotify.WatchManager()
        handler = EventHandler()
        notifier = pyinotify.Notifier(wm, handler)
        wm.add_watch(ABS_DST_DIR,
                    pyinotify.ALL_EVENTS,
                    rec=True)

        try:
            notifier.loop(daemonize=args.daemon,
                        callback=on_loop,
                        pid_file=make_abs(LUMI_WATCH_PID_FILE, WD),
                        stdout=make_abs(LUMI_WATCH_LOG_FILE, WD))
        except (pyinotify.NotifierError, err):
            sys.stdout.write(err)
