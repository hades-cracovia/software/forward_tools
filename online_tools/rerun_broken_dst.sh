#!/bin/bash

FORCE_RECHECK=0
PRETEND=
TIMEOUT=
while getopts fpt options; do
        case $options in
                f) FORCE_RECHECK=1;;
                p) PRETEND=echo;;
                t) TIMEOUT="timeout 10s";;
        esac
done
shift $((OPTIND-1))

# This script should call farm job to run task
# input params
# $1 - hld file

WD=$(dirname $(readlink -e ${BASH_SOURCE[0]}))

function make_abs()
{
    local __returnval=$3
    if [[ "$1" = /* ]]; then
        local myresult=$1
    else
        local myresult=$(readlink -e $2/$1)
    fi

    eval $__returnval="'$myresult'"
}

[ -f $WD/config.sh ] && . $WD/config.sh || exit 1

# data dir must be defined
[[ -n $DATA_DIR ]] || exit 1
make_abs "$DATA_DIR" "$WD" ABS_DATA_DIR
mkdir $ABS_DATA_DIR -p

# batch script must exists
make_abs "$DST_SUBMIT_SCRIPT" "$WD" ABS_DST_SUBMIT_SCRIPT
[[ -f $ABS_DST_SUBMIT_SCRIPT ]] || exit 2

# if variable is set, the file must exists
make_abs "$DST_CHECK_SCRIPT" "$WD" ABS_DST_CHECK_SCRIPT
[[ -z $DST_CHECK_SCRIPT ]] || [[ -f $ABS_DST_CHECK_SCRIPT ]] || exit 3

# status file must be set if the check script is set
[[ -z $HLD_CHECK_SCRIPT ]] || [[ -n $DST_STATUS_LIST ]] || exit 4
ABS_DST_STATUS_LIST=$ABS_DATA_DIR/$DST_STATUS_LIST
mkdir $(dirname $ABS_DST_STATUS_LIST) -p

REGEX="^(.*\/[a-z]{2}[0-9]{13}\.hld).*$"

echo "Checking in ${ABS_DST_STATUS_LIST}"

while IFS= read -r line; do
    stringarray=($line)

    dst=${stringarray[0]}
    status=${stringarray[1]}
    source=${stringarray[2]}

    echo -ne "Check: $dst => $status of $source\r"

    if [[ -z $source ]]; then
        if [[ $dst =~ $REGEX ]];
        then
            source=$(readlink -e $HLD_DIR/*/$(basename ${BASH_REMATCH[1]}))
        else
            echo "File name could not be recovered from $dst"
            exit 10
        fi
    fi

    if [[ $FORCE_RECHECK -eq 1 ]]; then
        oldstatus=$status
        echo -ne "Recheck: $dst with $oldstatus\r"
        #$TIMEOUT
        $ABS_DST_CHECK_SCRIPT $dst 2>/dev/null > /dev/null
        status=$?
        #echo "  => $status"

        if grep -q "^$dst" $ABS_DST_STATUS_LIST; then
            $PRETEND sed -e "s|$dst .* $source|$dst $status $source|" -i'' $ABS_DST_STATUS_LIST
#             $PRETEND sed -e "s|$dst .\(\ .*\)\?|$dst $status $source|" -i $ABS_DST_STATUS_LIST
        else
            $PRETEND echo "$dst $status $source" >> $ABS_DST_STATUS_LIST
        fi
        #echo $oldstatus $status
        if [[ $oldstatus != $status ]]; then
            echo -e "\n  ==> update $ABS_DST_STATUS_LIST status $?"
        fi

    else
        if [[ "$status" -ne "0" ]]; then
            echo "Submit: $ABS_DST_SUBMIT_SCRIPT -f $source"
            $PRETEND $ABS_DST_SUBMIT_SCRIPT -f ${source} || echo Error Status: $?
        fi
    fi

done < $ABS_DST_STATUS_LIST
