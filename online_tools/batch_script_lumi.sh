#!/bin/bash

date

if [[ -n ${SLURM_ARRAY_TASK_ID} ]]; then
    echo "ARRAY"
    file=$(sed -n $((${SLURM_ARRAY_TASK_ID}))p ${pattern})
else
    echo "NO ARRAY"
    file=$pattern
fi

. /lustre/hades/user/rlalik/hades/profile_feb22.sh


filebn=$(basename $file .hld)
file=$(readlink -e $file)
day=${filebn:4:3}

ldir=$(readlink -e $ldir)
ddir=$ldir/$day/

statusfile=${sfile/DAY/$day}

echo file=$file
echo ddir=$ddir
echo lumitool=$lumitool
echo statusfile=$statusfile

mkdir -p $ddir

ofile=$ddir/${filebn}.lumi


function update_with_lock {
    args=( "$@" )
    while ! flock -n -x $1 "${args[@]:1}"; do
        t=$(($RANDOM % 100))
        echo Retrying flock for $1 in $t
        sleep $t
    done
}



# mark that it is being to be run, may be killed by slurm
res=999
if grep -q $ofile $statusfile; then
#     update_with_lock $statusfile
    sed -e "s|$ofile .* .*|$ofile $res $file|" -i'' $statusfile
else
#     update_with_lock $statusfile
    echo "$ofile $res $file" >> $statusfile
fi

time $lumitool $file -o $ofile
res=$?

if grep -q $ofile $statusfile; then
#     update_with_lock $statusfile
    sed -e "s|$ofile .* .*|$ofile $res $file|" -i'' $statusfile
else
#     update_with_lock $statusfile
    echo "$ofile $res $file" >> $statusfile
fi
