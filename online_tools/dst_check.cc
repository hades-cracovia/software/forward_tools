#include <TEnv.h>
#include <TFile.h>

int main(int argc, char** argv)
{
    gEnv->SetValue("TFile.Recover", 0);

    if (argc < 2) return -1;

    TFile file(argv[1], "READ");

    if (file.IsZombie())
    {
        // something very wrong, cannot use this file, exit
        return 1;
    }

    if (file.TestBit(TFile::kRecovered))
    {
        // the Recover procedure has been run when opening the file
        // and the Recover was successful
        return 2;
    }
    else if (file.GetNkeys() == 0)
    {
        return 3;
    }
    else
    {
        // all OK
        return 0;
    }

    return 0;
}
