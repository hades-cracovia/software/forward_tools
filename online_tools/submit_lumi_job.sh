#!/bin/bash

FORCE_SUBMIT=0
PRETEND=
while getopts fp options; do
        case $options in
                f) FORCE_SUBMIT=1;;
                p) PRETEND=echo;;
        esac
done
shift $((OPTIND-1))

# This script should call farm job to run task
# input params
# $1 - hld file

WD=$(dirname $(readlink -e ${BASH_SOURCE[0]}))

function make_abs()
{
    local __returnval=$3
    if [[ "$1" = /* ]]; then
        local myresult=$1
    else
        local myresult=$(readlink -e $2/$1)
    fi

    eval $__returnval="'$myresult'"
}

[ -f $WD/config.sh ] && . $WD/config.sh || exit 1

# data dir must be defined
[[ -n $DATA_DIR ]] || exit 1
make_abs "$DATA_DIR" "$WD" ABS_DATA_DIR
mkdir $ABS_DATA_DIR -p

# batch script must exists
make_abs "$LUMI_BATCH_SCRIPT" "$WD" ABS_LUMI_BATCH_SCRIPT
[[ -f $ABS_LUMI_BATCH_SCRIPT ]] || exit 2

# if variable is set, the file must exists
make_abs "$LUMI_TOOL" "$WD" ABS_LUMI_TOOL
[[ -n $ABS_LUMI_TOOL ]] || exit 3

# lumi dir must be defined
[[ -n $LUMI_DIR ]] || exit 6
ABS_LUMI_DIR=$ABS_DATA_DIR/$LUMI_DIR
mkdir $ABS_LUMI_DIR -p

# submit file, containing directory must exists
ABS_LUMI_SUBMIT_LIST=$ABS_DATA_DIR/$LUMI_SUBMIT_LIST
mkdir $(dirname $ABS_LUMI_SUBMIT_LIST) -p
[ ! -e $ABS_LUMI_SUBMIT_LIST ] && touch $ABS_LUMI_SUBMIT_LIST

ABS_LUMI_STATUS_LIST=$ABS_LUMI_DIR/$LUMI_STATUS_LIST
mkdir $(dirname $ABS_LUMI_STATUS_LIST) -p

is_array=
array_string=

# if this is list, handle it, otherwise check if file was submitted
if [[ $1 == *.txt ]]; then
    is_array=1
    array_string="--array=1-$(cat $1 | wc -l)"
else
    grep -q "^$1" $ABS_LUMI_SUBMIT_LIST
    is_submitted=$?
fi

# execute it if either:
# - list is pass
# - file was not subbmited yet
# - or we enforce resubmission with -f option

if [[ $is_array -eq 1 || ! $is_submitted -eq 1 || $FORCE_SUBMIT -eq 1 ]]; then
    mkdir -p $ABS_DATA_DIR/slurm_log
    sbatch -o $ABS_DATA_DIR/slurm_log/$LUMI_LOG_PREFIX-%j.log \
        ${array_string} \
        --time=${LUMI_SUBMIT_TIME} --mem-per-cpu=100mb -p main -J L$(basename $1 .root) \
        --export="pattern=$1,events=$DST_EVENTS,lumitool=$ABS_LUMI_TOOL,ldir=$ABS_LUMI_DIR,sfile=$ABS_LUMI_STATUS_LIST" \
        -- $WD/wrap.sh $ABS_LUMI_BATCH_SCRIPT \
    && \
        flock -x $ABS_LUMI_SUBMIT_LIST echo "$1" >> $ABS_LUMI_SUBMIT_LIST
        # add dst file to list only if not add before
fi
