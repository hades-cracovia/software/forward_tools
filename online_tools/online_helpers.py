import fcntl
import os
import re
import subprocess
import sys
import tempfile

def make_abs(path, wd):
    if not os.path.isabs(path):
        return wd+'/'+path
    else:
        return path


def wildcard_to_regex(pattern, decorate=False):
    re_metachars='\\.^$+{}[]|()' # '*' and '?' are removed from the list
    s = pattern
    for c in re_metachars:
        s = s.replace(c, f'\{c}')
    s = s.replace('*', '.+').replace('?', '.')
    if decorate:
        return f'^{s}$'
    else:
        return s


def parse_shell_config(fn, verbose=True):
    """
    Read the config.sh file and import variables to global space.
    """

    class config:
        def __init__(self, dictionary):
            """Constructor"""
            for key in dictionary:
                setattr(self, key, dictionary[key])

    cfg = {}

    p = re.compile('^([A-Z_]+)\=(.*)$')
    with open(fn) as f:
        if verbose:
            print(f"* Read {fn}")
        for l in f.readlines():
            l = l.strip()
            if len(l) == 0 or l.startswith('#'):
                continue

            g = p.match(l.partition('#')[0].rstrip()).groups()
            if len(g) == 2:
                k = g[0]
                v = g[1].strip('"')
                if v == 'True':
                    v = True
                elif v == 'False':
                    v = False
                cfg[k] = os.path.expanduser(v)
                if verbose:
                    print(f" - Override {k}={cfg[k]}")

    return config(cfg)


def submit_file(args, verbose=False):
    args = [str(a) if not isinstance(a, str) else a for a in args if a is not None]
    if verbose:
        print(' '.join(args))
    subprocess.call(args)


def get_list_from_file(list_file, split=False):
    """
    Load list of files from file. If split is True, the split line into parts.
    """

    with open(list_file, "a+") as f:
        f.seek(0)
        while True:
            try:
                fcntl.flock(f.fileno(), fcntl.LOCK_EX)
                break
            except (IOError, OSError):
                pass
        return [x.strip().split() if split else x.strip() for x in f.readlines()]


def get_days_from_list(files_list):
    """
    From list of (hld) files, get all different yeardayoftheyear numbers,
    and files assigned to this days.
    """
    days=[]
    day_files = {}

    p = re.compile('^.*[a-z]{2}[0-9]{2}([0-9]{3})[0-9]{8}\..*$')
    for f in files_list:
        g = p.match(f).groups()
        if len(g) == 1:
            day = g[0]
            days.append(day)
            if day not in day_files:
                day_files[day] = []
            day_files[day].append(f)
    return set(days), day_files


def get_status_list_from_file(list_file):
    """ Split lines for status file """

    return get_list_from_file(list_file, split=True)


def filter_days(opts, days_list):
    selected_days = opts if len(opts) else days_list # take days from list or from cmdline

    filtered = []

    if len(selected_days):
        for sd in selected_days:
            if sd not in days_list:
                print(f"No such day {sd}", file=sys.stderr)
            print(f"List day {sd}", file=sys.stderr)
            filtered.append(sd)

    return filtered

def get_list_of_rerun_needed(status_list, check_new=True, check_not_valid=True):
    """
    Analyze status file (format: output status input) and create of inputs to run again
    based on:
    * check_new = True - the input is newer than output
    * check_not_valid - the status is not 0
    Returns set of unique inputs (some outputs may have same input)
    """

    to_rerun = []

    count = 0
    for s in status_list:
        count = count + 1
        print(f"Check files in list: {s[0]} {s[1]} ({count})", end='\r')

        try:
            t1_m = os.path.getmtime(s[0])
            t2_m = os.path.getmtime(s[2])
        except FileNotFoundError:
            print(f"\nProblem with files from: {s}")
            continue

        if check_new and t2_m > t1_m:
            print(f"\nAdd file to list: {s[2]}")
            to_rerun.append(s[2])
        elif check_not_valid and s[1] != '0':
            print(f"\nAdd file to list: {s[2]}")
            to_rerun.append(s[2])

    return list(set(to_rerun))


def add_file_to_list(name, current_files_list, new_files_list):
    """
    Check if files from 'name' are present in 'current_files_list',
    and if not, add them to 'new_files_list'.
    """

    if not isinstance(name, list):
        name=[name]

    count = 0
    for n in name:
        count = count + 1
        print(f"Check file in list: {n} ({count})", end='\r')
        if n not in current_files_list:
            print(f"\nAdd file to list: {n}")
            new_files_list.append(n)


def write_list(output_file, files_list):
    """ Write list to file """
    with open(output_file, "a")  as f:
        f.write('\n'.join(files_list))


def make_temp_lists(file_list, split_size = 10000, dir=None, prefix=None, suffix=None):
    """
    Get list of files, split into chunks,
    create tmp files, fill and return paths.
    """
    chunked_list = [file_list[i:i+split_size] for i in range(0, len(file_list), split_size)]
    tmp_files = []
    for l in chunked_list:
        # file for new files to submit
        tmp_list_file = tempfile.mktemp(dir=dir, prefix=prefix, suffix=suffix)
        write_list(tmp_list_file, l)
        tmp_files.append(tmp_list_file)
    return tmp_files
