#!/bin/bash

date

if [[ -n ${SLURM_ARRAY_TASK_ID} ]]; then
    echo "ARRAY"
    file=$(sed -n $((${SLURM_ARRAY_TASK_ID}))p ${INPUT})
else
    echo "NO ARRAY"
    file=$INPUT
fi

. $PROFILE

cd $WORKINGDIR

filebn=$(basename $file .hld)
file=$(readlink -e $file)
day=${filebn:4:3}

outputdir=$(readlink -e $OUTPUTDIR)
daydir=$outputdir/$day/

daystatusfile=$outputdir/${STATUSFILE/DAY/$day}

crashfile=$daydir/crash_await_${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.txt
checkfile=$daydir/check_failed_${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.txt

# mark that file is submitted, may be killed
echo $file > $crashfile

file=$(readlink -e $file)

echo PROFILE=$PROFILE
echo WORKINGDIR=$WORKINGDIR
echo file=$file
echo events=$EVENTS
echo outputdir=$outputdir
echo daydir=$daydir
echo daystatusfile=$daystatusfile

mkdir -p $daydir

if [ -z ${SLURM_TMPDIR+x} ]; then
    echo "SLURM_TMPDIR is unset"
    dsttmp=$(mktemp -d)
else
    echo "SLURM_TMPDIR is set to '$SLURM_TMPDIR'"
    dsttmp=$(mktemp /tmp/dst.XXXXXXXXXXXXXXXX -d)
fi

function update_with_lock {
    args=( "$@" )
    while ! flock -n -x $1 "${args[@]:1}"; do
        t=$(($RANDOM % 100))
        echo Retrying flock for $1 in $t
        sleep $t
    done
}

if [[ $EVENTS -gt 0 ]]; then
    echo ./analysisDST $file ${dsttmp} $EVENTS
    time ./analysisDST $file ${dsttmp} $EVENTS

    result_file=$(readlink -e ${dsttmp}/$filebn*.root)
    mv $result_file $daydir -v
    mkdir -p ${daydir}/qa
    mv ${dsttmp}/qa/* ${daydir}/qa/ -v
    rm -rf ${dsttmp}
else
    result_file=$(readlink -e ${daydir}/${filebn}*.root)
    sleep $(($RANDOM % 20))
fi

rm -v $crashfile

for ofile in $result_file; do
    f=$(readlink -e ${daydir}/$(basename $ofile))
    echo "Check $f"
    $checktool $f
    res=$?
    if [[ $res -neq 0 ]]; then
    	echo $result_file > $checkfile
    fi
done
