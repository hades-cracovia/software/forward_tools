#!/bin/bash

FORCE_SUBMIT=0
JOB_EVENTS=0
PRETEND=
ARRAY=
while getopts a:e:fp options; do
    case $options in
        a) ARRAY=$OPTARG;;
        e) JOB_EVENTS=$OPTARG;;
        f) FORCE_SUBMIT=1;;
        p) PRETEND=echo;;
    esac
done
shift $((OPTIND-1))

# This script should call farm job to run task
# input params
# $1 - hld file

WD=$(dirname $(readlink -e ${BASH_SOURCE[0]}))

function make_abs()
{
    local __returnval=$3
    if [[ "$1" = /* ]]; then
        local myresult=$1
    else
        local myresult=$(readlink -e $2/$1)
    fi

    eval $__returnval="'$myresult'"
}

[ -f $WD/config.sh ] && . $WD/config.sh || exit 1

# data dir must be defined
[[ -n $DATA_DIR ]] || exit 1
make_abs "$DATA_DIR" "$WD" ABS_DATA_DIR
mkdir $ABS_DATA_DIR -p

# profile must exists
make_abs "$HYDRA_PROFILE" "$WD" ABS_HYDRA_PROFILE
[[ -f $ABS_HYDRA_PROFILE ]] || exit 2

# batch script must exists
make_abs "$DST_BATCH_SCRIPT" "$WD" ABS_DST_BATCH_SCRIPT
[[ -f $ABS_DST_BATCH_SCRIPT ]] || exit 3

# batch WD must exists
make_abs "$DST_BATCH_WORKINGDIR" "$WD" ABS_DST_BATCH_WORKINGDIR
[[ -d $ABS_DST_BATCH_WORKINGDIR ]] || exit 4

# if variable is set, the file must exists
make_abs "$DST_CHECK_SCRIPT" "$WD" ABS_DST_CHECK_SCRIPT
[[ -z $DST_CHECK_SCRIPT ]] || [[ -f $ABS_DST_CHECK_SCRIPT ]] || exit 5


# status file must be set if the check script is set
[[ -z $HLD_CHECK_SCRIPT ]] || [[ -n $DST_STATUS_LIST ]] || exit 6
ABS_DST_STATUS_LIST=$ABS_DATA_DIR/$DST_STATUS_LIST
mkdir $(dirname $ABS_DST_STATUS_LIST) -p

# dst output dir must be defined
[[ -n $DST_DIR ]] || exit 7
ABS_DST_DIR=$ABS_DATA_DIR/$DST_DIR
mkdir $ABS_DST_DIR -p

# submit file, containing directory must exists
ABS_DST_SUBMIT_LIST=$ABS_DATA_DIR/$DST_SUBMIT_LIST
mkdir $(dirname $ABS_DST_SUBMIT_LIST) -p
[ ! -e $ABS_DST_SUBMIT_LIST ] && touch $ABS_DST_SUBMIT_LIST

is_array=
array_string=

# if this is list, handle it, otherwise check if file was submitted
if [[ -n $ARRAY ]]; then
    is_array=1
    array_string="--array=$ARRAY"
    log_name=$ABS_DATA_DIR/slurm_log/$DST_LOG_PREFIX-%A_%a.log
elif [[ $1 == *.txt ]]; then
    is_array=1
    array_string="--array=1-$(cat $1 | wc -l)"
    log_name=$ABS_DATA_DIR/slurm_log/$DST_LOG_PREFIX-%A_%a.log
else
    grep -q "^$1" $ABS_DST_SUBMIT_LIST
    is_submitted=$?
    log_name=$ABS_DATA_DIR/slurm_log/$DST_LOG_PREFIX-%j.log
fi

# execute it if either:
# - list is pass
# - file was not subbmited yet
# - or we enforce resubmission with -f option

[[ $JOB_EVENTS == 0 ]] && events=$DST_EVENTS || events=$JOB_EVENTS

if [[ $is_array -eq 1 || ! $is_submitted -eq 1 || $FORCE_SUBMIT -eq 1 ]]; then
    mkdir -p $ABS_DATA_DIR/slurm_log

    $PRETEND sbatch -o $log_name \
        ${array_string} \
        --time=${DST_SUBMIT_TIME} --mem-per-cpu=1000mb -p main -J D$(basename $1 .hld) \
        --export="PROFILE=$ABS_HYDRA_PROFILE,WORKINGDIR=$DST_BATCH_WORKINGDIR,INPUT=$1,EVENTS=$events,OUTPUTDIR=$ABS_DST_DIR,CHECKTOOL=$ABS_DST_CHECK_SCRIPT,STATUSFILE=$ABS_DST_STATUS_LIST" \
        -- $WD/wrap.sh $ABS_DST_BATCH_SCRIPT \
    && \
        $PRETEND flock -x $ABS_DST_SUBMIT_LIST echo "$1" >> $ABS_DST_SUBMIT_LIST
        # add hld file to list only if not add before
fi
