#!/usr/bin/env python3

import argparse
import glob
import pyinotify
import random
#import functools

from online_helpers import *

# global variables
HLD_WATCH_PID_FILE="~/.hld_watcher.pid"
HLD_WATCH_LOG_FILE="~/.hld_watcher.log"


WD=os.path.dirname(os.path.realpath(__file__))
config = parse_shell_config(make_abs('config.sh', WD))


full_name = make_abs(config.HLD_DIR + '/' + config.HLD_MASK, WD)
re_full_name = wildcard_to_regex(full_name, decorate=True)
p_full_name = re.compile(re_full_name)

ABS_DATA_DIR=make_abs(config.DATA_DIR, WD)
ABS_HLD_LIST_FILE=make_abs(config.HLD_LIST_FILE, ABS_DATA_DIR)
ABS_DST_SUBMIT_SCRIPT=make_abs(config.DST_SUBMIT_SCRIPT, WD)
ABS_DST_STATUS_LIST=make_abs(config.DST_STATUS_LIST, ABS_DATA_DIR)

class EventHandler(pyinotify.ProcessEvent):
    """
    Capture events and process. Add file to the the list file
    on IN_CLOSE_WRITE event.
    """
    #def process_IN_MODIFY(self, event):
        #print("Modifying", event.pathname)
    def process_IN_CLOSE_WRITE(self, event):
        if p_full_name.match(event.pathname):
            add_file_to_list(event.pathname, ABS_HLD_LIST_FILE)


def on_loop(notifier):
    pass


dirname = os.path.dirname(ABS_HLD_LIST_FILE)
try:
    os.mkdir(dirname)
except FileExistsError:
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--daemon', help='daemon mode', action='store_true')
    parser.add_argument('--all', help='list and send all files', action='store_true')
    parser.add_argument('-a', '--array', help='batch array', type=str, action='store')
    parser.add_argument('-e', '--events', help='events number', type=int, action='store')
    parser.add_argument('-l', '--list', help='list and send new files', action='store_true')
    parser.add_argument('-r', '--resend', help='resend invalid or modified files', action='store_true')
    parser.add_argument('-c', '--check', help='check files', action='store_true')
    parser.add_argument('-v', '--verbose', help='verbose mode', action='store_true')
    parser.add_argument('-p', '--pretend', help='verbose mode', action='store_true')
    parser.add_argument('--shuffle', help='shufle input files', action='store_true')
    #parser.add_argument('-s', '--submit', help='submit pending files', action='store_true')

    args, opts = parser.parse_known_args()

    extra_args = None
    array_args = None
    pretend_args = None
    events_args = None

    if args.array and len(args.array):
        array_args = f"-a{args.array}"

    if args.events:
        events_args = f"-e{args.events}"

    if args.pretend:
        pretend_args = '-p'

    if args.all:
        files_list = get_list_from_file(ABS_HLD_LIST_FILE) # current list content
        if len(files_list):
            tmp_files = make_temp_lists(files_list, dir=ABS_DATA_DIR, prefix='.dst_tmp_', suffix='.txt')
            for f in tmp_files:
                print(f"Created new list for submission: {f}")
                submit_file([ABS_DST_SUBMIT_SCRIPT, extra_args, array_args, events_args, pretend_args, f], args.verbose)
        else:
            print("No files to resubmit")

    elif args.check or args.resend:
        """
        Check selected files. First read all hld files and find all possible days.
        Run check for given or all days.
        """
        files_list = get_list_from_file(ABS_HLD_LIST_FILE) # current hld list content
        days_list, day_files = get_days_from_list(files_list) # current list of days

        selected_days = opts if len(opts) else days_list # take days from list or from cmdline

        if args.check:
            events_args=f"-e 0"

        if len(selected_days):
            files_to_check = []
            for sd in selected_days:
                if sd not in days_list:
                    print(f"No such day {sd}", out=file.stderr)
                    continue

                files_to_check = files_to_check + day_files[sd]
                print(f"Check day {sd}")

            if args.shuffle:
                random.shuffle(files_to_check)

            prefix='.dst_check_' if args.check else '.dst_resend_'
            tmp_files = make_temp_lists(files_to_check, dir=ABS_DATA_DIR, prefix=prefix, suffix='.txt')
            for f in tmp_files:
                print(f"Created new list for submitting: {f}")
                submit_file([ABS_DST_SUBMIT_SCRIPT, extra_args, array_args, pretend_args, events_args, f], args.verbose)
        else:
            print("No files to submit")

    elif args.list:
        files_list = get_list_from_file(ABS_HLD_LIST_FILE) # current list content
        new_files_list = [] # keeps new files

        r = glob.glob(full_name)
        add_file_to_list(r, files_list, new_files_list)

        print(f"Update list of all files: {ABS_HLD_LIST_FILE}")
        write_list(ABS_HLD_LIST_FILE, new_files_list)

        if len(new_files_list):
            tmp_files = make_temp_lists(new_files_list, dir=ABS_DATA_DIR, prefix='.dst_tmp_', suffix='.txt')
            for f in tmp_files:
                print(f"Created new list for submission: {f}")
                submit_file([ABS_DST_SUBMIT_SCRIPT, extra_args, array_args, pretend_args, events_args, f], args.verbose)
        else:
            print("No new files to submit")

    elif args.resend:
        current_list = get_status_list_from_file(ABS_DST_STATUS_LIST)
        rerun_list = get_list_of_rerun_needed(current_list)

        if len(rerun_list):
            tmp_files = make_temp_lists(rerun_list, dir=ABS_DATA_DIR, prefix='.dst_tmp_', suffix='.txt')
            for f in tmp_files:
                print(f"Created new list for submission: {f}")
                submit_file([ABS_DST_SUBMIT_SCRIPT, extra_args, array_args, pretend_args, events_args, f], args.verbose)
        else:
            print("No new files to resubmit")

    else:
        wm = pyinotify.WatchManager()
        handler = EventHandler()
        notifier = pyinotify.Notifier(wm, handler)
        wm.add_watch(HLD_DIR,
                    pyinotify.ALL_EVENTS,
                    rec=True)

        try:
            notifier.loop(daemonize=args.daemon,
                        callback=on_loop,
                        pid_file=make_abs(HLD_WATCH_PID_FILE, WD),
                        stdout=make_abs(HLD_WATCH_LOG_FILE, WD))
        except (pyinotify.NotifierError, err):
            sys.stdout.write(err)
