#!/bin/bash

PRETEND=
while getopts fp options; do
        case $options in
                p) PRETEND=echo;;
        esac
done
shift $((OPTIND-1))

# This script should call farm job to run task
# input params
# $1 - hld file

WD=$(dirname $(readlink -e ${BASH_SOURCE[0]}))

function make_abs()
{
    local __returnval=$3
    if [[ "$1" = /* ]]; then
        local myresult=$1
    else
        local myresult=$(readlink -e $2/$1)
    fi

    eval $__returnval="'$myresult'"
}

[ -f $WD/config.sh ] && . $WD/config.sh || exit 1

# data dir must be defined
[[ -n $DATA_DIR ]] || exit 1
make_abs "$DATA_DIR" "$WD" ABS_DATA_DIR
mkdir $ABS_DATA_DIR -p

make_abs "$LUMI_DIR" "$ABS_DATA_DIR" ABS_LUMI_DIR
[[ -d $ABS_LUMI_DIR ]] || exit 2

# batch script must exists
make_abs "$LUMI_SUBMIT_SCRIPT" "$WD" ABS_LUMI_SUBMIT_SCRIPT
[[ -f $ABS_LUMI_SUBMIT_SCRIPT ]] || exit 3

make_abs "$LUMI_STATUS_LIST" "$ABS_DATA_DIR" ABS_LUMI_STATUS_LIST
[[ -f $ABS_LUMI_STATUS_LIST ]] || exit 4

REGEX="^(.*\/[a-z]{2}[0-9]{13}\.hld).*$"

echo "Checking in ${ABS_LUMI_STATUS_LIST}"

while IFS= read -r line; do
    stringarray=($line)

    lumi=${stringarray[0]}
    status=${stringarray[1]}
    dst=${stringarray[2]}

    echo -e "\033[2K"
    echo -n "Check: $lumi => $status of $dst"

    if [[ -z $source ]]; then
        if [[ $lumi =~ $REGEX ]];
        then
            source=$(readlink -e $ABS_LUMI_DIR/*/$(basename ${BASH_REMATCH[1]}))
        else
            echo
            echo "File name could not be recovered from $lumi"
            exit 10
        fi
    fi

    
    if [[ "$status" -ne "0" ]]; then
        echo
        echo "Submit: $LUMI_SUBMIT_SCRIPT -f $dst"
        $PRETEND $ABS_LUMI_SUBMIT_SCRIPT -f $dst || echo Error Status: $?
    fi

done < $ABS_LUMI_STATUS_LIST
