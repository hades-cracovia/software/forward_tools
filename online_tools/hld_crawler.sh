#!/bin/bash

# directories and files
WD="$(readlink -e $(dirname "$0"))"     # working dir
DD="${DD:-$WD/../data}"                 # datadir

[ -e $WD/user.sh ] && source $WD/user.sh

HLDDIR=${HLDDIR:-/lustre/hades/raw/feb22/22}
HLDSTAMPDIR=${HLDSTAMPDIR:-$DD/stamps_hld}
HLDMASK=${HLDMASK:-be*}
DSTOUT=${DSTOUT:-$DD/out}

HLDLISTFILE=${HLDLISTFILE:-$DD/hldlist.txt}
DSTSTATUSFILE=${DSTSTATUSFILE:-$DD/dststatus.txt}

COOLDOWNTIME=120

MARKEXT=.found

# tool to convert hades filename into normal time
HADESTIMECONV=$WD/hadestime

# root checking tool
CHECKTOOL=$WD/test_root_file.C

[ ! -d $DD ] && mkdir -p $DD
[ ! -d $HLDSTAMPDIR ] && mkdir -p $HLDSTAMPDIR
[ ! -d $DSTOUT ] && mkdir -p $DSTOUT

allfiles=$(find ${HLDDIR} -name "${HLDMASK}.hld" -print)
for f in $allfiles; do
    ct=$(date +%s)          # current time, update each loop
    mt=$(stat -c %Y $f)     # file modification time
                            # while still being copied
                            # mt will be equal to current
                            # we need to be sure that file was fully copied
                            # ask for 2 minutes difference from ct
    tdiff=$(($ct - $mt))

    if [ $tdiff -gt $COOLDOWNTIME ]; then
        fn=$(basename $f .hld) #get just filename
        if [ ! -f $HLDSTAMPDIR/${fn}$MARKEXT ]; then
            date > $HLDSTAMPDIR/${fn}$MARKEXT    # store date of processing
            if ! grep -q $f $HLDLISTFILE; then
                echo $f >> $HLDLISTFILE         # add hld file to list only if not add before
            fi

            # run dst here
            echo $WD/submit_dst_job.sh $WD $DD $f $DSTOUT $CHECKTOOL $DSTSTATUSFILE
            exec $WD/submit_dst_job.sh $WD $DD $f $DSTOUT $CHECKTOOL $DSTSTATUSFILE &
        fi
    fi
done
