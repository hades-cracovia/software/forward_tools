add_executable(
    elastics_monitor

    elastics_monitor.cpp
)

target_link_libraries(elastics_monitor FD::Forward ${HYDRA2_LIBRARIES} ${ROOT_LIBRARIES})

# Install the export set for use with the install-tree
install(TARGETS elastics_monitor
    RUNTIME
        DESTINATION ${CMAKE_INSTALL_BINDIR}
)
