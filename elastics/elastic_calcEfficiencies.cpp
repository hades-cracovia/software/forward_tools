#include "config.h"
#include "forward_tools.h"

#include "hades.h"
#include "hcategorymanager.h"
#include "hloop.h"
#include "htool.h"

#include "hcategory.h"
#include "hforwardcand.h"
#include "hforwardcandsim.h"
#include "hlinearcategory.h"
#include "hparticlecand.h"
#include "hparticlecandsim.h"

#include "hgeantkine.h"
#include "hparticledef.h"

#include "hparticlegeant.h"
#include "hparticlegeantdecay.h"
#include "hparticlegeantevent.h"

#include <hrpccal.h>
#include <hrpccalpar.h>
#include <hrpccluster.h>
#include <hrpcdigipar.h>
#include <hrpcgeompar.h>
#include <hrpchit.h>
#include <hrpcraw.h>
#include <htofhit.h>
#include <htofraw.h>
#include <rpcdef.h>
#include <tofdef.h>

#include "TROOT.h"
#include "TTree.h"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <math.h>
#include <vector>

using namespace Particle;

Bool_t selectHadrons(HParticleCand* pcand)
{
    // build in selection function for hadron candidates.
    // Requires besides an RK + META and fitted
    // inner+outer segment.

    Bool_t test = kFALSE;
    if (pcand->isFlagAND(4, // ask for flags
                         Particle::kIsAcceptedHitInnerMDC, Particle::kIsAcceptedHitOuterMDC,
                         Particle::kIsAcceptedHitMETA,
                         Particle::kIsAcceptedRK) &&
        pcand->getInnerSegmentChi2() > 0 &&
        pcand->getChi2() < 10000 // RK, tracking chi2, tracking failed if 1e6
    )
        test = kTRUE;

    if (!test) return kFALSE;

    if (test)
        test = pcand->getMetaMatchQuality() < 3 ? kTRUE
                                                : kFALSE; // distance of outer segment to Meta hit

    return test;
}

Int_t calcEfficiencies(HLoop* loop, Int_t nEvents = -1)
{

    // Check if loop was properly initialized
    if (!loop->setInput(""))
    { // reading file structure
        std::cerr << "READBACK: ERROR : cannot read input !" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    TStopwatch timer;
    timer.Start();

    double deg2rad = TMath::DegToRad();

    // -----------------------------------------------------------------------
    // define output file and some histograms
    // -----------------------------------------------------------------------
    // set ouput file
    TFile* outfile = new TFile("pp_elastic_4200_feb21_calcLumiCorr.root", "recreate");

    TFile* said_lab = new TFile(
        "/lustre/hades/user/jrieger/pp_elastic/lumiMonitor/pp_elastic_4200_SAID_lab.root", "read");
    TH1F* h_said_cm = (TH1F*)said_lab->Get("hangle_cm");

    // TF1* f_thttht = new TF1("f_thttht", "arctan(1/tan(x/57.3)*3.23834)", 0., 90.);
    // TF1* f_ptht = new TF1("f_ptht", "5.052/(cos(x/57.3)*(1+pow(tan(x/57.3),2)*3.23834))",
    // 0., 90.);

    TH1F* hcostht_cm_gen = new TH1F("hcostht_cm_gen", "Scattering Angle (CM)", 100, 0, 1);
    hcostht_cm_gen->SetXTitle("cos(#theta_{CM})");
    hcostht_cm_gen->SetYTitle(" counts ");
    hcostht_cm_gen->GetXaxis()->SetTitleSize(0.05);
    hcostht_cm_gen->GetXaxis()->SetLabelSize(0.05);
    hcostht_cm_gen->GetYaxis()->SetTitleSize(0.05);
    hcostht_cm_gen->GetYaxis()->SetLabelSize(0.05);
    TH1F* hcostht_cm_rec = (TH1F*)hcostht_cm_gen->Clone("hcostht_cm_rec");
    TH1F* htht_cm_rec = (TH1F*)hcostht_cm_gen->Clone("htht_cm_rec");
    htht_cm_rec->SetBins(100, 0, 180);
    htht_cm_rec->SetXTitle("#theta_{CM}");
    TH1F* htht_cm_rec_pt1 = (TH1F*)htht_cm_rec->Clone("htht_cm_rec_pt1");
    TH1F* htht_cm_gen = (TH1F*)htht_cm_rec->Clone("htht_cm_gen");
    TH1F* htht_lab_rec = (TH1F*)hcostht_cm_gen->Clone("htht_lab_rec");
    htht_lab_rec->SetTitle("Scattering Angle (lab)");
    htht_lab_rec->SetBins(120, 0, 90);
    htht_lab_rec->SetXTitle("#theta_{lab}");
    TH1F* htht_lab_rec_cut = (TH1F*)htht_lab_rec->Clone("htht_lab_rec_cut");
    htht_lab_rec_cut->SetTitle("Scattering Angle (lab), el scattering cut");
    TH1F* htht_lab_rec_cut_hf = (TH1F*)htht_lab_rec_cut->Clone("htht_lab_rec_cut_hf");
    TH1F* htht_lab_rec_cut_hf_fine = (TH1F*)htht_lab_rec_cut_hf->Clone("htht_lab_rec_cut_hf_fine");
    htht_lab_rec_cut_hf_fine->SetBins(50, 2, 7);
    TH1F* htht_lab_rec_cut_hf_fine_pt1 =
        (TH1F*)htht_lab_rec_cut_hf_fine->Clone("htht_lab_rec_cut_hf_fine_pt1");
    TH1F* htht_lab_rec_cut_hh = (TH1F*)htht_lab_rec_cut->Clone("htht_lab_rec_cut_hh");
    TH1F* htht_lab_rec_cut_hh_pt1 = (TH1F*)htht_lab_rec_cut->Clone("htht_lab_rec_cut_hh_pt1");
    TH1F* htht_lab_gen = (TH1F*)htht_lab_rec->Clone("htht_lab_gen");
    TH1F* htht_lab_gen_hf = (TH1F*)htht_lab_gen->Clone("htht_lab_gen_hf");
    TH1F* htht_lab_gen_hf_fine = (TH1F*)htht_lab_gen_hf->Clone("htht_lab_gen_hf_fine");
    htht_lab_gen_hf_fine->SetBins(50, 2, 7);
    TH1F* htht_lab_gen_hh = (TH1F*)htht_lab_gen->Clone("htht_lab_gen_hh");

    TH2F* hptht_rec = new TH2F("hptht_rec", "mom vs. #theta in lab", 100, 0, 90, 100, 0, 5.2);
    hptht_rec->SetXTitle("#theta in deg");
    hptht_rec->SetYTitle(" p in GeV/c ");
    hptht_rec->GetXaxis()->SetTitleSize(0.05);
    hptht_rec->GetXaxis()->SetLabelSize(0.05);
    hptht_rec->GetYaxis()->SetTitleSize(0.05);
    hptht_rec->GetYaxis()->SetLabelSize(0.05);
    hptht_rec->GetZaxis()->SetLabelSize(0.05);
    TH2F* hptht_rec_cuts = (TH2F*)hptht_rec->Clone("hptht_rec_cuts");
    TH2F* hthttht_rec = (TH2F*)hptht_rec->Clone("hthttht_rec");
    hthttht_rec->SetXTitle("#theta_{1} in deg");
    hthttht_rec->SetYTitle("#theta_{2} in deg");
    hthttht_rec->GetYaxis()->Set(100, 0, 90);
    hthttht_rec->SetTitle("#theta_{p_{1}} vs. #theta_{p_{2}}");
    TH2F* hthttht_rec_cuts = (TH2F*)hthttht_rec->Clone("hthttht_rec_cuts");

    TH2F* hphitht_rec = (TH2F*)hptht_rec->Clone("hphitht_rec");
    hphitht_rec->SetYTitle("#phi in deg");
    hphitht_rec->SetTitle("#phi vs. #theta reconstructed");
    hphitht_rec->GetYaxis()->Set(100, 0, 360);
    TH2F* hphitht_rec_cuts = (TH2F*)hphitht_rec->Clone("hphitht_rec_cuts");
    hphitht_rec_cuts->SetTitle("#phi vs. #theta reconstructed, el. scattering cuts");
    TH2F* hphitht_gen = (TH2F*)hphitht_rec->Clone("hphitht_gen");
    hphitht_gen->SetTitle("#phi vs. #theta generated");

    TH1F* hcopl_hadfw = (TH1F*)htht_cm_gen->Clone("hcopl_hadfw");
    hcopl_hadfw->GetXaxis()->Set(100, 150, 210);
    hcopl_hadfw->SetTitle("Coplanarity");
    hcopl_hadfw->SetXTitle("#Delta#phi in deg");
    TH1F* hcopl_hadhad = (TH1F*)hcopl_hadfw->Clone("hcopl_hadhad");

    TH1F* htantan_hadfw = (TH1F*)htht_cm_gen->Clone("htantan_hadfw");
    htantan_hadfw->GetXaxis()->Set(100, 0.1, 0.5);
    htantan_hadfw->SetTitle("tan #theta_{1} * tan #theta_{2}");
    htantan_hadfw->SetXTitle("tan #theta_{1} * tan #theta_{2}");
    TH1F* htantan_hadfw_phicut = (TH1F*)htantan_hadfw->Clone("htantan_hadfw_phicut");
    TH1F* htantan_hadhad = (TH1F*)htantan_hadfw->Clone("htantan_hadhad");
    htantan_hadhad->GetXaxis()->Set(100, 0.26, 0.36);

    TH1F* hphi_fw = (TH1F*)htht_cm_gen->Clone("hphi_fw");
    hphi_fw->GetXaxis()->Set(100, 0, 360);
    hphi_fw->SetTitle("Azimuthal Angle");
    hphi_fw->SetXTitle("#phi in deg");
    TH1F* hphi_had = (TH1F*)hphi_fw->Clone("hphi_had");

    TH1F* hnumtrackevt = new TH1F("hnumtrackevt", "Number of Tracks per Event", 10, 0, 10);

    // -----------------------------------------------------------------------

    // select categories here
    if (!loop.setInput("-*,+HParticleCandSim,+HGeantKine,+HForwardCandSim"))
    {
        cout << "READBACK: ERROR : cannot read input !" << endl;
        exit(1);
    } // read all categories

    loop.printCategories();
    loop.printChain();

    HCategory* catParticle = loop.getCategory("HParticleCandSim");
    if (!catParticle)
    {
        std::cout << "No particleCat in input!" << std::endl;
        exit(1);
    }
    HCategory* catGeant = loop.getCategory("HGeantKine");
    if (!catGeant)
    {
        std::cout << "No kineCat in input!" << std::endl;
        exit(1);
    }
    HCategory* catFwParticle = loop.getCategory("HForwardCandSim");
    if (!catFwParticle)
    {
        std::cout << "No FWparticleCat in input!" << std::endl;
        exit(1);
    }

    // Categories for PT1 trigger
    HCategory* rpcCal = nullptr;
    rpcCal = HCategoryManager::getCategory(catRpcCal, kTRUE, "catRpcCal");
    if (!rpcCal) { cout << "No catRpcCal!" << endl; }
}

HCategory* tofRaw = nullptr;
tofRaw = HCategoryManager::getCategory(catTofRaw, kTRUE, "catTofRaw");
if (!tofRaw) { cout << "No catTofRaw!" << endl; }
}

// Variable definitions
Int_t t_PT1;
Int_t num_Events_HH_Elastic = 0;
Int_t num_Events_HF_Elastic = 0;
Int_t num_HH_Events_Elastic_PT1 = 0;
Int_t num_HF_Events_Elastic_PT1 = 0;
Int_t ntracks_rec = 0;
Int_t ntracks_gen = 0;
Int_t nFwTracks = 0;

Int_t entries = loop.getEntries();
if (nEvents > entries || nEvents <= 0) nEvents = entries;

cout << "events: " << nEvents << endl;

Double_t mp = 938.2720813;
TLorentzVector ppSystem(0, 0, 5051.8794, 2 * mp + 4200);

// start of the event loop
for (Int_t i = 1; i < nEvents; i++)
{
    //----------break if last event is reached-------------
    if (loop.nextEvent(i) <= 0)
    {
        cout << " end recieved " << endl;
        break;
    } // last event reached
    HTool::printProgress(i, nEvents, 1, "Analysing evt# :");

    // Manual PT1 Trigger:
    HRpcCal* rpc = nullptr;
    HTofRaw* tof = nullptr;

    int tof_ev = tofHit->getEntries();
    int rpc_ev = rpcCal->getEntries();

    int tofhit_count = 0;
    int rpcraw_count = 0;
    int tofhit = 0;
    int rpchit = 0;

    if (tof_ev)
    {
        for (int i = 0; i < tof_ev; i++)
        {

            tof = HCategoryManager::getObject(tof, catTofRaw, i);

            if (tof->getRightTime() > 0.)
            {
                // TofRightMult[(Int_t)tof->getSector()]++;
                tofhit_count++;
            }
            if (tof->getLeftTime() > 0.)
            {
                // TofLeftMult[(Int_t)tof->getSector()]++;
                tofhit_count++;
            }

            tofhit++;
        }
    } // tof_ev
    //*********************************************************
    if (rpc_ev)
    {
        for (int i = 0; i < rpc_ev; i++)
        {
            rpc = HCategoryManager::getObject(rpc, catRpcCal, i);

            if (rpc->getRightTime() > 0.)
            {
                // RpcRightMult[(Int_t)rpc->getSector()]++;
                rpcraw_count++;
            }
            if (rpc->getLeftTime() > 0.)
            {
                // RpcLeftMult[(Int_t)rpc->getSector()]++;
                rpcraw_count++;
            }

            rpchit++;
        }
    } // rpc_ev

    int tof_mult = int(tofhit_count / 2);
    int rpc_mult = int(rpcraw_count / 2);

    int trigM2 = rpc_mult + tof_mult;

    if (trigM2 >= 2)
    {
        num_Events_PT1++;
        is_pt1 = true;
        t_PT1 = 1;
    }

    auto has_hh_elastic = ForwardTools::Elastics::check_elastics_hh(
        fParticleCand, cut_HH_phi_diff_min, cut_HH_phi_diff_max, cut_HH_tan_theta_min,
        cut_HH_tan_theta_max);

    auto has_hf_elastic = ForwardTools::Elastics::check_elastics_hf(
        fParticleCand, fForwardCand, cut_HF_phi_diff_min, cut_HF_phi_diff_max, cut_HF_tan_theta_min,
        cut_HF_tan_theta_max);

    if (has_hh_elastic || has_hf_elastic) num_Events_Total_Elastic = num_Events_Total_Elastic + 1;

    if (has_hh_elastic)
    {
        num_Events_HH_Elastic = num_Events_HH_Elastic + 1;
        if (t_PT1 == 1) num_HH_Events_Elastic_PT1 = num_HH_Events_Elastic_PT1 + 1;

        // Particle 1 - HADES particle
        HParticleCand* cand1 = HCategoryManager::getObject(cand1, catParticle, 0);
        // Particle 2 - HADES particle
        HParticleCand* cand2 = HCategoryManager::getObject(cand2, catParticle, 1);

        htht_lab_rec_cut->Fill(cand1->getTheta());
        htht_lab_rec_cut->Fill(cand2->getTheta());
        hthttht_rec_cuts->Fill(cand1->getTheta(), cand2->getTheta());
        hptht_rec_cuts->Fill(cand1->getTheta(), cand1->getMomentum() / 1000);
        hptht_rec_cuts->Fill(cand2->getTheta(), cand2->getMomentum() / 1000);
        htht_lab_rec_cut_hh->Fill(cand1->getTheta());
        htht_lab_rec_cut_hh->Fill(cand2->getTheta());
        hphitht_rec_cuts->Fill(cand1->getTheta(), cand1->getPhi());
        hphitht_rec_cuts->Fill(cand2->getTheta(), cand2->getPhi());
        cand1->Boost(-ppSystem.BoostVector());
        htht_cm_rec->Fill(cand1->Theta() * 180 / M_PI);
        cand2->Boost(-ppSystem.BoostVector());
        htht_cm_rec->Fill(cand2->Theta() * 180 / M_PI);
        if (t_PT1 == 1)
        {
            htht_lab_rec_cut_hh_pt1->Fill(cand1->getTheta());
            htht_lab_rec_cut_hh_pt1->Fill(cand2->getTheta());
            htht_cm_rec_pt1->Fill(cand1->Theta() * 180 / M_PI);
            htht_cm_rec_pt1->Fill(cand2->Theta() * 180 / M_PI);
        }
    }

    if (has_hf_elastic)
    {
        num_Events_HF_Elastic = num_Events_HF_Elastic + 1;
        if (t_PT1 == 1) num_HF_Events_Elastic_PT1 = num_HF_Events_Elastic_PT1 + 1;

        // Particle 1 - HADES particle
        HParticleCand* cand1 = HCategoryManager::getObject(cand1, catParticle, 0);
        // Particle 2 - FwDet Particle
        HForwardCand* cand2 = HCategoryManager::getObject(cand2, catFwParticle, 0);
        Double_t t_z_start_P2 = fparticlecand->getZ(); // [mm]
        // Correction for track start position (Z_START)
        fwdetcand->setStartXYZ(0.0, 0.0,
                               t_z_start_P2); // We take the track start position from the
                                              // reconstructed HADES track paired with FwDet track
        fwdetcand->calcPoints(); // Recalculating ToF and Distance to account for correction
        fwdetcand->calc4vectorProperties(
            proton_mass); // Calculating 4 vector properties assuming particles are protons

        htht_lab_rec_cut->Fill(cand1->getTheta());
        htht_lab_rec_cut->Fill(cand2->getTheta());
        hthttht_rec_cuts->Fill(cand1->getTheta(), cand2->getTheta());
        hptht_rec_cuts->Fill(cand1->getTheta(), cand1->getMomentum() / 1000);
        hptht_rec_cuts->Fill(cand2->getTheta(), cand2->getMomentum() / 1000);
        htht_lab_rec_cut_hf->Fill(cand1->getTheta());
        htht_lab_rec_cut_hf->Fill(cand2->getTheta());
        htht_lab_rec_cut_hf_fine->Fill(cand2->getTheta());
        hphitht_rec_cuts->Fill(cand1->getTheta(), cand1->getPhi());
        hphitht_rec_cuts->Fill(cand2->getTheta(), cand2->getPhi());
        htht_lab_rec_cut_hf_fine_pt1->Fill(cand2->getTheta());
        cand1->Boost(-ppSystem.BoostVector());
        htht_cm_rec->Fill(cand1->Theta() * 180 / M_PI);
        cand2->Boost(-ppSystem.BoostVector());
        htht_cm_rec->Fill(cand2->Theta() * 180 / M_PI);
        if (t_PT1 == 1)
        {
            htht_lab_rec_cut_hf_fine_pt1->Fill(cand2->getTheta());
            htht_cm_rec_pt1->Fill(cand1->Theta() * 180 / M_PI);
            htht_cm_rec_pt1->Fill(cand2->Theta() * 180 / M_PI);
        }
    }

    // for each event there are a number of tracks
    // ntracks_rec = catParticle->getEntries();
    ntracks_gen = catGeant->getEntries();
    // nFwTracks = catFwParticle->getEntries();

    // std::vector<HParticleCandSim*> protons;
    // std::vector<HForwardCandSim*> protons_fw;
    std::vector<HGeantKine*> protons_gen;

    // hnumtrackevt->Fill(ntracks_rec+nFwTracks);
    /*
            for (Int_t j = 0; j < ntracks_rec; j++)
            {
                HParticleCandSim* cand = HCategoryManager::getObject(cand, catParticle, j);
                // skip ghost tracks (only avalible for MC events)
                //if (cand->isGhostTrack()) continue;
                // select "good" tracks
                //if (!cand->isFlagBit(Particle::kIsUsed)) continue;

                counter_phades++;
                if(cand->getGeantPID()==14 && cand->getGeantParentTrackNum()==0)
       htht_lab_rec->Fill(cand->getTheta()); protons.push_back(cand); } // end of had track loop

            for(Int_t j=0; j<nFwTracks; j++){
                HForwardCandSim *cand = HCategoryManager::getObject(cand,catFwParticle,j);

                counter_pfw++;
                if(cand->getGeantPID()==14 && cand->getGeantParentTrackNum()==0)
       htht_lab_rec->Fill(cand->getTheta()); protons_fw.push_back(cand); } // end of fw track loop
    */
    for (Int_t j = 0; j < ntracks_gen; j++)
    {
        HGeantKine* cand = HCategoryManager::getObject(cand, catGeant, j);

        if (cand->getID() == 14 && cand->getParentTrack() == 0)
        {
            protons_gen.push_back(cand);
            htht_lab_gen->Fill(cand->getThetaDeg());
            // cand->Boost(-ppSystem.BoostVector());
            // htht_cm_gen->Fill(cand->getThetaDeg());
            // hcostht_cm_gen->Fill(cos(cand->getThetaDeg()*M_PI/180);
            // htht_cm_gen->Fill(cand->Boost(ppSystem.BoostVector()).Theta()*57.3);
        }
    } // end of geant loop
      /*
              for (size_t k = 0; k < protons.size(); k++){
                  HParticleCandSim *cand1 = protons[k];
                  cand1->calc4vectorProperties(938.272);
                  hptht_rec->Fill(cand1->getTheta(), cand1->getMomentum()/1000);
                  hphi_had->Fill(cand1->getPhi());
                  for (size_t l = k+1; l < protons.size(); l++){
                      HParticleCandSim *cand2 = protons[l];
                      if (cand2->getGeantTrack()==cand1->getGeantTrack()) continue;
                      cand2->calc4vectorProperties(938.272);
                      counter_hadeshades++;
                      hthttht_rec->Fill(cand1->getTheta(), cand2->getTheta());
                      Double_t deltaphi = abs(cand1->getPhi() - cand2->getPhi());
                      Double_t tantht = tan(cand1->Theta())*tan(cand2->Theta());
                      hcopl_hadhad->Fill(deltaphi);
                      htantan_hadhad->Fill(tantht);
                      hphitht_rec->Fill(cand1->getTheta(), cand1->getPhi());
                      hphitht_rec->Fill(cand2->getTheta(), cand2->getPhi());
                      if ((deltaphi>178.5 && deltaphi < 181.5) && (tantht>(0.3088-0.015) &&
         tantht<(0.3088+0.015))) {   counter_hadeshades_elastic++;
                          htht_lab_rec_cut->Fill(cand1->getTheta());
                          htht_lab_rec_cut->Fill(cand2->getTheta());
                          hthttht_rec_cuts->Fill(cand1->getTheta(), cand2->getTheta());
                          hptht_rec_cuts->Fill(cand1->getTheta(), cand1->getMomentum()/1000);
                          hptht_rec_cuts->Fill(cand2->getTheta(), cand2->getMomentum()/1000);
                          htht_lab_rec_cut_hh->Fill(cand1->getTheta());
                          htht_lab_rec_cut_hh->Fill(cand2->getTheta());
                          hphitht_rec_cuts->Fill(cand1->getTheta(), cand1->getPhi());
                          hphitht_rec_cuts->Fill(cand2->getTheta(), cand2->getPhi());
                      }
                  }
                  for (size_t l = 0; l < protons_fw.size(); l++){
                      HForwardCandSim *cand2 = protons_fw[l];
                      if (cand2->getGeantTrack()==cand1->getGeantTrack()) continue;
                      cand2->calc4vectorProperties(938.272);
                      counter_hadesfw++;
                      hthttht_rec->Fill(cand1->getTheta(), cand2->getTheta());
                      Double_t deltaphi = abs(cand1->getPhi() - cand2->getPhi());
                      Double_t tantht = tan(cand1->Theta())*tan(cand2->Theta());
                      hcopl_hadfw->Fill(deltaphi);
                      htantan_hadfw->Fill(tantht);
                      hphitht_rec->Fill(cand1->getTheta(), cand1->getPhi());
                      hphitht_rec->Fill(cand2->getTheta(), cand2->getPhi());
                      if (deltaphi>173 && deltaphi < 187) htantan_hadfw_phicut->Fill(tantht);
                      if ((deltaphi>173 && deltaphi < 187) && (tantht>(0.3088-0.1) &&
         tantht<(0.3088+0.1))) {   counter_hadesfw_elastic++;
         htht_lab_rec_cut->Fill(cand1->getTheta());   htht_lab_rec_cut->Fill(cand2->getTheta());
                          hthttht_rec_cuts->Fill(cand1->getTheta(), cand2->getTheta());
                          hptht_rec_cuts->Fill(cand1->getTheta(), cand1->getMomentum()/1000);
                          hptht_rec_cuts->Fill(cand2->getTheta(), cand2->getMomentum()/1000);
                          htht_lab_rec_cut_hf->Fill(cand1->getTheta());
                          htht_lab_rec_cut_hf->Fill(cand2->getTheta());
                          htht_lab_rec_cut_hf_fine->Fill(cand2->getTheta());
                          hphitht_rec_cuts->Fill(cand1->getTheta(), cand1->getPhi());
                          hphitht_rec_cuts->Fill(cand2->getTheta(), cand2->getPhi());
                      }
                  }
                      cand1->Boost(-ppSystem.BoostVector());
                      hcostht_cm_rec->Fill(cos(cand1->Theta()));
                      htht_cm_rec->Fill(cand1->Theta()*180/M_PI);
              }
  
              for (size_t l = 0; l < protons_fw.size(); l++){
                  HForwardCandSim *cand2 = protons_fw[l];
                  cand2->calc4vectorProperties(938.272);
                  hptht_rec->Fill(cand2->getTheta(), cand2->getMomentum()/1000);
                  hphi_fw->Fill(cand2->getPhi());
                  //cout<<cand2->Theta()<<" "<<cand2->getTheta()<<endl;
                  cand2->Boost(-ppSystem.BoostVector()); //affects only lorentz vector
                  //cout<<cand2->Theta()<<" "<<cand2->getTheta()<<endl;
                  hcostht_cm_rec->Fill(cos(cand2->Theta()));
                  htht_cm_rec->Fill(cand2->Theta()*180/M_PI);
              }
      */

    Double_t theta1 = protons_gen[0]->getThetaDeg();
    Double_t theta2 = protons_gen[1]->getThetaDeg();
    hphitht_gen->Fill(protons_gen[0]->getThetaDeg(), protons_gen[0]->getPhiDeg());
    hphitht_gen->Fill(protons_gen[1]->getThetaDeg(), protons_gen[1]->getPhiDeg());
    if (theta1 < 88 && theta1 > 16 && theta2 < 88 && theta2 > 16)
    {
        counter_hadeshades_gen++;
        htht_lab_gen_hh->Fill(theta1);
        htht_lab_gen_hh->Fill(theta2);
    }
    else if ((theta1 < 88 && theta1 > 16 && theta2 < 7 && theta2 > 0) ||
             (theta1 < 7 && theta1 > 0 && theta2 < 88 && theta2 > 16))
    {
        counter_hadesfw_gen++;
        htht_lab_gen_hf->Fill(theta1);
        htht_lab_gen_hf->Fill(theta2);
        htht_lab_gen_hf_fine->Fill(theta1);
        htht_lab_gen_hf_fine->Fill(theta2);
    }

    /*
            for (size_t i = 0; i<protons_gen.size(); i++){
                HGeantKine *cand1 = protons_gen[i];
                if(cand1->getThetaDeg()>16 && cand1->getThetaDeg()<88){
                    for (size_t j = 1; j<protons_gen.size(); j++){
                        HGeantKine *cand2 = protons_gen[j];
                        if(cand2->getThetaDeg()>16 && cand2->getThetaDeg()<88){
                            counter_hadeshades_gen++;
                        }
                    }
                }
            }

            for (size_t i = 0; i<protons_gen.size(); i++){
                HGeantKine *cand1 = protons_gen[i];
                if(cand1->getThetaDeg()>16 && cand1->getThetaDeg()<88){
                    for (size_t j = 0; j<protons_gen.size(); j++){
                        HGeantKine *cand2 = protons_gen[j];
                        if(cand2->getThetaDeg()>0 && cand2->getThetaDeg()<7){
                            counter_hadesfw_gen++;
                        }
                    }
                }
            }*/

} // end of event loop

// Calculate Lumi factor
TH1F* h_eff_hh = (TH1F*)htht_lab_rec_cut_hh->Clone("h_eff_hh");
TH1F* h_eff_hh_pt1 = (TH1F*)htht_lab_rec_cut_hh_pt1->Clone("h_eff_hh_pt1");
TH1F* h_eff_hf = (TH1F*)htht_lab_rec_cut_hf->Clone("h_eff_hf");
TH1F* h_eff_hf_fine = (TH1F*)htht_lab_rec_cut_hf_fine->Clone("h_eff_hf_fine");
TH1F* h_eff_hf_fine_pt1 = (TH1F*)htht_lab_rec_cut_hf_fine_pt1->Clone("h_eff_hf_fine_pt1");
h_eff_hf->Divide(htht_lab_gen_hf);
h_eff_hf_fine->Divide(htht_lab_gen_hf_fine);
h_eff_hf_fine_pt1->Divide(htht_lab_gen_hf_fine);
h_eff_hh->Divide(htht_lab_gen_hh);
h_eff_hh_pt1->Divide(htht_lab_gen_hh);
/*
TH1F *htht_lab_eff = (TH1F*)h_eff_hh->Clone("htht_lab_eff");
htht_lab_eff->Add(h_eff_hf);
TH1F *htht_lumiCorr = (TH1F*)htht_lab_eff->Clone("htht_lumiCorr");
htht_lumiCorr->Multiply(h_said_lab);
*/
/*
    //Calculate efficiency
    Int_t FW_tht_min = htht_lab_gen->FindBin(0);
    Int_t FW_tht_max = htht_lab_gen->FindBin(7);
    Int_t HADES_tht_min = htht_lab_gen->FindBin(16);
    Int_t HADES_tht_max = htht_lab_gen->FindBin(88);
    Double_t FW_gen = htht_lab_gen->Integral(FW_tht_min, FW_tht_max);
    Double_t HADES_gen = htht_lab_gen->Integral(HADES_tht_min, HADES_tht_max);
    Double_t FW_rec = htht_lab_rec->Integral(FW_tht_min, FW_tht_max);
    Double_t FW_rec_pt1 = htht_lab_rec->Integral(FW_tht_min, FW_tht_max);
    Double_t HADES_rec = htht_lab_rec->Integral(HADES_tht_min, HADES_tht_max);
    Double_t HADES_rec_pt1 = htht_lab_rec->Integral(HADES_tht_min, HADES_tht_max);
    Double_t tot_fw_eff = htht_lab_rec_cut_hf_fine->Integral(htht_lab_rec_cut_hf_fine->FindBin(3),
   htht_lab_rec_cut_hf_fine->FindBin(3))/nEvents; Double_t tot_hades_eff =
   htht_lab_rec_cut_hh->Integral(htht_lab_rec_cut_hh->FindBin(18),
   htht_lab_rec_cut_hh->FindBin(40))/nEvents; Double_t tot_fw_eff_pt1 =
   htht_lab_rec_cut_hf_fine->Integral(htht_lab_rec_cut_hf_fine->FindBin(3),
   htht_lab_rec_cut_hf_fine->FindBin(3))/nEvents; Double_t tot_hades_eff_pt1 =
   htht_lab_rec_cut_hh->Integral(htht_lab_rec_cut_hh->FindBin(18),
   htht_lab_rec_cut_hh->FindBin(40))/nEvents;
*/
outfile->cd();
/*
    htht_cm_gen->Write();
    */
htht_cm_rec->Write();
htht_cm_rec_pt1->Write();

htht_lab_gen->Write();
htht_lab_rec->Write();
htht_lab_rec_cut->Write();
htht_lab_rec_cut_hf->Write();
htht_lab_rec_cut_hf_fine->Write();
htht_lab_rec_cut_hf_fine_pt1->Write();
htht_lab_rec_cut_hh->Write();
htht_lab_rec_cut_hh_pt1->Write();
/*
hcostht_cm_gen->Write();
*/
htht_lab_gen_hf->Write();
htht_lab_gen_hf_fine->Write();
htht_lab_gen_hh->Write();

hcostht_cm_rec->Write();
hptht_rec->Write();
hthttht_rec->Write();
hptht_rec_cuts->Write();
hthttht_rec_cuts->Write();
hcopl_hadfw->Write();
hcopl_hadhad->Write();
htantan_hadfw->Write();
htantan_hadfw_phicut->Write();
htantan_hadhad->Write();
hphi_fw->Write();
hphi_had->Write();
hphitht_rec->Write();
hphitht_rec_cuts->Write();
hphitht_gen->Write();
hnumtrackevt->Write();

h_eff_hh->Write();
h_eff_hf_fine->Write();
h_eff_hh_pt1->Write();
h_eff_hf_fine_pt1->Write();
// htht_lumiCorr->Write();

h_said_cm->Write();

outfile->Close();
/*
    lumiParFile->cd();
    htht_lumiCorr->Write();
    lumiParFile->Close();

    lumiParFile2->cd();
    htht_lab_eff->Write();
    h_said_lab->Write();
    lumiParFile2->Close();
*/
cout << "2p in Hades: " << counter_hadeshades << endl;
cout << "1p in Hades, 1 in FW: " << counter_hadesfw << endl;
cout << "2p in Hades, elastic: " << counter_hadeshades_elastic << endl;
cout << "1p in Hades, 1 in FW, elastic: " << counter_hadesfw_elastic << endl;
cout << "All p in Hades: " << counter_phades << endl;
cout << "All p in FW: " << counter_pfw << endl;
cout << "Generated p in Hades: " << HADES_gen << endl;
cout << "Generated p in FW: " << FW_gen << endl;
cout << "Generated 2p in Hades: " << counter_hadeshades_gen << endl;
cout << "Generated 1p in FW, 1p in HADES: " << counter_hadesfw_gen << endl;
cout << "Reco p in Hades: " << HADES_rec << endl;
cout << "Reco p in FW: " << FW_rec << endl;
cout << "Efficiency p in Hades: " << HADES_rec / HADES_gen << endl;
cout << "Efficiency p in FW: " << FW_rec / FW_gen << endl;
cout << "Efficiency 1p in FW, 1p in HADES: "
     << double(counter_hadesfw_elastic) / double(counter_hadesfw_gen) << endl;
cout << "Efficiency 2p in HADES: "
     << double(counter_hadeshades_elastic) / double(counter_hadeshades_gen) << endl;
cout << "Forward elastic/all generated elastic: " << tot_fw_eff << endl;
cout << "Hades elastic/all generated elastic: " << tot_hades_eff << endl;
// What use best for efficiency in HH?

return 0;

} // end of macro

// add main
#ifndef __CINT__
int main(int argc, char** argv)
{
    TROOT calcEfficiencies("calcEfficiencies", "compiled pp elastic efficiency calculation macro");

    if (argc == 1)
    {
        cout << "usage : " << argv[0] << " inputFiles" << endl;
        return 0;
    }

    HLoop* loop = new HLoop(kTRUE);

    Int_t optind = 1;
    if (optind < argc)
    {
        Bool_t ret;
        //              printf ("non-option ARGV-elements: ");
        while (optind < argc)
        {
            TString infile = argv[optind++];
            if (infile.Contains(","))
                ret = loop->addMultFiles(infile);
            else if (infile.Contains(".root"))
                ret = loop->addFiles(infile);
            else
                ret = loop->addFilesList(infile);

            if (!ret)
            {
                std::cerr << "READBACK: ERROR : cannot find inputfiles : " << infile.Data() << endl;
                std::exit(EXIT_FAILURE);
            }
        }
    }

    return lumiMonitor(loop); // Loop + lumiParFile
}
#endif
