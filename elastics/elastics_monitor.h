#ifndef LUMIMONITOR_H
#define LUMIMONITOR_H

#include <TString.h>

Int_t lumiMonitor(TString inFile, TString lumiPar, Int_t nEvents);

#endif /* LUMIMONITOR_H */
