#include "elastics_monitor.h"

#include "forward_tools.h"
#include "ft_config.h"

#include "hades.h"
#include "hcategorymanager.h"
#include "hloop.h"
#include "htool.h"

#include "hcategory.h"
#include "hforwardcand.h"
#include "hforwardcandsim.h"
#include "hlinearcategory.h"
#include "hparticlecand.h"
#include "hparticlecandsim.h"

#include "hgeantkine.h"
#include "hparticledef.h"

#include <hrpccal.h>
#include <hrpccalpar.h>
#include <hrpccluster.h>
#include <hrpcdigipar.h>
#include <hrpcgeompar.h>
#include <hrpchit.h>
#include <hrpcraw.h>
#include <htofhit.h>
#include <htofraw.h>
#include <rpcdef.h>
#include <tofdef.h>

#include "hparticlegeant.h"
#include "hparticlegeantdecay.h"
#include "hparticlegeantevent.h"

#include "TROOT.h"
#include "TTree.h"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <math.h>
#include <vector>

using namespace Particle;
using namespace ConfigBeamtime;

//----------------------------------------------------------------------------------------------
// Main function
//----------------------------------------------------------------------------------------------

// Int_t lumiMonitor(TString lumiPar, TString infileList, Int_t nEvents=-1){
Int_t lumiMonitor(HLoop* loop)
{
    // Int_t lumiMonitor(HLoop* loop, const AnaParameters& anapars){

    Int_t nEvents = -1;
    // Check if loop was properly initialized
    if (!loop->setInput(""))
    { // reading file structure
        std::cerr << "READBACK: ERROR : cannot read input !" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    TStopwatch timer;
    timer.Start();

    double deg2rad = TMath::DegToRad();

    // -----------------------------------------------------------------------
    // define output file and some histograms
    // -----------------------------------------------------------------------
    // set ouput file
    TFile* outfile = new TFile("pp_elastic_4200_feb21_efficiencies.root", "recreate");

    // TFile *said_lab = new
    // TFile("/lustre/hades/user/jrieger/pp_elastic/lumiMonitor/pp_elastic_4200_SAID_lab.root",
    // "read"); TH1F *h_said_cm = (TH1F *)said_lab->Get("hangle_cm");

    TH1F* htht_lab_rec_elcut = new TH1F("htht_lab_rec_elcut", "#theta_{p}", 120, 0, 90);
    htht_lab_rec_elcut->SetXTitle("#theta in deg");
    htht_lab_rec_elcut->GetXaxis()->SetTitleSize(0.05);
    htht_lab_rec_elcut->GetXaxis()->SetLabelSize(0.05);
    htht_lab_rec_elcut->GetYaxis()->SetTitleSize(0.05);
    htht_lab_rec_elcut->GetYaxis()->SetLabelSize(0.05);
    TH1F* htht_lab_rec_elcut_trig = (TH1F*)htht_lab_rec_elcut->Clone("htht_lab_rec_elcut_trig");

    TH1F* htht_lab_rec_elcut_hf = (TH1F*)htht_lab_rec_elcut->Clone("htht_lab_rec_elcut_hf");
    htht_lab_rec_elcut_hf->GetXaxis()->Set(50, 2, 7);
    TH1F* htht_lab_rec_elcut_hf_trig =
        (TH1F*)htht_lab_rec_elcut_hf->Clone("htht_lab_rec_elcut_hf_trig");
    TH1F* htht_lab_rec_elcut_hf_data =
        (TH1F*)htht_lab_rec_elcut_hf->Clone("htht_lab_rec_elcut_hf_data");
    TH1F* htht_lab_rec_elcut_hf_trig_data =
        (TH1F*)htht_lab_rec_elcut_hf->Clone("htht_lab_rec_elcut_hf_trig_data");

    TH1F* htht_lab_rec_elcut_hh = (TH1F*)htht_lab_rec_elcut->Clone("htht_lab_rec_elcut_hh");
    TH1F* htht_lab_rec_elcut_hh_trig =
        (TH1F*)htht_lab_rec_elcut_hh->Clone("htht_lab_rec_elcut_hh_trig");
    TH1F* htht_lab_rec_elcut_hh_data =
        (TH1F*)htht_lab_rec_elcut->Clone("htht_lab_rec_elcut_hh_data");
    TH1F* htht_lab_rec_elcut_hh_trig_data =
        (TH1F*)htht_lab_rec_elcut_hh->Clone("htht_lab_rec_elcut_hh_trig_data");

    TH1F* htht_lab_gen = (TH1F*)htht_lab_rec_elcut->Clone("htht_lab_gen");
    TH1F* htht_lab_gen_hf = (TH1F*)htht_lab_gen->Clone("htht_lab_gen_hf");
    htht_lab_gen_hf->SetBins(50, 2, 7);
    TH1F* htht_lab_gen_hh = (TH1F*)htht_lab_gen->Clone("htht_lab_gen_hh");

    TFile* efficiencies = new TFile(
        "/lustre/hades/user/jrieger/forward_tools_test/pp_elastic_4200_feb21_efficiencies.root",
        "read");
    TH1F* h_eff_hh = (TH1F*)efficiencies->Get("h_eff_hh");
    TH1F* h_eff_hf = (TH1F*)efficiencies->Get("h_eff_hf");
    TH1F* h_eff_hh_trig = (TH1F*)efficiencies->Get("h_eff_hh_trig");
    TH1F* h_eff_hf_trig = (TH1F*)efficiencies->Get("h_eff_hf_trig");

    // output file
    std::ofstream outtxt;

    // -----------------------------------------------------------------------

    // select categories here
    if (!loop->setInput(
            "-*,+HParticleCand,+HForwardCand,+HParticleCandSim,+HGeantKine,+HForwardCandSim"))
    {
        cout << "READBACK: ERROR : cannot read input !" << endl;
        exit(1);
    } // read all categories

    loop->printCategories();
    loop->printChain();

    HCategory* catParticleSim = loop->getCategory("HParticleCandSim");
    if (!catParticleSim)
    {
        std::cout << "No particleCatSim in input!" << std::endl;
        exit(1);
    }
    HCategory* catGeant = loop->getCategory("HGeantKine");
    if (!catGeant)
    {
        std::cout << "No kineCatSim in input!" << std::endl;
        exit(1);
    }
    HCategory* catFwParticleSim = loop->getCategory("HForwardCandSim");
    if (!catFwParticleSim)
    {
        std::cout << "No FWparticleCatSim in input!" << std::endl;
        exit(1);
    }
    HCategory* catParticle = loop->getCategory("HParticleCand");
    if (!catParticle)
    {
        std::cout << "No particleCat in input!" << std::endl;
        exit(1);
    }
    HCategory* catFwParticle = loop->getCategory("HForwardCand");
    if (!catFwParticle)
    {
        std::cout << "No FWparticleCat in input!" << std::endl;
        exit(1);
    }

    // Categories for PT1 trigger
    HCategory* rpcCal = nullptr;
    rpcCal = HCategoryManager::getCategory(catRpcCal, kTRUE, "catRpcCal");
    if (!rpcCal) { cout << "No catRpcCal!" << endl; }

    HCategory* tofRaw = nullptr;
    tofRaw = HCategoryManager::getCategory(catTofRaw, kTRUE, "catTofRaw");
    if (!tofRaw) { cout << "No catTofRaw!" << endl; }

    // Variable definitions
    Int_t t_PT1;
    Int_t pp_elastic_counter_hf = 0;
    Int_t pp_elastic_counter_hh = 0;
    Int_t pp_elastic_counter_hf_trig = 0;
    Int_t pp_elastic_counter_hh_trig = 0;
    Int_t pp_elastic_counter_hf_data = 0;
    Int_t pp_elastic_counter_hh_data = 0;
    Int_t pp_elastic_counter_hf_trig_data = 0;
    Int_t pp_elastic_counter_hh_trig_data = 0;

    Int_t entries = loop->getEntries();
    if (nEvents > entries || nEvents <= 0) nEvents = entries;

    cout << "events: " << nEvents << endl;

    Double_t mp = 938.2720813;
    TLorentzVector ppSystem(0, 0, 5051.8794, 2 * mp + 4200);
    /*
        // start of the event loop -- efficiency calculation
        for(Int_t i=1; i<nEvents; i++){
            //----------break if last event is reached-------------
            if(loop->nextEvent(i) <= 0) { cout<<" end recieved "<<endl; break; } // last event
       reached HTool::printProgress(i,nEvents,1,"Analysing evt# :");

            //Manual PT1 Trigger:
            t_PT1 = 0;
            //if (anapars.sim == 1){
                HRpcCal *rpc = nullptr;
                HTofRaw *tof = nullptr;

                int tof_ev=tofRaw->getEntries();
                int rpc_ev=rpcCal->getEntries();

                int tofhit_count = 0;
                int rpcraw_count = 0;
                int tofhit = 0;
                int rpchit = 0;

                if(tof_ev){
                    for (int i=0; i<tof_ev; i++){

                        tof = HCategoryManager::getObject(tof, catTofRaw, i);

                        if (tof->getRightTime()>0.)
                        {
                            //TofRightMult[(Int_t)tof->getSector()]++;
                            tofhit_count++;
                        }
                        if (tof->getLeftTime()>0.)
                        {
                            //TofLeftMult[(Int_t)tof->getSector()]++;
                            tofhit_count++;
                        }

                        tofhit++;

                        //cout<<event<<" "<<i<<" "<<sec<<endl;
                    }
                }//tof_ev
                //*********************************************************
                if(rpc_ev){
                    for(int i=0; i<rpc_ev; i++){
                        rpc = HCategoryManager::getObject(rpc, catRpcCal, i);

                        if (rpc->getRightTime()>0.)
                        {
                            //RpcRightMult[(Int_t)rpc->getSector()]++;
                            rpcraw_count++;
                        }
                        if (rpc->getLeftTime()>0.)
                        {
                            //RpcLeftMult[(Int_t)rpc->getSector()]++;
                            rpcraw_count++;
                        }

                        rpchit++;
                        //cout<<event<<" "<<i<<" "<<sec<<endl;
                    }
                }//rpc_ev

                int tof_mult = int(tofhit_count/2);
                int rpc_mult = int(rpcraw_count/2);

                int trigM2 = rpc_mult+tof_mult;

                if(trigM2>=2){
                    //num_Events_PT1++;
                    //is_pt1 = true;
                    cout<<t_PT1<<endl;
                    t_PT1 = 1;
                }
            //}

            auto has_hh_elastic = ForwardTools::Elastics::check_elastics_hh(catParticle,
                                                                            cut_HH_phi_diff_min,
                                                                            cut_HH_phi_diff_max,
                                                                            cut_HH_tan_theta_min,
                                                                            cut_HH_tan_theta_max);

            auto has_hf_elastic = ForwardTools::Elastics::check_elastics_hf(catParticle,
       catFwParticle, cut_HF_phi_diff_min, cut_HF_phi_diff_max, cut_HF_tan_theta_min,
                                                                            cut_HF_tan_theta_max);

            //if(has_hh_elastic || has_hf_elastic) num_Events_Total_Elastic =
       num_Events_Total_Elastic + 1;

            if(has_hh_elastic)
            {
                pp_elastic_counter_hh++;
                if(t_PT1 == 1) pp_elastic_counter_hh_trig++;

                // Particle 1 - HADES particle
                HParticleCand* cand1 = HCategoryManager::getObject(cand1, catParticle, 0);
                // Particle 2 - HADES particle
                HParticleCand* cand2 = HCategoryManager::getObject(cand2, catParticle, 1);

                htht_lab_rec_elcut->Fill(cand1->getTheta());
                htht_lab_rec_elcut->Fill(cand2->getTheta());
                htht_lab_rec_elcut_hh->Fill(cand1->getTheta());
                htht_lab_rec_elcut_hh->Fill(cand2->getTheta());
                if(t_PT1 == 1){
                    htht_lab_rec_elcut_trig->Fill(cand1->getTheta());
                    htht_lab_rec_elcut_trig->Fill(cand2->getTheta());
                    htht_lab_rec_elcut_hh_trig->Fill(cand1->getTheta());
                    htht_lab_rec_elcut_hh_trig->Fill(cand2->getTheta());
                }
            }

            if(has_hf_elastic)
            {
                pp_elastic_counter_hf++;
                if(t_PT1 == 1) pp_elastic_counter_hf_trig++;

                // Particle 1 - HADES particle
                HParticleCand* cand1 = HCategoryManager::getObject(cand1, catParticle, 0);
                // Particle 2 - FwDet Particle
                HForwardCand* cand2 = HCategoryManager::getObject(cand2, catFwParticle, 0);
                Double_t t_z_start_P2 = cand2->getZ(); // [mm]
                // Correction for track start position (Z_START)
                cand2->setStartXYZ(0.0,0.0,t_z_start_P2); // We take the track start position from
       the reconstructed HADES track paired with FwDet track cand2->calcPoints(); // Recalculating
       ToF and Distance to account for correction cand2->calc4vectorProperties(proton_mass); //
       Calculating 4 vector properties assuming particles are protons

                htht_lab_rec_elcut->Fill(cand1->getTheta());
                htht_lab_rec_elcut->Fill(cand2->getTheta());
                htht_lab_rec_elcut_hf->Fill(cand2->getTheta());
                if(t_PT1 == 1){
                    htht_lab_rec_elcut_trig->Fill(cand1->getTheta());
                    htht_lab_rec_elcut_trig->Fill(cand2->getTheta());
                    htht_lab_rec_elcut_hf_trig->Fill(cand2->getTheta());
                }
            }

            //GEANT events
            Int_t ntracks_gen = catGeant->getEntries();
            std::vector<HGeantKine*> protons_gen;

            for(Int_t j=0; j<ntracks_gen; j++){
                HGeantKine *cand = HCategoryManager::getObject(cand,catGeant,j);

                if (cand->getID()==14 && cand->getParentTrack() == 0) {
                    protons_gen.push_back(cand);
                    htht_lab_gen->Fill(cand->getThetaDeg());
                }
            } // end of geant loop

            Double_t theta1 = protons_gen[0]->getThetaDeg();
            Double_t theta2 = protons_gen[1]->getThetaDeg();
            if(theta1<88 && theta1>16 && theta2<88 && theta2>16){
                //counter_hadeshades_gen++;
                htht_lab_gen_hh->Fill(theta1);
                htht_lab_gen_hh->Fill(theta2);
            } else if((theta1<88 && theta1>16 && theta2<7 && theta2>0) || (theta1<7 && theta1>0 &&
       theta2<88 && theta2>16)){
                //counter_hadesfw_gen++;
                htht_lab_gen_hf->Fill(theta1);
                htht_lab_gen_hf->Fill(theta2);
            }

        } // end of event loop

        cout<<"Loop finished "<<endl;

        //Calculate efficiencies
        TH1F *h_eff_hh = (TH1F*)htht_lab_rec_elcut_hh->Clone("h_eff_hh");
        TH1F *h_eff_hh_trig = (TH1F*)htht_lab_rec_elcut_hh_trig->Clone("h_eff_hh_trig");
        TH1F *h_eff_hf = (TH1F*)htht_lab_rec_elcut_hf->Clone("h_eff_hf");
        TH1F *h_eff_hf_trig = (TH1F*)htht_lab_rec_elcut_hf_trig->Clone("h_eff_hf_trig");
        h_eff_hf -> Divide(htht_lab_gen_hf);
        h_eff_hf_trig -> Divide(htht_lab_gen_hf);
        h_eff_hh -> Divide(htht_lab_gen_hh);
        h_eff_hh_trig -> Divide(htht_lab_gen_hh);

        cout<<"Efficiencies calculated "<<endl;

        outfile->cd();

        htht_lab_rec_elcut->Write();
        htht_lab_rec_elcut_trig->Write();
        htht_lab_rec_elcut_hf->Write();
        htht_lab_rec_elcut_hf_trig->Write();
        htht_lab_rec_elcut_hh->Write();
        htht_lab_rec_elcut_hh_trig->Write();

        htht_lab_gen->Write();
        htht_lab_gen_hf->Write();
        htht_lab_gen_hh->Write();

        h_eff_hh->Write();
        h_eff_hf->Write();
        h_eff_hh_trig->Write();
        h_eff_hf_trig->Write();
        //h_said_cm->Write();

        outfile->Close();

        cout<<"Outfile eff written "<<endl;
        */

    // start of the event loop -- data events counting
    // nEvents = 10000;
    for (Int_t i = 1; i < nEvents; i++)
    {
        // cout<<"Loop started "<<endl;
        //----------break if last event is reached-------------
        if (loop->nextEvent(i) <= 0)
        {
            cout << " end recieved " << endl;
            break;
        } // last event reached
        HTool::printProgress(i, nEvents, 1, "Analysing evt# :");

        // Manual PT1 Trigger:
        t_PT1 = 0;
        // if (anapars.sim == 1){
        HRpcCal* rpc = nullptr;
        HTofRaw* tof = nullptr;

        int tof_ev = tofRaw->getEntries();
        int rpc_ev = rpcCal->getEntries();

        int tofhit_count = 0;
        int rpcraw_count = 0;
        int tofhit = 0;
        int rpchit = 0;

        if (tof_ev)
        {
            for (int i = 0; i < tof_ev; i++)
            {

                tof = HCategoryManager::getObject(tof, catTofRaw, i);

                if (tof->getRightTime() > 0.)
                {
                    // TofRightMult[(Int_t)tof->getSector()]++;
                    tofhit_count++;
                }
                if (tof->getLeftTime() > 0.)
                {
                    // TofLeftMult[(Int_t)tof->getSector()]++;
                    tofhit_count++;
                }

                tofhit++;

                // cout<<event<<" "<<i<<" "<<sec<<endl;
            }
        } // tof_ev
        //*********************************************************
        if (rpc_ev)
        {
            for (int i = 0; i < rpc_ev; i++)
            {
                rpc = HCategoryManager::getObject(rpc, catRpcCal, i);

                if (rpc->getRightTime() > 0.)
                {
                    // RpcRightMult[(Int_t)rpc->getSector()]++;
                    rpcraw_count++;
                }
                if (rpc->getLeftTime() > 0.)
                {
                    // RpcLeftMult[(Int_t)rpc->getSector()]++;
                    rpcraw_count++;
                }

                rpchit++;
                // cout<<event<<" "<<i<<" "<<sec<<endl;
            }
        } // rpc_ev

        int tof_mult = int(tofhit_count / 2);
        int rpc_mult = int(rpcraw_count / 2);

        int trigM2 = rpc_mult + tof_mult;

        if (trigM2 >= 2)
        {
            // num_Events_PT1++;
            // is_pt1 = true;
            t_PT1 = 1;
        }
        //}

        auto has_hh_elastic = ForwardTools::Elastics::check_elastics_hh(
            catParticle, cut_HH_phi_diff_min, cut_HH_phi_diff_max, cut_HH_tan_theta_min,
            cut_HH_tan_theta_max);

        auto has_hf_elastic = ForwardTools::Elastics::check_elastics_hf(
            catParticle, catFwParticle, cut_HF_phi_diff_min, cut_HF_phi_diff_max,
            cut_HF_tan_theta_min, cut_HF_tan_theta_max);

        // if(has_hh_elastic || has_hf_elastic) num_Events_Total_Elastic = num_Events_Total_Elastic
        // + 1;

        if (has_hh_elastic)
        {
            pp_elastic_counter_hh_data++;
            if (t_PT1 == 1) pp_elastic_counter_hh_trig_data++;

            // Particle 1 - HADES particle
            HParticleCand* cand1 = HCategoryManager::getObject(cand1, catParticle, 0);
            // Particle 2 - HADES particle
            HParticleCand* cand2 = HCategoryManager::getObject(cand2, catParticle, 1);

            htht_lab_rec_elcut_hh_data->Fill(cand1->getTheta());
            htht_lab_rec_elcut_hh_data->Fill(cand2->getTheta());
            if (t_PT1 == 1)
            {
                htht_lab_rec_elcut_hh_trig_data->Fill(cand1->getTheta());
                htht_lab_rec_elcut_hh_trig_data->Fill(cand2->getTheta());
            }
        }

        if (has_hf_elastic)
        {
            pp_elastic_counter_hf_data++;
            if (t_PT1 == 1) pp_elastic_counter_hf_trig_data++;

            // Particle 1 - HADES particle
            HParticleCand* cand1 = HCategoryManager::getObject(cand1, catParticle, 0);
            // Particle 2 - FwDet Particle
            HForwardCand* cand2 = HCategoryManager::getObject(cand2, catFwParticle, 0);
            Double_t t_z_start_P2 = cand2->getZ(); // [mm]
            // Correction for track start position (Z_START)
            cand2->setStartXYZ(0.0, 0.0,
                               t_z_start_P2); // We take the track start position from the
                                              // reconstructed HADES track paired with FwDet track
            cand2->calcPoints(); // Recalculating ToF and Distance to account for correction
            cand2->calc4vectorProperties(
                proton_mass); // Calculating 4 vector properties assuming particles are protons

            htht_lab_rec_elcut_hf_data->Fill(cand2->getTheta());
            if (t_PT1 == 1) { htht_lab_rec_elcut_hf_trig_data->Fill(cand2->getTheta()); }
        }

        /*
        // for each event there are a number of tracks
        Int_t ntracks_rec = catParticle->getEntries();
        Int_t nFwTracks = catFwParticle->getEntries();

        std::vector<HParticleCand*> protons;
        std::vector<HForwardCand*> protons_fw;

        for (Int_t j = 0; j < ntracks_rec; j++)
        {
            HParticleCand* cand = HCategoryManager::getObject(cand, catParticle, j);
            // skip ghost tracks (only avalible for MC events)
            //if (cand->isGhostTrack()) continue;
            // select "good" tracks
            //if (!cand->isFlagBit(Particle::kIsUsed)) continue;

            protons.push_back(cand);
        } // end of had track loop

        for(Int_t j=0; j<nFwTracks; j++){
            HForwardCandSim *cand = HCategoryManager::getObject(cand,catFwParticle,j);

            protons_fw.push_back(cand);
        } // end of fw track loop

        for (size_t k = 0; k < protons.size(); k++){
                HParticleCand *cand1 = protons[k];
            cand1->calc4vectorProperties(938.272);
            for (size_t l = k+1; l < protons.size(); l++){
                    HParticleCand *cand2 = protons[l];
                cand2->calc4vectorProperties(938.272);
                Double_t deltaphi = abs(cand1->getPhi() - cand2->getPhi());
                Double_t tantht = tan(cand1->Theta())*tan(cand2->Theta());
                 if ((deltaphi>178.5 && deltaphi < 181.5) && (tantht>(0.3088-0.015) &&
        tantht<(0.3088+0.015))) { pp_elastic_counter_hh++;
                    htht_lab_rec_elcut->Fill(cand1->getTheta());
                    htht_lab_rec_elcut->Fill(cand2->getTheta());
                    htht_lab_rec_elcut_hh->Fill(cand1->getTheta());
                    htht_lab_rec_elcut_hh->Fill(cand2->getTheta());
                }
            }
            for (size_t l = 0; l < protons_fw.size(); l++){
                    HForwardCand *cand2 = protons_fw[l];
                cand2->calc4vectorProperties(938.272);
                Double_t deltaphi = abs(cand1->getPhi() - cand2->getPhi());
                Double_t tantht = tan(cand1->Theta())*tan(cand2->Theta());
                if ((deltaphi>173 && deltaphi < 187) && (tantht>(0.3088-0.1) &&
        tantht<(0.3088+0.1))) { pp_elastic_counter_hf++;
                    htht_lab_rec_elcut->Fill(cand1->getTheta());
                    htht_lab_rec_elcut->Fill(cand2->getTheta());
                    htht_lab_rec_elcut_hf->Fill(cand2->getTheta());
                }
            }
        }*/

    } // end of event loop

    // htht_lab_rec_elcut->Draw();
    // htht_lab_rec_elcut->Divide(h_lumiPar);
    // htht_lab_rec_elcut->Divide(h_eff);
    htht_lab_rec_elcut_hf_data->Divide(h_eff_hf);
    htht_lab_rec_elcut_hh_data->Divide(h_eff_hh);
    htht_lab_rec_elcut_hf_trig_data->Divide(h_eff_hf_trig);
    htht_lab_rec_elcut_hh_trig_data->Divide(h_eff_hh_trig);
    Double_t nel_hf = htht_lab_rec_elcut_hf_data->Integral(
        h_eff_hf->FindBin(3), h_eff_hf->FindBin(5.5)); // *500000/206808; (for total cross section)
    Double_t nel_hh = htht_lab_rec_elcut_hh_data->Integral(
        h_eff_hh->FindBin(19), h_eff_hh->FindBin(44)); // *500000/3531; (for total cross section)
    Double_t nel_hf_trig =
        htht_lab_rec_elcut_hf_trig_data->Integral(h_eff_hf->FindBin(3), h_eff_hf->FindBin(5.5));
    Double_t nel_hh_trig =
        htht_lab_rec_elcut_hh_trig_data->Integral(h_eff_hh->FindBin(19), h_eff_hh->FindBin(44));

    Double_t sigma_hf = 5.11004; // check
    Double_t sigma_hh = 0.0829282;

    Double_t lumi_hf = nel_hf / sigma_hf;
    Double_t lumi_hh = nel_hh / sigma_hh;
    Double_t lumi_hf_trig = nel_hf_trig / sigma_hf;
    Double_t lumi_hh_trig = nel_hh_trig / sigma_hh;

    cout << "2p in Hades: " << pp_elastic_counter_hh_data << endl;
    cout << "1p in Hades, 1 in FW: " << pp_elastic_counter_hf_data << endl;
    cout << "N HF: " << nel_hf << endl;
    cout << "N HH: " << nel_hh << endl;
    cout << "Lumi HF: " << lumi_hf << endl;
    cout << "Lumi HH: " << lumi_hh << endl;
    cout << "2p in Hades, trigger: " << pp_elastic_counter_hh_trig_data << endl;
    cout << "1p in Hades, 1 in FW, trigger: " << pp_elastic_counter_hf_trig_data << endl;
    cout << "N HF, trigger: " << nel_hf_trig << endl;
    cout << "N HH, trigger: " << nel_hh_trig << endl;
    cout << "Lumi HF, trigger: " << lumi_hf_trig << endl;
    cout << "Lumi HH, trigger: " << lumi_hh_trig << endl;

    // Write line to .txt file with "start_time stop_time nEvents nElastic luminosity"
    outtxt.open("lumi.txt", std::ios_base::app);
    outtxt << "0    0   " << nEvents << "   "
           << pp_elastic_counter_hh_data + pp_elastic_counter_hf_data << "    " << lumi_hf << "    "
           << lumi_hh << std::endl;

    return 0;
}
/*
//add main
#ifndef __CINT__
int main(int argc, char **argv)
{
    TROOT LumiMonitor("LumiMonitor","compiled lumiMonitor macro");

    TString nevents;
    switch (argc)
    {
    case 3:
         return lumiMonitor(TString(argv[1]),TString(argv[2])); // inputfile + lumiParFile
        break;

    case 4:  // inputfile + lumiParFile + nevents
        nevents=argv[3];
        return lumiMonitor(TString(argv[1]),TString(argv[2]), nevents.Atoi());
        break;

    default:
        cout<<"usage : "<<argv[0]<<" inputfile lumiParameters [nevents]"<<endl;
        return 0;
    }
}
#endif
*/

// add main
#ifndef __CINT__
int main(int argc, char** argv)
{
    TROOT LumiMonitor("LumiMonitor", "compiled lumiMonitor macro");

    if (argc == 1)
    {
        // cout<<"usage : "<<argv[0]<<" lumiParFile inputFiles"<<endl;
        cout << "usage : " << argv[0] << " inputFiles" << endl;
        return 0;
    }

    HLoop* loop = new HLoop(kTRUE);

    // Int_t optind = 2;
    Int_t optind = 1;
    if (optind < argc)
    {
        Bool_t ret;
        //              printf ("non-option ARGV-elements: ");
        while (optind < argc)
        {
            TString infile = argv[optind++];
            if (infile.Contains(","))
                ret = loop->addMultFiles(infile);
            else if (infile.Contains(".root"))
                ret = loop->addFiles(infile);
            else
                ret = loop->addFilesList(infile);

            if (!ret)
            {
                std::cerr << "READBACK: ERROR : cannot find inputfiles : " << infile.Data() << endl;
                std::exit(EXIT_FAILURE);
            }
        }
    }

    // return lumiMonitor(loop,TString(argv[1])); // Loop + lumiParFile
    return lumiMonitor(loop); // Loop
}
#endif
