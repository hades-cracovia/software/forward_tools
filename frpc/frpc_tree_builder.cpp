#include "forward_tools.h"
#include "ft_config.h"

#include <hades.h>
#include <hloop.h>
#include <htaskset.h>

#include <hcategory.h>
#include <hcategorymanager.h>
#include <hrecevent.h>
#include <hreconstructor.h>
#include <hruntimedb.h>

//--------category definitions---------
#include <hgeantdef.h>
#include <hparticledef.h>
#include <hpiontrackerdef.h>
#include <hstartdef.h>
//-------------------------------------

//-------objects-----------------------
#include <heventheader.h>
#include <hgeantkine.h>
#include <hparticlecand.h>
#include <hparticlecandsim.h>
#include <hparticleevtinfo.h>
#include <hparticletracksorter.h>
#include <hpiontrackertrack.h>
#include <hstart2hit.h>

#include <hstart2cal.h>
//-------------------------------------
#include <hparticletool.h>
#include <hphysicsconstants.h>

#include <henergylosscorrpar.h>

#include <forwarddef.h>
#include <stsdef.h>
#include <frpcdef.h>
#include <hdst.h>
#include <hforwardcand.h>
#include <hfrpccal.h>
#include <hfrpccalpar.h>
#include <hfrpccluster.h>
#include <hfrpcdigipar.h>
#include <hfrpcgeompar.h>
#include <hfrpchit.h>
#include <hfrpcraw.h>
#include <hgeantfrpc.h>
#include <hgeantsts.h>
#include <hgeomcompositevolume.h>
#include <hgeomvector.h>
#include <hgeomvolume.h>
#include <hparasciifileio.h>
#include <hparticlecand.h>
#include <hrpccal.h>
#include <hrpccalpar.h>
#include <hrpccluster.h>
#include <hrpcdigipar.h>
#include <hrpcgeompar.h>
#include <hrpchit.h>
#include <hrpcraw.h>
#include <hspectrometer.h>
#include <hstart2hit.h>
#include <hstscal.h>
#include <hstsgeompar.h>
#include <hstsraw.h>
#include <htofhit.h>
#include <htofraw.h>
#include <rpcdef.h>
#include <tofdef.h>

#include <TCanvas.h>
#include <TColor.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1I.h>
#include <TH2F.h>
#include <TH2I.h>
#include <TH3I.h>
#include <THStack.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TMatrixD.h>
#include <TMultiGraph.h>
#include <TROOT.h>
#include <TStopwatch.h>
#include <TString.h>
#include <TStyle.h>
#include <TTree.h>
#include <TVector3.h>

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <string>
#include <vector>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

using namespace std;
using namespace ConfigBeamtime;

//##############################################################################################
// Usefull functions and definitions
//##############################################################################################

//----------------------------------------------------------------------------------------------
// Defining parameters
//----------------------------------------------------------------------------------------------

typedef void (*pad_callback)(TVirtualPad* pad);

struct AnaParameters
{
    TString outfile;
    TString outpath;
    TString paramfile;
    std::string label;
    int events;
    int start;
    int sim;
    int verbose;
    bool show_fakes;
    int cal_frpc_stage1{0};
    int cal_frpc_stage2{0};
    int cal_frpc_stage3{0};
    int cal_frpc_stage4{0};
    int cal_frpc_stage5{0};
    int pt1{0};
    int pt2{0};
    int pt3{0};
    int T0_cond{0};
    int alt_ToF{0};
};

const Float_t avg_beam_rate = 400e3; // 400 kHz

int no_fStart2Hit_cnt = 0;
int rejected_no_T0_cnt = 0;

//----------------------------------------------------------------------------------------------
// Functions for drawing stuff
//----------------------------------------------------------------------------------------------

TH1* format_h_3(TH1* h)
{
    h->SetTitle("");
    h->GetXaxis()->SetLabelSize(0.06);
    h->GetXaxis()->SetTitleSize(0.06);
    h->GetXaxis()->SetTitleOffset(0.95);
    h->GetXaxis()->SetNdivisions(505);
    h->GetYaxis()->SetLabelSize(0.06);
    h->GetYaxis()->SetTitleSize(0.06);
    h->GetYaxis()->SetTitleOffset(1.10);
    gPad->SetLeftMargin(0.15);
    gPad->SetTopMargin(0.05);
    gPad->SetRightMargin(0.12);
    gPad->SetBottomMargin(0.13);
    return h;
}

void frpc_draw_v_lines(TVirtualPad* pad)
{
    pad->cd();
    TLine line;
    line.SetNDC(1);
    line.DrawLine(0, 375, 32, 375);
    line.DrawLine(0, -375, 32, -375);
}

int rpc_pads_mapping[4] = {3, 0, 2, 1};

void rpc_draw_and_write(TCanvas* c, TH1** h, Int_t frpc_sectors, TDirectory* c_dir = nullptr,
                        TDirectory* h_dir = nullptr, const char* opts = "colz",
                        TGraph** gr = nullptr, pad_callback f = nullptr)
{
    for (Int_t i = 0; i < frpc_sectors; ++i)
    {
        c->cd(1 + i);
        if (h_dir) h_dir->cd();

        gPad->SetLeftMargin(0.2);
        gPad->SetBottomMargin(0.2);

        TH1* _h = h[rpc_pads_mapping[i]];
        _h->Draw(opts);
        format_h_3(_h);

        if (gr) gr[rpc_pads_mapping[i]]->Draw("same,P");

        if (f) (*f)(gPad);

        _h->Write();
    }

    if (c_dir) c_dir->cd();
    c->Write();
}

const Int_t metro[5] = {TColor::GetColor("#d11141"), TColor::GetColor("#00b159"),
                        TColor::GetColor("#00aedb"), TColor::GetColor("#f37735"),
                        TColor::GetColor("#ffc425")};

void rpc_draw_stack_and_write(TCanvas* c, THStack* s, TH1* h[], int sectors,
                              TDirectory* c_dir = nullptr, TDirectory* h_dir = nullptr,
                              const char* opts = "colz")
{
    char buff[200];
    c->cd();
    TLegend* leg = new TLegend(0.5, 0.5, 0.9, 0.9);
    for (int i = 0; i < sectors; ++i)
    {
        s->Add(h[i]);
        h[i]->SetLineColor(metro[i]);
        h[i]->SetLineWidth(2);
        sprintf(buff, "Sector %d", i);
        leg->AddEntry(h[i], buff, "lpf");
        if (h_dir) h_dir->cd();
        h[i]->Write();
    }
    if (c_dir) c_dir->cd();
    c->cd();
    s->Draw(opts);
    leg->Draw();
    c->Write();
}

TString format_name(TString path, TString name) { return path + "PICTURES/" + name + ".png"; }

//----------------------------------------------------------------------------------------------
// Drawing a red cross at specified point of a histogram
void draw_point(Float_t point_X, Float_t point_Y, Float_t point_Size)
{
    TLine line;
    //line.SetNDC(1);
    //line.SetLineStyle(9);
    line.SetLineWidth(2);
    line.SetLineColor(kRed);
    line.DrawLine(point_X-point_Size, point_Y-point_Size, point_X+point_Size, point_Y+point_Size);
    line.DrawLine(point_X+point_Size, point_Y-point_Size, point_X-point_Size, point_Y+point_Size);
}

//##############################################################################################
// fwdet_tests function
//##############################################################################################

Int_t fwdet_tests(HLoop* loop, const AnaParameters& anapars)
{
    // Check if loop was properly initialized
    // if (!loop->setInput(""))
    if (!loop->setInput("-*,+HParticleCand,+HForwardCand,+HFRpcRaw,+HFRpcCal,+HStart2Hit,+HRpcCal,+"
                        "HTofRaw")) // ,+HFRpcHit,+HFRpcClus
    {                               // reading file structure
        std::cerr << "READBACK: ERROR : cannot read input !" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    // Timer for checking analysis time
    TStopwatch timer;
    timer.Reset(); // Reset timer
    timer.Start(); // Start timer (T0)

    //////////////////////////////////////////////////////////////////////////////
    //      Fast tree builder for creating of ntuples                            //
    //////////////////////////////////////////////////////////////////////////////

    // Hades *myHades = new Hades;
    // myHades->setTreeBufferSize(8000);
    // HSpectrometer* spec = gHades->getSetup();

    //----------------------------------------------------------------------------------------------
    // Note on how to save terminal output to a txt file
    //----------------------------------------------------------------------------------------------

    // Add " | tee fRPC_progress.txt " to the end of the command executing the program to save all
    // the output to screen into a txt file Alternatively " 2>&1 | tee fRPC_progress.txt " to
    // capture also error output

    //----------------------------------------------------------------------------------------------
    // Input parameters file
    //----------------------------------------------------------------------------------------------

    cout << "Input parameters file ... " << endl;

    Int_t mdcMods[6][4] = {{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1},
                           {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};
    HDst::setupSpectrometer("feb22", mdcMods, "sts,frpc");

    TString asciiParFile = anapars.paramfile;  // File containing all parameters of the detector
    HRuntimeDb* rtdb = HRuntimeDb::instance(); // myHades -> getRuntimeDb();

    // Checking if parameters file was opened properly
    if (!asciiParFile.IsNull())
    {
        HParAsciiFileIo* input1 = new HParAsciiFileIo;
        input1->open(asciiParFile.Data(), "in");
        if (!input1->check())
        {
            std::cerr << "Param file " << asciiParFile << " not open!" << std::endl;
            abort();
        }
        rtdb->setFirstInput(input1);
    }

    // Initializing the parameter container for geometry
    HStsGeomPar* pStrawGeomPar = (HStsGeomPar*)gHades->getRuntimeDb()->getContainer("StsGeomPar");
    // Checking if parameters container for geometry was created properly
    if (!pStrawGeomPar)
    {
        Error("HStsDigitizer::init()", "Parameter container for geometry not created");
        return kFALSE;
    }

    // Initializing the parameter container for geometry
    HFRpcGeomPar* pFRpcGeomPar = (HFRpcGeomPar*)gHades->getRuntimeDb()->getContainer("FRpcGeomPar");
    // Checking if parameters container for geometry was created properly
    if (!pFRpcGeomPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for geometry not created");
        return kFALSE;
    }

    HFRpcDigiPar* pFRpcDigiPar = (HFRpcDigiPar*)gHades->getRuntimeDb()->getContainer("FRpcDigiPar");
    if (!pFRpcDigiPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for HFRpcDigiPar not created");
        return kFALSE;
    }

    HFRpcCalPar* pFRpcCalPar = (HFRpcCalPar*)gHades->getRuntimeDb()->getContainer("FRpcCalPar");
    if (!pFRpcCalPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for FRpcCalPar not created");
        return kFALSE;
    }

    rtdb->initContainers(0);
    rtdb->print();
    // pFRpcCalPar->printParams();

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Output parameters file
    //----------------------------------------------------------------------------------------------

    cout << "Output parameters file ... " << endl;

    HParAsciiFileIo* f_param_output = new HParAsciiFileIo;

    TString cal_file = anapars.outfile;
    cal_file.ReplaceAll(".root", ".txt");

    if (!f_param_output->open(anapars.outpath + cal_file, "out"))
    {
        std::cerr << "Output param file " << cal_file << " not open!" << std::endl;
        abort();
    }
    // rtdb->setOutput(f_param_output);

    // Print detector parameters on screen
    if (anapars.verbose)
    {
        printf("PARAMETERS LOADING\n");
        pStrawGeomPar->print();
        pFRpcGeomPar->print();
    }

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Accessing categories inside input file
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Accessing categories inside input file ... " << endl;

    // Hades Particle Candidates
    HCategory* fParticleCand =
        HCategoryManager::getCategory(catParticleCand, kTRUE, "catParticleCand");
    if (!fParticleCand) { cout << "No catParticleCand!" << endl; }

    // Forward Detector Candidates
    HCategory* fForwardCand =
        HCategoryManager::getCategory(catForwardCand, kTRUE, "catForwardCand");
    if (!fForwardCand) { cout << "No catForwardCand!" << endl; }

    // Forward Detector RPC Raw data
    HCategory* fFRpcRaw = HCategoryManager::getCategory(catFRpcRaw, kTRUE, "catFRpcRaw");
    if (!fFRpcRaw) { cout << "No catfFRpcRaw!" << endl; }

    // Forward Detector RPC Calibrated data
    HCategory* fFRpcCal = HCategoryManager::getCategory(catFRpcCal, kTRUE, "catFRpcCal");
    if (!fFRpcCal) { cout << "No catFRpcCal!" << endl; }

    // Forward Detector STS Calibrated data
    HCategory* fStsCal = HCategoryManager::getCategory(catStsCal, kTRUE, "catStsCal");
    if (!fStsCal) { cout << "No catStsCal!" << endl; }

    // hstart2hit
    HCategory* fStart2Hit = HCategoryManager::getCategory(catStart2Hit, kTRUE, "catStart2Hit");
    if (!fStart2Hit) { cout << "No catStart2Hit!" << endl; }

    // Categories for PT1 trigger
    HCategory* rpcCal = nullptr;
    rpcCal = HCategoryManager::getCategory(catRpcCal, kTRUE, "catRpcCal");
    if (!rpcCal) { cout << "No catRpcCal!" << endl; }

    HCategory* tofRaw = nullptr;
    tofRaw = HCategoryManager::getCategory(catTofRaw, kTRUE, "catTofRaw");
    if (!tofRaw) { cout << "No catTofRaw!" << endl; }

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Setting parameters for loop over events
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Setting parameters for loop over events ... " << endl;

    Int_t entries = loop->getEntries(); // Number of entries in loop
    int limit_sta = anapars.start;      // Limit START - Where to start the loop
    int limit_sto = 0;                  // Limit STOP - Where to stop the loop

    if (anapars.events >= 0)
        limit_sto = limit_sta + anapars.events;
    else
        limit_sto = entries;

    if (limit_sto > entries) limit_sto = entries;

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Output file and directories definition
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Output file and directories definition ... " << endl;

    // TFile *f_root_output_hists = new TFile("./Histograms_File.root","recreate");

    TFile* f_root_output = TFile::Open(anapars.outpath + anapars.outfile, "RECREATE");
    f_root_output->cd();

    TDirectory* dir_r_q = f_root_output->mkdir("R_RAW_Q");
    TDirectory* dir_r_cal_q = f_root_output->mkdir("R_CAL_Q");
    TDirectory* dir_r_raw = f_root_output->mkdir("R_RAW");
    TDirectory* dir_r_cal = f_root_output->mkdir("R_CAL");
    TDirectory* dir_out_frpc_cal = f_root_output->mkdir("fRPC_Cal");

    // Trigger specific histograms
    TDirectory* dir_out_all_triggers = f_root_output->mkdir("ALL_TRIGGERS");
    TDirectory* dir_out_PT1_trigger = f_root_output->mkdir("PT1_TRIGGER");
    TDirectory* dir_out_PT2_trigger = f_root_output->mkdir("PT2_TRIGGER");
    TDirectory* dir_out_PT3_trigger = f_root_output->mkdir("PT3_TRIGGER");

    // Walk Correction directories
    TDirectory* dir_out_frpc_cal_t_diff_vs_charge_tmp1 =
        f_root_output->mkdir("fRPC_Cal/STRIP_T_DIFF_VS_CHARGE_HISTS");
    TDirectory* dir_out_frpc_cal_t_diff_vs_charge_tmp2 =
        f_root_output->mkdir("fRPC_Cal/STRIP_T_DIFF_VS_CHARGE_CANS");

    TDirectory* dir_out_frpc_cal_t_diff_vs_charge_hists =
        dir_out_frpc_cal_t_diff_vs_charge_tmp1->GetDirectory("STRIP_T_DIFF_VS_CHARGE_HISTS");
    TDirectory* dir_out_frpc_cal_t_diff_vs_charge_cans =
        dir_out_frpc_cal_t_diff_vs_charge_tmp2->GetDirectory("STRIP_T_DIFF_VS_CHARGE_CANS");

    // 2D canvases directory
    TDirectory* dir_out_frpc_cal_tof_2D_tmp = f_root_output->mkdir("fRPC_Cal/TOF_2D");
    TDirectory* dir_out_frpc_cal_tof_2D = dir_out_frpc_cal_tof_2D_tmp->GetDirectory("TOF_2D");

    // ToF directories
    TDirectory* dir_out_frpc_cal_tof_tmp1 = f_root_output->mkdir("fRPC_Cal/STRIP_TOF_HISTS");
    TDirectory* dir_out_frpc_cal_tof_tmp2 = f_root_output->mkdir("fRPC_Cal/STRIP_TOF_CANS");
//     TDirectory* dir_out_frpc_cal_tof_tmp3 = f_root_output->mkdir("fRPC_Cal/STRIP_TEMP_HISTS");
    TDirectory* dir_out_frpc_cal_tof_tmp4 = f_root_output->mkdir("fRPC_Cal/fRPC_Hits");
    TDirectory* dir_out_frpc_cal_tof_tmp5 = f_root_output->mkdir("fRPC_Cal/STS_Hits");

    TDirectory* dir_out_frpc_cal_delta_tof_hists = dir_out_frpc_cal_tof_tmp1->GetDirectory("STRIP_TOF_HISTS");
    TDirectory* dir_out_frpc_cal_delta_tof_cans = dir_out_frpc_cal_tof_tmp2->GetDirectory("STRIP_TOF_CANS");
//     TDirectory* dir_out_frpc_cal_temp_hists = dir_out_frpc_cal_tof_tmp3->GetDirectory("STRIP_TEMP_HISTS");
    TDirectory* dir_out_frpc_cal_frpc_hits = dir_out_frpc_cal_tof_tmp4->GetDirectory("fRPC_Hits");
    TDirectory* dir_out_frpc_cal_sts_hits = dir_out_frpc_cal_tof_tmp5->GetDirectory("STS_Hits");

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //------------------------------------------------------------------------------------------------------
    // Important variables definition
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Important variables definition ... " << endl;

    char buff1[1000];
    char buff2[1000];

    // const Int_t stage = 4;
    const Int_t cal = 0; // 0 - pre-calibration, 1 - post-calibration

    Float_t HF_tof_range = 500.0;
    Float_t HF_tof_bins_per_ns = 1.0;
    Float_t HF_tof_bins = HF_tof_bins_per_ns * HF_tof_range;

//     Int_t HF_d_tof_bins = 100;
//     Float_t HF_d_tof_min = 250.0;
//     Float_t HF_d_tof_max = 350.0;

//     Float_t ToF_vs_Charge_Min = 270.0;
//     Float_t ToF_vs_Charge_Max = 330.0;
//     Float_t ToF_vs_Charge_Bins = HF_tof_bins_per_ns * (ToF_vs_Charge_Max - ToF_vs_Charge_Min);

    if (cal == 0)
    {
        HF_tof_range = 500.0;
        HF_tof_bins = HF_tof_bins_per_ns * HF_tof_range;
//         HF_d_tof_bins = 100;
//         HF_d_tof_min = 250.0;
//         HF_d_tof_max = 350.0;
    }
    else if (cal == 1)
    {
        HF_tof_range = 100.0;
        HF_tof_bins = HF_tof_bins_per_ns * HF_tof_range;
//         HF_d_tof_bins = 100;
//         HF_d_tof_min = -50.0;
//         HF_d_tof_max = 50.0;
    }

    Int_t t_PT1, t_PT2, t_PT3;
    Int_t t_Sector_P1, t_Sector_P2, t_Sector_Pair;
    Int_t t_T0, t_ToF_Rec, t_fRPC_Module, t_fRPC_Strip, t_fRPC_hit_num;
    // Float_t t_Phi_Diff_Cut, t_Tan_Theta_Product_Cut, t_HH_or_HF, t_H_or_F_or_Both, t_Elastic;
    Float_t t_FwDet_track_base_X_P2, t_FwDet_track_base_Y_P2, t_FwDet_track_base_Z_P2;
    Float_t t_FwDet_track_dir_X_P2, t_FwDet_track_dir_Y_P2, t_FwDet_track_dir_Z_P2;
    Float_t t_fRPC_hit_pos_X_P2, t_fRPC_hit_pos_Y_P2, t_fRPC_hit_pos_Z_P2;
    Float_t t_fRPC_charge_N_P2, t_fRPC_charge_F_P2;
    Float_t t_fRPC_time_N_P2, t_fRPC_time_F_P2;
    Float_t t_phi_diff, t_tan_theta_product;

    Float_t t_frpc_hit_pos_X_loc;
    Float_t t_frpc_hit_pos_Y_loc;
    Float_t t_frpc_hit_pos_U;
    Float_t t_frpc_hit_pos_V;

    /*
    //------------------------------------------------------------------------------------------------------
    // Important functions for plotting

    // Mom vs theta theoretical function (for elastic scattering)
    TF1 *f_mom_vs_theta = new TF1("f_mom_vs_theta","[0]/( (cos((TMath::Pi()/180.0)*x))*( 1 + pow(
    tan((TMath::Pi()/180.0)*x) * [1] , 2 ) ) )",0,90); f_mom_vs_theta->SetParameter(0,beam_p01);
    f_mom_vs_theta->SetParameter(1,beam_gamma_cm);

    // Theta1 vs theta2 for elastic proton scattering function
    TF1 *f_theta1_vs_theta2 = new
    TF1("f_theta1_vs_theta2","(180.0/TMath::Pi())*atan(0.3088/tan((TMath::Pi()/180.0)*x))",0,90);
    */

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Calculating angular orientation of straws in STS and strips in fRPC
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Calculating angular orientation of straws in STS and strips in fRPC ... " << endl;

    //----------------------------------------------------------------------------------------------
    // ----------------- STS

    Float_t sts_sina[STS_MAX_MODULES][STS_MAX_LAYERS]; // rotation matrix of the STS sector
    Float_t sts_cosa[STS_MAX_MODULES][STS_MAX_LAYERS];
    // Float_t sts_roty[STS_MAX_MODULES][STS_MAX_LAYERS];
     HGeomVector* stsCellsLab[STS_MAX_MODULES][STS_MAX_LAYERS][STS_MAX_CELLS];
    // Centre of the strip [mm]


    // Emptying of "stsCellsLab" vector
    for(Int_t m = 0; m < STS_MAX_MODULES; ++m)
        for(Int_t l = 0; l < STS_MAX_LAYERS; ++l)
            for(Int_t c = 0; c < STS_MAX_CELLS; ++c)
                stsCellsLab[m][l][c] = NULL;

    // Loops over modules and layers
    for (Int_t m = 0; m < STS_MAX_MODULES; ++m)
    {
        for (Int_t l = 0; l < STS_MAX_LAYERS; ++l)
        {
            HModGeomPar* fmodgeom = pStrawGeomPar->getModule(l, m);
            const HGeomTransform & labTrans = fmodgeom->getLabTransform();

            // sts_roty[m][l] = labTrans.getRotMatrix().getElement(2,2);
            sts_cosa[m][l] = labTrans.getRotMatrix().getElement(1,1); // (m,n) - m = row, n = column
            sts_sina[m][l] = -labTrans.getRotMatrix().getElement(0,1); // (m,n) - m = row, n = column

            // Correction to positive values of cos and sin (180 deg rotation doesn't matter here)
            if (sts_cosa[m][l] < 0)
            {
                sts_cosa[m][l] = -sts_cosa[m][l];
                sts_sina[m][l] = -sts_sina[m][l];
            }

            printf("STS Mod=%d   Lay=%d   CosA=%f   SinA=%f\n", m, l, sts_cosa[m][l], sts_sina[m][l]);


            HGeomCompositeVolume* fMod = fmodgeom->getRefVolume();

            for (Int_t c = 0; c < STS_MAX_CELLS; ++c)
            {
                HGeomVolume* fVol = fMod->getComponent(c);
                if (!fVol) break;
                if(stsCellsLab[m][l][c] == NULL) stsCellsLab[m][l][c] = new HGeomVector;
                HGeomVector* p = stsCellsLab[m][l][c];
                *p = fVol->getTransform().getTransVector();
                *p = labTrans.transFrom(*p);
            }

        }
    }

    //----------------------------------------------------------------------------------------------
    // ----------------- FRPC

    Float_t frpc_sina[FRPC_MAX_SECTORS]; // rotation matrix of the FRPC sector
    Float_t frpc_cosa[FRPC_MAX_SECTORS];

    HGeomVector* frpcCellsLab[FRPC_MAX_SECTORS][FRPC_MAX_STRIPS]; // Centre of the strip [mm]

    // Emptying of "frpcCellsLab" vector
    for (Int_t s = 0; s < FRPC_MAX_SECTORS; ++s)
        for (Int_t c = 0; c < FRPC_MAX_STRIPS; ++c)
            frpcCellsLab[s][c] = NULL;

    // Loop over sectors
    for (Int_t s = 0; s < FRPC_MAX_SECTORS; ++s)
    {
        HModGeomPar* fmodgeom = pFRpcGeomPar->getModule(s);
        HGeomTransform labTrans = fmodgeom->getLabTransform();

        frpc_cosa[s] = labTrans.getRotMatrix().getElement(0, 0); // (m,n) - m = row, n = column
        frpc_sina[s] = labTrans.getRotMatrix().getElement(1, 0); // (m,n) - m = row, n = column

        HGeomCompositeVolume* fMod = fmodgeom->getRefVolume();

        for (Int_t c = 0; c < FRPC_MAX_STRIPS; ++c)
        {
            HGeomVolume* fVol = fMod->getComponent(c);
            if (frpcCellsLab[s][c] == NULL) frpcCellsLab[s][c] = new HGeomVector;
            HGeomVector* p = frpcCellsLab[s][c];
            *p = fVol->getTransform().getTransVector();
            *p = labTrans.transFrom(*p);
        }
    }

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Histograms definition
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Histograms definition ... " << endl;

    //----------------------------------------------------------------------------------------------
    // Miscellaneous histograms
    //----------------------------------------------------------------------------------------------

    cout << " -> Miscellaneous histograms ... " << endl;

    const Int_t num_triggers = 4; // Three triggers and a "No trigger selection" case
    Float_t norm_factor = 0.0;

    if (anapars.sim == 0) { norm_factor = 17500.0; } // 10908.10;}
    else if (anapars.sim == 1)
    {
        norm_factor = 2018.0;
    }

    TH1D* h_ALL_theta = new TH1D("h_ALL_theta", "Theta angle (ALL);#theta [deg];counts", 360, 0, 90);
    TCanvas* c_ALL_theta = new TCanvas("c_ALL_theta", "c_ALL_theta", 800, 800);
    h_ALL_theta->SetMinimum(0.0);

    //----------------------------------------------------------------------------------------------
    // Phi distribution

    TH1D* h_HF_phi[num_triggers];
    TCanvas* c_HF_phi[num_triggers];

    TH1D* h_HH_phi[num_triggers];
    TCanvas* c_HH_phi[num_triggers];

    TH1D* h_HF_phi_el[num_triggers];
    TCanvas* c_HF_phi_el[num_triggers];

    TH1D* h_HH_phi_el[num_triggers];
    TCanvas* c_HH_phi_el[num_triggers];

    //----------------------------------------------------------------------------------------------
    // Theta distribution

    TH1D* h_HF_theta[num_triggers];
    TCanvas* c_HF_theta[num_triggers];

    TH1D* h_HH_theta[num_triggers];
    TCanvas* c_HH_theta[num_triggers];

    TH1D* h_HH_and_HF_theta[num_triggers];
    TCanvas* c_HH_and_HF_theta[num_triggers];

    TH1D* h_HF_theta_el[num_triggers];
    TCanvas* c_HF_theta_el[num_triggers];

    TH1D* h_HH_theta_el[num_triggers];
    TCanvas* c_HH_theta_el[num_triggers];

    TH1D* h_HH_and_HF_theta_el[num_triggers];
    TCanvas* c_HH_and_HF_theta_el[num_triggers];

    //----------------------------------------------------------------------------------------------
    // Phi diff

    TH1D* h_HF_phi_diff[num_triggers];
    TCanvas* c_HF_phi_diff[num_triggers];

    TH1D* h_HH_phi_diff[num_triggers];
    TCanvas* c_HH_phi_diff[num_triggers];

    TH1D* h_HF_phi_diff_cut[num_triggers];
    TCanvas* c_HF_phi_diff_cut[num_triggers];

    TH1D* h_HH_phi_diff_cut[num_triggers];
    TCanvas* c_HH_phi_diff_cut[num_triggers];

    //----------------------------------------------------------------------------------------------
    // Tan theta product

    TH1D* h_HF_tan_theta_prod[num_triggers];
    TCanvas* c_HF_tan_theta_prod[num_triggers];

    TH1D* h_HH_tan_theta_prod[num_triggers];
    TCanvas* c_HH_tan_theta_prod[num_triggers];

    TH1D* h_HF_tan_theta_prod_cut[num_triggers];
    TCanvas* c_HF_tan_theta_prod_cut[num_triggers];

    TH1D* h_HH_tan_theta_prod_cut[num_triggers];
    TCanvas* c_HH_tan_theta_prod_cut[num_triggers];

    //----------------------------------------------------------------------------------------------
    // Theta1 vs theta2

    TH2F* h_HH_and_HF_theta_P1_vs_theta_P2[num_triggers];
    TCanvas* c_HH_and_HF_theta_P1_vs_theta_P2[num_triggers];

    TH2F* h_HH_and_HF_theta_P1_vs_theta_P2_el[num_triggers];
    TCanvas* c_HH_and_HF_theta_P1_vs_theta_P2_el[num_triggers];

    //----------------------------------------------------------------------------------------------
    // Theta vs phi

    TH2F* h_HH_and_HF_theta_vs_phi_P1[num_triggers];
    TCanvas* c_HH_and_HF_theta_vs_phi_P1[num_triggers];

    TH2F* h_HH_and_HF_theta_vs_phi_P2[num_triggers];
    TCanvas* c_HH_and_HF_theta_vs_phi_P2[num_triggers];

    TH2F* h_HH_and_HF_theta_vs_phi_P1_el[num_triggers];
    TCanvas* c_HH_and_HF_theta_vs_phi_P1_el[num_triggers];

    TH2F* h_HH_and_HF_theta_vs_phi_P2_el[num_triggers];
    TCanvas* c_HH_and_HF_theta_vs_phi_P2_el[num_triggers];

    //----------------------------------------------------------------------------------------------
    // Mom vs theta

    TH2F* h_HH_and_HF_mom_vs_theta_P1[num_triggers][3];
    TCanvas* c_HH_and_HF_mom_vs_theta_P1[num_triggers][3];

    TH2F* h_HH_and_HF_mom_vs_theta_P2[num_triggers][3];
    TCanvas* c_HH_and_HF_mom_vs_theta_P2[num_triggers][3];

    TH2F* h_HH_and_HF_mom_vs_theta_P1_el[num_triggers][3];
    TCanvas* c_HH_and_HF_mom_vs_theta_P1_el[num_triggers][3];

    TH2F* h_HH_and_HF_mom_vs_theta_P2_el[num_triggers][3];
    TCanvas* c_HH_and_HF_mom_vs_theta_P2_el[num_triggers][3];

    Int_t trigger = 0;

    if (anapars.cal_frpc_stage4 || anapars.cal_frpc_stage5)
    {
        //----------------------------------------------------------------------------------------------
        // Loop over triggers
        for (Int_t i = 0; i < num_triggers; ++i)
        {
            if (i == 0)
                trigger = 0; // No trigger selection case
            else if (i == 1)
                trigger = 1; // Trigger PT1 = mult_ToF_fRPC >= 2
            else if (i == 2)
                trigger = 2; // Trigger PT2 = min bias
            else if (i == 3)
                trigger = 3; // Trigger PT3 =

            //----------------------------------------------------------------------------------------------
            // Phi distribution histograms
            //----------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------
            // h_HF_phi
            sprintf(buff1, "h_HF_phi_PT%d", trigger);
            sprintf(buff2, "Phi angle distribution HF PT%d;#phi [deg];counts", trigger);
            h_HF_phi[i] = new TH1D(buff1, buff2, 720, 0, 360);

            sprintf(buff1, "c_HF_phi_PT%d", trigger);
            c_HF_phi[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HH_phi
            sprintf(buff1, "h_HH_phi_PT%d", trigger);
            sprintf(buff2, "Phi angle distribution HH PT%d;#phi [deg];counts", trigger);
            h_HH_phi[i] = new TH1D(buff1, buff2, 720, 0, 360);

            sprintf(buff1, "c_HH_phi_PT%d", trigger);
            c_HH_phi[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HF_phi_el
            sprintf(buff1, "h_HF_phi_PT%d_el", trigger);
            sprintf(buff2, "Phi angle distribution HF PT%d (Elastic);#phi [deg];counts", trigger);
            h_HF_phi_el[i] = new TH1D(buff1, buff2, 720, 0, 360);

            sprintf(buff1, "c_HF_phi_PT%d_el", trigger);
            c_HF_phi_el[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HH_phi_el
            sprintf(buff1, "h_HH_phi_PT%d_el", trigger);
            sprintf(buff2, "Phi angle distribution HH PT%d (Elastic);#phi [deg];counts", trigger);
            h_HH_phi_el[i] = new TH1D(buff1, buff2, 720, 0, 360);

            sprintf(buff1, "c_HH_phi_PT%d_el", trigger);
            c_HH_phi_el[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // Theta distribution histograms
            //----------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------
            // h_HF_theta
            sprintf(buff1, "h_HF_theta_PT%d", trigger);
            sprintf(buff2, "Theta angle distribution HF PT%d;#theta [deg];counts", trigger);
            h_HF_theta[i] = new TH1D(buff1, buff2, 180, 0, 90);

            sprintf(buff1, "c_HF_theta_PT%d", trigger);
            c_HF_theta[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HH_theta
            sprintf(buff1, "h_HH_theta_PT%d", trigger);
            sprintf(buff2, "Theta angle distribution HH PT%d;#theta [deg];counts", trigger);
            h_HH_theta[i] = new TH1D(buff1, buff2, 180, 0, 90);

            sprintf(buff1, "c_HH_theta_PT%d", trigger);
            c_HH_theta[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HH_and_HF_theta
            sprintf(buff1, "h_HH_and_HF_theta_PT%d", trigger);
            sprintf(buff2, "Theta angle distribution HH and HF PT%d;#theta [deg];counts", trigger);
            h_HH_and_HF_theta[i] = new TH1D(buff1, buff2, 180, 0, 90);

            sprintf(buff1, "c_HH_and_HF_theta_PT%d", trigger);
            c_HH_and_HF_theta[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HF_theta_el
            sprintf(buff1, "h_HF_theta_PT%d_el", trigger);
            sprintf(buff2, "Theta angle distribution HF PT%d (Elastic);#theta [deg];counts",
                    trigger);
            h_HF_theta_el[i] = new TH1D(buff1, buff2, 180, 0, 90);

            sprintf(buff1, "c_HF_theta_PT%d_el", trigger);
            c_HF_theta_el[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HH_theta_el
            sprintf(buff1, "h_HH_theta_PT%d_el", trigger);
            sprintf(buff2, "Theta angle distribution HH PT%d (Elastic);#theta [deg];counts",
                    trigger);
            h_HH_theta_el[i] = new TH1D(buff1, buff2, 180, 0, 90);

            sprintf(buff1, "c_HH_theta_PT%d_el", trigger);
            c_HH_theta_el[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HH_and_HF_theta_el
            sprintf(buff1, "h_HH_and_HF_theta_PT%d_el", trigger);
            sprintf(buff2, "Theta angle distribution HH and HF PT%d (Elastic);#theta [deg];counts",
                    trigger);
            h_HH_and_HF_theta_el[i] = new TH1D(buff1, buff2, 180, 0, 90);

            sprintf(buff1, "c_HH_and_HF_theta_PT%d_el", trigger);
            c_HH_and_HF_theta_el[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // Phi difference histograms
            //----------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------
            // h_HF_phi_diff
            sprintf(buff1, "h_HF_phi_diff_PT%d", trigger);
            sprintf(buff2, "Phi angle difference HF PT%d;#Delta #phi [deg];counts", trigger);
            h_HF_phi_diff[i] = new TH1D(buff1, buff2, 720, 0, 360);

            sprintf(buff1, "c_HF_phi_diff_PT%d", trigger);
            c_HF_phi_diff[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HH_phi_diff
            sprintf(buff1, "h_HH_phi_diff_PT%d", trigger);
            sprintf(buff2, "Phi angle difference HH PT%d;#Delta #phi [deg];counts", trigger);
            h_HH_phi_diff[i] = new TH1D(buff1, buff2, 720, 0, 360);

            sprintf(buff1, "c_HH_phi_diff_PT%d", trigger);
            c_HH_phi_diff[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HF_phi_diff_cut
            sprintf(buff1, "h_HF_phi_diff_PT%d_cut", trigger);
            sprintf(
                buff2,
                "Phi angle difference HF PT%d (cut on tan theta product);#Delta #phi [deg];counts",
                trigger);
            h_HF_phi_diff_cut[i] = new TH1D(buff1, buff2, 720, 0, 360);

            sprintf(buff1, "c_HF_phi_diff_PT%d_cut", trigger);
            c_HF_phi_diff_cut[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HH_phi_diff_cut
            sprintf(buff1, "h_HH_phi_diff_PT%d_cut", trigger);
            sprintf(
                buff2,
                "Phi angle difference HH PT%d (cut on tan theta product);#Delta #phi [deg];counts",
                trigger);
            h_HH_phi_diff_cut[i] = new TH1D(buff1, buff2, 720, 0, 360);

            sprintf(buff1, "c_HH_phi_diff_PT%d_cut", trigger);
            c_HH_phi_diff_cut[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // Tan theta product histograms
            //----------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------
            // h_HF_tan_theta_prod
            sprintf(buff1, "h_HF_tan_theta_prod_PT%d", trigger);
            sprintf(buff2,
                    "Tan theta product HF PT%d;tan(#theta_{1})*tan(#theta_{2}) [a.u.];counts",
                    trigger);
            h_HF_tan_theta_prod[i] = new TH1D(buff1, buff2, 100, 0, 1);

            sprintf(buff1, "c_HF_tan_theta_prod_PT%d", trigger);
            c_HF_tan_theta_prod[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HH_tan_theta_prod
            sprintf(buff1, "h_HH_tan_theta_prod_PT%d", trigger);
            sprintf(buff2,
                    "Tan theta product HH PT%d;tan(#theta_{1})*tan(#theta_{2}) [a.u.];counts",
                    trigger);
            h_HH_tan_theta_prod[i] = new TH1D(buff1, buff2, 100, 0, 1);

            sprintf(buff1, "c_HH_tan_theta_prod_PT%d", trigger);
            c_HH_tan_theta_prod[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HF_tan_theta_prod_cut
            sprintf(buff1, "h_HF_tan_theta_prod_PT%d_cut", trigger);
            sprintf(buff2,
                    "Tan theta product HF PT%d (cut on phi angle "
                    "difference);tan(#theta_{1})*tan(#theta_{2}) [a.u.];counts",
                    trigger);
            h_HF_tan_theta_prod_cut[i] = new TH1D(buff1, buff2, 100, 0, 1);

            sprintf(buff1, "c_HF_tan_theta_prod_PT%d_cut", trigger);
            c_HF_tan_theta_prod_cut[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // h_HH_tan_theta_prod_cut
            sprintf(buff1, "h_HH_tan_theta_prod_PT%d_cut", trigger);
            sprintf(buff2,
                    "Tan theta product HH PT%d (cut on phi angle "
                    "difference);tan(#theta_{1})*tan(#theta_{2}) [a.u.];counts",
                    trigger);
            h_HH_tan_theta_prod_cut[i] = new TH1D(buff1, buff2, 100, 0, 1);

            sprintf(buff1, "c_HH_tan_theta_prod_PT%d_cut", trigger);
            c_HH_tan_theta_prod_cut[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // Theta1 vs theta2 histograms
            //----------------------------------------------------------------------------------------------

            // h_HH_and_HF_theta_P1_vs_theta_P2
            sprintf(buff1, "h_HH_and_HF_theta_P1_vs_theta_P2_PT%d", trigger);
            sprintf(buff2, "Theta P1 vs Theta P2 HH and HF PT%d;#theta_{2} [deg];#theta_{1} [deg]",
                    trigger);
            h_HH_and_HF_theta_P1_vs_theta_P2[i] = new TH2F(buff1, buff2, 180, 0, 90, 180, 0, 90);
            h_HH_and_HF_theta_P1_vs_theta_P2[i]->SetMinimum(0.0);

            sprintf(buff1, "c_HH_and_HF_theta_P1_vs_theta_P2_PT%d", trigger);
            c_HH_and_HF_theta_P1_vs_theta_P2[i] = new TCanvas(buff1, buff1, 800, 800);

            // h_HH_and_HF_theta_P1_vs_theta_P2_el
            sprintf(buff1, "h_HH_and_HF_theta_P1_vs_theta_P2_PT%d_el", trigger);
            sprintf(
                buff2,
                "Theta P1 vs Theta P2 HH and HF PT%d (Elastic);#theta_{2} [deg];#theta_{1} [deg]",
                trigger);
            h_HH_and_HF_theta_P1_vs_theta_P2_el[i] = new TH2F(buff1, buff2, 180, 0, 90, 180, 0, 90);
            h_HH_and_HF_theta_P1_vs_theta_P2_el[i]->SetMinimum(0.0);

            sprintf(buff1, "c_HH_and_HF_theta_P1_vs_theta_P2_PT%d_el", trigger);
            c_HH_and_HF_theta_P1_vs_theta_P2_el[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // Theta vs phi histograms
            //----------------------------------------------------------------------------------------------

            // h_HH_and_HF_theta_vs_phi_P1
            sprintf(buff1, "h_HH_and_HF_theta_vs_phi_P1_PT%d", trigger);
            sprintf(buff2, "Theta vs Phi - P1 - HH and HF - PT%d;#theta [deg];#phi [deg]", trigger);
            h_HH_and_HF_theta_vs_phi_P1[i] = new TH2F(buff1, buff2, 180, 0, 90, 720, 0, 360);
            h_HH_and_HF_theta_vs_phi_P1[i]->SetMinimum(0.0);

            sprintf(buff1, "c_HH_and_HF_theta_vs_phi_P1_PT%d", trigger);
            c_HH_and_HF_theta_vs_phi_P1[i] = new TCanvas(buff1, buff1, 800, 800);

            // h_HH_and_HF_theta_vs_phi_P2
            sprintf(buff1, "h_HH_and_HF_theta_vs_phi_P2_PT%d", trigger);
            sprintf(buff2, "Theta vs Phi - P2 - HH and HF - PT%d;#theta [deg];#phi [deg]", trigger);
            h_HH_and_HF_theta_vs_phi_P2[i] = new TH2F(buff1, buff2, 180, 0, 90, 720, 0, 360);
            h_HH_and_HF_theta_vs_phi_P2[i]->SetMinimum(0.0);

            sprintf(buff1, "c_HH_and_HF_theta_vs_phi_P2_PT%d", trigger);
            c_HH_and_HF_theta_vs_phi_P2[i] = new TCanvas(buff1, buff1, 800, 800);

            // h_HH_and_HF_theta_vs_phi_P1_el
            sprintf(buff1, "h_HH_and_HF_theta_vs_phi_P1_PT%d_el", trigger);
            sprintf(buff2, "Theta vs Phi - P1 - HH and HF - PT%d (Elastic);#theta [deg];#phi [deg]",
                    trigger);
            h_HH_and_HF_theta_vs_phi_P1_el[i] = new TH2F(buff1, buff2, 180, 0, 90, 720, 0, 360);
            h_HH_and_HF_theta_vs_phi_P1_el[i]->SetMinimum(0.0);

            sprintf(buff1, "c_HH_and_HF_theta_vs_phi_P1_PT%d_el", trigger);
            c_HH_and_HF_theta_vs_phi_P1_el[i] = new TCanvas(buff1, buff1, 800, 800);

            // h_HH_and_HF_theta_vs_phi_P2_el
            sprintf(buff1, "h_HH_and_HF_theta_vs_phi_P2_PT%d_el", trigger);
            sprintf(buff2, "Theta vs Phi - P2 - HH and HF - PT%d (Elastic);#theta [deg];#phi [deg]",
                    trigger);
            h_HH_and_HF_theta_vs_phi_P2_el[i] = new TH2F(buff1, buff2, 180, 0, 90, 720, 0, 360);
            h_HH_and_HF_theta_vs_phi_P2_el[i]->SetMinimum(0.0);

            sprintf(buff1, "c_HH_and_HF_theta_vs_phi_P2_PT%d_el", trigger);
            c_HH_and_HF_theta_vs_phi_P2_el[i] = new TCanvas(buff1, buff1, 800, 800);

            //----------------------------------------------------------------------------------------------
            // Mom vs theta histograms
            //----------------------------------------------------------------------------------------------

            for (Int_t j = 0; j < 3; ++j)
            {
                // h_HH_and_HF_mom_vs_theta_P1
                sprintf(buff1, "h_HH_and_HF_mom_vs_theta_P1_PT%d_Pair_%d-%d", trigger, j, j + 3);
                sprintf(buff2,
                        "Mom vs Theta - P1- HH and HF - PT%d_Pair_%d-%d;#theta [deg];P [MeV]",
                        trigger, j, j + 3);
                h_HH_and_HF_mom_vs_theta_P1[i][j] =
                    new TH2F(buff1, buff2, 180, 0, 90, 350, 0, 7000);
                h_HH_and_HF_mom_vs_theta_P1[i][j]->SetMinimum(0.0);

                sprintf(buff1, "c_HH_and_HF_mom_vs_theta_P1_PT%d_Pair_%d-%d", trigger, j, j + 3);
                c_HH_and_HF_mom_vs_theta_P1[i][j] = new TCanvas(buff1, buff1, 800, 800);

                // h_HH_and_HF_mom_vs_theta_P2
                sprintf(buff1, "h_HH_and_HF_mom_vs_theta_P2_PT%d_Pair_%d-%d", trigger, j, j + 3);
                sprintf(buff2,
                        "Mom vs Theta - P2 - HH and HF - PT%d_Pair_%d-%d;#theta [deg];P [MeV]",
                        trigger, j, j + 3);
                h_HH_and_HF_mom_vs_theta_P2[i][j] =
                    new TH2F(buff1, buff2, 180, 0, 90, 350, 0, 7000);
                h_HH_and_HF_mom_vs_theta_P2[i][j]->SetMinimum(0.0);

                sprintf(buff1, "c_HH_and_HF_mom_vs_theta_P2_PT%d_Pair_%d-%d", trigger, j, j + 3);
                c_HH_and_HF_mom_vs_theta_P2[i][j] = new TCanvas(buff1, buff1, 800, 800);

                // h_HH_and_HF_mom_vs_theta_P1_el
                sprintf(buff1, "h_HH_and_HF_mom_vs_theta_P1_PT%d_Pair_%d-%d_el", trigger, j, j + 3);
                sprintf(
                    buff2,
                    "Mom vs Theta - P1- HH and HF - PT%d_Pair_%d-%d (Elastic);#theta [deg];P [MeV]",
                    trigger, j, j + 3);
                h_HH_and_HF_mom_vs_theta_P1_el[i][j] =
                    new TH2F(buff1, buff2, 180, 0, 90, 350, 0, 7000);
                h_HH_and_HF_mom_vs_theta_P1_el[i][j]->SetMinimum(0.0);

                sprintf(buff1, "c_HH_and_HF_mom_vs_theta_P1_PT%d_Pair_%d-%d_el", trigger, j, j + 3);
                c_HH_and_HF_mom_vs_theta_P1_el[i][j] = new TCanvas(buff1, buff1, 800, 800);

                // h_HH_and_HF_mom_vs_theta_P2_el
                sprintf(buff1, "h_HH_and_HF_mom_vs_theta_P2_PT%d_Pair_%d-%d_el", trigger, j, j + 3);
                sprintf(buff2,
                        "Mom vs Theta - P2 - HH and HF - PT%d_Pair_%d-%d (Elastic);#theta [deg];P "
                        "[MeV]",
                        trigger, j, j + 3);
                h_HH_and_HF_mom_vs_theta_P2_el[i][j] =
                    new TH2F(buff1, buff2, 180, 0, 90, 350, 0, 7000);
                h_HH_and_HF_mom_vs_theta_P2_el[i][j]->SetMinimum(0.0);

                sprintf(buff1, "c_HH_and_HF_mom_vs_theta_P2_PT%d_Pair_%d-%d_el", trigger, j, j + 3);
                c_HH_and_HF_mom_vs_theta_P2_el[i][j] = new TCanvas(buff1, buff1, 800, 800);
            }

            timer.Stop();
            cout << "  -> PT" << trigger << " - Finished, T = " << timer.RealTime() << " s" << endl;
            timer.Start(kFALSE);
        }
    }

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // fRPC calibration     histograms
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << " -> fRPC histograms ... " << endl;

    //----------------------------------------------------------------------------------------------
    // RPC
    TH2I* h_frpc_raw_mult[frpc_sectors];
    TH2I* h_frpc_raw_cnts[frpc_sectors];
    TH1I* h_frpc_raw_mults[frpc_sectors];
    TH1I* h_frpc_raw_q_n[frpc_sectors][frpc_cstrips];
    TH1I* h_frpc_raw_q_f[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_raw_q_n[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_raw_q_f[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_raw_q[frpc_sectors];
    TH1I* h_frpc_cal_q_n[frpc_sectors][frpc_cstrips];
    TH1I* h_frpc_cal_q_f[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_q_n[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_q_f[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_q[frpc_sectors];
    TH1I* h_frpc_cal_mults[frpc_sectors];
    TH2I* h_frpc_cal_v[frpc_sectors];
    TH2I* h_frpc_cal_v_manual[frpc_sectors];
    TH2I* h_frpc_cal_time_n[frpc_sectors];
    TH2I* h_frpc_cal_time_f[frpc_sectors];

    //----------------------------------------------------------------------------------------------
    // fRPC - Walk Correction - ToF Diff vs Charge - elastic
    TH2F* h_frpc_t_diff_vs_charge[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_t_diff_vs_charge[frpc_sectors][frpc_cstrips];

    // fRPC - Walk Correction - ToF Diff vs Charge - elastic
    TH2F* h_frpc_t_diff_vs_charge_el[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_t_diff_vs_charge_el[frpc_sectors][frpc_cstrips];

    //----------------------------------------------------------------------------------------------
    // fRPC ToF calibration

    TH1I* h_HF_fRPC_strip_hits[frpc_sectors];
//     TCanvas* c_HF_fRPC_Strip[frpc_sectors];

    // HADES - ToF EXP - histograms elastic
    TH1I* h_hades_tof_exp_el[frpc_sectors][frpc_cstrips];
    TCanvas* c_hades_tof_exp_el[frpc_sectors][frpc_cstrips];

    // HADES - ToF THEOR - histograms elastic
    TH1I* h_hades_tof_theor_el[frpc_sectors][frpc_cstrips];
    TCanvas* c_hades_tof_theor_el[frpc_sectors][frpc_cstrips];

    // fRPC - ToF calibration - histograms elastic
    TH1I* h_frpc_cal_tof_el[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_tof_strips_el[frpc_sectors][frpc_cstrips];
//     TCanvas* c_frpc_cal_tof_el[frpc_sectors];

    // fRPC - Delta ToF calibration - histograms elastic
    TH1I* h_frpc_cal_delta_tof_el[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_delta_tof_strips_el[frpc_sectors][frpc_cstrips];
//     TCanvas* c_frpc_cal_delta_tof_el[frpc_sectors];

    // fRPC - ToF vs Theta calibration - histograms elastic
    TH2I* h_frpc_cal_tof_vs_theta_el[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_tof_vs_theta_strips_el[frpc_sectors][frpc_cstrips];

    // fRPC - Delta ToF vs Theta calibration - histograms elastic
    TH2I* h_frpc_cal_delta_tof_vs_theta_el[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_delta_tof_vs_theta_strips_el[frpc_sectors][frpc_cstrips];

    // fRPC - Delta ToF vs Hit position V calibration - histograms elastic
    TH2I* h_frpc_cal_delta_tof_vs_V_el[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_delta_tof_vs_V_strips_el[frpc_sectors][frpc_cstrips];
//     TCanvas* c_frpc_cal_delta_tof_vs_V_el[frpc_sectors];

    // fRPC - ToF - 2D histogram elastic
    TH2I* h_frpc_cal_tof_2D_el[frpc_sectors];
    TCanvas* c_frpc_cal_tof_2D_el[frpc_sectors];

    // fRPC - Delta ToF - 2D histogram elastic
    TH2I* h_frpc_cal_delta_tof_2D_el[frpc_sectors];
    TCanvas* c_frpc_cal_delta_tof_2D_el[frpc_sectors];

    /*
    // fRPC - ToF vs Theta - 2D histogram elastic
    TH2I* h_frpc_cal_tof_vs_theta_el[frpc_sectors];
    TCanvas* c_frpc_cal_tof_vs_theta_el[frpc_sectors];

    // fRPC - Delta ToF vs Theta - 2D histogram elastic
    TH2I* h_frpc_cal_delta_tof_vs_theta_el[frpc_sectors];
    TCanvas* c_frpc_cal_delta_tof_vs_theta_el[frpc_sectors];
    */

    // fRPC - Hit U for calibration - histograms elastic
    TH1I* h_frpc_cal_hit_U[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_hit_U[frpc_sectors][frpc_cstrips];

    // fRPC - Hit V for calibration - histograms elastic
    TH1I* h_frpc_cal_hit_V[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_hit_V[frpc_sectors][frpc_cstrips];

    // fRPC - Hit XY - histograms
    TH2F* h_frpc_hit_XY[frpc_sectors];
    TCanvas* c_frpc_hit_XY[frpc_sectors];

    // fRPC - Hit XY - histograms elastic
    TH2F* h_frpc_hit_XY_el[frpc_sectors];
    TCanvas* c_frpc_hit_XY_el[frpc_sectors];

    TH2F* h_frpc_hit_XY_ALL = new TH2F("h_frpc_hit_XY_ALL", "fRPC Hit XY ;X [mm]; Y [mm]", 800, -800, 800, 800, -800, 800);
    TCanvas* c_frpc_hit_XY_ALL = new TCanvas("c_frpc_hit_XY_ALL", "c_frpc_hit_XY_ALL", 800, 800);

    TH2F* h_frpc_hit_XY_ALL_el = new TH2F("h_frpc_hit_XY_ALL_el", "fRPC Hit XY (ELASTIC);X [mm]; Y [mm]", 800, -800, 800, 800, -800, 800);
    TCanvas* c_frpc_hit_XY_ALL_el = new TCanvas("c_frpc_hit_XY_ALL_el", "c_frpc_hit_XY_ALL_el", 800, 800);

    // fRPC - Hit XY - histograms
    TH2F* h_sts_hit_XY[STS_MAX_MODULES][STS_MAX_LAYERS];
    TCanvas* c_sts_hit_XY[STS_MAX_MODULES][STS_MAX_LAYERS];

    // fRPC - Hit XY - histograms elastic
    TH2F* h_sts_hit_XY_el[STS_MAX_MODULES][STS_MAX_LAYERS];
    TCanvas* c_sts_hit_XY_el[STS_MAX_MODULES][STS_MAX_LAYERS];

    // Loops over modules and layers
    for (Int_t m = 0; m < STS_MAX_MODULES; ++m)
    {
        for (Int_t l = 0; l < STS_MAX_LAYERS; ++l)
        {
            //----------------------------------------------------------------------------------------------
            // STS - Hit XY
            sprintf(buff1, "h_sts_hit_XY_%d_%d", m, l);
            sprintf(buff2, "STS Hit XY, module=%d, layer=%d f;X [mm]; Y [mm]", m, l);
            h_sts_hit_XY[m][l] = new TH2F(buff1, buff2, 800, -800, 800, 800, -800, 800);
            h_sts_hit_XY[m][l]->SetMinimum(0.0);

            sprintf(buff1, "c_sts_hit_XY_%d_%d", m, l);
            c_sts_hit_XY[m][l] = new TCanvas(buff1, buff1, 1200, 1200);

            // STS - Hit XY - elastic
            sprintf(buff1, "h_sts_hit_XY_el_%d_%d", m, l);
            sprintf(buff2, "STS Hit XY (elastic scattering), module=%d, layer=%d f;X [mm]; Y [mm]", m, l);
            h_sts_hit_XY_el[m][l] = new TH2F(buff1, buff2, 800, -800, 800, 800, -800, 800);
            h_sts_hit_XY_el[m][l]->SetMinimum(0.0);

            sprintf(buff1, "c_sts_hit_XY_el_%d_%d", m, l);
            c_sts_hit_XY_el[m][l] = new TCanvas(buff1, buff1, 1200, 1200);
        }
    }

    //----------------------------------------------------------------------------------------------
    // Loop over sectors in fRPC
    for (Int_t i = 0; i < frpc_sectors; ++i)
    {

        sprintf(buff1, "h_frpc_raw_cnts_%d", i);
        if (i % 2 == 0)
        {
            sprintf(buff2, "RAW: count statistics, sector=%d;strip;column;counts", i);
            h_frpc_raw_cnts[i] = new TH2I(buff1, buff2, 16, 0, 16, 2, 0, 2);
        }
        else
        {
            sprintf(buff2, "RAW: count statistics, sector=%d;column;strip;counts", i);
            h_frpc_raw_cnts[i] = new TH2I(buff1, buff2, 2, 0, 2, 16, 0, 16);
        }

        sprintf(buff1, "h_frpc_raw_mult_%d", i);
        sprintf(buff2, "RAW: strip multiplicity statistics, sector=%d;strip;multiplicity", i);
        h_frpc_raw_mult[i] = new TH2I(buff1, buff2, 32, 0, 32, FRPC_MAX_HITS, 0, FRPC_MAX_HITS);

        sprintf(buff1, "h_frpc_raw_mults_%d", i);
        sprintf(buff2, "RAW: multiplicity statistics, sector=%d;mult;counts", i);
        h_frpc_raw_mults[i] = new TH1I(buff1, buff2, 32, 0, 32);

        sprintf(buff1, "h_frpc_cal_mults_%d", i);
        sprintf(buff2, "CAL: multiplicity statistics, sector=%d;mult;counts", i);
        h_frpc_cal_mults[i] = new TH1I(buff1, buff2, 32, 0, 32);

        sprintf(buff1, "h_frpc_cal_V_%d", i);
        sprintf(buff2, "CAL: V distribution, sector=%d;strip;V-coordinate", i);
        h_frpc_cal_v[i] = new TH2I(buff1, buff2, 32, 0, 32, 500, -600, 1400);

        sprintf(buff1, "h_frpc_cal_V_manual_%d", i);
        sprintf(buff2, "CAL: V distribution (manual), sector=%d;strip;V-coordinate", i);
        h_frpc_cal_v_manual[i] = new TH2I(buff1, buff2, 32, 0, 32, 500, -600, 1400);

        sprintf(buff1, "h_frpc_cal_Time_N_%d", i);
        sprintf(buff2, "CAL: Time N, sector=%d;strip;TimeN [ns]", i);
        h_frpc_cal_time_n[i] = new TH2I(buff1, buff2, 32, 0, 32, 1500, 0, 1500);

        sprintf(buff1, "h_frpc_cal_Time_F_%d", i);
        sprintf(buff2, "CAL: Time F, sector=%d;strip;TimeF [ns]", i);
        h_frpc_cal_time_f[i] = new TH2I(buff1, buff2, 32, 0, 32, 1500, 0, 1500);

        //----------------------------------------------------------------------------------------------
        // Loop over strips in given module in fRPC
        for (Int_t j = 0; j < frpc_cstrips; ++j)
        {
            //----------------------------------------------------------------------------------------------
            // Charge fRPC raw

            sprintf(buff1, "h_frpc_raw_q_n_%d_%d", i, j);
            sprintf(buff2, "RAW: charge, sector=%d strip=%d n;charge;counts", i, j);
            h_frpc_raw_q_n[i][j] = new TH1I(buff1, buff2, 200, 0, 100);
            h_frpc_raw_q_n[i][j]->SetLineColor(2);

            sprintf(buff1, "c_frpc_raw_q_n_%d_%d", i, j);
            c_frpc_raw_q_n[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            sprintf(buff1, "h_frpc_raw_q_f_%d_%d", i, j);
            sprintf(buff2, "RAW: charge, sector=%d strip=%d f;charge;counts", i, j);
            h_frpc_raw_q_f[i][j] = new TH1I(buff1, buff2, 200, 0, 100);
            h_frpc_raw_q_f[i][j]->SetLineColor(4);

            sprintf(buff1, "c_frpc_raw_q_f_%d_%d", i, j);
            c_frpc_raw_q_f[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            //----------------------------------------------------------------------------------------------
            // Charge fRPC cal

            sprintf(buff1, "h_frpc_cal_q_n_%d_%d", i, j);
            sprintf(buff2, "CAL: charge, sector=%d strip=%d n;charge;counts", i, j);
            h_frpc_cal_q_n[i][j] = new TH1I(buff1, buff2, 200, 0, 100);
            h_frpc_cal_q_n[i][j]->SetLineColor(2);

            sprintf(buff1, "c_frpc_cal_q_n_%d_%d", i, j);
            c_frpc_cal_q_n[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            sprintf(buff1, "h_frpc_cal_q_f_%d_%d", i, j);
            sprintf(buff2, "CAL: charge, sector=%d strip=%d f;charge;counts", i, j);
            h_frpc_cal_q_f[i][j] = new TH1I(buff1, buff2, 200, 0, 100);
            h_frpc_cal_q_f[i][j]->SetLineColor(4);

            sprintf(buff1, "c_frpc_cal_q_f_%d_%d", i, j);
            c_frpc_cal_q_f[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            //----------------------------------------------------------------------------------------------
            // fRPC - Walk Correction - ToF Diff vs Charge
            sprintf(buff1, "h_frpc_t_diff_vs_charge_%d_%d", i, j);
            sprintf(buff2,
                    "WalkCorr: ToF Diff vs Charge, sector=%d strip=%d f;Charge [??]; T_diff [ns]",
                    i, j);
            h_frpc_t_diff_vs_charge[i][j] = new TH2F(buff1, buff2, 800, 0, 800, 400, -10, 10);
            h_frpc_t_diff_vs_charge[i][j]->SetMinimum(0.0);
            h_frpc_t_diff_vs_charge[i][j]->SetLineColor(3);

            sprintf(buff1, "c_frpc_t_diff_vs_charge_%d_%d", i, j);
            c_frpc_t_diff_vs_charge[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            // fRPC - Walk Correction - ToF Diff vs Charge - elastic
            sprintf(buff1, "h_frpc_t_diff_vs_charge_el_%d_%d", i, j);
            sprintf(buff2,
                    "WalkCorr: ToF Diff vs Charge (elastic scattering), sector=%d strip=%d "
                    "f;Charge [??]; T_diff [ns]",
                    i, j);
            h_frpc_t_diff_vs_charge_el[i][j] = new TH2F(buff1, buff2, 800, 0, 800, 400, -10, 10);
            h_frpc_t_diff_vs_charge_el[i][j]->SetMinimum(0.0);
            h_frpc_t_diff_vs_charge_el[i][j]->SetLineColor(3);

            sprintf(buff1, "c_frpc_t_diff_vs_charge_el_%d_%d", i, j);
            c_frpc_t_diff_vs_charge_el[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            //----------------------------------------------------------------------------------------------
            // ToF fRPC calibration

            // h_hades_tof_exp_el - Elastic
            sprintf(buff1, "h_hades_tof_exp_el_%d_%d", i, j);
            sprintf(buff2, "CAL: HADES ToF EXP (elastic scattering), sector=%d strip=%d f;ToF [ns];counts", i, j);
            h_hades_tof_exp_el[i][j] = new TH1I(buff1, buff2, HF_tof_bins, 0, HF_tof_range);
            h_hades_tof_exp_el[i][j]->SetMinimum(0.0);
            h_hades_tof_exp_el[i][j]->SetLineColor(3);

            sprintf(buff1, "c_hades_tof_exp_el_%d_%d", i, j);
            c_hades_tof_exp_el[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            // h_hades_tof_theor_el - Elastic
            sprintf(buff1, "h_hades_tof_theor_el_%d_%d", i, j);
            sprintf(buff2, "CAL: HADES ToF THEOR (elastic scattering), sector=%d strip=%d f;ToF [ns];counts", i, j);
            h_hades_tof_theor_el[i][j] = new TH1I(buff1, buff2, HF_tof_bins, 0, HF_tof_range);
            h_hades_tof_theor_el[i][j]->SetMinimum(0.0);
            h_hades_tof_theor_el[i][j]->SetLineColor(3);

            sprintf(buff1, "c_hades_tof_theor_el_%d_%d", i, j);
            c_hades_tof_theor_el[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            // h_frpc_cal_delta_tof_el - Elastic
            sprintf(buff1, "h_frpc_cal_delta_tof_el_%d_%d", i, j);
            sprintf(buff2, "CAL: #Delta ToF (elastic scattering), sector=%d strip=%d f;#Delta ToF [ns];counts", i, j);
            h_frpc_cal_delta_tof_el[i][j] = new TH1I(buff1, buff2, HF_tof_bins, 0, HF_tof_range);
            h_frpc_cal_delta_tof_el[i][j]->SetMinimum(0.0);
            h_frpc_cal_delta_tof_el[i][j]->SetLineColor(3);

            sprintf(buff1, "c_frpc_cal_delta_tof_el_%d_%d", i, j);
            c_frpc_cal_delta_tof_strips_el[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            // h_frpc_cal_tof_el - Elastic
            sprintf(buff1, "h_frpc_cal_tof_el_%d_%d", i, j);
            sprintf(buff2, "CAL: ToF (elastic scattering), sector=%d strip=%d f;ToF [ns];counts", i, j);
            h_frpc_cal_tof_el[i][j] = new TH1I(buff1, buff2, HF_tof_bins, 0, HF_tof_range);
            h_frpc_cal_tof_el[i][j]->SetMinimum(0.0);
            h_frpc_cal_tof_el[i][j]->SetLineColor(3);

            sprintf(buff1, "c_frpc_cal_tof_el_%d_%d", i, j);
            c_frpc_cal_tof_strips_el[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            // h_frpc_cal_tof_vs_theta_el - Elastic
            sprintf(buff1, "h_frpc_cal_tof_vs_theta_el_%d_%d", i, j);
            sprintf(buff2, "CAL: ToF vs #theta (elastic scattering), sector=%d strip=%d f;Hit V [mm];ToF [ns]", i, j);
            h_frpc_cal_tof_vs_theta_el[i][j] = new TH2I(buff1, buff2, 100, 0, 10, HF_tof_bins, 0, HF_tof_range);
            h_frpc_cal_tof_vs_theta_el[i][j]->SetMinimum(0.0);
            h_frpc_cal_tof_vs_theta_el[i][j]->SetLineColor(3);

            sprintf(buff1, "c_frpc_cal_tof_vs_theta_el_%d_%d", i, j);
            c_frpc_cal_tof_vs_theta_strips_el[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            // h_frpc_cal_delta_tof_vs_theta_el - Elastic
            sprintf(buff1, "h_frpc_cal_delta_tof_vs_theta_el_%d_%d", i, j);
            sprintf(buff2, "CAL: #Delta ToF vs #theta (elastic scattering), sector=%d strip=%d f;Hit V [mm];#DeltaToF [ns]", i, j);
            h_frpc_cal_delta_tof_vs_theta_el[i][j] = new TH2I(buff1, buff2, 100, 0, 10, HF_tof_bins, 0, HF_tof_range);
            h_frpc_cal_delta_tof_vs_theta_el[i][j]->SetMinimum(0.0);
            h_frpc_cal_delta_tof_vs_theta_el[i][j]->SetLineColor(3);

            sprintf(buff1, "c_frpc_cal_delta_tof_vs_theta_el_%d_%d", i, j);
            c_frpc_cal_delta_tof_vs_theta_strips_el[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            // h_frpc_cal_delta_tof_vs_V_el - Elastic
            sprintf(buff1, "h_frpc_cal_delta_tof_vs_V_el_%d_%d", i, j);
            sprintf(buff2, "CAL: #Delta ToF (elastic scattering), sector=%d strip=%d f;Hit V [mm];#DeltaToF [ns]", i, j);
            h_frpc_cal_delta_tof_vs_V_el[i][j] = new TH2I(buff1, buff2, 300, -900, 900, HF_tof_bins, 0, HF_tof_range);
            h_frpc_cal_delta_tof_vs_V_el[i][j]->SetMinimum(0.0);
            h_frpc_cal_delta_tof_vs_V_el[i][j]->SetLineColor(3);

            sprintf(buff1, "c_frpc_cal_delta_tof_vs_V_el_%d_%d", i, j);
            c_frpc_cal_delta_tof_vs_V_strips_el[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            //----------------------------------------------------------

            // h_frpc_cal_hit_U - Local
            sprintf(buff1, "h_frpc_cal_hit_U_%d_%d", i, j);
            sprintf(buff2, "CAL: Hit U position fRPC, sector=%d strip=%d f;Hit U [mm];counts", i,
                    j);
            h_frpc_cal_hit_U[i][j] = new TH1I(buff1, buff2, 300, -900, 900);
            h_frpc_cal_hit_U[i][j]->SetMinimum(0.0);
            h_frpc_cal_hit_U[i][j]->SetLineColor(3);

            sprintf(buff1, "c_frpc_cal_hit_U_%d_%d", i, j);
            c_frpc_cal_hit_U[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            // h_frpc_cal_hit_V - Local
            sprintf(buff1, "h_frpc_cal_hit_V_%d_%d", i, j);
            sprintf(buff2, "CAL: Hit V position fRPC, sector=%d strip=%d f;Hit V [mm];counts", i,
                    j);
            h_frpc_cal_hit_V[i][j] = new TH1I(buff1, buff2, 300, -900, 900);
            h_frpc_cal_hit_V[i][j]->SetMinimum(0.0);
            h_frpc_cal_hit_V[i][j]->SetLineColor(3);

            sprintf(buff1, "c_frpc_cal_hit_V_%d_%d", i, j);
            c_frpc_cal_hit_V[i][j] = new TCanvas(buff1, buff1, 1200, 1000);
        }

        //----------------------------------------------------------------------------------------------
        // fRPC - Hit XY
        sprintf(buff1, "h_frpc_hit_XY_%d", i);
        sprintf(buff2, "fRPC Hit XY, sector=%d f;X [mm]; Y [mm]", i);
        h_frpc_hit_XY[i] = new TH2F(buff1, buff2, 800, -800, 800, 800, -800, 800);
        h_frpc_hit_XY[i]->SetMinimum(0.0);

        sprintf(buff1, "c_frpc_hit_XY_%d", i);
        c_frpc_hit_XY[i] = new TCanvas(buff1, buff1, 1200, 1200);

        // fRPC - Hit XY - elastic
        sprintf(buff1, "h_frpc_hit_XY_el_%d", i);
        sprintf(buff2, "fRPC Hit XY (elastic scattering), sector=%d f;X [mm]; Y [mm]", i);
        h_frpc_hit_XY_el[i] = new TH2F(buff1, buff2, 800, -800, 800, 800, -800, 800);
        h_frpc_hit_XY_el[i]->SetMinimum(0.0);

        sprintf(buff1, "c_frpc_hit_XY_el_%d", i);
        c_frpc_hit_XY_el[i] = new TCanvas(buff1, buff1, 1200, 1200);

        //----------------------------------------------------------------------------------------------
        // fRPC - charge raw collective canvas
        sprintf(buff1, "h_frpc_raw_q_%d", i);
        c_frpc_raw_q[i] = new TCanvas(buff1, buff1, 800, 800);
        c_frpc_raw_q[i]->DivideSquare(frpc_cstrips);

        // fRPC - charge cal collective canvas
        sprintf(buff1, "h_frpc_cal_q_%d", i);
        c_frpc_cal_q[i] = new TCanvas(buff1, buff1, 800, 800);
        c_frpc_cal_q[i]->DivideSquare(frpc_cstrips);

        //----------------------------------------------------------------------------------------------
        // Hit multiplicity in fRPC strips
        sprintf(buff1, "h_HF_fRPC_strip_hits_%d", i);
        sprintf(buff2,
                "Number of hits in each strip in fRPC (Elastic), sector=%d; fRPC Strip;counts", i);
        h_HF_fRPC_strip_hits[i] = new TH1I(buff1, buff2, frpc_cstrips, 0, frpc_cstrips);
        h_HF_fRPC_strip_hits[i]->SetMinimum(0.0);

//         sprintf(buff1, "c_HF_fRPC_Strip_%d", i);
//         c_HF_fRPC_Strip[i] = new TCanvas(buff1, buff1, 800, 800);

        //----------------------------------------------------------------------------------------------
        // h_frpc_cal_tof 2D hist elastic
        sprintf(buff1, "h_frpc_cal_tof_2D_el_%d", i);
        sprintf(buff2, "CAL: ToF 2D elastic, sector=%d f;Strip;ToF [ns]", i);
        h_frpc_cal_tof_2D_el[i] = new TH2I(buff1, buff2, frpc_cstrips, 0, frpc_cstrips, HF_tof_bins, 0, HF_tof_range);
        h_frpc_cal_tof_2D_el[i]->SetLineColor(4);

        // fRPC - ToF calibration canvas 2D hist elastic
        sprintf(buff1, "c_frpc_cal_tof_2D_el_%d", i);
        c_frpc_cal_tof_2D_el[i] = new TCanvas(buff1, buff1, 800, 800);

        // h_frpc_cal_delta_tof 2D hist elastic
        sprintf(buff1, "h_frpc_cal_delta_tof_2D_el_%d", i);
        sprintf(buff2, "CAL: #Delta ToF 2D elastic, sector=%d f;Strip;#Delta ToF [ns]", i);
        h_frpc_cal_delta_tof_2D_el[i] = new TH2I(buff1, buff2, frpc_cstrips, 0, frpc_cstrips, HF_tof_bins, 0, HF_tof_range);
        h_frpc_cal_delta_tof_2D_el[i]->SetLineColor(4);

        // fRPC - Delta ToF canvas 2D hist elastic
        sprintf(buff1, "c_frpc_cal_delta_tof_2D_el_%d", i);
        c_frpc_cal_delta_tof_2D_el[i] = new TCanvas(buff1, buff1, 800, 800);

        //----------------------------------------------------------------------------------------------
        /*
        // h_frpc_cal_tof_vs_theta_el
        sprintf(buff1, "h_frpc_cal_tof_vs_theta_el_%d", i);
        sprintf(buff2, "CAL: ToF vs Theta, sector=%d f;Strip;ToF [ns]", i);
        h_frpc_cal_tof_vs_theta_el[i] = new TH2I(buff1, buff2, 20, 0, 10, HF_tof_bins, 0, HF_tof_range);
        h_frpc_cal_tof_vs_theta_el[i]->SetLineColor(4);

        // fRPC - ToF vs Theta
        sprintf(buff1, "c_frpc_cal_tof_vs_theta_el_%d", i);
        c_frpc_cal_tof_vs_theta_el[i] = new TCanvas(buff1, buff1, 800, 800);

        //h_frpc_cal_delta_tof_vs_theta_el
        sprintf(buff1, "h_frpc_cal_delta_tof_vs_theta_el_%d", i);
        sprintf(buff2, "CAL: #Delta ToF vs Theta, sector=%d f;Strip;#Delta ToF [ns]", i);
        h_frpc_cal_delta_tof_vs_theta_el[i] = new TH2I(buff1, buff2, 20, 0, 10, HF_tof_bins, 0, HF_tof_range);
        h_frpc_cal_delta_tof_vs_theta_el[i]->SetLineColor(4);

        // fRPC - ToF vs Theta
        sprintf(buff1, "c_frpc_cal_delta_tof_vs_theta_el_%d", i);
        c_frpc_cal_delta_tof_vs_theta_el[i] = new TCanvas(buff1, buff1, 800, 800);
        */

        timer.Stop();
        cout << "  -> Module = " << i << " - Finished, T = " << timer.RealTime() << " s" << endl;
        timer.Start(kFALSE);
    }

    TCanvas* c_frpc_raw_cnts = new TCanvas("c_frpc_raw_cnts", "c_frpc_raw_cnts", 800, 800);
    c_frpc_raw_cnts->DivideSquare(frpc_sectors);

    TCanvas* c_frpc_raw_mult = new TCanvas("c_frpc_raw_mult", "c_frpc_raw_mult", 800, 800);
    c_frpc_raw_mult->DivideSquare(frpc_sectors);

    TCanvas* c_frpc_raw_mults = new TCanvas("c_frpc_raw_mults", "c_frpc_raw_mults", 800, 800);
    c_frpc_raw_mults->DivideSquare(frpc_sectors);
    THStack* s_frpc_raw_mults = new THStack("s_frpc_raw_mults", "s_frpc_raw_mults");

    TCanvas* c_frpc_cal_v = new TCanvas("c_frpc_cal_v", "c_frpc_cal_v", 800, 800);
    c_frpc_cal_v->DivideSquare(frpc_sectors);

    TCanvas* c_frpc_cal_v_manual = new TCanvas("c_frpc_cal_v_manual", "c_frpc_cal_v_manual", 800, 800);
    c_frpc_cal_v_manual->DivideSquare(frpc_sectors);

    TCanvas* c_frpc_cal_time_n = new TCanvas("c_frpc_cal_time_n", "c_frpc_cal_time_n", 800, 800);
    c_frpc_cal_time_n->DivideSquare(frpc_sectors);

    TCanvas* c_frpc_cal_time_f = new TCanvas("c_frpc_cal_time_f", "c_frpc_cal_time_f", 800, 800);
    c_frpc_cal_time_f->DivideSquare(frpc_sectors);

    TCanvas* c_frpc_cal_mults = new TCanvas("c_frpc_cal_mults", "c_frpc_cal_mults", 800, 800);
    c_frpc_cal_mults->DivideSquare(frpc_sectors);
    THStack* s_frpc_cal_mults = new THStack("s_frpc_cal_mults", "s_frpc_cal_mults");

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Loop over events in input file
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "//"
            "--------------------------------------------------------------------------------------"
            "--------"
         << endl;
    cout << "Loop over events in input file ... " << endl;
    cout << "//"
            "--------------------------------------------------------------------------------------"
            "--------"
         << endl;

    // Total number of events
    Int_t num_Events_Total = 0;
    Int_t num_Events_Total_Elastic = 0;
    Int_t num_Events_HH = 0;
    Int_t num_Events_HF = 0;
    Int_t num_Events_HH_Elastic = 0;
    Int_t num_Events_HF_Elastic = 0;

    // Number of events with specific triggers
    Int_t num_Events_PT1 = 0;
    Int_t num_Events_PT2 = 0;
    Int_t num_Events_PT3 = 0;
    Int_t num_HH_Events_PT1 = 0;
    Int_t num_HH_Events_PT2 = 0;
    Int_t num_HH_Events_PT3 = 0;
    Int_t num_HF_Events_PT1 = 0;
    Int_t num_HF_Events_PT2 = 0;
    Int_t num_HF_Events_PT3 = 0;
    Int_t num_HH_Events_Elastic_PT1 = 0;
    Int_t num_HH_Events_Elastic_PT2 = 0;
    Int_t num_HH_Events_Elastic_PT3 = 0;
    Int_t num_HF_Events_Elastic_PT1 = 0;
    Int_t num_HF_Events_Elastic_PT2 = 0;
    Int_t num_HF_Events_Elastic_PT3 = 0;

    // Number of  events with fRPC hit
    Int_t num_Events_HF_fRPC_hit = 0;
    Int_t num_HF_Events_PT1_fRPC_hit = 0;
    Int_t num_HF_Events_PT2_fRPC_hit = 0;
    Int_t num_HF_Events_PT3_fRPC_hit = 0;

    // Number of elastic events with fRPC hit
    Int_t num_Events_HF_Elastic_fRPC_hit = 0;
    Int_t num_HF_Events_Elastic_PT1_fRPC_hit = 0;
    Int_t num_HF_Events_Elastic_PT2_fRPC_hit = 0;
    Int_t num_HF_Events_Elastic_PT3_fRPC_hit = 0;

    // Number of entires in specific cases
    Int_t num_HH_Entries = 0;
    Int_t num_HF_Entries = 0;
    Int_t num_HH_Elastic_Entries = 0;
    Int_t num_HF_Elastic_Entries = 0;

    // Number of entires in specific cases (PT1 trigger)
    Int_t num_HH_Entries_PT1 = 0;
    Int_t num_HF_Entries_PT1 = 0;
    Int_t num_HH_Elastic_Entries_PT1 = 0;
    Int_t num_HF_Elastic_Entries_PT1 = 0;

    // Number of entires in specific cases (PT2 trigger)
    Int_t num_HH_Entries_PT2 = 0;
    Int_t num_HF_Entries_PT2 = 0;
    Int_t num_HH_Elastic_Entries_PT2 = 0;
    Int_t num_HF_Elastic_Entries_PT2 = 0;

    // Number of entires in specific cases (PT3 trigger)
    Int_t num_HH_Entries_PT3 = 0;
    Int_t num_HF_Entries_PT3 = 0;
    Int_t num_HH_Elastic_Entries_PT3 = 0;
    Int_t num_HF_Elastic_Entries_PT3 = 0;

    Int_t particle_cand_cnt = 0;
    Int_t forward_cand_cnt = 0;

    Int_t num_fRPC_Raw = 0;
    Int_t mult_fRPC_Raw = 0;

     //----------------------------------------------------------------------------------------------
    // Beam position finding loop

    Double_t xBeam = 1.866; // TODO this needs to be changed as soon as we have beam and alignment. Otherwise calculating z-Prime does not works
    Double_t yBeam = -1.438;

    /*
    Int_t beam_pos_event_limit;
    if ((limit_sto - limit_sta) < 1000 ) beam_pos_event_limit = (limit_sto - limit_sta);
    else beam_pos_event_limit = 1000;

    cout<<beam_pos_event_limit<<endl;

    for (Int_t i = limit_sta; i < limit_sta + beam_pos_event_limit; i++)
    {

        HEventHeader* header  = gHades->getCurrentEvent()->getHeader();
        HVertex vertex      = header->getVertexReco();

        xBeam = xBeam + vertex.getX();
        yBeam = yBeam + vertex.getY();

        if(i%100 == 0) cout<<vertex.getX()<<" ; "<<vertex.getY()<<endl;
    }

    xBeam = xBeam / (Double_t)beam_pos_event_limit;
    yBeam = yBeam /(Double_t)beam_pos_event_limit;
    */

    cout<<"xBeam = "<<xBeam<<" ; yBeam = "<<yBeam<<endl;

    //----------------------------------------------------------------------------------------------
    // Event loop
    for (Int_t i = limit_sta; i < limit_sto; i++)
    {
        num_Events_Total++;

        if (i % 10000 == 0)
        {
            timer.Stop();
            printf("Event nr.: %d, Progress: %.2f%%, ", i,
                   (i - limit_sta) / (limit_sto - limit_sta) * 100.);
            timer.Print();
            timer.Start(kFALSE);
        }

        //----------------------------------------------------------------------------------------------
        // Get next event. Categories will be cleared before
        loop->nextEvent(i);

        HEventHeader* event_header = NULL;
        if (!(event_header = gHades->getCurrentEvent()->getHeader())) continue;

        auto TBit = event_header->getTBit();
        //         Int_t DF     =  event_header->getDownscalingFlag();
        //         Int_t SeqNum =  event_header->getEventSeqNumber();
        //         Int_t TDec   =  event_header->getTriggerDecision();

        //----------------------------------------------------------------------------------------------
        // Checking trigger
        bool is_pt1 = false;
        bool is_pt2 = false;
        bool is_pt3 = false;

        // Binary flags for each trigger type
        t_PT1 = 0;
        t_PT2 = 0;
        t_PT3 = 0;

        if ((TBit & 2048) == 2048)
        {
            // bit=1;//PT1
            num_Events_PT1++;
            is_pt1 = true;
            t_PT1 = 1;
        }
        if ((TBit & 4096) == 4096)
        {
            // bit=2;//PT2
            num_Events_PT2++;
            is_pt2 = true;
            t_PT2 = 1;
        }
        if ((TBit & 8192) == 8192)
        {
            // bit=3;//PT3
            num_Events_PT3++;
            is_pt3 = true;
            t_PT3 = 1;
        }

        // Selecting only the desired trigger events - all others are discarded
        if (!(anapars.pt1 and is_pt1) and !(anapars.pt2 and is_pt2) and !(anapars.pt3 and is_pt3)) continue;
        //if (t_PT2 == 0) continue;

        //----------------------------------------------------------------------------------------------
        // fStart2Hit
        //----------------------------------------------------------------------------------------------

        t_T0 = 1; // T0 flag initialized as 1 If there's no T0, it'll be changed to 0
        HStart2Hit* fstarthit = 0;

        if (!fStart2Hit)
        {
            printf("No fStart2Hit in event nr. %d!\n", i);
            t_T0 = 0;
        }

        // printf("Number of entries in fStart2Hit in event nr. %d: %d!\n", i,
        // fStart2Hit->getEntries());

        if (fStart2Hit->getEntries() == 0)
        {
            // printf("Error - no entries in fStart2Hit in event nr. %d! Count = %d\n", i,
            // no_fStart2Hit_cnt + 1);
            no_fStart2Hit_cnt = no_fStart2Hit_cnt + 1;
            t_T0 = 0;
        }

        if (fStart2Hit && fStart2Hit->getEntries() > 0)
        {
            fstarthit = HCategoryManager::getObject(fstarthit, fStart2Hit, 0);
            if (fstarthit->getFlag() == 0)
            {
                // printf("Flag = 0 in fStart2Hit in event nr. %d! Count = %d\n", i,
                // rejected_no_T0_cnt+1);
                rejected_no_T0_cnt = rejected_no_T0_cnt + 1;
                t_T0 = 0;
            }
        }

        if (anapars.T0_cond and t_T0 == 0) continue;

        // PlaneStats ps[100000];
        // std::map<Int_t, Int_t> used_tracks_sts, used_tracks_sts_f;
        // std::map<Int_t, Int_t> used_tracks_frpc, used_tracks_frpc_f;

        //----------------------------------------------------------------------------------------------
        // Elastic events checks
        //----------------------------------------------------------------------------------------------

        particle_cand_cnt = 0;
        forward_cand_cnt = 0;

        auto has_hh_elastic = ForwardTools::Elastics::check_elastics_hh(
            fParticleCand, cut_HH_phi_diff_min, cut_HH_phi_diff_max, cut_HH_tan_theta_min,
            cut_HH_tan_theta_max);

        auto has_hf_elastic = ForwardTools::Elastics::check_elastics_hf(
            fParticleCand, fForwardCand, cut_HF_phi_diff_min, cut_HF_phi_diff_max,
            cut_HF_tan_theta_min, cut_HF_tan_theta_max);

        if (has_hh_elastic || has_hf_elastic)
            num_Events_Total_Elastic = num_Events_Total_Elastic + 1;

        if (has_hh_elastic)
        {
            num_Events_HH_Elastic = num_Events_HH_Elastic + 1;
            if (t_PT1 == 1) num_HH_Events_Elastic_PT1 = num_HH_Events_Elastic_PT1 + 1;
            if (t_PT2 == 1) num_HH_Events_Elastic_PT2 = num_HH_Events_Elastic_PT2 + 1;
            if (t_PT3 == 1) num_HH_Events_Elastic_PT3 = num_HH_Events_Elastic_PT3 + 1;
        }

        if (has_hf_elastic)
        {
            num_Events_HF_Elastic = num_Events_HF_Elastic + 1;
            if (t_PT1 == 1) num_HF_Events_Elastic_PT1 = num_HF_Events_Elastic_PT1 + 1;
            if (t_PT2 == 1) num_HF_Events_Elastic_PT2 = num_HF_Events_Elastic_PT2 + 1;
            if (t_PT3 == 1) num_HF_Events_Elastic_PT3 = num_HF_Events_Elastic_PT3 + 1;
        }

        /*

        //----------------------------------------------------------------------------------------------
        // STS hit positions
        //----------------------------------------------------------------------------------------------

        forward_cand_cnt = 0;
        if (fForwardCand)
        {
            forward_cand_cnt = fForwardCand->getEntries();

            for (int k = 0; k < forward_cand_cnt; ++k)
            {
                HForwardCand* fwdetcand = HCategoryManager::getObject(fwdetcand, fForwardCand, k);

                // Track position
                Float_t x_track_pos = fwdetcand->getBaseX();
                Float_t y_track_pos = fwdetcand->getBaseY();
                Float_t z_track_pos = fwdetcand->getBaseZ();

                // Track direction
                Float_t x_track_dir = fwdetcand->getDirTx();
                Float_t y_track_dir = fwdetcand->getDirTy();
                Float_t z_track_dir = 1.0;

                // "Stop" -
                Float_t t_FwDet_track_stop_X = t_FwDet_track_base_X_P2 + t_FwDet_track_dir_X_P2;
                Float_t t_FwDet_track_stop_Y = t_FwDet_track_base_Y_P2 + t_FwDet_track_dir_Y_P2;
                Float_t t_FwDet_track_stop_Z = t_FwDet_track_base_Z_P2 + t_FwDet_track_dir_Z_P2;

                // Track vectors of position and direction
                TVector3 v_track_pos, v_track_dir;
                v_track_pos.SetXYZ(x_track_pos,y_track_pos,z_track_pos);
                v_track_dir.SetXYZ(x_track_dir,y_track_dir,z_track_dir);

                // Number of hits used in track reconstruction
                int fcand_num_of_hits = fwdetcand->getNofHits();

                if (fStsCal)
                {
                    //int fwdet_cal_cnt = fStsCal->getEntries();
                    HStsCal* fwdetstraw = 0;

                    for (int j = 0; j < fcand_num_of_hits; ++j) // Loop over hits used in track reconstruction
                    {
                        int hit_index = fwdetcand->getHitIndex(j); // Recalculating from internal hit_index within track to general hit_index in STS
                        fwdetstraw = HCategoryManager::getObject(fwdetstraw, fStsCal, hit_index);

                        // Initializing extracted variables
                        Char_t mod, lay, ud;      // mod - Module, lay - Layer, ud - upper/lower (short) or long straw
                        Int_t straw; // Straw index, plane index (0-7)
                        // Float_t time, width; // time - time of first registration of signal (crossing threshold), width - Time over Threshold

                        // Getting values of each variable in this entry using functions
                        fwdetstraw->getAddress(mod, lay, straw, ud);
                        Float_t u = fwdetstraw->getU();
                        Float_t z = fwdetstraw->getZ();

                        // Straw position
                        Float_t x_sts_pos = u * sts_cosa[mod][lay];
                        Float_t y_sts_pos = u * sts_sina[mod][lay];
                        Float_t z_sts_pos = z;

                        // Straw direction
                        Float_t x_sts_dir = sts_sina[mod][lay];
                        Float_t y_sts_dir = sts_cosa[mod][lay];
                        Float_t z_sts_dir = 0.0;

                        // fRPC hit coordinates
                        t_fRPC_hit_pos_X_P2 = ForwardTools::Miscellaneous::calc_hit_pos(t_FwDet_track_base_X_P2, t_FwDet_track_stop_X, t_FwDet_track_base_Z_P2, t_FwDet_track_stop_Z, frpcCellsLab[t_fRPC_Module][t_fRPC_Strip]->Z()); // [mm]
                        t_fRPC_hit_pos_Y_P2 = ForwardTools::Miscellaneous::calc_hit_pos(t_FwDet_track_base_Y_P2, t_FwDet_track_stop_Y, t_FwDet_track_base_Z_P2, t_FwDet_track_stop_Z, frpcCellsLab[t_fRPC_Module][t_fRPC_Strip]->Z()); // [mm]
                        t_fRPC_hit_pos_Z_P2 = frpcCellsLab[t_fRPC_Module][t_fRPC_Strip]->Z() - t_z_start_P2; // [mm]

                        // Straw vectors of position and direction
                        TVector3 v_sts_pos, v_sts_dir;
                        v_sts_pos.SetXYZ(x_sts_pos,y_sts_pos,z_sts_pos);
                        v_sts_dir.SetXYZ(x_sts_dir,y_sts_dir,z_sts_dir);

                        // Calculating residuals (distance between straw and track)
                        TVector3 A = v_sts_dir.Cross(v_track_dir);
                        TVector3 B = (v_sts_pos - v_track_pos);
                        Float_t sts_track_dist = abs( (A.Dot(B)) / (A.Mag()) );

                        // Filling residuals histograms
                        h_sts_residuals[mod][lay]->Fill(sts_track_dist);
                    }
                }
            }
        }
        */

        //----------------------------------------------------------------------------------------------
        // Forward Detector RPC Raw
        //----------------------------------------------------------------------------------------------

        Float_t T0 = 100000.0;
        int frpc_raw_cnt = 0;

        if (fFRpcRaw)
        {
            int raw_mult[4] = {0};
            frpc_raw_cnt = fFRpcRaw->getEntries();
            // printf("RPC RAW entries: %d\n", frpc_raw_cnt);

            //----------------------------------------------------------------------------------------------
            // Loop over entires in fFRpcRaw
            for (int j = 0; j < frpc_raw_cnt; ++j)
            {
                HFRpcRaw* frpcraw = HCategoryManager::getObject(frpcraw, fFRpcRaw, j);

                Char_t sec, col, strip;
                Float_t time, width;

                frpcraw->getAddress(sec, col, strip);
                // frpcraw->print();
                int cstrip = strip * 2 + col;

                h_frpc_raw_mult[sec]->Fill(
                    cstrip, std::max(frpcraw->getHitsNumN(), frpcraw->getHitsNumF()));

                num_fRPC_Raw = num_fRPC_Raw + 1;
                mult_fRPC_Raw = mult_fRPC_Raw + std::max(frpcraw->getHitsNumN(), frpcraw->getHitsNumF());

                // Loop over N hits in frpcraw
                for (int k = 0; k < frpcraw->getHitsNumN(); ++k)
                {
                    frpcraw->getTimeAndWidthN(k, time, width);
                    h_frpc_raw_q_n[sec][cstrip]->Fill(width);

                    if (time < T0)
                    {
                        T0 = time;
                        // T0_z = frpcCellsLab[sec][cstrip]->Z();
                    }
                }

                // Loop over F hits in frpcraw
                for (int k = 0; k < frpcraw->getHitsNumF(); ++k)
                {
                    frpcraw->getTimeAndWidthF(k, time, width);
                    h_frpc_raw_q_f[sec][cstrip]->Fill(width);

                    if (time < T0)
                    {
                        T0 = time;
                        // T0_z = frpcCellsLab[sec][cstrip]->Z();
                    }
                }

                ++raw_mult[sec];
                if (sec % 2 == 0) { h_frpc_raw_cnts[sec]->Fill(strip, col); }
                else
                {
                    h_frpc_raw_cnts[sec]->Fill(col, strip);
                }
            }

            for (Int_t i = 0; i < frpc_sectors; ++i)
            {
                if (raw_mult[i]) h_frpc_raw_mults[i]->Fill(raw_mult[i]);
            }
        }

        //----------------------------------------------------------------------------------------------
        // Forward Detector RPC Cal - Strip Length and Offset Calibration + Walk Correction
        //----------------------------------------------------------------------------------------------

        if (fFRpcCal)
        {
            int frpc_cal_cnt = fFRpcCal->getEntries();

            HFRpcCal* frpccal = 0;

            // For effective length and strip offset calibration
            for (int j = 0; j < frpc_cal_cnt; ++j)
            {
                frpccal = HCategoryManager::getObject(frpccal, fFRpcCal, j);

                Char_t sec, cstrip;
                Float_t v;
                Float_t time_n, time_f;
                Float_t charge_n, charge_f;

                frpccal->getAddress(sec, cstrip); // Strip address
                t_fRPC_Module = sec;       // Hit in module nr. "sec"
                t_fRPC_Strip = cstrip;     // Hit in strip nr. "cstrip"

                frpccal->getHit(time_n,time_f,charge_n,charge_f);

                // Hit position along the strip - for effective length and strip offset calibration
                v = frpccal->getV();
                h_frpc_cal_v[t_fRPC_Module]->Fill(t_fRPC_Strip, v);
                h_frpc_cal_v_manual[t_fRPC_Module]->Fill(t_fRPC_Strip, (time_f-time_n)*203.0/2.0 );
                h_frpc_cal_time_n[t_fRPC_Module]->Fill(t_fRPC_Strip, time_n);
                h_frpc_cal_time_f[t_fRPC_Module]->Fill(t_fRPC_Strip, time_f);
                h_frpc_cal_q_n[t_fRPC_Module][t_fRPC_Strip]->Fill(charge_n);
                h_frpc_cal_q_f[t_fRPC_Module][t_fRPC_Strip]->Fill(charge_f);
            }

            // For walk effect correction
            if (frpc_cal_cnt == 2)
            {
                Char_t sec, cstrip;
                Int_t temp_sec_1, temp_sec_2, temp_strip_1, temp_strip_2;
                Float_t temp_tof_1, temp_tof_2;
                Float_t temp_time_N_1, temp_time_N_2, temp_time_F_1, temp_time_F_2;
                Float_t temp_charge_N_1, temp_charge_N_2, temp_charge_F_1, temp_charge_F_2;

                // Hit 1
                frpccal = HCategoryManager::getObject(frpccal, fFRpcCal, 0);
                frpccal->getAddress(sec, cstrip); // Strip address
                temp_sec_1 = sec;          // Hit in module nr. "sec"
                temp_strip_1 = cstrip;     // Hit in strip nr. "cstrip"

                temp_tof_1 = frpccal->getTof(); // [ns]
                frpccal->getHit(temp_time_N_1, temp_time_F_1, temp_charge_N_1, temp_charge_F_1);

                // Hit 2
                frpccal = HCategoryManager::getObject(frpccal, fFRpcCal, 1);
                frpccal->getAddress(sec, cstrip); // Strip address
                temp_sec_2 = sec;          // Hit in module nr. "sec"
                temp_strip_2 = cstrip;     // Hit in strip nr. "cstrip"

                temp_tof_2 = frpccal->getTof(); // [ns]
                frpccal->getHit(temp_time_N_2, temp_time_F_2, temp_charge_N_2, temp_charge_F_2);

                Float_t temp_t_diff = temp_tof_1 - temp_tof_2;
                Float_t temp_charge_sum =
                    temp_charge_N_1 + temp_charge_F_1 + temp_charge_N_2 + temp_charge_F_2;

                // If same module and overlapping strip
                if (temp_sec_1 == temp_sec_2 &&
                    (temp_strip_1 == temp_strip_2 - 1 || temp_strip_1 == temp_strip_2 + 1))
                {
                    h_frpc_t_diff_vs_charge[temp_sec_1][temp_strip_1]->Fill(temp_charge_sum,
                                                                            temp_t_diff);
                    if (has_hf_elastic)
                    {
                        h_frpc_t_diff_vs_charge_el[temp_sec_1][temp_strip_1]->Fill(temp_charge_sum,
                                                                                   temp_t_diff);
                    }
                }
            }
        }

        //----------------------------------------------------------------------------------------------
        // Simulation - all events
        //----------------------------------------------------------------------------------------------

        if (anapars.sim == 1)
        {
            Int_t particle_cand_cnt = 0;
            Int_t forward_cand_cnt = 0;

            if (fParticleCand)
            {
                particle_cand_cnt = fParticleCand->getEntries();
                for (int j = 0; j < particle_cand_cnt; ++j)
                {
                    HParticleCand* fparticlecand =
                        HCategoryManager::getObject(fparticlecand, fParticleCand, j);
                    h_ALL_theta->Fill(fparticlecand->getTheta());
                }
            }

            if (fForwardCand)
            {
                forward_cand_cnt = fForwardCand->getEntries();
                for (int j = 0; j < forward_cand_cnt; ++j)
                {
                    HForwardCand* fwdetcand =
                        HCategoryManager::getObject(fwdetcand, fForwardCand, j);
                    h_ALL_theta->Fill(fwdetcand->getTheta());
                }
            }
        }

        //----------------------------------------------------------------------------------------------
        // fRPC analysis - ToF calibration
        //----------------------------------------------------------------------------------------------
        // !!! We only look at 2 particle cases !!!
        //----------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------
        // HADES - HADES case
        //----------------------------------------------------------------------------------------------

        if (fParticleCand)
        {
            particle_cand_cnt = fParticleCand->getEntries();

            //----------------------------------------------------------------------------------------------
            // HADES - HADES case
            //----------------------------------------------------------------------------------------------

            if (particle_cand_cnt == 2 && (anapars.cal_frpc_stage4 || anapars.cal_frpc_stage5))
            {
                num_Events_HH = num_Events_HH + 1;
                if (t_PT1 == 1) num_HH_Events_PT1 = num_HH_Events_PT1 + 1;
                if (t_PT2 == 1) num_HH_Events_PT2 = num_HH_Events_PT2 + 1;
                if (t_PT3 == 1) num_HH_Events_PT3 = num_HH_Events_PT3 + 1;

                //----------------------------------------------------------------------------------------------
                // Particle 1 - HADES particle
                HParticleCand* fparticlecand =
                    HCategoryManager::getObject(fparticlecand, fParticleCand, 0);
                t_Sector_P1 = fparticlecand->getSector();

                // t_z_start_P1 = fparticlecand->getZ(); // [mm]
                // t_track_length_P1 = fparticlecand->getDistanceToMetaHit(); // [mm]

                auto t_phi_P1 = fparticlecand->getPhi();     // [deg]
                auto t_theta_P1 = fparticlecand->getTheta(); // [deg]

                auto t_mom_P1 = fparticlecand->getMomentum(); // [MeV/c]
                // t_tof_P1 = fparticlecand->getTof(); // [ns]
                // t_beta_P1 = fparticlecand->getBeta(); // [ ]

                // t_mom_theor_P1 = ForwardTools::Elastics::calc_theor_el_mom(TMath::DegToRad() *
                // t_theta_P1); // [MeV/c] t_tof_theor_P1 =
                // ForwardTools::Elastics::calc_theor_el_ToF(t_track_length_P1, TMath::DegToRad() *
                // t_theta_P1); // [ns] t_beta_theor_P1 =
                // ForwardTools::Elastics::calc_theor_el_beta(TMath::DegToRad() * t_theta_P1); // [
                // ] t_E_P1 = t_mom_P1*t_mom_P1 + m*m; // [MeV/c^2] t_E_theor_P1 =
                // ForwardTools::Elastics::calc_theor_el_energy(TMath::DegToRad() * t_theta_P1); //
                // [MeV/c^2]

                //----------------------------------------------------------------------------------------------
                // Particle 2 - HADES particle
                fparticlecand = HCategoryManager::getObject(fparticlecand, fParticleCand, 1);
                t_Sector_P2 = fparticlecand->getSector();

                // t_z_start_P2 = fparticlecand->getZ(); // [mm]
                // t_track_length_P2 = fparticlecand->getDistanceToMetaHit(); // [mm]

                auto t_phi_P2 = fparticlecand->getPhi();     // [deg]
                auto t_theta_P2 = fparticlecand->getTheta(); // [deg]

                auto t_mom_P2 = fparticlecand->getMomentum(); // [MeV/c]
                // t_tof_P2 = fparticlecand->getTof(); // [ns]
                // t_beta_P2 = fparticlecand->getBeta(); // [ ]
                // t_E_P2 = t_mom_P2*t_mom_P2 + m*m; // [MeV/c^2]

                // t_mom_theor_P2 = ForwardTools::Elastics::calc_theor_el_mom(TMath::DegToRad() *
                // t_theta_P2); // [MeV/c] t_tof_theor_P2 =
                // ForwardTools::Elastics::calc_theor_el_ToF(t_track_length_P2, TMath::DegToRad() *
                // t_theta_P2); // [ns] t_beta_theor_P2 =
                // ForwardTools::Elastics::calc_theor_el_beta(TMath::DegToRad() * t_theta_P2); // [
                // ] t_E_theor_P2 = ForwardTools::Elastics::calc_theor_el_energy(TMath::DegToRad() *
                // t_theta_P2); // [MeV/c^2]

                //----------------------------------------------------------------------------------------------
                // Two Partices Variables
                t_phi_diff = TMath::Abs(t_phi_P1 - t_phi_P2); // [deg]
                t_tan_theta_product = TMath::Tan(TMath::DegToRad() * t_theta_P1) *
                                      TMath::Tan(TMath::DegToRad() * t_theta_P2); // [ ]

                t_Sector_Pair = -99;
                if ((t_Sector_P1 == 0 && t_Sector_P2 == 3) ||
                    (t_Sector_P1 == 3 && t_Sector_P2 == 0))
                    t_Sector_Pair = 0;
                else if ((t_Sector_P1 == 1 && t_Sector_P2 == 4) ||
                         (t_Sector_P1 == 4 && t_Sector_P2 == 1))
                    t_Sector_Pair = 1;
                else if ((t_Sector_P1 == 2 && t_Sector_P2 == 5) ||
                         (t_Sector_P1 == 5 && t_Sector_P2 == 2))
                    t_Sector_Pair = 2;

                /*
                h_HH_phi_diff->Fill(t_phi_diff);
                h_HH_tan_theta_prod->Fill(t_tan_theta_product);

                if (t_tan_theta_product >= cut_HH_tan_theta_min && t_tan_theta_product <=
                cut_HH_tan_theta_max) h_HH_phi_diff_cut->Fill(t_phi_diff); if (t_phi_diff >=
                cut_HH_phi_diff_min && t_phi_diff <= cut_HH_phi_diff_max)
                h_HH_tan_theta_prod_cut->Fill(t_tan_theta_product);
                */
                num_HH_Entries++;
                if (t_PT1 == 1) num_HH_Entries_PT1++;
                if (t_PT2 == 1) num_HH_Entries_PT2++;
                if (t_PT3 == 1) num_HH_Entries_PT3++;

                if (has_hh_elastic)
                {
                    num_HH_Elastic_Entries++;
                    if (t_PT1 == 1) num_HH_Elastic_Entries_PT1++;
                    if (t_PT2 == 1) num_HH_Elastic_Entries_PT2++;
                    if (t_PT3 == 1) num_HH_Elastic_Entries_PT3++;

                    // h_HH_and_HF_mom_vs_theta_el_P1->Fill(t_theta_P1,t_mom_P1);
                    // h_HH_and_HF_mom_vs_theta_el_P2->Fill(t_theta_P2,t_mom_P2);
                    // h_HH_and_HF_theta_P1_vs_theta_P2_el->Fill(t_theta_P1,t_theta_P2);
                }

                //----------------------------------------------------------------------------------------------
                // Histogram filling
                for (Int_t t = 0; t < num_triggers; t++)
                {
                    // If t == 0 histograms will be filled regardless of active triggers in event
                    if (t == 1 && t_PT1 != 1) continue; // Do not fill PT1 hist if t_PT1 != 1
                    if (t == 2 && t_PT2 != 1) continue; // Do not fill PT2 hist if t_PT1 != 1
                    if (t == 3 && t_PT3 != 1) continue; // Do not fill PT3 hist if t_PT1 != 1

                    h_HH_phi[t]->Fill(t_phi_P1);
                    h_HH_phi[t]->Fill(t_phi_P2);
                    h_HH_theta[t]->Fill(t_theta_P1);
                    h_HH_theta[t]->Fill(t_theta_P2);
                    h_HH_and_HF_theta[t]->Fill(t_theta_P1);
                    h_HH_and_HF_theta[t]->Fill(t_theta_P2);
                    h_HH_phi_diff[t]->Fill(t_phi_diff);
                    h_HH_tan_theta_prod[t]->Fill(t_tan_theta_product);
                    h_HH_and_HF_theta_P1_vs_theta_P2[t]->Fill(t_theta_P1, t_theta_P2);
                    h_HH_and_HF_theta_vs_phi_P1[t]->Fill(t_theta_P1, t_phi_P1);
                    h_HH_and_HF_theta_vs_phi_P2[t]->Fill(t_theta_P2, t_phi_P2);
                    if (t_Sector_Pair >= 0)
                    {
                        h_HH_and_HF_mom_vs_theta_P1[t][t_Sector_Pair]->Fill(t_theta_P1, t_mom_P1);
                        h_HH_and_HF_mom_vs_theta_P2[t][t_Sector_Pair]->Fill(t_theta_P2, t_mom_P2);
                    }
                    if (t_tan_theta_product >= cut_HH_tan_theta_min &&
                        t_tan_theta_product <= cut_HH_tan_theta_max)
                        h_HH_phi_diff_cut[t]->Fill(t_phi_diff);
                    if (t_phi_diff >= cut_HH_phi_diff_min && t_phi_diff <= cut_HH_phi_diff_max)
                        h_HH_tan_theta_prod_cut[t]->Fill(t_tan_theta_product);

                    if (has_hh_elastic)
                    {
                        h_HH_phi_el[t]->Fill(t_phi_P1);
                        h_HH_phi_el[t]->Fill(t_phi_P2);
                        h_HH_theta_el[t]->Fill(t_theta_P1);
                        h_HH_theta_el[t]->Fill(t_theta_P2);
                        h_HH_and_HF_theta_el[t]->Fill(t_theta_P1);
                        h_HH_and_HF_theta_el[t]->Fill(t_theta_P2);
                        h_HH_and_HF_theta_P1_vs_theta_P2_el[t]->Fill(t_theta_P1, t_theta_P2);
                        h_HH_and_HF_theta_vs_phi_P1_el[t]->Fill(t_theta_P1, t_phi_P1);
                        h_HH_and_HF_theta_vs_phi_P2_el[t]->Fill(t_theta_P2, t_phi_P2);
                        if (t_Sector_Pair >= 0)
                        {
                            h_HH_and_HF_mom_vs_theta_P1_el[t][t_Sector_Pair]->Fill(t_theta_P1,
                                                                                   t_mom_P1);
                            h_HH_and_HF_mom_vs_theta_P2_el[t][t_Sector_Pair]->Fill(t_theta_P2,
                                                                                   t_mom_P2);
                        }
                    }
                }
            }
        }

        //----------------------------------------------------------------------------------------------
        // HADES - FwDet case
        //----------------------------------------------------------------------------------------------

        if (particle_cand_cnt == 1 && (anapars.cal_frpc_stage4 || anapars.cal_frpc_stage5))
        {
            forward_cand_cnt = 0;
            if (fForwardCand)
            {
                forward_cand_cnt = fForwardCand->getEntries();

                // We are looking for elastic scattering, so we expect only 1 track in hades and 1
                // track in forward det
                if (forward_cand_cnt == 1)
                {
                    num_Events_HF++;
                    if (t_PT1 == 1) num_HF_Events_PT1++;
                    if (t_PT2 == 1) num_HF_Events_PT2++;
                    if (t_PT3 == 1) num_HF_Events_PT3++;

                    //----------------------------------------------------------------------------------------------
                    // Particle 1 - HADES particle
                    HParticleCand* fparticlecand =
                        HCategoryManager::getObject(fparticlecand, fParticleCand, 0);
                    t_Sector_P1 = fparticlecand->getSector();

//                     auto t_z_start_P1 = fparticlecand->getZ();                      // [mm]
                    auto t_track_length_P1 = fparticlecand->getDistanceToMetaHit(); // [mm]

                    auto t_phi_P1 = fparticlecand->getPhi();     // [deg]
                    auto t_theta_P1 = fparticlecand->getTheta(); // [deg]

                    auto t_mom_P1 = fparticlecand->getMomentum(); // [MeV/c]
                    auto t_tof_P1 = fparticlecand->getTof();      // [ns]
//                     auto t_beta_P1 = fparticlecand->getBeta();    // [ ]

//                     auto t_mom_theor_P1 = ForwardTools::Elastics::calc_theor_el_mom(TMath::DegToRad() * t_theta_P1); // [MeV/c]
                    auto t_tof_theor_P1 = ForwardTools::Elastics::calc_theor_el_ToF(t_track_length_P1, TMath::DegToRad() * t_theta_P1); // [ns]
//                     auto t_beta_theor_P1 = ForwardTools::Elastics::calc_theor_el_beta(TMath::DegToRad() * t_theta_P1); // [ ]
//                     auto t_E_P1 = t_mom_P1 * t_mom_P1 + proton_mass * proton_mass; // [MeV/c^2]
//                     auto t_E_theor_P1 = ForwardTools::Elastics::calc_theor_el_energy(TMath::DegToRad() * t_theta_P1); // [MeV/c^2]

                    //----------------------------------------------------------------------------------------------
                    // Particle 2 - FwDet Particle
                    HForwardCand* fwdetcand = HCategoryManager::getObject(fwdetcand, fForwardCand, 0);

                    if (fwdetcand->getTofRec() <= 0) t_ToF_Rec = 0;
                    else if (fwdetcand->getTofRec() > 0) t_ToF_Rec = 1;

                    Float_t rPlane = fparticlecand->getRprime(xBeam, yBeam);
                    Float_t zPlane = fparticlecand->getZprime(xBeam, yBeam, rPlane);

                    auto t_z_start_P2 = zPlane; //fparticlecand->getZ(); // [mm]

                    // Correction for track start position (Z_START)
                    fwdetcand->setStartXYZ(0.0, 0.0, t_z_start_P2); // We take the track start position from the reconstructed HADES track paired with FwDet track
                    fwdetcand->calcPoints(); // Recalculating ToF and Distance to account for correction
                    fwdetcand->calc4vectorProperties(proton_mass); // Calculating 4 vector properties assuming particles are protons

                    auto t_phi_P2 = fwdetcand->getPhi();     // [deg]
                    auto t_theta_P2 = fwdetcand->getTheta(); // [deg]

                    // Base of track
                    t_FwDet_track_base_X_P2 = fwdetcand->getBaseX();
                    t_FwDet_track_base_Y_P2 = fwdetcand->getBaseY();
                    t_FwDet_track_base_Z_P2 = fwdetcand->getBaseZ();

                    // Direction of track
                    t_FwDet_track_dir_X_P2 = fwdetcand->getDirTx();
                    t_FwDet_track_dir_Y_P2 = fwdetcand->getDirTy();
                    t_FwDet_track_dir_Z_P2 = 1.0;

                    // "Stop" -
                    Float_t t_FwDet_track_stop_X = t_FwDet_track_base_X_P2 + t_FwDet_track_dir_X_P2;
                    Float_t t_FwDet_track_stop_Y = t_FwDet_track_base_Y_P2 + t_FwDet_track_dir_Y_P2;
                    Float_t t_FwDet_track_stop_Z = t_FwDet_track_base_Z_P2 + t_FwDet_track_dir_Z_P2;

                    t_fRPC_hit_num = 0;

                    //----------------------------------------------------------------------------------------------
                    // Two Particles Variables
                    t_phi_diff = TMath::Abs(t_phi_P1 - t_phi_P2); // [deg]
                    t_tan_theta_product = TMath::Tan(TMath::DegToRad() * t_theta_P1) *
                                          TMath::Tan(TMath::DegToRad() * t_theta_P2); // [ ]

                    if (t_Sector_P1 == 0 || t_Sector_P1 == 3)
                        t_Sector_Pair = 0;
                    else if (t_Sector_P1 == 1 || t_Sector_P1 == 4)
                        t_Sector_Pair = 1;
                    else if (t_Sector_P1 == 2 || t_Sector_P1 == 5)
                        t_Sector_Pair = 2;

                    //----------------------------------------------------------------------------------------------
                    // Event counting

                    if (t_ToF_Rec == 1)
                    {
                        num_Events_HF_fRPC_hit++;
                        if (t_PT1 == 1) num_HF_Events_PT1_fRPC_hit++;
                        if (t_PT2 == 1) num_HF_Events_PT2_fRPC_hit++;
                        if (t_PT3 == 1) num_HF_Events_PT3_fRPC_hit++;

                        if (has_hf_elastic)
                        {
                            num_Events_HF_Elastic_fRPC_hit++;
                            if (t_PT1 == 1) num_HF_Events_Elastic_PT1_fRPC_hit++;
                            if (t_PT2 == 1) num_HF_Events_Elastic_PT2_fRPC_hit++;
                            if (t_PT3 == 1) num_HF_Events_Elastic_PT3_fRPC_hit++;
                        }
                    }

                    //----------------------------------------------------------------------------------------------
                    // Histogram filling
                    if (t_ToF_Rec == 1)
                    {
                        for (Int_t t = 0; t < num_triggers; t++)
                        {
                            // If t == 0 histograms will be filled regardless of active triggers in
                            // event
                            if (t == 1 && t_PT1 != 1)
                                continue; // Do not fill PT1 hist if t_PT1 != 1
                            if (t == 2 && t_PT2 != 1)
                                continue; // Do not fill PT2 hist if t_PT1 != 1
                            if (t == 3 && t_PT3 != 1)
                                continue; // Do not fill PT3 hist if t_PT1 != 1

                            h_HF_phi[t]->Fill(t_phi_P1);
                            h_HF_phi[t]->Fill(t_phi_P2);
                            h_HF_theta[t]->Fill(t_theta_P1);
                            h_HF_theta[t]->Fill(t_theta_P2);
                            h_HH_and_HF_theta[t]->Fill(t_theta_P1);
                            h_HH_and_HF_theta[t]->Fill(t_theta_P2);
                            h_HF_phi_diff[t]->Fill(t_phi_diff);
                            h_HF_tan_theta_prod[t]->Fill(t_tan_theta_product);
                            h_HH_and_HF_theta_P1_vs_theta_P2[t]->Fill(t_theta_P1, t_theta_P2);
                            h_HH_and_HF_theta_vs_phi_P1[t]->Fill(t_theta_P1, t_phi_P1);
                            h_HH_and_HF_theta_vs_phi_P2[t]->Fill(t_theta_P2, t_phi_P2);
                            if (t_Sector_Pair >= 0)
                            {
                                h_HH_and_HF_mom_vs_theta_P1[t][t_Sector_Pair]->Fill(t_theta_P1,
                                                                                    t_mom_P1);
                                // h_HH_and_HF_mom_vs_theta_P2[t][t_Sector_Pair]->Fill(t_theta_P2,t_mom_P2);
                            }

                            if (t_tan_theta_product >= cut_HF_tan_theta_min &&
                                t_tan_theta_product <= cut_HF_tan_theta_max)
                                h_HF_phi_diff_cut[t]->Fill(t_phi_diff);
                            if (t_phi_diff >= cut_HF_phi_diff_min &&
                                t_phi_diff <= cut_HF_phi_diff_max)
                                h_HF_tan_theta_prod_cut[t]->Fill(t_tan_theta_product);

                            if (has_hf_elastic)
                            {
                                h_HF_phi_el[t]->Fill(t_phi_P1);
                                h_HF_phi_el[t]->Fill(t_phi_P2);
                                h_HF_theta_el[t]->Fill(t_theta_P1);
                                h_HF_theta_el[t]->Fill(t_theta_P2);
                                h_HH_and_HF_theta_el[t]->Fill(t_theta_P1);
                                h_HH_and_HF_theta_el[t]->Fill(t_theta_P2);
                                h_HH_and_HF_theta_P1_vs_theta_P2_el[t]->Fill(t_theta_P1,
                                                                             t_theta_P2);
                                h_HH_and_HF_theta_vs_phi_P1_el[t]->Fill(t_theta_P1, t_phi_P1);
                                h_HH_and_HF_theta_vs_phi_P2_el[t]->Fill(t_theta_P2, t_phi_P2);
                                if (t_Sector_Pair >= 0)
                                {
                                    h_HH_and_HF_mom_vs_theta_P1_el[t][t_Sector_Pair]->Fill(
                                        t_theta_P1, t_mom_P1);
                                    // h_HH_and_HF_mom_vs_theta_P2_el[t][t_Sector_Pair]->Fill(t_theta_P2,t_mom_P2);
                                }
                            }
                        }
                    }

                    if (fStsCal)
                    {
                        //int fwdet_cal_cnt = fStsCal->getEntries();
                        HStsCal* fwdetstraw = 0;

                        // Number of hits used in track reconstruction
                        int fcand_num_of_hits = fwdetcand->getNofHits();

                        for (int j = 0; j < fcand_num_of_hits; ++j) // Loop over hits used in track reconstruction
                        {
                            int hit_index = fwdetcand->getHitIndex(j); // Recalculating from internal hit_index within track to general hit_index in STS
                            fwdetstraw = HCategoryManager::getObject(fwdetstraw, fStsCal, hit_index);

                            // Initializing extracted variables
                            Char_t mod, lay, ud;      // mod - Module, lay - Layer, ud - upper/lower (short) or long straw
                            Int_t straw; // Straw index, plane index (0-7)
                            // Float_t time, width; // time - time of first registration of signal (crossing threshold), width - Time over Threshold

                            // Getting values of each variable in this entry using functions
                            fwdetstraw->getAddress(mod, lay, straw, ud);
//                             Float_t u = fwdetstraw->getU();
//                             Float_t z = fwdetstraw->getZ();

                            // Straw position
//                             auto t_STS_straw_pos_X_P2 = stsCellsLab[mod][lay][straw]->X(); // [mm]
//                             auto t_STS_straw_pos_Y_P2 = stsCellsLab[mod][lay][straw]->Y(); // [mm]
                            auto t_STS_straw_pos_Z_P2 = stsCellsLab[mod][lay][straw]->Z() - t_z_start_P2; // [mm]

                            /*
                            t_STS_straw_pos_X_P2 = u * sts_cosa[mod][lay]; // [mm]
                            t_STS_straw_pos_Y_P2 = u * sts_sina[mod][lay]; // [mm]
                            t_STS_straw_pos_Z_P2 = z - t_z_start_P2; // [mm]
                            */

                            // fRPC hit coordinates
                            auto t_STS_hit_pos_X_P2 = ForwardTools::Miscellaneous::calc_hit_pos(t_FwDet_track_base_X_P2, t_FwDet_track_stop_X, t_FwDet_track_base_Z_P2, t_FwDet_track_stop_Z, t_STS_straw_pos_Z_P2); // [mm]
                            auto t_STS_hit_pos_Y_P2 = ForwardTools::Miscellaneous::calc_hit_pos(t_FwDet_track_base_Y_P2, t_FwDet_track_stop_Y, t_FwDet_track_base_Z_P2, t_FwDet_track_stop_Z, t_STS_straw_pos_Z_P2); // [mm]
//                             auto t_STS_hit_pos_Z_P2 = t_STS_straw_pos_Z_P2; // [mm]

                            h_sts_hit_XY[mod][lay]->Fill(t_STS_hit_pos_X_P2,t_STS_hit_pos_Y_P2);
                            if (has_hf_elastic && t_ToF_Rec == 1)
                            {
                                h_sts_hit_XY_el[mod][lay]->Fill(t_STS_hit_pos_X_P2,t_STS_hit_pos_Y_P2);
                            }
                        }
                    }

                    //----------------------------------------------------------------------------------------------
                    // fRPC accessing (for calibration purposes)
                    if (fFRpcCal)
                    {
                        int frpc_cal_cnt = fFRpcCal->getEntries();

                        HFRpcCal* frpccal = 0;

                        for (int j = 0; j < frpc_cal_cnt; ++j)
                        {
                            //fTof= (fTimeN+fTimeF - L/v)/2.0 - to;   // tof
                            //fV = (fTimeF-fTimeN)*v/2. - po;        // pos along time axis
                            //fX = pos->X() - fV * sina;
                            //fY = pos->Y() + fV * cosa;

                            frpccal = HCategoryManager::getObject(frpccal, fFRpcCal, j);
                            t_fRPC_hit_num = t_fRPC_hit_num + 1;

                            Char_t sec, cstrip;

                            frpccal->getAddress(sec, cstrip); // Strip address
                            t_fRPC_Module = sec;       // Hit in module nr. "sec"
                            t_fRPC_Strip = cstrip;     // Hit in strip nr. "cstrip"

                            // fRPC strip coordinates
                            auto t_fRPC_strip_pos_X_P2 = frpcCellsLab[t_fRPC_Module][t_fRPC_Strip]->X(); // [mm]
                            auto t_fRPC_strip_pos_Y_P2 = frpcCellsLab[t_fRPC_Module][t_fRPC_Strip]->Y(); // [mm]
                            auto t_fRPC_strip_pos_Z_P2 = frpcCellsLab[t_fRPC_Module][t_fRPC_Strip]->Z() - t_z_start_P2; // [mm]

                            // fRPC hit coordinates
                            t_fRPC_hit_pos_X_P2 = ForwardTools::Miscellaneous::calc_hit_pos(t_FwDet_track_base_X_P2, t_FwDet_track_stop_X, t_FwDet_track_base_Z_P2, t_FwDet_track_stop_Z, t_fRPC_strip_pos_Z_P2); // [mm]
                            t_fRPC_hit_pos_Y_P2 = ForwardTools::Miscellaneous::calc_hit_pos(t_FwDet_track_base_Y_P2, t_FwDet_track_stop_Y, t_FwDet_track_base_Z_P2, t_FwDet_track_stop_Z, t_fRPC_strip_pos_Z_P2); // [mm]
                            t_fRPC_hit_pos_Z_P2 = t_fRPC_strip_pos_Z_P2; // [mm]

                            frpccal->getHit(t_fRPC_time_N_P2, t_fRPC_time_F_P2, t_fRPC_charge_N_P2, t_fRPC_charge_F_P2);

                            auto t_track_length_P2 = TMath::Sqrt(t_fRPC_hit_pos_X_P2 * t_fRPC_hit_pos_X_P2 +
                                            t_fRPC_hit_pos_Y_P2 * t_fRPC_hit_pos_Y_P2 +
                                            t_fRPC_hit_pos_Z_P2 * t_fRPC_hit_pos_Z_P2); // [mm]

                            // Position of hit in local coordinates of the strip - start in X-Y
                            // coordinates with (0,0) at the middle of the strip
                            t_frpc_hit_pos_X_loc = (t_fRPC_hit_pos_X_P2 - t_fRPC_strip_pos_X_P2);
                            t_frpc_hit_pos_Y_loc = (t_fRPC_hit_pos_Y_P2 - t_fRPC_strip_pos_Y_P2);
                            t_frpc_hit_pos_U = 0.0;
                            t_frpc_hit_pos_V = 0.0;

                            t_frpc_hit_pos_U = t_frpc_hit_pos_Y_loc * frpc_sina[sec] +
                                               t_frpc_hit_pos_X_loc * frpc_cosa[sec];
                            t_frpc_hit_pos_V = t_frpc_hit_pos_Y_loc * frpc_cosa[sec] -
                                               t_frpc_hit_pos_X_loc * frpc_sina[sec];

                            if (t_ToF_Rec == 1)
                            {
                                h_frpc_cal_hit_U[t_fRPC_Module][t_fRPC_Strip]->Fill(t_frpc_hit_pos_U);
                                h_frpc_cal_hit_V[t_fRPC_Module][t_fRPC_Strip]->Fill(t_frpc_hit_pos_V);
                            }

                            auto t_tof_P2 = frpccal->getTof(); // [ns]

                            // Selection of ToF calculation scheme for calibration
                            if (anapars.alt_ToF)
                            {
                                t_tof_P2 = ForwardTools::Elastics::alt_ToF_calc(t_tof_P1, t_tof_theor_P1, t_tof_P2); // [ns]
                            }
                            /*
                            else
                            {
                                t_tof_P2 = frpccal->getTof(); // [ns]
                            }
                            */

                            auto t_tof_theor_P2 = ForwardTools::Elastics::calc_theor_el_ToF(t_track_length_P2, TMath::DegToRad() * t_theta_P2); // [ns]

                            //----------------------------------------------------------------------------------------------
                            // Entries counting
                            num_HF_Entries = num_HF_Entries + 1;
                            if (t_PT1 == 1) num_HF_Entries_PT1 = num_HF_Entries_PT1 + 1;
                            if (t_PT2 == 1) num_HF_Entries_PT2 = num_HF_Entries_PT2 + 1;
                            if (t_PT3 == 1) num_HF_Entries_PT3 = num_HF_Entries_PT3 + 1;

                            h_frpc_hit_XY[t_fRPC_Module]->Fill(t_fRPC_hit_pos_X_P2,t_fRPC_hit_pos_Y_P2);
                            h_frpc_hit_XY_ALL->Fill(t_fRPC_hit_pos_X_P2,t_fRPC_hit_pos_Y_P2);

                            if (has_hf_elastic && t_ToF_Rec == 1)
                            {
                                num_HF_Elastic_Entries = num_HF_Elastic_Entries + 1;
                                if (t_PT1 == 1) num_HF_Elastic_Entries_PT1 = num_HF_Elastic_Entries_PT1 + 1;
                                if (t_PT2 == 1) num_HF_Elastic_Entries_PT2 = num_HF_Elastic_Entries_PT2 + 1;
                                if (t_PT3 == 1) num_HF_Elastic_Entries_PT3 = num_HF_Elastic_Entries_PT3 + 1;

                                // fRPC - ToF Calibration - ToF hists - elastic
                                h_hades_tof_exp_el[t_fRPC_Module][t_fRPC_Strip]->Fill(t_tof_P1);
                                h_hades_tof_theor_el[t_fRPC_Module][t_fRPC_Strip]->Fill(t_tof_theor_P1);
                                h_frpc_cal_tof_el[t_fRPC_Module][t_fRPC_Strip]->Fill(t_tof_P2);
                                h_frpc_cal_delta_tof_el[t_fRPC_Module][t_fRPC_Strip]->Fill(t_tof_P2 - t_tof_theor_P2);
                                h_frpc_cal_tof_2D_el[t_fRPC_Module]->Fill(t_fRPC_Strip, t_tof_P2);
                                h_frpc_cal_delta_tof_2D_el[t_fRPC_Module]->Fill(t_fRPC_Strip, (t_tof_P2 - t_tof_theor_P2));
                                //h_frpc_cal_tof_vs_theta_el[t_fRPC_Module]->Fill(t_theta_P2, t_tof_P2);
                                //h_frpc_cal_delta_tof_vs_theta_el[t_fRPC_Module]->Fill(t_theta_P2, (t_tof_P2 - t_tof_theor_P2));
                                h_frpc_cal_delta_tof_vs_V_el[t_fRPC_Module][t_fRPC_Strip]->Fill(t_frpc_hit_pos_V, (t_tof_P2 - t_tof_theor_P2));
                                h_frpc_cal_tof_vs_theta_el[t_fRPC_Module][t_fRPC_Strip]->Fill(t_theta_P2, t_tof_P2);
                                h_frpc_cal_delta_tof_vs_theta_el[t_fRPC_Module][t_fRPC_Strip]->Fill(t_theta_P2, (t_tof_P2 - t_tof_theor_P2));
                                h_frpc_hit_XY_el[t_fRPC_Module]->Fill(t_fRPC_hit_pos_X_P2,t_fRPC_hit_pos_Y_P2);
                                h_frpc_hit_XY_ALL_el->Fill(t_fRPC_hit_pos_X_P2,t_fRPC_hit_pos_Y_P2);
                            }
                        }
                    }
                }
            }
        }
    } // end eventloop

    //----------------------------------------------------------------------------------------------
    // End of eventloop
    //----------------------------------------------------------------------------------------------

    timer.Stop();
    cout << "//----------------------------------------------------------------------------------------------" << endl;
    cout << "End of eventloop, T = " << timer.RealTime() << " s" << endl;
    cout << "//----------------------------------------------------------------------------------------------" << endl;

    //----------------------------------------------------------------------------------------------
    // fRPC calibration
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "fRPC calibration ... " << endl;

    // 300, -900, 900
//     Int_t project_step = 1800 / 30;
//     Int_t project_min = -900;

    //ForwardTools::Drawing::draw_and_write_2D(c_frpc_hit_XY_ALL, h_frpc_hit_XY_ALL, dir_out_frpc_cal_frpc_hits, -999.9);
    //ForwardTools::Drawing::draw_and_write_2D(c_frpc_hit_XY_ALL_el, h_frpc_hit_XY_ALL_el, dir_out_frpc_cal_frpc_hits, -999.9);

    c_frpc_hit_XY_ALL->cd();    h_frpc_hit_XY_ALL->Draw("colz");
    c_frpc_hit_XY_ALL_el->cd();    h_frpc_hit_XY_ALL_el->Draw("colz");

    for (auto i = frpc_min_sector; i < frpc_sectors; ++i) // Loop over sectors
    {
        for (auto j = 0; j < frpc_cstrips; ++j) // Loop over cstrips
        {
            cout << "Sector = " << i << " ; Strip = " << j << endl;

            //----------------------------------------------------------------------------------------------
            // Stage 1 - Thresholds calibration
            //----------------------------------------------------------------------------------------------
            c_frpc_raw_q[i]->cd(1 + j);
            format_h_3(h_frpc_raw_q_n[i][j]);
            format_h_3(h_frpc_raw_q_f[i][j]);

            h_frpc_raw_q_n[i][j]->Draw();
            h_frpc_raw_q_f[i][j]->Draw("same");

            ForwardTools::Drawing::draw_and_write(c_frpc_raw_q_n[i][j], h_frpc_raw_q_n[i][j], dir_r_q, -999.9);
            ForwardTools::Drawing::draw_and_write(c_frpc_raw_q_f[i][j], h_frpc_raw_q_f[i][j], dir_r_q, -999.9);

            /*
            c_frpc_raw_q_n[i][j]->cd();
            h_frpc_raw_q_n[i][j]->Draw();

            c_frpc_raw_q_f[i][j]->cd();
            h_frpc_raw_q_f[i][j]->Draw();

            dir_r_q->cd();
            h_frpc_raw_q_n[i][j]->Write();
            c_frpc_raw_q_n[i][j]->Write();
            h_frpc_raw_q_f[i][j]->Write();
            c_frpc_raw_q_f[i][j]->Write();
            */

            //----------------------------------------------------------------------------------------------

            c_frpc_cal_q[i]->cd(1 + j);
            format_h_3(h_frpc_cal_q_n[i][j]);
            format_h_3(h_frpc_cal_q_f[i][j]);

            h_frpc_cal_q_n[i][j]->Draw();
            h_frpc_cal_q_f[i][j]->Draw("same");

            ForwardTools::Drawing::draw_and_write(c_frpc_cal_q_n[i][j], h_frpc_cal_q_n[i][j], dir_r_cal_q, -999.9);
            ForwardTools::Drawing::draw_and_write(c_frpc_cal_q_f[i][j], h_frpc_cal_q_f[i][j], dir_r_cal_q, -999.9);

            /*
            c_frpc_cal_q_n[i][j]->cd();
            h_frpc_cal_q_n[i][j]->Draw();

            c_frpc_cal_q_f[i][j]->cd();
            h_frpc_cal_q_f[i][j]->Draw();

            dir_r_cal_q->cd();
            h_frpc_cal_q_n[i][j]->Write();
            c_frpc_cal_q_n[i][j]->Write();
            h_frpc_cal_q_f[i][j]->Write();
            c_frpc_cal_q_f[i][j]->Write();
            */

            //----------------------------------------------------------------------------------------------
            // Stage 4 - Time of Flight calibration
            //----------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------
            // Calculating Delta ToF from distribution fitting fRPC ToF elasting scattering histograms (per strip)
            c_frpc_cal_delta_tof_strips_el[i][j]->cd();    h_frpc_cal_delta_tof_el[i][j]->Draw();
            c_frpc_cal_delta_tof_vs_V_strips_el[i][j]->cd();    h_frpc_cal_delta_tof_vs_V_el[i][j]->Draw("colz");
            c_frpc_cal_tof_vs_theta_strips_el[i][j]->cd();    h_frpc_cal_tof_vs_theta_el[i][j]->Draw("colz");
            c_frpc_cal_delta_tof_vs_theta_strips_el[i][j]->cd();    h_frpc_cal_delta_tof_vs_theta_el[i][j]->Draw("colz");
            c_frpc_cal_tof_strips_el[i][j]->cd();    h_frpc_cal_tof_el[i][j]->Draw();

            /*
            TH1D *h_temp_project;
            dir_out_frpc_cal_temp_hists->cd();
            for (Int_t p = 0; p<30; p++)
            {
                sprintf(buff1, "c_fcand_delta_tof_vs_V_projection_M=%d_S=%d_B=%d", i, j, p);
                h_temp_project= h_frpc_cal_delta_tof_vs_V_el[i][j]->ProjectionY(buff1,(project_min +
            project_step*p),(project_min + project_step*(p+1)),""); gaus_params
            frpc_delta_tof_project_el = ForwardTools::Miscellaneous::fit_gaus(h_temp_project);
                Float_t frpc_delta_tof_project_el_mean = frpc_delta_tof_project_el.gaus_mean;
                h_frpc_cal_delta_tof_el[i][j]->Write();
            }
            */

            //----------------------------------------------------------------------------------------------
            // Writing hists to file
            dir_out_frpc_cal_delta_tof_hists->cd();
            h_frpc_cal_tof_vs_theta_el[i][j]->Write();
            h_frpc_cal_delta_tof_vs_theta_el[i][j]->Write();
            h_frpc_cal_delta_tof_vs_V_el[i][j]->Write();

            h_frpc_cal_delta_tof_el[i][j]->Write();
            h_frpc_cal_tof_el[i][j]->Write();
            h_hades_tof_exp_el[i][j]->Write();
            h_hades_tof_theor_el[i][j]->Write();

            dir_out_frpc_cal_delta_tof_cans->cd();
            c_frpc_cal_tof_vs_theta_strips_el[i][j]->Write();
            c_frpc_cal_delta_tof_vs_theta_strips_el[i][j]->Write();
            c_frpc_cal_delta_tof_vs_V_strips_el[i][j]->Write();

            c_frpc_cal_delta_tof_strips_el[i][j]->Write();
            c_frpc_cal_tof_strips_el[i][j]->Write();
            c_hades_tof_exp_el[i][j]->Write();
            c_hades_tof_theor_el[i][j]->Write();

            c_frpc_cal_hit_U[i][j]->cd();    h_frpc_cal_hit_U[i][j]->Draw();
            c_frpc_cal_hit_V[i][j]->cd();    h_frpc_cal_hit_V[i][j]->Draw();

            dir_out_frpc_cal_delta_tof_hists->cd();
            h_frpc_cal_hit_U[i][j]->Write();    h_frpc_cal_hit_V[i][j]->Write();
            dir_out_frpc_cal_delta_tof_cans->cd();
            c_frpc_cal_hit_U[i][j]->Write();    c_frpc_cal_hit_V[i][j]->Write();

            //----------------------------------------------------------------------------------------------
            // Stage 5 - Walk Effect correction
            //----------------------------------------------------------------------------------------------

            // Drawing T_diff strip histograms on canvases
            c_frpc_t_diff_vs_charge[i][j]->cd();
            h_frpc_t_diff_vs_charge[i][j]->Draw();
            c_frpc_t_diff_vs_charge_el[i][j]->cd();
            h_frpc_t_diff_vs_charge_el[i][j]->Draw();

            dir_out_frpc_cal_t_diff_vs_charge_hists->cd();
            h_frpc_t_diff_vs_charge[i][j]->Write();
            h_frpc_t_diff_vs_charge_el[i][j]->Write();
            dir_out_frpc_cal_t_diff_vs_charge_cans->cd();
            c_frpc_t_diff_vs_charge[i][j]->Write();
            c_frpc_t_diff_vs_charge_el[i][j]->Write();

        }

        c_frpc_hit_XY_el[i]->cd();    h_frpc_hit_XY_el[i]->Draw("colz");
        c_frpc_hit_XY[i]->cd();    h_frpc_hit_XY[i]->Draw("colz");

        for (uint j = 0; j < frpc_cstrips; ++j) // Loop over cstrips
        {
            // fRPC hit positions
            c_frpc_hit_XY_el[i]->cd();    draw_point(frpcCellsLab[i][j]->X(), frpcCellsLab[i][j]->Y(), 5.0);
            c_frpc_hit_XY[i]->cd();    draw_point(frpcCellsLab[i][j]->X(), frpcCellsLab[i][j]->Y(), 5.0);

            c_frpc_hit_XY_ALL->cd();    draw_point(frpcCellsLab[i][j]->X(), frpcCellsLab[i][j]->Y(), 5.0);
            c_frpc_hit_XY_ALL_el->cd();    draw_point(frpcCellsLab[i][j]->X(), frpcCellsLab[i][j]->Y(), 5.0);
        }

        dir_out_frpc_cal_frpc_hits->cd();    h_frpc_hit_XY_el[i]->Write();    h_frpc_hit_XY[i]->Write();
        dir_out_frpc_cal_frpc_hits->cd();    c_frpc_hit_XY_el[i]->Write();    c_frpc_hit_XY[i]->Write();

        //ForwardTools::Drawing::draw_and_write_2D(c_frpc_hit_XY[i], h_frpc_hit_XY[i], dir_out_frpc_cal_frpc_hits, -999.9);
        //ForwardTools::Drawing::draw_and_write_2D(c_frpc_hit_XY_el[i], h_frpc_hit_XY_el[i], dir_out_frpc_cal_frpc_hits, -999.9);

        dir_r_q->cd();
        c_frpc_raw_q[i]->Write();

        ForwardTools::Drawing::draw_and_write_2D(c_frpc_cal_tof_2D_el[i], h_frpc_cal_tof_2D_el[i], dir_out_frpc_cal_tof_2D, -999.9);
        ForwardTools::Drawing::draw_and_write_2D(c_frpc_cal_delta_tof_2D_el[i], h_frpc_cal_delta_tof_2D_el[i], dir_out_frpc_cal_tof_2D, -999.9);

        //ForwardTools::Drawing::draw_and_write_2D(c_frpc_cal_tof_vs_theta_el[i], h_frpc_cal_tof_vs_theta_el[i], dir_out_frpc_cal_tof_2D, -999.9);
        //ForwardTools::Drawing::draw_and_write_2D(c_frpc_cal_delta_tof_vs_theta_el[i], h_frpc_cal_delta_tof_vs_theta_el[i], dir_out_frpc_cal_tof_2D, -999.9);
    }

    dir_out_frpc_cal_frpc_hits->cd();    h_frpc_hit_XY_ALL_el->Write();    h_frpc_hit_XY_ALL->Write();
    dir_out_frpc_cal_frpc_hits->cd();    c_frpc_hit_XY_ALL_el->Write();    c_frpc_hit_XY_ALL->Write();

    // Loops over modules and layers
    for (Int_t m = 0; m < STS_MAX_MODULES; ++m)
    {
        for (Int_t l = 0; l < STS_MAX_LAYERS; ++l)
        {
            c_sts_hit_XY_el[m][l]->cd();    h_sts_hit_XY_el[m][l]->Draw("colz");
            c_sts_hit_XY[m][l]->cd();    h_sts_hit_XY[m][l]->Draw("colz");

            for (Int_t c = 0; c < STS_MAX_CELLS; ++c)
            {
                c_sts_hit_XY_el[m][l]->cd();    draw_point(stsCellsLab[m][l][c]->X(), stsCellsLab[m][l][c]->Y(), 5.0);
                c_sts_hit_XY[m][l]->cd();    draw_point(stsCellsLab[m][l][c]->X(), stsCellsLab[m][l][c]->Y(), 5.0);

            }

            dir_out_frpc_cal_sts_hits->cd();    h_sts_hit_XY_el[m][l]->Write();    h_sts_hit_XY[m][l]->Write();
            dir_out_frpc_cal_sts_hits->cd();    c_sts_hit_XY_el[m][l]->Write();    c_sts_hit_XY[m][l]->Write();

            //ForwardTools::Drawing::draw_and_write_2D(c_sts_hit_XY[m][l], h_sts_hit_XY[m][l], dir_out_frpc_cal_sts_hits, -999.9);
            //ForwardTools::Drawing::draw_and_write_2D(c_sts_hit_XY_el[m][l], h_sts_hit_XY_el[m][l], dir_out_frpc_cal_sts_hits, -999.9);
        }
    }

    f_root_output->cd();
    timer.Stop();
    cout << "-> Finished, T = " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Drawing, writing and saving (as pictures) histograms and canvases
    //----------------------------------------------------------------------------------------------

    cout << "Drawing, writing and saving (as pictures) histograms and canvases ... " << endl;

    //----------------------------------------------------------------------------------------------
    // Miscellaneous histograms drawing

    TDirectory* dir_out_temp = dir_out_all_triggers;
    Double_t factor = 1.0;

    if (anapars.sim == 1)
    {
        cout << "Number of entries in \"h_ALL_theta\" histogram = "
             << h_ALL_theta->GetEntries() << endl;
        h_ALL_theta->Scale(factor / 500000);
        ForwardTools::Drawing::draw_write_and_save_picture(
            c_ALL_theta, h_ALL_theta, dir_out_all_triggers, -999.9,
            format_name(anapars.outpath, c_ALL_theta->GetName()));
    }

    if (anapars.cal_frpc_stage4 || anapars.cal_frpc_stage5)
    {
        for (Int_t t = 0; t < num_triggers; t++)
        {
            if (t == 0)
                dir_out_temp = dir_out_all_triggers;
            else if (t == 1)
                dir_out_temp = dir_out_PT1_trigger;
            else if (t == 2)
                dir_out_temp = dir_out_PT2_trigger;
            else if (t == 3)
                dir_out_temp = dir_out_PT3_trigger;

            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HF_phi[t], h_HF_phi[t], dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HF_phi[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HF_phi_el[t], h_HF_phi_el[t], dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HF_phi_el[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HF_theta[t], h_HF_theta[t], dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HF_theta[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HF_theta_el[t], h_HF_theta_el[t], dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HF_theta_el[t]->GetName()));

            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_phi[t], h_HH_phi[t], dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HH_phi[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_phi_el[t], h_HH_phi_el[t], dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HH_phi_el[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_theta[t], h_HH_theta[t], dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HH_theta[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_theta_el[t], h_HH_theta_el[t], dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HH_theta_el[t]->GetName()));

            h_HH_and_HF_theta[t]->Scale(factor / norm_factor);
            h_HH_and_HF_theta_el[t]->Scale(factor / norm_factor);
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_and_HF_theta[t], h_HH_and_HF_theta[t], dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HH_and_HF_theta[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_and_HF_theta_el[t], h_HH_and_HF_theta_el[t], dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HH_and_HF_theta_el[t]->GetName()));

            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HF_phi_diff[t], h_HF_phi_diff[t], dir_out_temp, 180.0,
                format_name(anapars.outpath, c_HF_phi_diff[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HF_phi_diff_cut[t], h_HF_phi_diff_cut[t], dir_out_temp, 180.0,
                format_name(anapars.outpath, c_HF_phi_diff_cut[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_phi_diff[t], h_HH_phi_diff[t], dir_out_temp, 180.0,
                format_name(anapars.outpath, c_HH_phi_diff[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_phi_diff_cut[t], h_HH_phi_diff_cut[t], dir_out_temp, 180.0,
                format_name(anapars.outpath, c_HH_phi_diff_cut[t]->GetName()));

            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HF_tan_theta_prod[t], h_HF_tan_theta_prod[t], dir_out_temp, 0.3088,
                format_name(anapars.outpath, c_HF_tan_theta_prod[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HF_tan_theta_prod_cut[t], h_HF_tan_theta_prod_cut[t], dir_out_temp, 0.3088,
                format_name(anapars.outpath, c_HF_tan_theta_prod_cut[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_tan_theta_prod[t], h_HH_tan_theta_prod[t], dir_out_temp, 0.3088,
                format_name(anapars.outpath, c_HF_phi[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_tan_theta_prod_cut[t], h_HH_tan_theta_prod_cut[t], dir_out_temp, 0.3088,
                format_name(anapars.outpath, c_HH_tan_theta_prod_cut[t]->GetName()));

            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_and_HF_theta_P1_vs_theta_P2[t], h_HH_and_HF_theta_P1_vs_theta_P2[t],
                dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HH_and_HF_theta_P1_vs_theta_P2[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_and_HF_theta_P1_vs_theta_P2_el[t], h_HH_and_HF_theta_P1_vs_theta_P2_el[t],
                dir_out_temp, -999.9,
                format_name(anapars.outpath, c_HH_and_HF_theta_P1_vs_theta_P2_el[t]->GetName()));

            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_and_HF_theta_vs_phi_P1[t], h_HH_and_HF_theta_vs_phi_P1[t], dir_out_temp,
                -999.9, format_name(anapars.outpath, c_HH_and_HF_theta_vs_phi_P1[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_and_HF_theta_vs_phi_P1_el[t], h_HH_and_HF_theta_vs_phi_P1_el[t], dir_out_temp,
                -999.9, format_name(anapars.outpath, c_HH_and_HF_theta_vs_phi_P1_el[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_and_HF_theta_vs_phi_P2[t], h_HH_and_HF_theta_vs_phi_P2[t], dir_out_temp,
                -999.9, format_name(anapars.outpath, c_HH_and_HF_theta_vs_phi_P2[t]->GetName()));
            ForwardTools::Drawing::draw_write_and_save_picture(
                c_HH_and_HF_theta_vs_phi_P2_el[t], h_HH_and_HF_theta_vs_phi_P2_el[t], dir_out_temp,
                -999.9, format_name(anapars.outpath, c_HH_and_HF_theta_vs_phi_P2_el[t]->GetName()));

            for (Int_t p = 0; p < 3; ++p)
            {
                ForwardTools::Drawing::draw_write_and_save_picture(
                    c_HH_and_HF_mom_vs_theta_P1[t][p], h_HH_and_HF_mom_vs_theta_P1[t][p],
                    dir_out_temp, -999.9,
                    format_name(anapars.outpath, c_HH_and_HF_mom_vs_theta_P1[t][p]->GetName()));
                ForwardTools::Drawing::draw_write_and_save_picture(
                    c_HH_and_HF_mom_vs_theta_P1_el[t][p], h_HH_and_HF_mom_vs_theta_P1_el[t][p],
                    dir_out_temp, -999.9,
                    format_name(anapars.outpath, c_HH_and_HF_mom_vs_theta_P1_el[t][p]->GetName()));
                ForwardTools::Drawing::draw_write_and_save_picture(
                    c_HH_and_HF_mom_vs_theta_P2[t][p], h_HH_and_HF_mom_vs_theta_P2[t][p],
                    dir_out_temp, -999.9,
                    format_name(anapars.outpath, c_HH_and_HF_mom_vs_theta_P2[t][p]->GetName()));
                ForwardTools::Drawing::draw_write_and_save_picture(
                    c_HH_and_HF_mom_vs_theta_P2_el[t][p], h_HH_and_HF_mom_vs_theta_P2_el[t][p],
                    dir_out_temp, -999.9,
                    format_name(anapars.outpath, c_HH_and_HF_mom_vs_theta_P2_el[t][p]->GetName()));
            }
        }
    }
    dir_out_frpc_cal->cd();

    // FRPC
    rpc_draw_and_write(c_frpc_raw_cnts, reinterpret_cast<TH1**>(h_frpc_raw_cnts), frpc_sectors, f_root_output, dir_r_raw, "colz");
    rpc_draw_and_write(c_frpc_raw_mult, reinterpret_cast<TH1**>(h_frpc_raw_mult), frpc_sectors, f_root_output, dir_r_raw, "colz");
    rpc_draw_stack_and_write(c_frpc_raw_mults, s_frpc_raw_mults, reinterpret_cast<TH1**>(h_frpc_raw_mults), 4, f_root_output, dir_r_raw, "h,text");
    rpc_draw_stack_and_write(c_frpc_cal_mults, s_frpc_cal_mults, reinterpret_cast<TH1**>(h_frpc_cal_mults), 4, f_root_output, dir_r_cal, "h,text");
    rpc_draw_and_write(c_frpc_cal_v, reinterpret_cast<TH1**>(h_frpc_cal_v), frpc_sectors, f_root_output, dir_r_cal, "colz"); //, gr_frpc_cal_v, &frpc_draw_v_lines);
    rpc_draw_and_write(c_frpc_cal_v_manual, reinterpret_cast<TH1**>(h_frpc_cal_v_manual), frpc_sectors, f_root_output, dir_r_cal, "colz"); //, gr_frpc_cal_v, &frpc_draw_v_lines);
    rpc_draw_and_write(c_frpc_cal_time_n, reinterpret_cast<TH1**>(h_frpc_cal_time_n), frpc_sectors, f_root_output, dir_r_cal, "colz"); //, gr_frpc_cal_v, &frpc_draw_v_lines);
    rpc_draw_and_write(c_frpc_cal_time_f, reinterpret_cast<TH1**>(h_frpc_cal_time_f), frpc_sectors, f_root_output, dir_r_cal, "colz"); //, gr_frpc_cal_v, &frpc_draw_v_lines);

    //------------------------------------------------------------------------------------------------------
    // Writing calibration parameters to file and statistics to screen

    cout << "Writing calibration parameters to file and statistics to screen ... " << endl;

    pFRpcCalPar->write(f_param_output);
    f_param_output->close();

    cout << "//"
            "--------------------------------------------------------------------------------------"
            "----------------"
         << endl;
    cout << "Events counts:" << endl;

    cout << "Number of events with no entires in fStart2Hit: " << no_fStart2Hit_cnt << " out of "
         << (limit_sto - limit_sta) << endl;
    cout << "Number of events with Flag = 0 in fStart2Hit: " << rejected_no_T0_cnt << " out of "
         << (limit_sto - limit_sta - no_fStart2Hit_cnt) << endl;

    cout << "Number of elastic ALL events = " << num_Events_Total_Elastic << " out of "
         << num_Events_Total << " events" << endl;
    cout << "Number of elastic HH events = " << num_Events_HH_Elastic << " out of " << num_Events_HH
         << " events" << endl;
    cout << "Number of elastic HF events = " << num_Events_HF_Elastic << " out of " << num_Events_HF
         << " events" << endl;

    cout << "Number of elastic HH events (PT1) = " << num_HH_Events_Elastic_PT1 << " out of "
         << num_HH_Events_PT1 << " HH events (PT1); out of " << num_Events_PT1 << " events (PT1)"
         << endl;
    cout << "Number of elastic HH events (PT2) = " << num_HH_Events_Elastic_PT2 << " out of "
         << num_HH_Events_PT2 << " HH events (PT2); out of " << num_Events_PT2 << " events (PT2)"
         << endl;
    cout << "Number of elastic HH events (PT3) = " << num_HH_Events_Elastic_PT3 << " out of "
         << num_HH_Events_PT3 << " HH events (PT3); out of " << num_Events_PT3 << " events (PT3)"
         << endl;

    cout << "Number of elastic HF events (PT1) = " << num_HF_Events_Elastic_PT1 << " out of "
         << num_HF_Events_PT1 << " HF events (PT1); out of " << num_Events_PT1 << " events (PT1)"
         << endl;
    cout << "Number of elastic HF events (PT2) = " << num_HF_Events_Elastic_PT2 << " out of "
         << num_HF_Events_PT2 << " HF events (PT2); out of " << num_Events_PT2 << " events (PT2)"
         << endl;
    cout << "Number of elastic HF events (PT3) = " << num_HF_Events_Elastic_PT3 << " out of "
         << num_HF_Events_PT3 << " HF events (PT3); out of " << num_Events_PT3 << " events (PT3)"
         << endl;

    cout << "//"
            "--------------------------------------------------------------------------------------"
            "----------------"
         << endl;
    cout << "Events with fRPC hits counts:" << endl;

    cout << "Number of elastic HF events = " << num_Events_HF_Elastic_fRPC_hit << "; out of "
         << num_Events_HF_fRPC_hit << " HF events [with hits in fRPC]" << endl;
    cout << "Number of elastic HF events (PT1) = " << num_HF_Events_Elastic_PT1_fRPC_hit
         << "; out of " << num_HF_Events_PT1_fRPC_hit << " HF events (PT1) [with hits in fRPC]"
         << endl;
    cout << "Number of elastic HF events (PT2) = " << num_HF_Events_Elastic_PT2_fRPC_hit
         << "; out of " << num_HF_Events_PT2_fRPC_hit << " HF events (PT2) [with hits in fRPC]"
         << endl;
    cout << "Number of elastic HF events (PT3) = " << num_HF_Events_Elastic_PT3_fRPC_hit
         << "; out of " << num_HF_Events_PT3_fRPC_hit << " HF events (PT3) [with hits in fRPC]"
         << endl;

    cout << "//"
            "--------------------------------------------------------------------------------------"
            "----------------"
         << endl;
    cout << "fRPC hits counts:" << endl;

    cout << "Number of elastic HF entries = " << num_HF_Elastic_Entries << "; out of "
         << num_HF_Entries << " HF entries" << endl;
    cout << "Number of elastic HF entries (PT1) = " << num_HF_Elastic_Entries_PT1 << "; out of "
         << num_HF_Entries_PT1 << " HF entries (PT1)" << endl;
    cout << "Number of elastic HF entries (PT2) = " << num_HF_Elastic_Entries_PT2 << "; out of "
         << num_HF_Entries_PT2 << " HF entries (PT2)" << endl;
    cout << "Number of elastic HF entries (PT3) = " << num_HF_Elastic_Entries_PT3 << "; out of "
         << num_HF_Entries_PT3 << " HF entries (PT3)" << endl;

    cout << "fRPC Raw Entries = " << num_fRPC_Raw << endl;
    cout << "fRPC Raw Multiplicity = " << mult_fRPC_Raw << endl;

    printf("Label = %s\n", anapars.label.c_str());
    printf("[%c] PT1 = %d\n", anapars.pt1 ? '+' : ' ', num_Events_PT1);
    printf("[%c] PT2 = %d\n", anapars.pt2 ? '+' : ' ', num_Events_PT2);
    printf("[%c] PT3 = %d\n", anapars.pt3 ? '+' : ' ', num_Events_PT3);

    cout << "Simulation param = " << anapars.sim << endl;
    cout << "Norm factor = " << norm_factor << endl;

    f_root_output->Close();

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    cout << "//"
            "--------------------------------------------------------------------------------------"
            "--------"
         << endl;
    cout << "End of program:" << endl;
    timer.Print();
    cout << "//"
            "--------------------------------------------------------------------------------------"
            "--------"
         << endl;

    return 0;
}

//##############################################################################################
// Main fRPC calibration function
//##############################################################################################

int main(int argc, char** argv)
{
    TROOT Analysis("Analysis", "compiled analysis macro");
    gStyle->SetOptStat(0);

    int c;

    AnaParameters anapars;
    anapars.start = 0;
    anapars.events = -1;
    anapars.show_fakes = 0;
    anapars.cal_frpc_stage1 = 0;
    anapars.cal_frpc_stage2 = 0;
    anapars.cal_frpc_stage3 = 0;
    anapars.cal_frpc_stage4 = 0;
    anapars.cal_frpc_stage5 = 0;
    anapars.alt_ToF = 0;
    anapars.T0_cond = 0;
    anapars.sim = 0;
    anapars.outpath = "";
    anapars.outfile = "output.root";
    anapars.paramfile = "feb22_dst_params.txt";

    while (1)
    {
        static struct option long_options[] = {
            /* These options set a flag. */
            {"verbose", no_argument, &anapars.verbose, 1},
            {"brief", no_argument, &anapars.verbose, 0},
            {"sim", no_argument, &anapars.sim, 1},
            {"exp", no_argument, &anapars.sim, 0},
            {"cal-frpc-stage1", no_argument, &anapars.cal_frpc_stage1, 1},
            {"cal-frpc-stage2", no_argument, &anapars.cal_frpc_stage2, 1},
            {"cal-frpc-stage3", no_argument, &anapars.cal_frpc_stage3, 1},
            {"cal-frpc-stage4", no_argument, &anapars.cal_frpc_stage4, 1},
            {"cal-frpc-stage5", no_argument, &anapars.cal_frpc_stage5, 1},
            {"pt1", no_argument, &anapars.pt1, 1},
            {"pt2", no_argument, &anapars.pt2, 1},
            {"pt3", no_argument, &anapars.pt3, 1},
            {"T0_cond", no_argument, &anapars.T0_cond, 1},
            {"alt_ToF", no_argument, &anapars.alt_ToF, 1},

            /* These options don’t set a flag.
             *              We distinguish them by their indices. */
            {"output", required_argument, 0, 'o'},
            {"dir", required_argument, 0, 'd'},
            {"param", required_argument, 0, 'p'},
            {"events", required_argument, 0, 'e'},
            {"start", required_argument, 0, 's'},
            {"label", required_argument, 0, 'l'},
            {0, 0, 0, 0}};
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long(argc, argv, "o:d:p:e:s:l:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1) break;

        switch (c)
        {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0) break;
                printf("option %s", long_options[option_index].name);
                if (optarg) printf(" with arg %s", optarg);
                printf("\n");
                break;

            case 'o':
                anapars.outfile = optarg;
                break;

            case 'd':
                anapars.outpath = optarg;
                break;

            case 'p':
                anapars.paramfile = optarg;
                break;

            case 'e':
                anapars.events = atol(optarg);
                break;

            case 's':
                anapars.start = atol(optarg);
                break;

            case 'l':
                anapars.label = optarg;
                break;

            case '?':
                /* getopt_long already printed an error message. */
                break;

            default:
                abort();
        }
    }

    /* Instead of reporting ‘--verbose’
     *      and ‘--brief’ as they are encountered,
     *           we report the final status resulting from them. */
    if (anapars.verbose) puts("verbose flag is set");

    HLoop* loop = new HLoop(kTRUE);

    /* Print any remaining command line arguments (not options). */
    if (optind < argc)
    {
        Bool_t ret;
        // 		printf ("non-option ARGV-elements: ");
        while (optind < argc)
        {
            TString infile = argv[optind++];
            if (infile.Contains(","))
                ret = loop->addMultFiles(infile);
            else if (infile.Contains(".root"))
                ret = loop->addFiles(infile);
            else
                ret = loop->addFilesList(infile);

            if (!ret)
            {
                std::cerr << "READBACK: ERROR : cannot find inputfiles : " << infile.Data() << endl;
                std::exit(EXIT_FAILURE);
            }
        }
    }

    if (!anapars.pt1 && !anapars.pt2 && !anapars.pt3)
    {
        anapars.pt1 = 1;
        anapars.pt2 = 1;
        anapars.pt3 = 1;
    }
    printf("PT config %d %d %d\n", anapars.pt1, anapars.pt2, anapars.pt3);
    if (!anapars.outpath.IsNull()) anapars.outpath += "/";

    fwdet_tests(loop, anapars);

    exit(0);
}
