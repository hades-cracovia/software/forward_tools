#include "forward_tools.h"
#include "ft_config.h"

#include <hades.h>
#include <hloop.h>
#include <htaskset.h>

#include <hcategory.h>
#include <hcategorymanager.h>
#include <hrecevent.h>
#include <hreconstructor.h>
#include <hruntimedb.h>

//--------category definitions---------
#include <hgeantdef.h>
#include <hparticledef.h>
#include <hpiontrackerdef.h>
#include <hstartdef.h>
//-------------------------------------

//-------objects-----------------------
#include <heventheader.h>
#include <hgeantkine.h>
#include <hparticlecand.h>
#include <hparticlecandsim.h>
#include <hparticleevtinfo.h>
#include <hparticletracksorter.h>
#include <hpiontrackertrack.h>
#include <hstart2hit.h>

#include <hstart2cal.h>
//-------------------------------------
#include <hparticletool.h>
#include <hphysicsconstants.h>

#include <henergylosscorrpar.h>

#include <forwarddef.h>
#include <stsdef.h>
#include <frpcdef.h>
#include <hdst.h>
#include <hforwardcand.h>
#include <hfrpccal.h>
#include <hfrpccalpar.h>
#include <hfrpccluster.h>
#include <hfrpcdigipar.h>
#include <hfrpcgeompar.h>
#include <hfrpchit.h>
#include <hfrpcraw.h>
#include <hgeantfrpc.h>
#include <hgeantsts.h>
#include <hgeomcompositevolume.h>
#include <hgeomvector.h>
#include <hgeomvolume.h>
#include <hparasciifileio.h>
#include <hparticlecand.h>
#include <hrpccal.h>
#include <hrpccalpar.h>
#include <hrpccluster.h>
#include <hrpcdigipar.h>
#include <hrpcgeompar.h>
#include <hrpchit.h>
#include <hrpcraw.h>
#include <hspectrometer.h>
#include <hstart2hit.h>
#include <hstscal.h>
#include <hstsgeompar.h>
#include <hstsraw.h>
#include <htofhit.h>
#include <htofraw.h>
#include <rpcdef.h>
#include <tofdef.h>

#include <TCanvas.h>
#include <TColor.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1I.h>
#include <TH2F.h>
#include <TH2I.h>
#include <TH3I.h>
#include <THStack.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TMatrixD.h>
#include <TMultiGraph.h>
#include <TROOT.h>
#include <TStopwatch.h>
#include <TString.h>
#include <TStyle.h>
#include <TTree.h>
#include <TVector3.h>

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <string>
#include <vector>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

using namespace std;
using namespace ConfigBeamtime;

//##############################################################################################
// Usefull functions and definitions
//##############################################################################################

//----------------------------------------------------------------------------------------------
// Defining parameters
//----------------------------------------------------------------------------------------------

typedef void (*pad_callback)(TVirtualPad* pad);

struct AnaParameters
{
    TString infile;
    TString outfile;
    TString outpath;
    TString paramfile;
    std::string label;
    int events;
    int start;
    int sim;
    int verbose;
    bool show_fakes;
    int cal_frpc_stage1{0};
    int cal_frpc_stage2{0};
    int cal_frpc_stage3{0};
    int cal_frpc_stage4{0};
    int cal_frpc_stage5{0};
    int cal_frpc_stage6{0};
    int pt1{0};
    int pt4{0};
    int pt5{0};
    int pt8{0};
    int no_t0{0};
    int alt_ToF{0};
};

const Float_t avg_beam_rate = 400e3; // 400 kHz

int no_fStart2Hit_cnt = 0;
int rejected_no_T0_cnt = 0;

//----------------------------------------------------------------------------------------------
// Functions for drawing stuff
//----------------------------------------------------------------------------------------------

TH1* format_h_3(TH1* h)
{
    h->SetTitle("");
    h->GetXaxis()->SetLabelSize(0.06);
    h->GetXaxis()->SetTitleSize(0.06);
    h->GetXaxis()->SetTitleOffset(0.95);
    h->GetXaxis()->SetNdivisions(505);
    h->GetYaxis()->SetLabelSize(0.06);
    h->GetYaxis()->SetTitleSize(0.06);
    h->GetYaxis()->SetTitleOffset(1.10);
    gPad->SetLeftMargin(0.15);
    gPad->SetTopMargin(0.05);
    gPad->SetRightMargin(0.12);
    gPad->SetBottomMargin(0.13);
    return h;
}

void frpc_draw_v_lines(TVirtualPad* pad)
{
    pad->cd();
    TLine line;
    line.SetNDC(1);
    line.DrawLine(0, 375, 32, 375);
    line.DrawLine(0, -375, 32, -375);
}

int rpc_pads_mapping[4] = {3, 0, 2, 1};

void rpc_draw_and_write(TCanvas* c, TH1** h, Int_t frpc_sectors, TDirectory* c_dir = nullptr,
                        TDirectory* h_dir = nullptr, const char* opts = "colz",
                        TGraph** gr = nullptr, pad_callback f = nullptr)
{
    for (Int_t i = 0; i < frpc_sectors; ++i)
    {
        c->cd(1 + i);
        if (h_dir) h_dir->cd();

        gPad->SetLeftMargin(0.2);
        gPad->SetBottomMargin(0.2);

        TH1* _h = h[rpc_pads_mapping[i]];
        _h->Draw(opts);
        format_h_3(_h);

        if (gr) gr[rpc_pads_mapping[i]]->Draw("same,P");

        if (f) (*f)(gPad);

        _h->Write();
    }

    if (c_dir) c_dir->cd();
    c->Write();
}

const Int_t metro[5] = {TColor::GetColor("#d11141"), TColor::GetColor("#00b159"),
                        TColor::GetColor("#00aedb"), TColor::GetColor("#f37735"),
                        TColor::GetColor("#ffc425")};

void rpc_draw_stack_and_write(TCanvas* c, THStack* s, TH1* h[], int sectors,
                              TDirectory* c_dir = nullptr, TDirectory* h_dir = nullptr,
                              const char* opts = "colz")
{
    char buff[200];
    c->cd();
    TLegend* leg = new TLegend(0.5, 0.5, 0.9, 0.9);
    for (int i = 0; i < sectors; ++i)
    {
        s->Add(h[i]);
        h[i]->SetLineColor(metro[i]);
        h[i]->SetLineWidth(2);
        sprintf(buff, "Sector %d", i);
        leg->AddEntry(h[i], buff, "lpf");
        if (h_dir) h_dir->cd();
        h[i]->Write();
    }
    if (c_dir) c_dir->cd();
    c->cd();
    s->Draw(opts);
    leg->Draw();
    c->Write();
}

TString format_name(TString path, TString name) { return path + "PICTURES/" + name + ".png"; }


//----------------------------------------------------------------------------------------------
// fRPC stage one calibration function - charge thresholds
//----------------------------------------------------------------------------------------------

Float_t rpc_raw_calc_q_threshold(TH1* h)
{
    int max_bin = h->GetMaximumBin();            // Bin z maximum
    auto max_val = h->GetBinContent(max_bin); // Maximum value
    Int_t num_bins = 200;

    if (max_val == 0.0) return 0.0;

    Float_t arr_bin_diff[200] = {0.0};
    for (Int_t i = max_bin - 1; i > 0; i--) // Pętla po binach od max_bin - 1 do 0
    {
        Float_t prev_val = h->GetBinContent(i + 1);
        Float_t next_val = h->GetBinContent(i - 1);
        //Float_t current_val = h->GetBinContent(i);

        arr_bin_diff[i] = prev_val - next_val;
    }

    Int_t diff_max_index = 0;
    Float_t diff_max_val = 0.0;
    for (Int_t i = 0; i < num_bins; i++)
    {
        if (arr_bin_diff[i] > diff_max_val)
        {
            diff_max_val = arr_bin_diff[i];
            diff_max_index = i;
        }
    }

    return h->GetBinCenter(diff_max_index - 2);
}

//----------------------------------------------------------------------------------------------
// fRPC calibration stage two and three functions - V distribution thresholds
//----------------------------------------------------------------------------------------------

Float_t fRPC_V_threshold_left(TH1* h)
{
    int max_bin = h->GetMaximumBin();            // Bin z maximum
    auto max_val = h->GetBinContent(max_bin); // Maximum value
    Int_t num_bins = 500;

    if (max_val == 0.0) return 0.0;

    Double_t arr_bin_diff[500] = {0.0};
    Double_t arr_bin_content[500] = {0.0};
    for (Int_t i = max_bin; i > 0; i--) // Pętla po binach od max_bin do 0
    {
        Float_t prev_val = h->GetBinContent(i + 2);
        Float_t next_val = h->GetBinContent(i - 2);
        Float_t current_val = h->GetBinContent(i);

        arr_bin_diff[i] = prev_val - next_val;
        arr_bin_content[i] = current_val;
    }

    Int_t diff_max_index = 0;
    auto diff_max_val = 0.0;
    for (Int_t i = 0; i < num_bins; i++)
    {
        if (arr_bin_diff[i] > diff_max_val && arr_bin_content[i-2] < 0.15*max_val)
        {
            diff_max_val = arr_bin_diff[i];
            diff_max_index = i;
        }
    }

    return h->GetBinCenter(diff_max_index - 2);
}

Float_t fRPC_V_threshold_right(TH1* h)
{
    int max_bin = h->GetMaximumBin();            // Bin z maximum
    auto max_val = h->GetBinContent(max_bin); // Maximum value
    Int_t num_bins = 500;

    if (max_val == 0.0) return 0.0;

    Double_t arr_bin_diff[500] = {0.0};
    Double_t arr_bin_content[500] = {0.0};
    for (Int_t i = max_bin; i < num_bins; i++) // Pętla po binach od max_bin do num_bins
    {
        Float_t prev_val = h->GetBinContent(i - 2);
        Float_t next_val = h->GetBinContent(i + 2);
        Float_t current_val = h->GetBinContent(i);

        arr_bin_diff[i] = prev_val - next_val;
        arr_bin_content[i] = current_val;
    }

    Int_t diff_max_index = 0;
    auto diff_max_val = 0.0;
    for (Int_t i = 0; i < num_bins; i++)
    {
        if (arr_bin_diff[i] > diff_max_val && arr_bin_content[i+2] < 0.15*max_val)
        {
            diff_max_val = arr_bin_diff[i];
            diff_max_index = i;
        }
    }

    return h->GetBinCenter(diff_max_index + 2);
}

//##############################################################################################
// fwdet_tests function
//##############################################################################################

Int_t fwdet_tests(HLoop* /*loop*/, const AnaParameters& anapars)
{
    // Timer for checking analysis time
    TStopwatch timer;
    timer.Reset(); // Reset timer
    timer.Start(); // Start timer (T0)

    //----------------------------------------------------------------------------------------------
    // Note on how to save terminal output to a txt file
    //----------------------------------------------------------------------------------------------

    // Add " | tee fRPC_progress.txt " to the end of the command executing the program to save all
    // the output to screen into a txt file Alternatively " 2>&1 | tee fRPC_progress.txt " to
    // capture also error output

    //----------------------------------------------------------------------------------------------
    // Input parameters file
    //----------------------------------------------------------------------------------------------

    cout << "Input parameters file ... " << endl;

    Int_t mdcMods[6][4] = {{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1},
                           {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};
    HDst::setupSpectrometer("feb22", mdcMods, "sts,frpc");

    TString asciiParFile = anapars.paramfile;  // File containing all parameters of the detector
    HRuntimeDb* rtdb = HRuntimeDb::instance(); // myHades -> getRuntimeDb();

    // Checking if parameters file was opened properly
    if (!asciiParFile.IsNull())
    {
        HParAsciiFileIo* input1 = new HParAsciiFileIo;
        input1->open(asciiParFile.Data(), "in");
        if (!input1->check())
        {
            std::cerr << "Param file " << asciiParFile << " not open!" << std::endl;
            abort();
        }
        rtdb->setFirstInput(input1);
    }

    // Initializing the parameter container for geometry
    HStsGeomPar* pStrawGeomPar = (HStsGeomPar*)gHades->getRuntimeDb()->getContainer("StsGeomPar");
    // Checking if parameters container for geometry was created properly
    if (!pStrawGeomPar)
    {
        Error("HStsDigitizer::init()", "Parameter container for geometry not created");
        return kFALSE;
    }

    // Initializing the parameter container for geometry
    HFRpcGeomPar* pFRpcGeomPar = (HFRpcGeomPar*)gHades->getRuntimeDb()->getContainer("FRpcGeomPar");
    // Checking if parameters container for geometry was created properly
    if (!pFRpcGeomPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for geometry not created");
        return kFALSE;
    }

    HFRpcDigiPar* pFRpcDigiPar = (HFRpcDigiPar*)gHades->getRuntimeDb()->getContainer("FRpcDigiPar");
    if (!pFRpcDigiPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for HFRpcDigiPar not created");
        return kFALSE;
    }

    HFRpcCalPar* pFRpcCalPar = (HFRpcCalPar*)gHades->getRuntimeDb()->getContainer("FRpcCalPar");
    if (!pFRpcCalPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for FRpcCalPar not created");
        return kFALSE;
    }

    rtdb->initContainers(0);
    rtdb->print();
    // pFRpcCalPar->printParams();

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Output parameters file
    //----------------------------------------------------------------------------------------------

    cout << "Output parameters file ... " << endl;

    HParAsciiFileIo* f_param_output = new HParAsciiFileIo;

    TString cal_file = anapars.outfile;
    cal_file.ReplaceAll(".root", ".txt");

    if (!f_param_output->open(anapars.outpath + cal_file, "out"))
    {
        std::cerr << "Output param file " << cal_file << " not open!" << std::endl;
        abort();
    }
    // rtdb->setOutput(f_param_output);

    // Print detector parameters on screen
    if (anapars.verbose)
    {
        printf("PARAMETERS LOADING\n");
        pStrawGeomPar->print();
        pFRpcGeomPar->print();
    }

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //------------------------------------------------------------------------------------------------------
    // Important variables definition
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Important variables definition ... " << endl;

    char buff1[1000];

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Calculating angular orientation of straws in STS and strips in fRPC
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Calculating angular orientation of straws in STS and strips in fRPC ... " << endl;

    //----------------------------------------------------------------------------------------------
    // ----------------- STS

    Float_t sts_sina[STS_MAX_MODULES][STS_MAX_LAYERS]; // rotation matrix of the STS sector
    Float_t sts_cosa[STS_MAX_MODULES][STS_MAX_LAYERS];
    // Float_t sts_roty[STS_MAX_MODULES][STS_MAX_LAYERS];
    HGeomVector* stsCellsLab[STS_MAX_MODULES][STS_MAX_LAYERS][STS_MAX_CELLS];
    // Centre of the strip [mm]

    // Emptying of "stsCellsLab" vector
    for (Int_t m = 0; m < STS_MAX_MODULES; ++m)
        for (Int_t l = 0; l < STS_MAX_LAYERS; ++l)
            for (Int_t c = 0; c < STS_MAX_CELLS; ++c)
                stsCellsLab[m][l][c] = NULL;

    // Loops over modules and layers
    for (Int_t m = 0; m < STS_MAX_MODULES; ++m)
    {
        for (Int_t l = 0; l < STS_MAX_LAYERS; ++l)
        {
            HModGeomPar* fmodgeom = pStrawGeomPar->getModule(l, m);
            const HGeomTransform& labTrans = fmodgeom->getLabTransform();

            // sts_roty[m][l] = labTrans.getRotMatrix().getElement(2,2);
            sts_cosa[m][l] =
                labTrans.getRotMatrix().getElement(1, 1); // (m,n) - m = row, n = column
            sts_sina[m][l] =
                -labTrans.getRotMatrix().getElement(0, 1); // (m,n) - m = row, n = column

            // Correction to positive values of cos and sin (180 deg rotation doesn't matter here)
            if (sts_cosa[m][l] < 0)
            {
                sts_cosa[m][l] = -sts_cosa[m][l];
                sts_sina[m][l] = -sts_sina[m][l];
            }

            printf("STS Mod=%d   Lay=%d   CosA=%f   SinA=%f\n", m, l, sts_cosa[m][l],
                   sts_sina[m][l]);

            HGeomCompositeVolume* fMod = fmodgeom->getRefVolume();

            for (Int_t c = 0; c < STS_MAX_CELLS; ++c)
            {
                HGeomVolume* fVol = fMod->getComponent(c);
                if (!fVol) break;
                if (stsCellsLab[m][l][c] == NULL) stsCellsLab[m][l][c] = new HGeomVector;
                HGeomVector* p = stsCellsLab[m][l][c];
                *p = fVol->getTransform().getTransVector();
                *p = labTrans.transFrom(*p);
            }
        }
    }

    //----------------------------------------------------------------------------------------------
    // ----------------- FRPC

//     Float_t frpc_sina[FRPC_MAX_SECTORS]; // rotation matrix of the FRPC sector
//     Float_t frpc_cosa[FRPC_MAX_SECTORS];

    HGeomVector* frpcCellsLab[FRPC_MAX_SECTORS][FRPC_MAX_STRIPS]; // Centre of the strip [mm]

    // Emptying of "frpcCellsLab" vector
    for (Int_t s = 0; s < FRPC_MAX_SECTORS; ++s)
        for (Int_t c = 0; c < FRPC_MAX_STRIPS; ++c)
            frpcCellsLab[s][c] = NULL;

    // Loop over sectors
    for (Int_t s = 0; s < FRPC_MAX_SECTORS; ++s)
    {
        HModGeomPar* fmodgeom = pFRpcGeomPar->getModule(s);
        HGeomTransform labTrans = fmodgeom->getLabTransform();

//         frpc_cosa[s] = labTrans.getRotMatrix().getElement(0, 0); // (m,n) - m = row, n = column
//         frpc_sina[s] = labTrans.getRotMatrix().getElement(1, 0); // (m,n) - m = row, n = column

        HGeomCompositeVolume* fMod = fmodgeom->getRefVolume();

        for (Int_t c = 0; c < FRPC_MAX_STRIPS; ++c)
        {
            HGeomVolume* fVol = fMod->getComponent(c);
            if (frpcCellsLab[s][c] == NULL) frpcCellsLab[s][c] = new HGeomVector;
            HGeomVector* p = frpcCellsLab[s][c];
            *p = fVol->getTransform().getTransVector();
            *p = labTrans.transFrom(*p);
        }
    }

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    //------------------------------------------------------------------------------------------------------
    // Input file definition and accessing directories
    //------------------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Input file definition and accessing directories" << endl;

    // Defining and accessing input file
    TFile* f_root_input = TFile::Open(anapars.infile, "READ");

    // Input directories
    TDirectory* dir_in_frpc_raw_q = (TDirectory*)f_root_input->Get("R_RAW_Q");
    TDirectory* dir_in_frpc_r_cal = (TDirectory*)f_root_input->Get("R_CAL");

    TDirectory* dir_in_frpc_cal = (TDirectory*)f_root_input->Get("fRPC_Cal");
    TDirectory* dir_in_frpc_cal_delta_tof_hists =
        (TDirectory*)dir_in_frpc_cal->Get("STRIP_TOF_HISTS");
    TDirectory* dir_in_frpc_cal_t_diff_vs_charge_hists =
        (TDirectory*)dir_in_frpc_cal->Get("STRIP_T_DIFF_VS_CHARGE_HISTS");

    timer.Stop();
    cout << "    -> Finished after: " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Output file and directories definition
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Output file and directories definition ... " << endl;

    // Output file
    TFile* f_root_output = TFile::Open(anapars.outpath + anapars.outfile, "RECREATE");
    f_root_output->cd();

    // Output directories
    TDirectory* dir_out_frpc_raw_q = f_root_output->mkdir("R_RAW_Q");
    TDirectory* dir_out_frpc_cal = f_root_output->mkdir("R_CAL");

    TDirectory* dir_out_frpc_cal_tof_tmp1 = f_root_output->mkdir("fRPC_Cal/STRIP_TOF_HISTS");
    TDirectory* dir_out_frpc_cal_tof_tmp2 = f_root_output->mkdir("fRPC_Cal/STRIP_TOF_CANS");

    TDirectory* dir_out_frpc_cal_delta_tof_hists =
        dir_out_frpc_cal_tof_tmp1->GetDirectory("STRIP_TOF_HISTS");
    TDirectory* dir_out_frpc_cal_delta_tof_cans =
        dir_out_frpc_cal_tof_tmp2->GetDirectory("STRIP_TOF_CANS");

    // Walk Correction directories
    TDirectory* dir_out_frpc_cal_t_diff_vs_charge_tmp1 =
        f_root_output->mkdir("fRPC_Cal/STRIP_T_DIFF_VS_CHARGE_HISTS");
    TDirectory* dir_out_frpc_cal_t_diff_vs_charge_tmp2 =
        f_root_output->mkdir("fRPC_Cal/STRIP_T_DIFF_VS_CHARGE_CANS");

    TDirectory* dir_out_frpc_cal_t_diff_vs_charge_hists =
        dir_out_frpc_cal_t_diff_vs_charge_tmp1->GetDirectory("STRIP_T_DIFF_VS_CHARGE_HISTS");
    TDirectory* dir_out_frpc_cal_t_diff_vs_charge_cans =
        dir_out_frpc_cal_t_diff_vs_charge_tmp2->GetDirectory("STRIP_T_DIFF_VS_CHARGE_CANS");

    timer.Stop();
    cout << "    -> Finished after: " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // Canvases definition
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "Canvases definition ... " << endl;

    //----------------------------------------------------------------------------------------------
    // fRPC
    // TH2I* h_frpc_raw_mult[frpc_sectors];
    // TH2I* h_frpc_raw_cnts[frpc_sectors];
    // TH1I* h_frpc_raw_mults[frpc_sectors];
    TH1I* h_frpc_raw_q_n[frpc_sectors][frpc_cstrips];
    TH1I* h_frpc_raw_q_f[frpc_sectors][frpc_cstrips];
    TH1I* h_frpc_raw_q_n_mod[frpc_sectors][frpc_cstrips];
    TH1I* h_frpc_raw_q_f_mod[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_raw_q_n[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_raw_q_f[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_raw_q[frpc_sectors];
    TCanvas* c_frpc_raw_q_n_mod[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_raw_q_f_mod[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_raw_q_mod[frpc_sectors];
    // TH1I* h_frpc_cal_mults[frpc_sectors];
    TH2I* h_frpc_cal_v[frpc_sectors];
    TH1D* h_frpc_cal_V_strip[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_V_strip[frpc_sectors][frpc_cstrips];

    //----------------------------------------------------------------------------------------------
    // fRPC - Walk Correction - ToF Diff vs Charge - elastic
    TH2F* h_frpc_t_diff_vs_charge[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_t_diff_vs_charge[frpc_sectors][frpc_cstrips];

    // fRPC - Walk Correction - ToF Diff vs Charge - elastic
    TH2F* h_frpc_t_diff_vs_charge_el[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_t_diff_vs_charge_el[frpc_sectors][frpc_cstrips];

    //----------------------------------------------------------------------------------------------
    // fRPC ToF calibration

    // fRPC - Delta ToF calibration - histograms elastic
    TH1I* h_frpc_cal_delta_tof_el[frpc_sectors][frpc_cstrips];
    TCanvas* c_frpc_cal_delta_tof_strips_el[frpc_sectors][frpc_cstrips];
//     TCanvas* c_frpc_cal_delta_tof_el[frpc_sectors];

    // fRPC - Delta ToF vs Hit position V calibration - histograms elastic
//     TH2I* h_frpc_cal_delta_tof_vs_V_el[frpc_sectors][frpc_cstrips];
//     TCanvas* c_frpc_cal_delta_tof_vs_V_strips_el[frpc_sectors][frpc_cstrips];
//     TCanvas* c_frpc_cal_delta_tof_vs_V_el[frpc_sectors];

    // fRPC - ToF calibration - 2D histogram elastic
//     TH2I* h_frpc_cal_delta_tof_2D_el[frpc_sectors];
//     TCanvas* c_frpc_cal_delta_tof_2D_el[frpc_sectors];

    //----------------------------------------------------------------------------------------------
    // Loop over sectors in fRPC
    for (Int_t i = 0; i < frpc_sectors; ++i)
    {
        //----------------------------------------------------------------------------------------------
        // Loop over strips in given module in fRPC
        for (Int_t j = 0; j < frpc_cstrips; ++j)
        {
            //----------------------------------------------------------------------------------------------
            // fRPC - Charge calibration
            sprintf(buff1, "c_frpc_raw_q_n_%d_%d", i, j);
            c_frpc_raw_q_n[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            sprintf(buff1, "c_frpc_raw_q_n_mod_%d_%d", i, j);
            c_frpc_raw_q_n_mod[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            sprintf(buff1, "c_frpc_raw_q_f_%d_%d", i, j);
            c_frpc_raw_q_f[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            sprintf(buff1, "c_frpc_raw_q_f_mod_%d_%d", i, j);
            c_frpc_raw_q_f_mod[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            //----------------------------------------------------------------------------------------------
            // fRPC - Strip length and offset calibration
            sprintf(buff1, "c_frpc_cal_V_strip_%d_%d", i, j);
            c_frpc_cal_V_strip[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            //----------------------------------------------------------------------------------------------
            // fRPC - Walk Correction - ToF Diff vs Charge
            sprintf(buff1, "c_frpc_t_diff_vs_charge_%d_%d", i, j);
            c_frpc_t_diff_vs_charge[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            // fRPC - Walk Correction - ToF Diff vs Charge - elastic
            sprintf(buff1, "c_frpc_t_diff_vs_charge_el_%d_%d", i, j);
            c_frpc_t_diff_vs_charge_el[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            //----------------------------------------------------------------------------------------------
            // ToF fRPC calibration

            // h_frpc_cal_delta_tof_el - Elastic
            sprintf(buff1, "c_frpc_cal_delta_tof_el_%d_%d", i, j);
            c_frpc_cal_delta_tof_strips_el[i][j] = new TCanvas(buff1, buff1, 1200, 1000);

            // h_frpc_cal_delta_tof_vs_V_el - Elastic
//             sprintf(buff1, "c_frpc_cal_delta_tof_vs_V_el_%d_%d", i, j);
//             c_frpc_cal_delta_tof_vs_V_strips_el[i][j] = new TCanvas(buff1, buff1, 1200, 1000);
        }

        //----------------------------------------------------------------------------------------------
        // fRPC - charge calibration collective canvas
        sprintf(buff1, "h_frpc_raw_q_%d", i);
        c_frpc_raw_q[i] = new TCanvas(buff1, buff1, 800, 800);
        c_frpc_raw_q[i]->DivideSquare(frpc_cstrips);

        sprintf(buff1, "h_frpc_raw_q_mod_%d", i);
        c_frpc_raw_q_mod[i] = new TCanvas(buff1, buff1, 800, 800);
        c_frpc_raw_q_mod[i]->DivideSquare(frpc_cstrips);

        //----------------------------------------------------------------------------------------------
        // Hit multiplicity in fRPC strips
        // sprintf(buff1, "c_HF_fRPC_Strip_%d", i);
        // c_HF_fRPC_Strip[i] = new TCanvas(buff1, buff1, 800, 800);

        //----------------------------------------------------------------------------------------------
        // fRPC - ToF calibration canvas 2D hist elastic
//         sprintf(buff1, "c_frpc_cal_delta_tof_2D_el_%d", i);
//         c_frpc_cal_delta_tof_2D_el[i] = new TCanvas(buff1, buff1, 800, 800);

        timer.Stop();
        cout << "  -> Module = " << i << " - Finished, T = " << timer.RealTime() << " s" << endl;
        timer.Start(kFALSE);
    }

    TCanvas* c_frpc_cal_v = new TCanvas("c_frpc_cal_v", "c_frpc_cal_v", 800, 800);
    c_frpc_cal_v->DivideSquare(frpc_sectors);

    timer.Stop();
    cout << "    -> Finished after: " << timer.RealTime() << " s" << endl;

    //----------------------------------------------------------------------------------------------
    // fRPC calibration
    //----------------------------------------------------------------------------------------------

    timer.Start(kFALSE);
    cout << "fRPC calibration ... " << endl;

    Float_t data[6]; // Calibration parameters array
    // Float_t data[9]; // Calibration parameters array
    // TGraph* gr_frpc_cal_v[frpc_sectors];

    // 300, -900, 900
//     Int_t project_step = 1800 / 30;
//     Int_t project_min = -900;

    for (auto i = frpc_min_sector; i < frpc_sectors; ++i) // Loop over sectors
    {
        HFRpcCalParSec& sec = (*pFRpcCalPar)[i]; // Take sector class

        // gr_frpc_cal_v[i] = new TGraph(frpc_cstrips);
        // gr_frpc_cal_v[i] = new TGraph(h_frpc_cal_v[i]->GetNbinsX());
        // gr_frpc_cal_v[i]->SetLineColor(1);
        // gr_frpc_cal_v[i]->SetMarkerColor(1);
        // gr_frpc_cal_v[i]->SetMarkerStyle(20);

        for (auto j = 0; j < frpc_cstrips; ++j) // Loop over cstrips
        {
            cout << "Sector = " << i << " ; Strip = " << j << endl;

            //----------------------------------------------------------------------------------------------
            // Strip position (with correction for target position (Z_START))
            // Float_t z_target = -115.0;
//             Float_t fRPC_X = frpcCellsLab[i][j]->X();
//             Float_t fRPC_Y = frpcCellsLab[i][j]->Y();
//             Float_t fRPC_Z = frpcCellsLab[i][j]->Z() - z_target;

//             Float_t frpc_el_hit_X_mean = 0.0;
//             Float_t frpc_el_hit_Y_mean = 0.0;

            // Extracting current calibration parameters
            HFRpcCalParCell& chan = sec[j]; // Take
            chan.getData(data);

            //----------------------------------------------------------------------------------------------
            // Consequent stages of calibration
            //----------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------
            // Stage 1 - Charge thresholds
            //----------------------------------------------------------------------------------------------

            if (anapars.cal_frpc_stage1) // First step of calibration (thresholds)
            {
                c_frpc_raw_q[i]->cd(1 + j);

                sprintf(buff1, "h_frpc_raw_q_n_%d_%d", i, j);
                h_frpc_raw_q_n[i][j] = (TH1I*)dir_in_frpc_raw_q->Get(buff1);

                sprintf(buff1, "h_frpc_raw_q_f_%d_%d", i, j);
                h_frpc_raw_q_f[i][j] = (TH1I*)dir_in_frpc_raw_q->Get(buff1);

                h_frpc_raw_q_n[i][j]->Draw();
                h_frpc_raw_q_f[i][j]->Draw("same");

                sprintf(buff1, "h_frpc_raw_q_n_mod_%d_%d", i, j);
                h_frpc_raw_q_n_mod[i][j] = (TH1I*)h_frpc_raw_q_n[i][j]->Rebin(2, buff1);
                h_frpc_raw_q_n_mod[i][j]->Smooth();
                h_frpc_raw_q_n_mod[i][j]->SetLineColor(2);

                sprintf(buff1, "h_frpc_raw_q_f_mod_%d_%d", i, j);
                h_frpc_raw_q_f_mod[i][j] = (TH1I*)h_frpc_raw_q_f[i][j]->Rebin(2, buff1);
                h_frpc_raw_q_f_mod[i][j]->Smooth();
                h_frpc_raw_q_f_mod[i][j]->SetLineColor(2);

                Float_t thr_n = rpc_raw_calc_q_threshold(h_frpc_raw_q_n_mod[i][j]);
                Float_t thr_f = rpc_raw_calc_q_threshold(h_frpc_raw_q_f_mod[i][j]);

                // Drawing and writing normal histograms
                ForwardTools::Drawing::draw_vertical_line(gPad, h_frpc_raw_q_n[i][j], thr_n);
                ForwardTools::Drawing::draw_vertical_line(gPad, h_frpc_raw_q_f[i][j], thr_f);

                c_frpc_raw_q_n[i][j]->cd();
                h_frpc_raw_q_n[i][j]->Draw();
                ForwardTools::Drawing::draw_vertical_line(gPad, h_frpc_raw_q_n[i][j], thr_n);

                c_frpc_raw_q_f[i][j]->cd();
                h_frpc_raw_q_f[i][j]->Draw();
                ForwardTools::Drawing::draw_vertical_line(gPad, h_frpc_raw_q_f[i][j], thr_f);

                dir_out_frpc_raw_q->cd();
                h_frpc_raw_q_n[i][j]->Write();
                c_frpc_raw_q_n[i][j]->Write();
                h_frpc_raw_q_f[i][j]->Write();
                c_frpc_raw_q_f[i][j]->Write();

                // Drawing and writing mod histograms
                c_frpc_raw_q_mod[i]->cd(1 + j);
                h_frpc_raw_q_n_mod[i][j]->Draw();
                h_frpc_raw_q_f_mod[i][j]->Draw("same");

                ForwardTools::Drawing::draw_vertical_line(gPad, h_frpc_raw_q_n_mod[i][j], thr_n);
                ForwardTools::Drawing::draw_vertical_line(gPad, h_frpc_raw_q_f_mod[i][j], thr_f);

                c_frpc_raw_q_n_mod[i][j]->cd();
                h_frpc_raw_q_n_mod[i][j]->Draw();
                ForwardTools::Drawing::draw_vertical_line(gPad, h_frpc_raw_q_n_mod[i][j], thr_n);

                c_frpc_raw_q_f_mod[i][j]->cd();
                h_frpc_raw_q_f_mod[i][j]->Draw();
                ForwardTools::Drawing::draw_vertical_line(gPad, h_frpc_raw_q_f_mod[i][j], thr_f);

                dir_out_frpc_raw_q->cd();
                h_frpc_raw_q_n_mod[i][j]->Write();
                c_frpc_raw_q_n_mod[i][j]->Write();
                h_frpc_raw_q_f_mod[i][j]->Write();
                c_frpc_raw_q_f_mod[i][j]->Write();

                // Writing parameters to file
                auto max_val_q_n = h_frpc_raw_q_n_mod[i][j]->GetBinContent(
                    h_frpc_raw_q_n_mod[i][j]->GetMaximumBin()); // Maximum value in histogram
                auto max_val_q_f = h_frpc_raw_q_f_mod[i][j]->GetBinContent(
                    h_frpc_raw_q_f_mod[i][j]->GetMaximumBin()); // Maximum value in histogram

                if (max_val_q_n > 10.0 && max_val_q_f > 10.0) // If signal registered - apply calibration
                {
                    data[0] = thr_n;
                    data[1] = thr_f;
                }
                else
                {
                    data[0] = 0.0;
                    data[1] = 0.0;
                }
                data[2] = 0.0;
                data[3] = 0.0;
                data[4] = 1.0;
            }

            //----------------------------------------------------------------------------------------------
            // Stage 2, 3 and 4 - Effective length calibration and  strip position offset correction
            //----------------------------------------------------------------------------------------------

            if (anapars.cal_frpc_stage2 || anapars.cal_frpc_stage3 || anapars.cal_frpc_stage4)
            {
                sprintf(buff1, "h_frpc_cal_V_%d", i);
                h_frpc_cal_v[i] = (TH2I*)dir_in_frpc_r_cal->Get(buff1);

                //TH1D *h_temp_project;
                sprintf(buff1, "h_frpc_cal_V_projection_M=%d_S=%d", i, j);
                cout<<buff1<<endl;
                h_frpc_cal_V_strip[i][j] = h_frpc_cal_v[i]->ProjectionY(buff1,j+1,j+1,"");
                h_frpc_cal_V_strip[i][j]->Rebin(2);
                h_frpc_cal_V_strip[i][j]->Smooth();

                auto edge_l = fRPC_V_threshold_left(h_frpc_cal_V_strip[i][j]);
                auto edge_r = fRPC_V_threshold_right(h_frpc_cal_V_strip[i][j]);
                auto offset_p = ((edge_l + edge_r)/2.0f);
                auto eff_len = fabs(edge_r - edge_l);

                // Drawing calibration histograms and lines
                dir_out_frpc_cal->cd();
                c_frpc_cal_V_strip[i][j]->cd();    h_frpc_cal_V_strip[i][j]->Draw();
                ForwardTools::Drawing::draw_vertical_line_color(gPad, h_frpc_cal_V_strip[i][j], edge_l, 4); // Blue
                ForwardTools::Drawing::draw_vertical_line_color(gPad, h_frpc_cal_V_strip[i][j], edge_r, 4); // Blue
                ForwardTools::Drawing::draw_vertical_line_color(gPad, h_frpc_cal_V_strip[i][j], offset_p, 3); // Blue
                c_frpc_cal_V_strip[i][j]->Write();    h_frpc_cal_V_strip[i][j]->Write();
                //c_frpc_cal_V_strip[i][j]->SaveAs(format_name(anapars.outpath, c_frpc_cal_V_strip[i][j]->GetName()));

                if (anapars.cal_frpc_stage2) // Second step of calibration (strip length correction)
                {
                    // Writing parameters to file
                    if ( h_frpc_cal_V_strip[i][j]->GetBinContent(h_frpc_cal_V_strip[i][j]->GetMaximumBin()) > 10.0 )
                    {
                        data[3] = offset_p;
                        data[4] = 1.0;
                    }
                    else
                    {
                        data[3] = 0.0;
                        data[4] = 1.0;
                    }
                }
                if (anapars.cal_frpc_stage3) // Third step of calibration (strip length correction)
                {
                    // Writing parameters to file
                    if ( h_frpc_cal_V_strip[i][j]->GetBinContent(h_frpc_cal_V_strip[i][j]->GetMaximumBin()) > 10.0 )
                    {
                        //data[3] = 0.0;
                        data[4] = pFRpcDigiPar->getStripLength() / eff_len;
                    }
                    else
                    {
                        //data[3] = 0.0;
                        data[4] = 1.0;
                    }
                }
                else if (anapars.cal_frpc_stage4) // Fourth step of calibration (strip position offset correction)
                {
                    // Writing parameters to file
                    if ( h_frpc_cal_V_strip[i][j]->GetBinContent(h_frpc_cal_V_strip[i][j]->GetMaximumBin()) > 10.0 )
                    {
                        data[3] = data[3] + offset_p;
                    }
                    else
                    {
                        data[3] = 0.0;
                    }
                }
            }

            /*
                // T0_z = pFRpcGeomPar->getSectorZ(sec);
                offset_p = 0.;
                edge_l = -1e9;
                edge_r = -1e9;
                cnt = 0;
                for (int k = 0; k < h_frpc_cal_v[i]->GetNbinsY(); ++k)
                {
                    Float_t bc = h_frpc_cal_v[i]->GetBinContent(1 + j, 1 + k);
                    if (bc > 2)
                    {
                        edge_r = h_frpc_cal_v[i]->GetYaxis()->GetBinCenter(1 + k); // Change right edge value if there's any filled
                        if (edge_l == -1e9) edge_l = edge_r; // Left edge changed only once
                        offset_p += edge_r;
                        ++cnt;
                    }
                }
                eff_len = fabs(edge_r - edge_l);

                data[3] = 0.0;
                data[4] = eff_len > 0.0 ? pFRpcDigiPar->getStripLength() / eff_len : 1.0;
            }
            if (anapars.cal_frpc_stage3) // Third step of calibration (strip position offset correction)
            {
                // data[3] = 0.0;
                data[3] = cnt > 0 ? offset_p / cnt : 0.;
                // gr_frpc_cal_v[i]->SetPoint(j, h_frpc_cal_v[i]->GetXaxis()->GetBinCenter(1 + j),data[3]);
            }
            else
            {
                // gr_frpc_cal_v[i]->SetPoint(j, h_frpc_cal_v[i]->GetXaxis()->GetBinCenter(1 + j), 0.);
            }
            */

            //----------------------------------------------------------------------------------------------
            // Stage 5 - Time of Flight calibration
            //----------------------------------------------------------------------------------------------

            if (anapars.cal_frpc_stage5) // Fourth step of calibration (ToF calibration)
            {
                sprintf(buff1, "h_frpc_cal_delta_tof_el_%d_%d", i, j);
                h_frpc_cal_delta_tof_el[i][j] = (TH1I*)dir_in_frpc_cal_delta_tof_hists->Get(buff1);

                // sprintf(buff1, "h_frpc_cal_delta_tof_vs_V_el_%d_%d", i, j);
                // h_frpc_cal_delta_tof_vs_V_el[i][j] =
                // (TH1I*)dir_in_frpc_cal_delta_tof_hists->Get(buff1);

                // fRPC ToF elasting scattering histograms (per strip)
                c_frpc_cal_delta_tof_strips_el[i][j]->cd();
                h_frpc_cal_delta_tof_el[i][j]->Draw();
                // c_frpc_cal_delta_tof_vs_V_strips_el[i][j]->cd();
                // h_frpc_cal_delta_tof_vs_V_el[i][j]->Draw("colz");

                ForwardTools::Miscellaneous::gaus_params frpc_delta_tof_el =
                    ForwardTools::Miscellaneous::fit_gaus(h_frpc_cal_delta_tof_el[i][j]);

                auto frpc_delta_tof_el_mean = frpc_delta_tof_el.gaus_mean;
//                 auto frpc_delta_tof_el_mean_err = frpc_delta_tof_el.gaus_mean_error;
                auto frpc_delta_tof_el_sigma = frpc_delta_tof_el.gaus_sigma;

                cout << "fRPC; Sector = " << i << "; Strip = " << j << endl;
                cout << "--> Delta_ToF_el_mean = " << frpc_delta_tof_el_mean
                     << " [ns]; Delta_ToF_el_sigma = " << frpc_delta_tof_el_sigma << " [ns]"
                     << endl;

                /*
                TH1D *h_temp_project;
                dir_out_frpc_cal_temp_hists->cd();
                for (Int_t p = 0; p<30; p++)
                {
                    sprintf(buff1, "c_fcand_delta_tof_vs_V_projection_M=%d_S=%d_B=%d", i, j, p);
                    h_temp_project=
                h_frpc_cal_delta_tof_vs_V_el[i][j]->ProjectionY(buff1,(project_min +
                project_step*p),(project_min + project_step*(p+1)),"");
                    ForwardTools::Miscellaneous::gaus_params frpc_delta_tof_project_el =
                ForwardTools::Miscellaneous::fit_gaus(h_temp_project); Float_t
                frpc_delta_tof_project_el_mean = frpc_delta_tof_project_el.gaus_mean;
                    h_frpc_cal_delta_tof_el[i][j]->Write();
                }
                */

                //----------------------------------------------------------------------------------------------
                // Drawing lines on and writing hists to file

                c_frpc_cal_delta_tof_strips_el[i][j]->cd();
                ForwardTools::Drawing::draw_vertical_line_color(
                    gPad, h_frpc_cal_delta_tof_el[i][j], frpc_delta_tof_el_mean, 4); // Blue = 4

                dir_out_frpc_cal_delta_tof_hists->cd();
                h_frpc_cal_delta_tof_el[i][j]
                    ->Write(); // h_frpc_cal_delta_tof_vs_V_el[i][j]->Write();
                dir_out_frpc_cal_delta_tof_cans->cd();
                c_frpc_cal_delta_tof_strips_el[i][j]
                    ->Write(); // c_frpc_cal_delta_tof_vs_V_strips_el[i][j]->Write();

                //----------------------------------------------------------------------------------------------
                // Filling arrays for ToF theor vs exp graphs

                // arr_frpc_delta_tof_theor[i][j] = frpc_el_tof_theor;
                // arr_frpc_delta_tof_el[i][j] = frpc_delta_tof_el_mean;
                // arr_frpc_delta_tof_el_error[i][j] = frpc_delta_tof_el_mean_err;

                auto max_val_tof = h_frpc_cal_delta_tof_el[i][j]->GetBinContent(h_frpc_cal_delta_tof_el[i][j]->GetMaximumBin()); // Maximum value in histogram

                if (max_val_tof > 10.0) // If signal registered - apply calibration
                {
                    // data[2] = 0.0;
                    data[2] = frpc_delta_tof_el_mean;
                }
                else // If no signal - no calibration
                {
                    data[2] = 0.0;
                }
            }

            //----------------------------------------------------------------------------------------------
            // Stage 6 - Walk Effect correction
            //----------------------------------------------------------------------------------------------

            if (anapars.cal_frpc_stage6) // Fifth step of calibration (Walk Effect correction)
            {
                sprintf(buff1, "h_frpc_t_diff_vs_charge_%d_%d", i, j);
                h_frpc_t_diff_vs_charge[i][j] =
                    (TH2F*)dir_in_frpc_cal_t_diff_vs_charge_hists->Get(buff1);

                sprintf(buff1, "h_frpc_t_diff_vs_charge_el_%d_%d", i, j);
                h_frpc_t_diff_vs_charge_el[i][j] =
                    (TH2F*)dir_in_frpc_cal_t_diff_vs_charge_hists->Get(buff1);

                // Drawing T_diff strip histograms on canvases
                c_frpc_t_diff_vs_charge[i][j]->cd();
                h_frpc_t_diff_vs_charge[i][j]->Draw();
                c_frpc_t_diff_vs_charge_el[i][j]->cd();
                h_frpc_t_diff_vs_charge_el[i][j]->Draw();

                // ToF vs Charge function (for fitting)
                TF1* f_t_diff_vs_charge =
                    new TF1("f_t_diff_vs_charge", "[0] + [1]*TMath::Exp(-[2]*x)", 0, 200);
                // f_t_diff_vs_charge->SetParameter(0,0.05);
                // f_t_diff_vs_charge->SetParameter(1,0.03);
                // f_t_diff_vs_charge->SetParameter(2,1.0);

                h_frpc_t_diff_vs_charge_el[i][j]->Fit(f_t_diff_vs_charge, "R Q");

                f_t_diff_vs_charge->SetLineColor(kBlue);
                f_t_diff_vs_charge->Draw("same");

                cout << "Walk Effect correction - Exp function fitting: A + B*TMath::Exp(-C*x)"
                     << endl;
                cout << "- A = " << f_t_diff_vs_charge->GetParameter(0) << " +/- "
                     << f_t_diff_vs_charge->GetParError(0) << endl;
                cout << "- B = " << f_t_diff_vs_charge->GetParameter(1) << " +/- "
                     << f_t_diff_vs_charge->GetParError(1) << endl;
                cout << "- C = " << f_t_diff_vs_charge->GetParameter(2) << " +/- "
                     << f_t_diff_vs_charge->GetParError(2) << endl;

                dir_out_frpc_cal_t_diff_vs_charge_hists->cd();
                h_frpc_t_diff_vs_charge[i][j]->Write();
                h_frpc_t_diff_vs_charge_el[i][j]->Write();
                dir_out_frpc_cal_t_diff_vs_charge_cans->cd();
                c_frpc_t_diff_vs_charge[i][j]->Write();
                c_frpc_t_diff_vs_charge_el[i][j]->Write();
            }

            chan.fill(data);
            cout << "Data: " << data[0] << "  " << data[1] << "  " << data[2] << "  " << data[3]
                 << "  " << data[4] << "  " << data[5] << "  " << endl;
        }
        dir_out_frpc_raw_q->cd();
        c_frpc_raw_q[i]->Write();
    }

    timer.Stop();
    cout << "-> Finished, T = " << timer.RealTime() << " s" << endl;

    /*
    //----------------------------------------------------------------------------------------------
    // Drawing, writing and saving (as pictures) histograms and canvases
    //----------------------------------------------------------------------------------------------

    cout<<"Drawing, writing and saving (as pictures) histograms and canvases ... "<<endl;

    f_root_output->cd();
    dir_out_frpc_cal->cd();

    // FRPC
    //rpc_draw_and_write(c_frpc_raw_cnts, (TH1**)h_frpc_raw_cnts, frpc_sectors, f_root_output,
    dir_out_frpc_raw_q,"colz");
    //rpc_draw_and_write(c_frpc_raw_mult, (TH1**)h_frpc_raw_mult, frpc_sectors, f_root_output,
    dir_out_frpc_raw_q,"colz");
    //rpc_draw_stack_and_write(c_frpc_raw_mults, s_frpc_raw_mults, (TH1**)h_frpc_raw_mults, 4,
    f_root_output, dir_out_frpc_raw_q, "h,text");
    //rpc_draw_stack_and_write(c_frpc_cal_mults, s_frpc_cal_mults, (TH1**)h_frpc_cal_mults, 4,
    f_root_output, dir_out_frpc_cal, "h,text"); rpc_draw_and_write(c_frpc_cal_v,
    (TH1**)h_frpc_cal_v, frpc_sectors, f_root_output, dir_out_frpc_cal, "colz");//, gr_frpc_cal_v,
    &frpc_draw_v_lines);

    timer.Stop();
    cout<<"-> Finished, T = "<<timer.RealTime()<<" s"<<endl;
    */

    //------------------------------------------------------------------------------------------------------
    // Writing calibration parameters to file and statistics to screen

    cout << "Writing calibration parameters to file and statistics to screen ... " << endl;

    pFRpcCalPar->write(f_param_output);
    f_param_output->close();

    f_root_output->Close();

    timer.Stop();
    cout << " -> Finished, T = " << timer.RealTime() << " s" << endl;

    cout << "//"
            "--------------------------------------------------------------------------------------"
            "--------"
         << endl;
    cout << "End of program:" << endl;
    timer.Print();
    cout << "//"
            "--------------------------------------------------------------------------------------"
            "--------"
         << endl;

    return 0;
}

//##############################################################################################
// Main fRPC calibration function
//##############################################################################################

int main(int argc, char** argv)
{
    TROOT Analysis("Analysis", "compiled analysis macro");
    gStyle->SetOptStat(0);

    int c;

    AnaParameters anapars;
    anapars.start = 0;
    anapars.events = -1;
    anapars.show_fakes = 0;
    anapars.cal_frpc_stage1 = 0;
    anapars.cal_frpc_stage2 = 0;
    anapars.cal_frpc_stage3 = 0;
    anapars.cal_frpc_stage4 = 0;
    anapars.cal_frpc_stage5 = 0;
    anapars.cal_frpc_stage6 = 0;
    anapars.alt_ToF = 0;
    anapars.sim = 0;
    anapars.outpath = "";
    anapars.outfile = "output.root";
    anapars.paramfile = "feb22_dst_params.txt";

    while (1)
    {
        static struct option long_options[] = {
            /* These options set a flag. */
            {"verbose", no_argument, &anapars.verbose, 1},
            {"brief", no_argument, &anapars.verbose, 0},
            {"sim", no_argument, &anapars.sim, 1},
            {"exp", no_argument, &anapars.sim, 0},
            {"cal-frpc-stage1", no_argument, &anapars.cal_frpc_stage1, 1},
            {"cal-frpc-stage2", no_argument, &anapars.cal_frpc_stage2, 1},
            {"cal-frpc-stage3", no_argument, &anapars.cal_frpc_stage3, 1},
            {"cal-frpc-stage4", no_argument, &anapars.cal_frpc_stage4, 1},
            {"cal-frpc-stage5", no_argument, &anapars.cal_frpc_stage5, 1},
            {"cal-frpc-stage6", no_argument, &anapars.cal_frpc_stage6, 1},
            {"pt1", no_argument, &anapars.pt1, 1},
            {"pt4", no_argument, &anapars.pt4, 1},
            {"pt5", no_argument, &anapars.pt5, 1},
            {"pt8", no_argument, &anapars.pt8, 1},
            {"no_t0", no_argument, &anapars.no_t0, 1},
            {"alt_ToF", no_argument, &anapars.alt_ToF, 1},

            /* These options don’t set a flag.
             *              We distinguish them by their indices. */
            {"output", required_argument, 0, 'o'},
            {"dir", required_argument, 0, 'd'},
            {"param", required_argument, 0, 'p'},
            {"events", required_argument, 0, 'e'},
            {"start", required_argument, 0, 's'},
            {"label", required_argument, 0, 'l'},
            {0, 0, 0, 0}};
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long(argc, argv, "o:d:p:e:s:l:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1) break;

        switch (c)
        {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0) break;
                printf("option %s", long_options[option_index].name);
                if (optarg) printf(" with arg %s", optarg);
                printf("\n");
                break;

            case 'o':
                anapars.outfile = optarg;
                break;

            case 'd':
                anapars.outpath = optarg;
                break;

            case 'p':
                anapars.paramfile = optarg;
                break;

            case 'e':
                anapars.events = atol(optarg);
                break;

            case 's':
                anapars.start = atol(optarg);
                break;

            case 'l':
                anapars.label = optarg;
                break;

            case '?':
                /* getopt_long already printed an error message. */
                break;

            default:
                abort();
        }
    }

    /* Instead of reporting ‘--verbose’
     *      and ‘--brief’ as they are encountered,
     *           we report the final status resulting from them. */
    if (anapars.verbose) puts("verbose flag is set");

    HLoop* loop = new HLoop(kTRUE);

    /* Print any remaining command line arguments (not options). */
    if (optind < argc)
    {
        anapars.infile = argv[optind++];
        // break;
    }
    else
    {
        std::cerr << "No input file given!\n";
        std::exit(EXIT_FAILURE);
    }

    if (!anapars.pt1 && !anapars.pt4 && !anapars.pt5 && !anapars.pt8)
    {
        anapars.pt1 = 1;
        anapars.pt4 = 1;
        anapars.pt5 = 1;
        anapars.pt8 = 1;
    }
    printf("PT config %d %d %d %d\n", anapars.pt1, anapars.pt4, anapars.pt5, anapars.pt8);
    if (!anapars.outpath.IsNull()) anapars.outpath += "/";

    fwdet_tests(loop, anapars);

    exit(0);
}
