cmake_minimum_required(VERSION 3.16)
project(ForwardTools
    VERSION 0.0.1
    LANGUAGES C CXX
)

#-------------------------------------------------------------------------------
# cmake_tools

include(FetchContent)

FetchContent_Declare(cmake_tools
    GIT_REPOSITORY https://github.com/rlalik/cmake_tools
    GIT_TAG        master
)

FetchContent_MakeAvailable(cmake_tools)
list(APPEND CMAKE_MODULE_PATH
    ${CMAKE_SOURCE_DIR}/Modules
    ${cmake_tools_SOURCE_DIR}
)

include(check_3rd_party_tool)
#-------------------------------------------------------------------------------

# fetch deps

check_3rd_party_tool(cmake-scripts 22.01 https://github.com/rlalik/cmake-scripts TAG temp_branch ON)
list(APPEND CMAKE_MODULE_PATH
    ${cmake-scripts_SOURCE_DIR}
)

include(code-coverage)
include(sanitizers)

add_code_coverage_all_targets()
#-------------------------------------------------------------------------------

add_compile_options(
  -Wall
  -Wextra
  # -Wshadow require higher than c++11
  -Wnon-virtual-dtor
  -Wold-style-cast
  -Wcast-align
  -Wno-char-subscripts
  -Wunused
  -Woverloaded-virtual
  -Wpedantic
  #-Wconversion  # to much noise from hydra sadly
  -Wsign-conversion
  # -Wmisleading-indentation -Wduplicated-cond -Wduplicated-branches
  -Wlogical-op
  # -Wnull-dereference
  -Wuseless-cast
  -Wdouble-promotion
  -Wformat=2
)

set(BEAMTIME FEB22 CACHE STRING "Beamtime configuration to use")
#option(BEAMTIME "Select beamtime")
set_property(CACHE BEAMTIME PROPERTY STRINGS FEB22;FEB21)

if(BEAMTIME STREQUAL "FEB22")
    set(CONFIG_BEAMTIME_FEB22 1)
elseif(BEAMTIME STREQUAL "FEB21")
    set(CONFIG_BEAMTIME_FEB21 1)
endif()

configure_file(config.h.in ft_config.h)

if(EXISTS $ENV{ROOTSYS}/cmake/ROOTConfig.cmake)
    list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
else()
    list(APPEND CMAKE_MODULE_PATH
        $ENV{ROOTSYS}/etc/cmake
        $ENV{ROOTSYS}/cmake/modules
)
endif()

#include(GetGlibcVersion)
#message(STATUS "libc Version: ${GLIBC_VERSION_TEXT}")
#if (GLIBC_VERSION_TEXT VERSION_GREATER 2.25)
#message(STATUS "Needs tirpc to support RPC")
#find_package(TIRPC REQUIRED)
#endif()

find_package(ROOT REQUIRED
    REQUIRED COMPONENTS Core Hist
    OPTIONAL_COMPONENTS Cint Cling)
message(STATUS "ROOT USE FILE ${ROOT_USE_FILE}")
#include("ROOTConfig.cmake")
#include(${ROOT_USE_FILE})
include_directories(${CMAKE_BINARY_DIR} SYSTEM ${ROOT_INCLUDE_DIRS})
link_directories(${ROOT_LIBRARY_DIRS})

macro(use_cxx11)
  if (CMAKE_VERSION VERSION_LESS "3.1")
    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
    endif ()
  else ()
    set (CMAKE_CXX_STANDARD 11)
  endif ()
endmacro(use_cxx11)

use_cxx11()

##### set paths
include(GNUInstallDirs)
set(CMAKE_INSTALL_CMAKEDIR ${DEF_CMAKE_INSTALL_CMAKEDIR} CACHE PATH "Installation directory for CMake files")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

##### make targets
find_package(HYDRA2 REQUIRED)

##### directories
include_directories(AFTER SYSTEM
    ${HYDRA2_INCLUDE_DIR}
)

add_subdirectory(common)
add_subdirectory(sts)
add_subdirectory(frpc)
add_subdirectory(itof)
add_subdirectory(elastics)
add_subdirectory(online_tools)
add_subdirectory(lumi_monitor)
add_subdirectory(tracking)

#####################################################################
# TESTS
option(ENABLE_TESTING "Enable testing" OFF)
if(ENABLE_TESTING)
    enable_testing()
    add_subdirectory(tests)
endif()


configure_file(Doxyfile.in Doxyfile)

#####################################################################

# messages
MESSAGE(STATUS "<<< Configuration >>>
Project:        ${CMAKE_PROJECT_NAME}

Build type      ${CMAKE_BUILD_TYPE}
Install path    ${CMAKE_INSTALL_PREFIX}

Compiler:
C               ${CMAKE_C_COMPILER}
C++             ${CMAKE_CXX_COMPILER}

Linker:
Ld              ${CMAKE_LINKER}

Compiler flags:
C               ${CMAKE_C_FLAGS}
C++             ${CMAKE_CXX_FLAGS}

Linker flags:
Executable      ${CMAKE_EXE_LINKER_FLAGS}
Module          ${CMAKE_MODULE_LINKER_FLAGS}
Shared          ${CMAKE_SHARED_LINKER_FLAGS}

Hydra Mode      ${HYDRA_MODE}
Hades dEdx Eq   ${HadesdEdxEqualizer_FOUND} ${HAS_HADESDEDXEQUALIZER}
")

foreach(p LIB BIN INCLUDE CMAKE)
    message(STATUS "CMAKE_INSTALL_${p}DIR: ${CMAKE_INSTALL_${p}DIR}")
endforeach()
