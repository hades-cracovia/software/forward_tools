#!/bin/bash

find common frpc sts itof elastics lumi_monitor tracking \( -name "*.h" -or -name "*.cc" -or -name "*.cpp" -or -name "*.C" \) ! -name "Linkdef.h" -exec clang-format --style=file -i {} \;
