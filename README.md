Tools for the HADES Forward Detector.

The package provides:
* `sts_calibrator` - calibrates the STS detector
* `frpc_calibrator` - calibrates the fRPC detector
* `elastics_monitor` - monitors elastic scatterings and calculates (integrated) luminosity

# Requirements

* installed Hydra2 package and exported `HADDIR` environment variable
* installed ROOT package and exported `ROOTSYS` environment variable
* cmake
* c++ compiler (g++ or clang)
* for enabling tests, also GoogleTest package

# Installation

#### Download
```bash
https://git.gsi.de/hades-cracovia/software/forward_tools
```

#### Compile
```bash
cd forward_tools
mkdir build
cd build
cmake ..
make
```

# Usage

## Sts calibration

Update me
```bash
./bin/sts_calibrator [ inputs ...]
```

## fRPC calibration

Command to run the fRPC calibration program, general structure.
#### Part 1 - generation of calibration histograms
```bash
./bin/frpc_tree_builder [location of DST analysis output files - input for the program] -p [location of parameters file] -o [name of the output file] -d [path where the output files are to be saved] -e [number of events to analyse] [fRPC calibration stage parameter e.g. `--cal-frpc-stage4`] [trigger parameter e.g `--pt1`]
```
#### Part 2 - calibration using generated histograms
```bash
./bin/frpc_cal_from_hists [location of root file with histograms produced in part 1 - input for the program] -p [location of parameters file] -o [name of the output file] -d [path where the output files are to be saved]  [fRPC calibration stage parameter e.g. `--cal-frpc-stage4`] [trigger parameter e.g `--pt1`]
```

Example command:
#### Part 1 - generation of calibration histograms
```bash
./bin/frpc_tree_builder ../../input_dir/be21045*.hld_dst_feb21.root -p ../../feb21_dst_params.txt -o output_hists.root -d ./out -e 500000 --cal-frpc-stage4 --pt1
```
#### Part 2 - calibration using generated histograms
```bash
./bin/frpc_cal_from_hists ./out/output_hists.root -p ../../feb21_dst_params.txt -o output_cal.root -d ./out -e 500000 --cal-frpc-stage4 --pt1
```
For all parameters, except for the location of DST analysis output files, order of parameters is irrelevant. Parameters with user input values (such as file locations or numbers) require prefacing with corresponding index, e.g. "-p" for location of parameters file. All parameters are:
- Location of DST analysis output files (input for the program). First parameter in order, input without an index prefacing it. Example: `../../output_dir_Pre_fRPC_Cal_New/be2104506572506.hld_dst_feb21.root` (multiple files can be selected using asterisks or question-marks, e.g. `../../input_dir/be21045*.hld_dst_feb21.root`).
- Location of parameters file, prefaced with index `-p`. If not specified, defaults to `./feb21_dst_params.txt`. Example: `-p ../../feb21_dst_params.txt`.
- Name of the root output file containing histograms (the name is also used to name the .txt file with calculated calibration parameters) prefaced with index `-o`. If not specified, defaults to `output.root`. Example: `-o output.root`.
- Path to where both output files (root file with histograms and txt file with calculated calibration parameters) should be saved (given folder needs to be manually created, if path to nonexistent folder is given, the files won't be saved) prefaced with index `-d`. If not specified, defaults to the same directory the program was executed in. Example: `-d ./out`.
- Number of events to analyze, prefaced with index `-e`. If not specified, all events in selected files will be analyzed. Example: `-e 500000`.
- Stage of calibration. Select one of:
 - `--cal-frpc-stage1` - First step of calibration (thresholds).
 - `--cal-frpc-stage2` - Second step of calibration (effective strip length correction).
 - `--cal-frpc-stage3` - Third step of calibration (strip position offset correction).
 - `--cal-frpc-stage4` - Fourth step of calibration (ToF distribution position correction).
- Trigger selection. Multiple triggers can be selected. Events without any of selected triggers will be skipped entirely. If no trigger is specified, all triggers will be selected (no events will be skipped).
 - `--pt1` - mult_TOFRPC>= 2 - currently recommended trigger for fRPC calibration.
 - `--pt2` - Min bias trigger for Forward Detector
 - `--pt3` - ???

Optionally, `2>&1 | tee fRPC_progress.txt` can also be added at the end of the command to save all the screen output to `fRPC_progress.txt` file. Useful if you use "Screen" program (which doesn't allow scrolling back through terminal output) on linux to run analysis on GSI computers without keeping your own computer running.

## Elastics scattering monitor and luminosity calculator
#### Elastic scattering and vertex reconstruction
```bash
./bin/fwdet_elastics /lustre/hades/dst/feb22/online/035/root/be2203513* -p ../../feb22_dst_params.txt -o output_el.root -d ./frpc_cal_output/ -e 5000000 2>&1 | tee ./frpc_cal_output/fRPC_el_progress.txt
```
Parameters function in the same way as those for fRPC calibration, but  only the: input file location, parameters file location, output file name and location as well as number of analysed events are used. Currently, the PT2 trigger is hard selected in the code.

## Online full-dst production

The setup provides tools to monitor new hld files and submit dst production jobs to the farm. All the scripts will be installed with make install command to the `$CMAKE_INSTALL_PREFIX/bin` directory.

Before start, one need to prepare config file. There is provided default one `config.sh.example` so:
```bash
cp config.sh.example config.sh
```
The default configuration:
```bash
# general
DATA_DIR=../data

# hld watcher and crawler
HLD_WATCH_PID_FILE=/tmp/.hld_watcher.pid
HLD_WATCH_LOG_FILE=/tmp/.hld_watcher.log
HLD_LIST_FILE=hld_list.txt
HLD_DIR=/lustre/hades/raw/feb22/22
HLD_MASK=*/be*.hld
HLD_DST_STATUS_FILE=hld_dst_status.txt
HLD_DST_SUBMIT_FILE=hld_dst_submit.txt
DST_DIR=data/out
DST_EVENTS=1000000
DST_LOG_PREFIX=dst_slurm
DST_SUBMIT_SCRIPT=submit_dst_job.sh
DST_BATCH_SCRIPT=dst_batch_script.sh
DST_CHECK_SCRIPT=
DST_LIST_FILE=../data/dst_list.txt
DST_MASK=*/be*.hld_*.root

# dst watcher and crawler
DST_LUMI_WATCH_PID_FILE=/tmp/.dst_lumi_watcher.pid
DST_LUMI_WATCH_LOG_FILE=/tmp/.dst_lumi_watcher.log
DST_LUMI_SUBMIT_SCRIPT=submit_lumi_job.sh
DST_LUMI_BATCH_SCRIPT=lumi_batch_script.sh
DST_LUMI_SUBMIT_FILE=dst_lumi_submit.txt
LUMI_LOG_PREFIX=dst_lumi_slurm
LUMI_TOOL=lumi_monitor
LUMI_DIR=lumi
LUMI_LIST_FILE=lumi_list.txt
```
includes several variables, some of them are worth to mention:
* `DATA_DIR` - directory where results will be written, this path will be prepend to the following variables:
 * `HLD_LIST_FILE` - keeps list of all found hld files
 * `HLD_DST_SUBMIT_FILE` - keeps list of all submitted dst jobs (files)
 * `HLD_DST_STATUS_FILE` - keeps status of the dst job, whether dst succeeded or failed
 * `DST_LIST_FILE` - keeps list of all dst files
 * `DST_DIR` - place where dst output files will be stored
 * `LUMI_LIST_FILE` - list of the found dst inputs
 * `DST_LUMI_SUBMIT_FILE` - keeps list of all submitted lumi inputs (files)
 * `LUMI_DIR` - place where lumi files will be stored
*  `*_PID_FILE` and `*_LOG_FILE`store process id and outputs for apps running as daemons
* `*_LOG_PREFIX` - prefix for slurm log files, which will be stored in `$DATA_DIR/slurm_log/$XXX_LOG_PREFIX-slurm-%j.log`
* `HLD_DIR` - directory to browse or monitor for new hld files
* `HLD_MASK` - search mask (glob), the files are searched inside `$HLD_DIR/$HLD_MASK`
* `DST_EVENTS` - number of events to analyze from hld file
* `*_SUBMIT_SCRIPT` - script which executes `sbatch` command, takes file as input, other variables are read out from config file
* `*_BATCH_SCRIPT` - script which is executed on the batch farm
* `DST_CHECK_SCRIPT` - program to check whether the dst production is correct
* `DST_MASK` - similar like `HLD_MASK`
* `LUMI_TOOL` - luminosity monitoring tool
* `LUMIDIR` output for lumi tool, relative to `DATA_DIR`
* `LUMI_LIST_FILE`- list of run dst files for lumi too

For most of the cases, one needs to adjust only `HLD_DIR` and `DATA_DIR`.

### Online dst

It is recommend to set up installation path with `cmake -DCMAKE_INSTALL_PREFIX=some_dir` and install files with `make install`

The basic usage is to start hld monitoring tool. Assuming that we are located in `some_dir/bin`:
```bash
./hld_watcher.py
```
Thsi will monitor every file created in `HLD_DIR` matching the `HLD_MASK` and submit the job if the new file appears. The app can be also run as a daemon with `-d` option, then the PID will be stored in `HLD_WATCH_PID_FILE` and out log in `HLD_WATCH_LOG_FILE`.

In case that some files could me missed (watcher was not working) one can run
```bash
./hld_watcher.py -l
```
which will list the directory and submit those files which were not processed yet. It is recommended to do it only a few times per day, e.g. using cron.

Each submitted file is stored in `HLD_DST_SUBMIT_FILE` therefore the tools above know which one were already send and will not submit the second time. If there is need to restart some jobs, one can remove these files from `HLD_DST_SUBMIT_FILE` and run command
```bash
./hld_watcher.py -s
```
which will go through `HLD_LIST` (so it will not list directory again) and submit those files which are not present in `HLD_DST_SUBMIT_FILE`.
See
```bash
./hld_watcher.py -h
```
for list of options.

### Luminosity monitoring
The muli is running on the DST files. The `lumi_watcher.py` program monitors the `DST_OUT` file for new files and submits jobs. It works in same way like the `hld_watcher.py` including options.

## Tracking tools

### Forward Detector event display

There is dedicated macro `tracking/gui_draw_event.C` to display event by event data.
When installed, the macro will be available under `CMAKE_INSTALL_REPFIX/share/forward_tools/macros/gui_draw_event.C`.

Typical usage:
```bash
root dst_file.root [more_files ...] gui_draw_event.C+
```

The macro is suppousd to be used in the ACLiC mode (the `+` at the end of macro name).

Be aware that the macro file must be after the input files, files listed after macro will be ignored but still availble to other root macros.

Due to configuration of event builder only EB 01 files contain STS data, thus these files are recommended for usage with this event display.
