#include <forwarddef.h>
#include <hades.h>
#include <hcategorymanager.h>
#include <hdst.h>
#include <hforwardcand.h>
#include <hfrpccal.h>
#include <hfrpccalpar.h>
#include <hfrpccluster.h>
#include <hfrpcdigipar.h>
#include <hfrpcgeompar.h>
#include <hfrpchit.h>
#include <hfrpcraw.h>
#include <hgeantfrpc.h>
#include <hgeantsts.h>
#include <hgeomcompositevolume.h>
#include <hgeomvector.h>
#include <hgeomvolume.h>
#include <hloop.h>
#include <hparasciifileio.h>
#include <hparticlecand.h>
#include <hruntimedb.h>
#include <hspectrometer.h>
#include <hstart2hit.h>
#include <hstartdef.h>
#include <hstscal.h>
#include <hstsgeompar.h>
#include <hstsraw.h>

#include <TCanvas.h>
#include <TColor.h>
#include <TF1.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TGraph.h>
#include <TH2I.h>
#include <TH3I.h>
#include <THStack.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TMatrixD.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TVector3.h>

#include <algorithm>
#include <cstdlib>
#include <getopt.h>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

using namespace std;

struct AnaParameters
{
    TString outfile{"output.root"};
    std::string label;
    int events{-1};
    int start{0};
    int sim;
    int verbose;
    bool show_fakes{false};
};

typedef void (*pad_callback)(TVirtualPad* pad);

const Float_t avg_beam_rate = 400e3; // 400 kHz

int no_fStart2Hit_cnt = 0;
int rejected_no_T0_cnt = 0;

const Int_t beach[5] = {TColor::GetColor("#fe4a49"), TColor::GetColor("#2ab7ca"),
                        TColor::GetColor("#fed766"), TColor::GetColor("#e6e6ea"),
                        TColor::GetColor("#f4f4f8")};

const Int_t metro[5] = {TColor::GetColor("#d11141"), TColor::GetColor("#00b159"),
                        TColor::GetColor("#00aedb"), TColor::GetColor("#f37735"),
                        TColor::GetColor("#ffc425")};
/*
HGeomVector calcPrimVertex_Track_Mother(const std::vector<HParticleCand*> cands,
                                        const HGeomVector& DecayVertex,
                                        const HGeomVector& dirMother, int trackA_num,
                                        int trackB_num);
HGeomVector calcPrimVertex_Track_Mother(const std::vector<HParticleCand*> cands,
                                        const HGeomVector& beamVector,
                                        const HGeomVector& DecayVertex,
                                        const HGeomVector& dirMother, int trackA_num,
                                        int trackB_num);
*/
// Adapted from
// https://stackoverflow.com/questions/48154210/3d-point-closest-to-multiple-lines-in-3d-space

void solve(TMatrixD& m, TVector3& b, TVector3& x, int n); // linear solver

void find_nearest_point(TVector3& p, const std::vector<TVector3>& a, const std::vector<TVector3>& d,
                        int n)
{
    TMatrixD m(3, 3);
    TVector3 b(0, 0, 0);
    for (int i = 0; i < n; ++i)
    {
        //     double d2 = dot(d[i], d[i]), da = dot(d[i], a[i]);
        double d2 = d[i] * d[i], da = d[i] * a[i];

        for (int ii = 0; ii < 3; ++ii)
        {
            for (int jj = 0; jj < 3; ++jj)
                m[ii][jj] += d[i][ii] * d[i][jj];
            m[ii][ii] -= d2;
            b[ii] += d[i][ii] * da - a[i][ii] * d2;
        }
    }
    solve(m, b, p, 3);
}

void pp(const TVector3& v, const char* l, const char* r)
{
    printf("%s%.3lf, %.3lf, %.3lf%s", l, v[0], v[1], v[2], r);
}

void pv(const TVector3& v) { pp(v, "(", ")"); }

// void pm(MAT m) { for (int i = 0; i < 3; ++i) pp(m[i], "\n[", "]"); }

// Verifier
double dist2(const TVector3& p, const TVector3& a, const TVector3& d)
{
    TVector3 pa = a - p;
    double dpa = d * pa;
    return (d * d) * (pa * pa) - dpa * dpa;
}

double sum_dist2(const TVector3& p, const std::vector<TVector3>& a, const std::vector<TVector3>& d,
                 int n)
{
    double sum = 0;
    for (int i = 0; i < n; ++i)
        sum += dist2(p, a[i], d[i]);
    return sum;
}

// Check 26 nearby points and verify the provided one is nearest.
int is_nearest(const TVector3& p, const std::vector<TVector3>& a, const std::vector<TVector3>& d,
               int n)
{
    double min_d2 = 1e100;
    int ii = 2, jj = 2, kk = 2;
#define D 0.01
    for (int i = -1; i <= 1; ++i)
        for (int j = -1; j <= 1; ++j)
            for (int k = -1; k <= 1; ++k)
            {
                TVector3 pp = {p[0] + D * i, p[1] + D * j, p[2] + D * k};
                double d2 = sum_dist2(pp, a, d, n);
                // Prefer provided point among equals.
                if (d2 < min_d2 || (i == 0 && j == 0 && k == 0 && d2 == min_d2))
                {
                    min_d2 = d2;
                    ii = i;
                    jj = j;
                    kk = k;
                }
            }
    return ii == 0 && jj == 0 && kk == 0;
}

void normalize(TVector3& v)
{
    double len = v.Mag();
    v[0] /= len;
    v[1] /= len;
    v[2] /= len;
}

int test(void)
{
    std::vector<TVector3> a = {{-14.2, 17, -1}, {1, 1, 1}, {2.3, 4.1, 9.8}, {1, 2, 3}};
    std::vector<TVector3> d = {{1.3, 1.3, -10}, {12.1, -17.2, 1.1}, {19.2, 31.8, 3.5}, {4, 5, 6}};
    int n = 4;
    for (int i = 0; i < n; ++i)
        normalize(d[i]);
    TVector3 p;
    find_nearest_point(p, a, d, n);
    pv(p);
    printf("\n");
    if (!is_nearest(p, a, d, n)) printf("Woops. Not nearest.\n");
    return 0;
}

// From rosettacode (with bug fix: added a missing fabs())
#define mat_elem(a, y, x, n) (a + ((y) * (n) + (x)))

void swap_row(TMatrixD& a, TVector3& b, int r1, int r2, int n)
{
    double p1, p2;
    int i;

    if (r1 == r2) return;
    for (i = 0; i < n; i++)
    {
        p1 = a[i][r1];
        p2 = a[i][r2];
        a[i][r1] = p2;
        a[i][r2] = p1;
    }
    double tmp = b[r1];
    b[r1] = b[r2];
    b[r2] = tmp;
}

void solve(TMatrixD& m, TVector3& b, TVector3& x, int n)
{
#define A(y, x) (*mat_elem(m, y, x, n))
    int j, col, row, max_row, dia;
    double max, tmp;

    for (dia = 0; dia < n; dia++)
    {
        max_row = dia, max = fabs(m[dia][dia]);
        for (row = dia + 1; row < n; row++)
            if ((tmp = fabs(m[row][dia])) > max) max_row = row, max = tmp;
        swap_row(m, b, dia, max_row, n);
        for (row = dia + 1; row < n; row++)
        {
            tmp = m[row][dia] / m[dia][dia];
            for (col = dia + 1; col < n; col++)
                m[row][col] -= tmp * m[dia][col];
            m[row][dia] = 0;
            b[row] -= tmp * b[dia];
        }
    }
    for (row = n - 1; row >= 0; row--)
    {
        tmp = b[row];
        for (j = n - 1; j > row; j--)
            tmp -= x[j] * m[row][j];
        x[row] = tmp / m[row][row];
    }
#undef A
}

//----------------------------------------------------------------------------------------------
// Functions for formating histograms
//----------------------------------------------------------------------------------------------

TH1* format_h(TH1* h)
{
    // h->SetLineStyle(9);
    h->SetTitle("");
    h->GetXaxis()->SetLabelSize(0.055);
    h->GetXaxis()->SetTitleSize(0.055);
    h->GetXaxis()->SetTitleOffset(1.20);
    h->GetXaxis()->SetNdivisions(505);
    h->GetYaxis()->SetLabelSize(0.055);
    h->GetYaxis()->SetTitleSize(0.055);
    h->GetYaxis()->SetTitleOffset(1.30);
    gPad->SetLeftMargin(0.14);
    gPad->SetTopMargin(0.05);
    gPad->SetRightMargin(0.05);
    gPad->SetBottomMargin(0.14);
    return h;
}

TH1* format_h_1_1_2_1(TH1* h)
{
    // h->SetLineStyle(9);
    h->SetTitle("");
    h->GetXaxis()->SetLabelSize(0.065);
    h->GetXaxis()->SetTitleSize(0.065);
    h->GetXaxis()->SetTitleOffset(0.95);
    h->GetXaxis()->SetNdivisions(505);
    h->GetYaxis()->SetLabelSize(0.065);
    h->GetYaxis()->SetTitleSize(0.065);
    h->GetYaxis()->SetTitleOffset(1.05);
    gPad->SetLeftMargin(0.14);
    gPad->SetTopMargin(0.05);
    gPad->SetRightMargin(0.05);
    gPad->SetBottomMargin(0.14);
    return h;
}

TH1* format_h_3(TH1* h)
{
    h->SetTitle("");
    h->GetXaxis()->SetLabelSize(0.06);
    h->GetXaxis()->SetTitleSize(0.06);
    h->GetXaxis()->SetTitleOffset(0.95);
    h->GetXaxis()->SetNdivisions(505);
    h->GetYaxis()->SetLabelSize(0.06);
    h->GetYaxis()->SetTitleSize(0.06);
    h->GetYaxis()->SetTitleOffset(1.10);
    gPad->SetLeftMargin(0.15);
    gPad->SetTopMargin(0.05);
    gPad->SetRightMargin(0.12);
    gPad->SetBottomMargin(0.13);
    return h;
}

void frpc_draw_v_lines(TVirtualPad* pad)
{
    pad->cd();
    TLine line;
    line.SetNDC(1);
    line.DrawLine(0, 375, 32, 375);
    line.DrawLine(0, -375, 32, -375);
}

//----------------------------------------------------------------------------------------------
// Usefull functions definitions
//----------------------------------------------------------------------------------------------

// Chisquare density distribution for nrFree degrees of freedom
Double_t ChiSquareDistr(Double_t* x, Double_t* par)
{
    Double_t nrFree = par[0];
    Double_t scale = par[1];
    Double_t sup = par[2];
    Double_t chi2 = x[0];

    if (chi2 > 0)
    {
        Double_t lambda = nrFree / 2.;
        Double_t norm = TMath::Gamma(lambda) * TMath::Power(2., lambda);
        return scale * TMath::Power(chi2 * sup, lambda - 1) * TMath::Exp(-0.5 * chi2 * sup) / norm;
    }
    else
        return 0.0;
}

struct TrackTracker
{
    unsigned int layer[8];
    TrackTracker() { clear(); }
    void clear()
    {
        for (int i = 0; i < 8; ++i)
            layer[i] = 0;
    }
    bool is_good()
    {
        for (int i = 0; i < 8; ++i)
        {
            if (layer[i] == 0) return false;
        }
        return true;
    }
    int count()
    {
        int cnt = 0;
        for (int i = 0; i < 8; ++i)
        {
            if (layer[i] > 0) ++cnt;
        }
        return cnt;
    }
};

// Function for clearing vector
void clean(std::vector<Int_t>& vec)
{
    sort(vec.begin(), vec.end());
    vec.erase(unique(vec.begin(), vec.end()), vec.end());
}

//----------------------------------------------------------------------------------------------
// Main function
//----------------------------------------------------------------------------------------------

Int_t analysis(HLoop* loop, const AnaParameters& anapars)
{
    // Check if loop was properly initialized
    if (!loop->setInput("-*,+HForwardCand,+HParticleCand,+HGeantKine"))
    { // reading file structure
        std::cerr << "READBACK: ERROR : cannot read input !" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    // Timer for checking analysis time
    TStopwatch timer;
    timer.Reset(); // Reset timer
    timer.Start(); // Start timer (T0)

    //////////////////////////////////////////////////////////////////////////////
    //      Fast tree builder for creating of ntuples                            //
    //////////////////////////////////////////////////////////////////////////////

    // Hades *myHades = new Hades;
    // myHades->setTreeBufferSize(8000);
    // HSpectrometer* spec = gHades->getSetup();

    //----------------------------------------------------------------------------------------------
    // Parameters file opening
    //----------------------------------------------------------------------------------------------

    Int_t mdcMods[6][4] = {{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1},
                           {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};
    HDst::setupSpectrometer("feb22", mdcMods, "sts,frpc");

    TString asciiParFile = "feb22_dst_params.txt"; // File containing all parameters of the detector
    HRuntimeDb* rtdb = HRuntimeDb::instance();     // myHades -> getRuntimeDb();

    // Checking if parameters file was opened properly
    if (!asciiParFile.IsNull())
    {
        HParAsciiFileIo* input1 = new HParAsciiFileIo;
        input1->open((Text_t*)asciiParFile.Data(), "in");
        if (!input1->check())
        {
            std::cerr << "Param file " << asciiParFile << " not open!" << std::endl;
            abort();
        }
        rtdb->setFirstInput(input1);
    }

    // Initializing the parameter container for geometry
    HStsGeomPar* pStrawGeomPar = (HStsGeomPar*)gHades->getRuntimeDb()->getContainer("StsGeomPar");
    // Checking if parameters container for geometry was created properly
    if (!pStrawGeomPar)
    {
        Error("HStsDigitizer::init()", "Parameter container for geometry not created");
        return kFALSE;
    }

    // Initializing the parameter container for geometry
    HFRpcGeomPar* pFRpcGeomPar = (HFRpcGeomPar*)gHades->getRuntimeDb()->getContainer("FRpcGeomPar");
    // Checking if parameters container for geometry was created properly
    if (!pFRpcGeomPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for geometry not created");
        return kFALSE;
    }

    HFRpcDigiPar* pFRpcDigiPar = (HFRpcDigiPar*)gHades->getRuntimeDb()->getContainer("FRpcDigiPar");
    if (!pFRpcDigiPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for HFRpcDigiPar not created");
        return kFALSE;
    }

    HFRpcCalPar* pFRpcCalPar = (HFRpcCalPar*)gHades->getRuntimeDb()->getContainer("FRpcCalPar");
    if (!pFRpcCalPar)
    {
        Error("HFRpcDigitizer::init()", "Parameter container for FRpcCalPar not created");
        return kFALSE;
    }

    rtdb->initContainers(0);
    rtdb->print();
    // pFRpcCalPar->printParams();

    // Print detector parameters on screen
    if (anapars.verbose)
    {
        printf("PARAMETERS LOADING\n");
        pStrawGeomPar->print();
        pFRpcGeomPar->print();
    }

    //----------------------------------------------------------------------------------------------
    // Accessing categories inside input file
    //----------------------------------------------------------------------------------------------

    // Forward Detector Raw data
    HCategory* fKine = HCategoryManager::getCategory(catGeantKine, kTRUE, "catGeantKine");
    if (!fKine) { cout << "No HGeantKine!\n"; }

    // Forward Detector Raw data
    HCategory* fStsRaw = HCategoryManager::getCategory(catStsRaw, kTRUE, "catStsRaw");
    if (!fStsRaw) { cout << "No fStsRaw!\n"; }

    // Forward Detector Calibrated data
    HCategory* fStsCal = HCategoryManager::getCategory(catStsCal, kTRUE, "catStsCal");
    if (!fStsCal) { cout << "No catStsCal!\n"; }

    // Forward Detector Candidates
    HCategory* fForwardCand =
        HCategoryManager::getCategory(catForwardCand, kTRUE, "catForwardCand");
    if (!fForwardCand) { cout << "No catForwardCand!\n"; }

    // Forward Detector RPC Raw data
    HCategory* fFRpcRaw = HCategoryManager::getCategory(catFRpcRaw, kTRUE, "catFRpcRaw");
    if (!fFRpcRaw) { cout << "No fFRpcRaw!\n"; }

    // Forward Detector RPC Calibrated data
    HCategory* fFRpcCal = HCategoryManager::getCategory(catFRpcCal, kTRUE, "catFRpcCal");
    if (!fFRpcCal) { cout << "No catFRpcCal!\n"; }

    // Forward Detector RPC Cluster
    HCategory* fFRpcClus = HCategoryManager::getCategory(catFRpcClus, kTRUE, "catFRpcCluster");
    if (!fFRpcClus) { cout << "No catFRpcClus!\n"; }

    // Forward Detector RPC Hits
    HCategory* fFRpcHit = HCategoryManager::getCategory(catFRpcHit, kTRUE, "catFRpcHit");
    if (!fFRpcHit) { cout << "No catFRpcHit!\n"; }

    // hstart2hit
    HCategory* fStart2Hit = HCategoryManager::getCategory(catStart2Hit, kTRUE, "catStart2Hit");
    if (!fStart2Hit) { cout << "No catStart2Hit!\n"; }

    // Hades Particle Candidates
    HCategory* fParticleCand =
        HCategoryManager::getCategory(catParticleCand, kTRUE, "catParticleCand");
    if (!fParticleCand) { cout << "No catParticleCand!\n"; }

    HGeomVector* frpcCellsLab[FRPC_MAX_SECTORS][FRPC_MAX_STRIPS] = {
        {nullptr}};                      // Centre of the strip [mm]
//     Float_t frpc_sina[FRPC_MAX_SECTORS]; // rotation matrix of the FRPC sector
//     Float_t frpc_cosa[FRPC_MAX_SECTORS];

    for (Int_t s = 0; s < FRPC_MAX_SECTORS; ++s)
    {
        HModGeomPar* fmodgeom = pFRpcGeomPar->getModule(s);
        HGeomTransform labTrans = fmodgeom->getLabTransform();

//         frpc_cosa[s] = labTrans.getRotMatrix().getElement(0, 0);
//         frpc_sina[s] = labTrans.getRotMatrix().getElement(1, 0);

        HGeomCompositeVolume* fMod = fmodgeom->getRefVolume();

        for (Int_t c = 0; c < FRPC_MAX_STRIPS; ++c)
        {
            HGeomVolume* fVol = fMod->getComponent(c);
            if (frpcCellsLab[s][c] == nullptr) frpcCellsLab[s][c] = new HGeomVector;
            HGeomVector* p = frpcCellsLab[s][c];
            *p = fVol->getTransform().getTransVector();
            *p = labTrans.transFrom(*p);
        }
    }

    //----------------------------------------------------------------------------------------------
    // Setting parameters for loop over events
    //----------------------------------------------------------------------------------------------

    Int_t entries = loop->getEntries(); // Number of entries in loop
    int limit_sta = anapars.start;      // Limit START - Where to start the loop
    int limit_sto = 0;                  // Limit STOP - Where to stop the loop

    if (anapars.events >= 0)
        limit_sto = limit_sta + anapars.events;
    else
        limit_sto = entries;

    if (limit_sto > entries) limit_sto = entries;

    //----------------------------------------------------------------------------------------------
    // Specifying output file
    //----------------------------------------------------------------------------------------------

    TFile* output_file = TFile::Open(anapars.outfile, "RECREATE");
    output_file->cd();

    //----------------------------------------------------------------------------------------------
    // Defining histograms and canvases for tracks
    //----------------------------------------------------------------------------------------------
    /*
        // TRACKS
        TH1I* h_tr_mult = new TH1I("h_tr_mult", "Tracks mult;multiplicity", 100, 0, 100);

        TH1I* h_tr_sts_mult = new TH1I("h_tr_sts_mult", "STS Tracks mult;multiplicity", 50, 0, 50);
        TH2I* h_tr_sts_mult_vs_hits =
            new TH2I("h_tr_sts_mult_vs_hits", "STS Tracks vs hits;tracks;hits", 50, 0, 50, 300, 0,
       300); TH1I* h_tr_sts_num = new TH1I("h_tr_sts_num", "STS Tracks num;number", 1, 0, 1); TH1I*
       h_tr_sts_planes = new TH1I("h_tr_sts_planes", "STS Planes used by track;used planes;count",
       8, 1, 9); TH1I* h_vec_sts_mult = new TH1I("h_vec_sts_mult", "Vectors mult;multiplicity", 15,
       0, 15); TH1I* h_vec_sts_num = new TH1I("h_vec_sts_num", "Tracks num;number", 1, 0, 1); TH1I*
       h_vec_sts_good_num = new TH1I("h_vec_sts_good_num", "Good tracks num;number", 1, 0, 1);

        TH1I* h_tr_frpc_mult = new TH1I("h_tr_frpc_mult", "RPC Tracks mult;multiplicity", 50, 0,
       50);

        TH2I* h_tr_sts_vs_frpc_mult = new TH2I("h_tr_sts_vs_frpc_mult",
                                               "STS vs RPC Tracks mult;Unique tracks in STS "
                                               "multiplicity;Unique tracks in RPC multiplicity",
                                               50, 0, 50, 50, 0, 50);

        TCanvas* c_tr_fd_mult = new TCanvas("c_tr_fd_mult", "STS and RPC mult", 1200, 600);
        c_tr_fd_mult->Divide(2, 1);

        TH2I* h_tr_vec_sts_mult =
            new TH2I("h_tr_vec_sts_mult", "Vectors mult;tracks;vectors", 10, 0, 10, 10, 0, 10);
        TH2I* h_tr_good_vec_sts_mult = new TH2I(
            "h_tr_good_vec_sts_mult", "Vectors mult;good tracks;vectors", 10, 0, 10, 10, 0, 10);
        TH2I* h_tr_good_vec_sts_good_mult =
            new TH2I("h_tr_good_vec_sts_good_mult", "Good vectors mult;good tracks;vectors", 10, 0,
       10, 10, 0, 10);

        TH1I* h_vec_chi2 = new TH1I("h_vec_chi2", ";#chi^{2}/ndf;counts", 250, 0, 5);
        TH1I* h_vec_chi2_good = new TH1I("h_vec_chi2_good", "Chi2/ndf;counts", 250, 0, 5);
        TH1I* h_vec_chi2_notofrec = new TH1I("h_vec_chi2_notofrec", "Chi2/ndf;counts", 2500, 0, 50);

        TCanvas* c_track_qa1 = new TCanvas("c_track_qa1", "c_track_qa1", 800, 800);
        c_track_qa1->DivideSquare(6);
        TCanvas* c_track_qa2 = new TCanvas("c_track_qa2", "c_track_qa2", 1200, 800);
        c_track_qa2->DivideSquare(5);
        TCanvas* c_track_qa_paper = new TCanvas("c_track_qa_paper", "c_track_qa_paper", 800, 400);
        // c_track_qa_paper->DivideSquare(4);

        TCanvas* c_track_qa_feb21 = new TCanvas("c_track_qa_feb21", "c_track_qa_feb21", 1800, 300);
        c_track_qa_feb21->Divide(6, 1);

        TH3I* h_trg_point =
            new TH3I("h_trg_point", "Target point;X [mm];Y [mm]; Z [mm]", 100, -500, 500, 100, -500,
       500, 100, -2000, 5000); TCanvas* c_trg_point = new TCanvas("c_trg_point", "h_trg_point", 800,
       600);

        TH3I* h_trg_point2 =
            new TH3I("h_trg_point2", "Target point2;X [mm]; Y [mm]; Z [mm]", 100, -50, 50, 100, -50,
       50, 100, -500, 100); TCanvas* c_trg_point2 = new TCanvas("c_trg_point2", "h_trg_point2", 800,
       600);

        TH2I* h_trg_rpoint = new TH2I("h_trg_rpoint", "Target point;Z [mm];R [mm]", 100, -2000,
       5000, 50, 0, 500); TCanvas* c_trg_rpoint = new TCanvas("c_trg_rpoint", "h_trg_point", 800,
       600);

        TH2I* h_trg_rpoint2 = new TH2I("h_trg_rpoint2", "Target point2;Z [mm];R [mm]", 100, -500,
       100, 50, 0, 50); TCanvas* c_trg_rpoint2 = new TCanvas("c_trg_rpoint2", "h_trg_rpoint2", 800,
       600);

        TH2I* h_trg_rpoint3 = new TH2I("h_trg_rpoint3", "Target point LC;Z [mm];R [mm]", 100, -500,
       100, 50, 0, 50); TCanvas* c_trg_rpoint3 = new TCanvas("c_trg_rpoint3", "h_trg_rpoint2", 800,
       600);

        TH2I* h_frpc_clus_xy =
            new TH2I("h_frpc_clus_xy", "h_frpc_clus_xy", 100, -1000, 1000, 100, -1000, 1000);
        TCanvas* c_frpc_clus_xy = new TCanvas("c_frpc_clus_xy", "RPC HIT xy", 800, 800);

        TH2I* h_frpc_hit_xy =
            new TH2I("h_frpc_hit_xy", "h_frpc_hit_xy", 200, -1000, 1000, 200, -1000, 1000);
        TCanvas* c_frpc_hit_xy = new TCanvas("c_frpc_hit_xy", "RPC HIT xy", 800, 800);

        // RECO
        TH2I* h_t_xy =
            new TH2I("h_t_xy", "Tracking: X-Y;x [mm];y [mm]", 100, -400., 400., 100, -400., 400.);
        TH2I* h_t_txty = new TH2I("h_t_txty", "Tracking: Tx-Ty;tan(#Alpha);tan(#Beta)", 100, -0.2,
       0.2, 100, -0.2, 0.2);

        TCanvas* c_t = new TCanvas("c_t", "c_t", 800, 800);
        c_t->DivideSquare(4);
    */
    TH2I* h_target_proj80_xy =
        new TH2I("h_target_proj80_xy", "Target reco @ -80 mm: X-Y", 100, -50., 50., 100, -50., 50.);
    TCanvas* c_target_proj80 = new TCanvas("c_target_proj80", "c_target_proj80", 800, 800);

    TH2I* h_target_proj124_xy = new TH2I("h_target_proj124_xy", "Target reco @ -124 mm: X-Y", 100,
                                         -50., 50., 100, -50., 50.);
    TCanvas* c_target_proj124 = new TCanvas("c_target_proj124", "c_target_proj124", 800, 800);

    TH2I* h_target_projZ_xy = new TH2I("h_target_projZ_xy", "Target reco @ -Zprime mm: X-Y", 100,
                                       -50., 50., 100, -50., 50.);
    TCanvas* c_target_projZ = new TCanvas("c_target_projZ", "c_target_projZ", 800, 800);

    TH2I* h_target_projZr_xy =
        new TH2I("h_target_projZr_xy", "Target reco @ -Zprime (ring) mm: X-Y", 100, -50., 50., 100,
                 -50., 50.);
    TCanvas* c_target_projZr = new TCanvas("c_target_projZr", "c_target_projZr", 800, 800);

    TH1I* h_zprime = new TH1I("h_zprime", "Z prime;z [mm];counts", 300, -300, 0);
    TH1I* h_z_avg = new TH1I("h_z_avg", "Z avg;z [mm];counts", 300, -300, 0);
    TCanvas* c_z_prime = new TCanvas("c_z_prime", "c_z_prime", 800, 800);

    TH2I* h_r_zprime = new TH2I("h_r_zprime", "R-Z;z [mm];r [mm]counts", 300, -300, 0, 50, 0, 50);
    TCanvas* c_r_zprime = new TCanvas("c_r_zprime", "c_r_zprime", 800, 800);

    int event_with_tracks = 0;

    auto xBeam = 1.866f;
    auto yBeam = -1.438f;

    if (fKine)
    {
        xBeam = 0.f;
        yBeam = 0.f;
    }

    for (Int_t i = limit_sta; i < limit_sto; i++) // event loop
    {
        if (i % 10000 == 0)
        {
            printf("Event nr.: %d, progress: %.2f%%\n", i,
                   (double)(i - limit_sta) / (limit_sto - limit_sta) * 100.);
        }

        /*Int_t nbytes =*/loop->nextEvent(i); // get next event. categories will be cleared before

        HEventHeader* event_header = NULL;
        if (!(event_header = gHades->getCurrentEvent()->getHeader())) continue;

        //----------------------------------------------------------------------------------------------
        // Forward Detector RPC Raw
        //----------------------------------------------------------------------------------------------

        Float_t T0 = 100000.0;
        Float_t T0_z = 0.;

        //----------------------------------------------------------------------------------------------
        // Forward Detector RPC Cluster
        //----------------------------------------------------------------------------------------------
        /*        if (fFRpcClus)
                {
                    Int_t hcnt = fFRpcClus->getEntries();

                    HFRpcCluster* frpcclus = 0;

                    for (int j = 0; j < hcnt; ++j)
                    {
                        frpcclus = HCategoryManager::getObject(frpcclus, fFRpcClus, j);
                        // frpcclus->print();
                        h_frpc_clus_xy->Fill(frpcclus->getX(), frpcclus->getY());
                    }
                }
        */
        Int_t vcnt = 0;
        Int_t vcnt_good = 0;

        // Lukas Chlad way
        auto zAvg = 0.f;
        auto acc_cand = 0;
        if (fParticleCand)
        {
            auto n = fParticleCand->getEntries();

            for (int j = 0; j < n; ++j)
            {
                HParticleCand* fcand = HCategoryManager::getObject(fcand, fParticleCand, j);
                if (!fcand->isFlagBit(kIsUsed)) continue;

                auto rPrime = fcand->getRprime(xBeam, yBeam);
                auto zPrime = fcand->getZprime(xBeam, yBeam, rPrime);
                zAvg += zPrime;
                ++acc_cand;
                h_zprime->Fill(zPrime);
            }
            zAvg /= acc_cand;
            h_z_avg->Fill(zAvg);
        }

        if (fForwardCand)
        {
            vcnt = fForwardCand->getEntries();

            if (vcnt) ++event_with_tracks;

            std::vector<TVector3> a;
            std::vector<TVector3> d;
            for (int j = 0; j < vcnt; ++j)
            {
                HForwardCand* fwdetvec = HCategoryManager::getObject(fwdetvec, fForwardCand, j);
                // fwdetvec->print();
                a.push_back(
                    TVector3(fwdetvec->getBaseX(), fwdetvec->getBaseY(), fwdetvec->getBaseZ()));
                d.push_back(TVector3(fwdetvec->getDirTx(), fwdetvec->getDirTy(), 1).Unit());

                //                 h_t_xy->Fill(fwdetvec->getBaseX(), fwdetvec->getBaseY());
                //                 h_t_txty->Fill(fwdetvec->getDirTx(), fwdetvec->getDirTy());

                Float_t z = fwdetvec->getBaseZ();
                h_target_proj80_xy->Fill(fwdetvec->getBaseX() - (z - -80) * fwdetvec->getDirTx(),
                                         fwdetvec->getBaseY() - (z - -80) * fwdetvec->getDirTy());
                h_target_proj124_xy->Fill(fwdetvec->getBaseX() - (z - -124) * fwdetvec->getDirTx(),
                                          fwdetvec->getBaseY() - (z - -124) * fwdetvec->getDirTy());

                auto x_avg = fwdetvec->getBaseX() - (z - zAvg) * fwdetvec->getDirTx();
                auto y_avg = fwdetvec->getBaseY() - (z - zAvg) * fwdetvec->getDirTy();
                auto r_avg = sqrt(x_avg * x_avg + y_avg * y_avg);

                h_r_zprime->Fill(zAvg, r_avg);

                if (acc_cand)
                {
                    h_target_projZ_xy->Fill(x_avg, y_avg);

                    if (zAvg > -250.f and zAvg < -180.f) h_target_projZr_xy->Fill(x_avg, y_avg);
                }

                // residuals
                /*
                                if (fwdetvec->getTofRec() == 1)
                                {
                //                     Float_t bx = fwdetvec->getBaseX();
                //                     Float_t by = fwdetvec->getBaseY();
                //                     Float_t bz = fwdetvec->getBaseZ();
                //                     Float_t dx = fwdetvec->getDirTx();
                //                     Float_t dy = fwdetvec->getDirTy();

                                    //                     fwdetvec->print();
                                }

                                //                 TVector2 v_n(gf.pzHit, gf.pxHit);   // track
                vector dir.  n
                                //                 v_n = v_n/v_n.Mod();
                                //                 TVector2 p_a(hit_z, hit_x);         // track hit
                position a
                                //                 TVector2 p_p(cell_z, cell_x);       // wire
                position      p

                                // residuals end
                                if (fwdetvec->getTofRec())
                                {
                                    fwdetvec->calc4vectorProperties(938);

                                    h_frpc_tof->Fill(fwdetvec->getTof());
                                    h_frpc_len->Fill(fwdetvec->getDistance());
                                    h_frpc_len_p->Fill(fwdetvec->getDistance(), fwdetvec->P());
                                    h_frpc_len_tof->Fill(fwdetvec->getDistance(),
                fwdetvec->getTof()); h_p->Fill(fwdetvec->P());
                                }
                                else
                                {
                                    h_vec_chi2_notofrec->Fill(fwdetvec->getChi2() /
                fwdetvec->getNDF());
                                }

                                h_vec_chi2->Fill(fwdetvec->getChi2() / fwdetvec->getNDF());
                */
            }
            /*
                        h_vec_sts_mult->Fill(vcnt);
                        h_vec_sts_num->SetBinContent(1, h_vec_sts_num->GetBinContent(1) + vcnt);
                        h_vec_sts_good_num->SetBinContent(1, h_vec_sts_good_num->GetBinContent(1) +
               vcnt_good);

                        if (vcnt > 1)
                        {
                            TVector3 p;
                            find_nearest_point(p, a, d, a.size());
                            h_trg_point->Fill(p.X(), p.Y(), p.Z());
                            h_trg_point2->Fill(p.X(), p.Y(), p.Z());

                            float r = sqrt(p.X() * p.X() + p.Y() * p.Y());
                            h_trg_rpoint->Fill(p.Z(), r);
                            h_trg_rpoint2->Fill(p.Z(), r);
                            //                 pv(p);
                            //                 printf("\n");
                            //                 if (!is_nearest(p, a, d, a.size())) printf("Woops.
               Not
                            //                 nearest.\n");
                        }
            */
        }
    } // end eventloop

    cout << "Number of events skipped due to no entires in fStart2Hit: " << no_fStart2Hit_cnt
         << " out of " << (limit_sto - limit_sta) << endl;
    cout << "Number of events skipped due to Flag = 0 in fStart2Hit: " << rejected_no_T0_cnt
         << " out of " << (limit_sto - limit_sta - no_fStart2Hit_cnt) << endl;

    output_file->cd();
    /*
        c_frpc_clus_xy->cd();
        h_frpc_clus_xy->Draw("colz");
        format_h_3(h_frpc_clus_xy);
        h_frpc_clus_xy->Write();
        c_frpc_clus_xy->Write();

        // TF1 * f_gaus = new TF1("f_gaus", "gaus", -5, 5);
        TLatex* tex = new TLatex;
        tex->SetNDC(kTRUE);
        // digitizer
    */
    c_target_proj80->cd();
    h_target_proj80_xy->Draw("colz");
    c_target_proj80->Write();
    h_target_proj80_xy->Write();

    c_target_proj124->cd();
    h_target_proj124_xy->Draw("colz");
    c_target_proj124->Write();
    h_target_proj124_xy->Write();

    c_target_projZ->cd();
    h_target_projZ_xy->Draw("colz");
    c_target_projZ->Write();
    h_target_projZ_xy->Write();

    c_target_projZr->cd();
    h_target_projZr_xy->Draw("colz");
    c_target_projZr->Write();
    h_target_projZr_xy->Write();

    c_z_prime->cd();
    h_zprime->Draw();
    h_zprime->SetLineWidth(2);
    h_z_avg->Draw("same");
    h_z_avg->SetLineColor(kBlack);
    h_z_avg->SetLineWidth(2);
    c_z_prime->BuildLegend();
    c_z_prime->Write();
    h_zprime->Write();
    h_z_avg->Write();

    c_r_zprime->cd();
    h_r_zprime->Draw("colz");
    c_r_zprime->Write();
    h_r_zprime->Write();

    /*
        c_t->cd(1);
        h_t_xy->Draw("colz");
        c_t->cd(2);
        h_t_txty->Draw("colz");
        c_t->cd(3);
        h_p->Draw();
        h_p_gea->Draw("same");
        h_p_gea->SetLineColor(2);
        h_p_rec_14->Draw("same");
        h_p_rec_14->SetLineColor(3);

        TLegend* leg_p = new TLegend(0.5, 0.5, 0.85, 0.85);
        leg_p->AddEntry(h_p, "All tracks", "l");
        leg_p->AddEntry(h_p_rec_14, "Protons", "l");
        leg_p->AddEntry(h_p_gea, "Protons from Geant", "l");
        leg_p->Draw();

        c_t->cd(4);
        h_p_res2->Draw("colz");

        c_frpc->cd(1);
        h_frpc_tof->Draw();
        c_frpc->cd(2);
        h_frpc_len->Draw();
        c_frpc->cd(3);
        h_frpc_len_p->Draw("colz");
        c_frpc->cd(4);
        h_frpc_len_tof->Draw("colz");

        h_tr_sts_mult->SetTitle("Tracks multiplicity in the STS (blue) and RPC (red) detectors");
        c_tr_fd_mult->cd(1)->SetLogy();
        h_tr_sts_mult->Draw("text20,h");
        h_tr_frpc_mult->Draw("text20,h,same");
        h_tr_sts_mult->SetLineWidth(2);
        h_tr_frpc_mult->SetLineWidth(2);
        h_tr_frpc_mult->SetLineColor(kRed);

        c_tr_fd_mult->cd(2)->SetLogz();
        h_tr_sts_vs_frpc_mult->Draw("colz,text");

        c_tr_fd_mult->Write();

        TF1* fchi2 = new TF1("chi-square distribution", ChiSquareDistr, 0.0, 10, 3);
        fchi2->SetParameter(0, 12.0);
        fchi2->SetParameter(1, 6500.0);
        fchi2->SetParameter(2, 12.0);
        fchi2->SetLineColor(30);

        c_track_qa1->cd(1);
        h_tr_sts_mult->Draw("h,text");
        gPad->SetLogy();
        c_track_qa1->cd(2);
        h_tr_mult->Draw("h,text");
        gPad->SetLogy();
        c_track_qa1->cd(3);
        h_tr_sts_num->Draw("h,text");
        c_track_qa1->cd(4);
        h_tr_sts_planes->Draw("h,text");
        c_track_qa1->cd(5);
        h_vec_sts_mult->Draw("h,text");
        c_track_qa1->cd(6);
        h_vec_sts_num->Draw("h,text");
        c_track_qa2->cd(1);
        h_tr_vec_sts_mult->Draw("colz,text");
        gPad->SetLogz();
        c_track_qa2->cd(2);
        h_tr_good_vec_sts_mult->Draw("colz,text");
        gPad->SetLogz();
        c_track_qa2->cd(3);
        h_tr_good_vec_sts_good_mult->Draw("colz,text");
        gPad->SetLogz();
        c_track_qa2->cd(4);
        h_vec_chi2->Draw();
        h_vec_chi2_good->Draw("same");
        h_vec_chi2_good->SetLineColor(kRed);
        fchi2->Draw("same");
        c_track_qa2->cd(6);
        h_tr_sts_planes->Draw("h,text");
        c_track_qa1->Write();
        c_track_qa2->Write();

        TGaxis::SetMaxDigits(3);

        c_track_qa_paper->cd();
        h_vec_chi2->Draw();
        h_vec_chi2->SetLineColor(12);
        h_vec_chi2->SetLineWidth(2);
        h_vec_chi2->SetLineStyle(7);
        format_h_1_1_2_1(h_vec_chi2);
        TLegend* leg_chi2 = new TLegend(0.5, 0.7, 0.9, 0.9);
        leg_chi2->SetBorderSize(0);
        leg_chi2->AddEntry(h_vec_chi2, "#chi^{2} for #sigma = 200 #mum", "lpf");
        leg_chi2->Draw();

        TGaxis::SetMaxDigits(5);

        //     c_track_qa_paper->cd(2);
        //     format_h(h_vec_gea_cors);
        //     h_vec_gea_cors->DrawNormalized("text");
        //     h_vec_gea_cors->SetMarkerSize(0.3);

        c_track_qa_paper->Write();

        printf("Tracks=%.0f  vectors=%.0f  (%.1f%%)\n", h_tr_sts_num->Integral(),
               h_vec_sts_num->Integral(), 100.0 * h_vec_sts_num->Integral() /
       h_tr_sts_num->Integral()); printf("Valid= %.0f  vectors=%.0f  (%.1f%%)\n",
       h_tr_sts_planes->GetBinContent(8), h_vec_sts_num->Integral(), 100.0 *
       h_vec_sts_num->Integral() / h_tr_sts_planes->GetBinContent(8)); printf("Valid= %.0f  good
       vs=%.0f  (%.1f%%)\n", h_tr_sts_planes->GetBinContent(8), h_vec_sts_good_num->Integral(),
               100.0 * h_vec_sts_good_num->Integral() / h_tr_sts_planes->GetBinContent(8));

        h_t_xy->Write();
        h_t_txty->Write();
        c_t->Write();

        h_frpc_tof->Write();
        h_frpc_len->Write();
        h_frpc_len_p->Write();
        h_frpc_len_tof->Write();
        h_p->Write();
        h_p_gea->Write();
        h_p_res2->Write();
        h_p_rec_14->Write();
        c_frpc->Write();

        h_tr_mult->Write();

        h_tr_sts_mult->Write();
        h_tr_sts_mult_vs_hits->Write();
        h_tr_sts_num->Write();
        h_tr_sts_planes->Write();
        h_vec_sts_mult->Write();
        h_vec_sts_num->Write();
        h_vec_sts_good_num->Write();
        h_tr_vec_sts_mult->Write();
        h_tr_good_vec_sts_mult->Write();
        h_tr_good_vec_sts_good_mult->Write();
        h_vec_chi2->Write();
        h_vec_chi2_good->Write();
        h_vec_chi2_notofrec->Write();
        h_tr_frpc_mult->Write();
        h_tr_sts_vs_frpc_mult->Write();
    */
    // FEB21 QA
    /*    c_track_qa_feb21->cd(1);
        h_t_xy->Draw("colz");
        c_track_qa_feb21->cd(2);
        h_t_txty->Draw("colz");
        c_track_qa_feb21->cd(3);
        h_vec_sts_mult->Draw("hist,text30");
        c_track_qa_feb21->cd(4);
        h_target_proj80_xy->Draw("colz");
        c_track_qa_feb21->cd(5);
        h_target_proj124_xy->Draw("colz");

        c_track_qa_feb21->cd(6);
        TLatex latex;
        int Nev = limit_sto - limit_sta;
        const float basestart = 0.8;
        const float baseskip = 0.08;

        c_track_qa_feb21->Write();

        c_trg_point->cd();
        h_trg_point->Draw();

        c_trg_point2->cd();
        h_trg_point2->Draw();

        c_trg_point->Write();
        h_trg_point->Write();

        c_trg_point2->Write();
        h_trg_point2->Write();

        c_trg_rpoint->cd();
        h_trg_rpoint->Draw();

        c_trg_rpoint2->cd();
        h_trg_rpoint2->Draw();

        c_trg_rpoint->Write();
        h_trg_rpoint->Write();

        c_trg_rpoint2->Write();
        h_trg_rpoint2->Write();
    */

    output_file->Close();
    cout << "writing root tree done\n";

    timer.Stop();
    timer.Print();

    return 0;
}

int main(int argc, char** argv)
{
    TROOT Analysis("Analysis", "compiled analysis macro");
    gStyle->SetOptStat(0);

    int c;

    AnaParameters anapars;
    anapars.start = 0;
    anapars.events = -1;
    anapars.show_fakes = 0;
    anapars.outfile = "vertex.root";

    while (1)
    {
        static struct option long_options[] = {
            /* These options set a flag. */
            {"verbose", no_argument, &anapars.verbose, 1},
            {"brief", no_argument, &anapars.verbose, 0},
            {"sim", no_argument, &anapars.sim, 1},
            {"exp", no_argument, &anapars.sim, 0},

            /* These options don’t set a flag.
             *              We distinguish them by their indices. */
            {"events", required_argument, 0, 'e'},
            {"label", required_argument, 0, 'l'},
            {"output", required_argument, 0, 'o'},
            {"start", required_argument, 0, 's'},
            {0, 0, 0, 0}};
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long(argc, argv, "e:o:l:s:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1) break;

        switch (c)
        {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0) break;
                printf("option %s", long_options[option_index].name);
                if (optarg) printf(" with arg %s", optarg);
                printf("\n");
                break;

            case 'e':
                anapars.events = atol(optarg);
                break;

            case 'o':
                anapars.outfile = optarg;
                break;

            case 'l':
                anapars.label = optarg;
                break;

            case 's':
                anapars.start = atol(optarg);
                break;

            case '?':
                /* getopt_long already printed an error message. */
                break;

            default:
                abort();
        }
    }

    /* Instead of reporting ‘--verbose’
     *      and ‘--brief’ as they are encountered,
     *           we report the final status resulting from them. */
    if (anapars.verbose) puts("verbose flag is set");

    HLoop* loop = new HLoop(kTRUE);

    /* Print any remaining command line arguments (not options). */
    if (optind < argc)
    {
        Bool_t ret;
        //              printf ("non-option ARGV-elements: ");
        while (optind < argc)
        {
            TString infile = argv[optind++];
            if (infile.Contains(","))
                ret = loop->addMultFiles(infile);
            else if (infile.Contains(".root"))
                ret = loop->addFiles(infile);
            else
                ret = loop->addFilesList(infile);

            if (!ret)
            {
                std::cerr << "READBACK: ERROR : cannot find inputfiles : " << infile.Data() << endl;
                std::exit(EXIT_FAILURE);
            }
        }
    }

    analysis(loop, anapars);

    exit(0);
}
