#ifndef CONFIG_FEB22_H
#define CONFIG_FEB22_H

#include "ft_config.h"

#include <Rtypes.h>
#include <cmath>

constexpr Float_t pi_value = 3.14159265359;    // Pi constant value
constexpr Float_t c_value = 299.792458;        // Speed of light (In mm/ns)
constexpr Float_t proton_mass = 938.272081358; // Proton mass [MeV/c^2]

//! Configuration and constants for Feb21 beam
namespace ConfigBeamtime
{
constexpr Beamtime bt{Beamtime::Feb22};

constexpr auto eff_file = "pp_elastic_4200_feb21_efficiencies.root";

// Cuts for elastic scattering

constexpr Float_t inverse_gamma2 = 0.29429;

// Cuts for elastic scattering
constexpr Float_t cut_HF_phi_diff_min = 175.0;
constexpr Float_t cut_HF_phi_diff_max = 185.0;

constexpr Float_t cut_HH_phi_diff_min = 178.5;
constexpr Float_t cut_HH_phi_diff_max = 181.5;

constexpr Float_t cut_HF_tan_theta_min = inverse_gamma2 - 0.1f;
constexpr Float_t cut_HF_tan_theta_max = inverse_gamma2 + 0.1f;

constexpr Float_t cut_HH_tan_theta_min = inverse_gamma2 - 0.015f;
constexpr Float_t cut_HH_tan_theta_max = inverse_gamma2 + 0.015f;

constexpr Float_t HF_phi_diff_sigma = 2.18156;
constexpr Float_t HF_tan_theta_prod_sigma = 0.0266234;
constexpr Float_t HH_phi_diff_sigma = 0.710401;
constexpr Float_t HH_tan_theta_prod_sigma = 0.00566902;

constexpr Float_t cut_EL_dist = 3.0; // Cut on elyptical distance from middle of theoretical distritubtion [sigma]
constexpr Float_t cut_BKG_dist_min = 3.5;
constexpr Float_t cut_BKG_dist_max = sqrt( pow(cut_EL_dist,2) + pow(cut_BKG_dist_min,2) );

// Constants for fRPC and HADES
// Lowest fRPC sector number we start iterating from (to skip empty sectors)
constexpr int frpc_min_sector = 0;
constexpr int frpc_sectors = 4;                     // Number of sectors in fRPC
constexpr int frpc_cstrips = 32;                    // Number of strips in each module of fRPC
constexpr Float_t z_target = -115.0;                  // Position of target (middle point) [mm]
constexpr Float_t beam_Ekin = 4500;                   // Kinetic energy of beam particle [MeV]
constexpr Float_t beam_E01 = beam_Ekin + proton_mass; // Total energy of beam particle [MeV]
constexpr Float_t beam_gamma_cm =
    sqrt((beam_E01 + proton_mass) / (2 * proton_mass)); // Gamma of Center of Mass system
constexpr Float_t beam_p01 =
    sqrt(pow(beam_E01, 2) - pow(proton_mass, 2)); // Momentum of beam particle [MeV/c]

}; // namespace ConfigBeamtime

#endif /* CONFIG_FEB22_H */
