#ifndef FORWARD_TOOLS_H
#define FORWARD_TOOLS_H

#include "ft_config.h"
#include <Rtypes.h>

#include <TCanvas.h>
#include <TColor.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1I.h>
#include <TH2F.h>
#include <TH2I.h>
#include <TH3I.h>
#include <THStack.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TMatrixD.h>
#include <TMultiGraph.h>
#include <TROOT.h>
#include <TStopwatch.h>
#include <TString.h>
#include <TStyle.h>
#include <TTree.h>
#include <TVector3.h>

using namespace ConfigBeamtime;

class HCategory;

//! Usefull tools for the package
namespace ForwardTools
{

//! Elastics scattering related stuff
namespace Elastics
{
/**
 * Check whether there is elastic scattering in HADES-HADES case data.
 * \param pcat particle cand category
 * \param phi_diff_min phi dif lower cut edge
 * \param phi_diff_max phi dif upper cut edge
 * \param thetap_diff_min
 * \param thetap_diff_max
 * \return true if event has elastic event
 */
bool check_elastics_hh(HCategory* pcat, Float_t phi_diff_min, Float_t phi_diff_max,
                       Float_t thetap_diff_min, Float_t thetap_diff_max);

/**
 * Check whether there is elastic scattering in HADES-FwDet case data.
 * \param pcat particle cand category
 * \param fcat forward cand category
 * \param phi_diff_min phi dif lower cut edge
 * \param phi_diff_max phi dif upper cut edge
 * \param thetap_diff_min
 * \param thetap_diff_max
 * \return true if event has elastic event
 */
bool check_elastics_hf(HCategory* pcat, HCategory* fcat, Float_t phi_diff_min, Float_t phi_diff_max,
                       Float_t thetap_diff_min, Float_t thetap_diff_max);

/**
 * Alternate ToF calculation scheme to bypass lack of T0 measurement
 * \param h_exp_ToF experimental ToF value of HADES particle
 * \param h_theor_ToF theoretical ToF value of HADES particle
 * \param f_exp_ToF experimental ToF value of FwDet particle
 * \return alternate ToF value
 */
Float_t alt_ToF_calc(Float_t h_exp_ToF, Float_t h_theor_ToF, Float_t f_exp_ToF);

/**
 * Theoretical momentum for elasticaly scattered proton calculation
 * \param theta scattering angle !!! IMPORTANT - Theta has to be input in radians !!!
 * \return theoretical momentum value
 */
Float_t calc_theor_el_mom(Float_t theta);

/**
 * Theoretical energy for elasticaly scattered proton calculation
 * \param theta scattering angle !!! IMPORTANT - Theta has to be input in radians !!!
 * \return theoretical energy value
 */
Float_t calc_theor_el_energy(Float_t theta);

/**
 * Theoretical beta for elasticaly scattered proton calculation
 * \param theta scattering angle !!! IMPORTANT - Theta has to be input in radians !!!
 * \return theoretical beta value
 */
Float_t calc_theor_el_beta(Float_t theta);

/**
 * Theoretical ToF for elasticaly scattered proton calculation
 * \param dist track length
 * \param theta scattering angle !!! IMPORTANT - Theta has to be input in radians !!!
 * \return theoretical ToF value
 */
Float_t calc_theor_el_ToF(Float_t dist, Float_t theta);

} // namespace Elastics

//! T0 calculation functions
namespace T0
{
}

//! Histogram and canvas drawing
namespace Drawing
{
/**
 * Drawing a red vertical line on histogram at a given position
 * \param can canvas on which the line should be drawn
 * \param hist histogram drawn on the canvas (for line height information)
 * \param line_position position (X axis) where the line should be drawn
 */
void draw_vertical_line(TVirtualPad* can, TH1* hist, Float_t line_position);

/**
 * Drawing a vertical line of chosen color on histogram at a given position
 * \param can canvas on which the line should be drawn
 * \param hist histogram drawn on the canvas (for line height information)
 * \param line_position position (X axis) where the line should be drawn
 * \param color chosen color of the line (number code according to ROOT TColor class)
 */
void draw_vertical_line_color(TVirtualPad* can, TH1* hist, Float_t line_position, Int_t color);

/**
 * Drawing a histogram on canvas and writing both to a given directory in an open root file (option
 * to draw vertical line) \param can canvas on which the histogram should be drawn \param hist
 * histogram drawn on the canvas \param dir directory in an open root file where can and hist should
 * be written to \param line_pos position where to draw a vertical line (if set to "-999.9" no line
 * will be drawn)
 */
void draw_and_write(TCanvas* can, TH1* hist, TDirectory* dir, Float_t line_pos);

/**
 * Drawing a histogram on canvas, writing both to a given directory in an open root file and saving
 * the canvas as a ".png" file to a specified folder (option to draw vertical line) \param can
 * canvas on which the histogram should be drawn \param hist histogram drawn on the canvas \param
 * dir directory in an open root file where can and hist should be written to \param line_pos
 * position where to draw a vertical line (if set to "-999.9" no line will be drawn) \param name
 * path and name for saving the canvas as picture
 */
void draw_write_and_save_picture(TCanvas* can, TH1* hist, TDirectory* dir, Float_t line_pos,
                                 TString name);

/**
 * Drawing a histogram on canvas and writing both to a given directory in an open root file (option
 * to draw vertical line) \param can canvas on which the histogram should be drawn \param hist1
 * first histogram drawn on the canvas \param hist2 second histogram drawn on the canvas \param dir
 * directory in an open root file where can and hist should be written to \param line_pos position
 * where to draw a vertical line (if set to "-999.9" no line will be drawn)
 */
void draw_and_write_x2(TCanvas* can, TH1* hist1, TH1* hist2, TDirectory* dir, Float_t line_pos);

/**
 * Drawing a histogram on canvas, writing both to a given directory in an open root file and saving
 * the canvas as a ".png" file to a specified folder (option to draw vertical line) \param can
 * canvas on which the histogram should be drawn \param hist1 first histogram drawn on the canvas
 * \param hist2 second histogram drawn on the canvas
 * \param dir directory in an open root file where can and hist should be written to
 * \param line_pos position where to draw a vertical line (if set to "-999.9" no line will be drawn)
 * \param name path and name for saving the canvas as picture
 */
void draw_write_and_save_picture_x2(TCanvas* can, TH1* hist1, TH1* hist2, TDirectory* dir,
                                    Float_t line_pos, TString name);

/**
 * Drawing a histogram on canvas and writing both to a given directory in an open root file (option
 * to draw vertical line and mathematical function) \param can canvas on which the histogram should
 * be drawn \param hist histogram drawn on the canvas \param dir directory in an open root file
 * where can and hist should be written to \param line_pos position where to draw a vertical line
 * (if set to "-999.9" no line will be drawn) \param fun mathematical function to be drawn on
 * histogram (if set to "f_empty" no function will be drawn)
 */
void draw_and_write(TCanvas* can, TH1* hist, TDirectory* dir, Float_t line_pos, TF1* fun);

/**
 * Drawing a histogram on canvas, writing both to a given directory in an open root file and saving
 * the canvas as a ".png" file to a specified folder (option to draw vertical line and mathematical
 * function) \param can canvas on which the histogram should be drawn \param hist histogram drawn on
 * the canvas \param dir directory in an open root file where can and hist should be written to
 * \param line_pos position where to draw a vertical line (if set to "-999.9" no line will be drawn)
 * \param fun mathematical function to be drawn on histogram (if set to "f_empty" no function will
 * be drawn) \param name path and name for saving the canvas as picture
 */
void draw_write_and_save_picture(TCanvas* can, TH1* hist, TDirectory* dir, Float_t line_pos,
                                 TF1* fun, TString name);

/**
 * Drawing a histogram on canvas and writing both to a given directory in an open root file (option
 * to draw vertical line) \param can canvas on which the histogram should be drawn \param hist 2D
 * histogram drawn on the canvas \param dir directory in an open root file where can and hist should
 * be written to \param line_pos position where to draw a vertical line (if set to "-999.9" no line
 * will be drawn)
 */
void draw_and_write_2D(TCanvas* can, TH2* hist, TDirectory* dir, Float_t line_pos);

/**
 * Drawing a histogram on canvas, writing both to a given directory in an open root file and saving
 * the canvas as a ".png" file to a specified folder (option to draw vertical line) \param can
 * canvas on which the histogram should be drawn \param hist 2D histogram drawn on the canvas \param
 * dir directory in an open root file where can and hist should be written to \param line_pos
 * position where to draw a vertical line (if set to "-999.9" no line will be drawn) \param name
 * path and name for saving the canvas as picture
 */
void draw_write_and_save_picture_2D(TCanvas* can, TH2* hist, TDirectory* dir, Float_t line_pos,
                                    TString name);

/**
 * Drawing a histogram on canvas and writing both to a given directory in an open root file (option
 * to draw vertical line and mathematical function) \param can canvas on which the histogram should
 * be drawn \param hist 2D histogram drawn on the canvas \param dir directory in an open root file
 * where can and hist should be written to \param line_pos position where to draw a vertical line
 * (if set to "-999.9" no line will be drawn) \param fun mathematical function to be drawn on
 * histogram (if set to "f_empty" no function will be drawn)
 */
void draw_and_write_2D(TCanvas* can, TH2* hist, TDirectory* dir, Float_t line_pos, TF1* fun);

/**
 * Drawing a histogram on canvas, writing both to a given directory in an open root file and saving
 * the canvas as a ".png" file to a specified folder (option to draw vertical line and mathematical
 * function) \param can canvas on which the histogram should be drawn \param hist 2D histogram drawn
 * on the canvas \param dir directory in an open root file where can and hist should be written to
 * \param line_pos position where to draw a vertical line (if set to "-999.9" no line will be drawn)
 * \param fun mathematical function to be drawn on histogram (if set to "f_empty" no function will
 * be drawn) \param name path and name for saving the canvas as picture
 */
void draw_write_and_save_picture_2D(TCanvas* can, TH2* hist, TDirectory* dir, Float_t line_pos,
                                    TF1* fun, TString name);

} // namespace Drawing

//! Miscellaneous functions
namespace Miscellaneous
{
// Structure for storing gaus fitting parameters
struct gaus_params
{
    Float_t gaus_amp;
    Float_t gaus_mean;
    Float_t gaus_sigma;

    Float_t gaus_amp_error;
    Float_t gaus_mean_error;
    Float_t gaus_sigma_error;
};

/**
 * Calculating x (or y) position of track intersection with plane (perpendicular to z axis) of a
 * detector \param x1 Coordinate X (or Y) of first point defining track (e.g. track start point)
 * \param x2 Coordinate X (or Y) of second point defining track (e.g. track start point + track dir
 * vector) \param z1 Coordinate Z of first point defining track (e.g. track start point) \param z2
 * Coordinate Z of second point defining track (e.g. track start point + track dir vector) \param
 * z_strip Coordinate Z of the detector plane \return Coordinate X (or Y) of track intersection with
 * detector plane
 */
Float_t calc_hit_pos(Float_t x1, Float_t x2, Float_t z1, Float_t z2, Float_t z_strip);

/**
 * Fitting a gaussian distribution to histogram (width +/- 7 around max value)
 * \param hist Histogram on which fitting is to be performed
 * \return Fitting parameters in the form of "gaus_params" structure
 */
gaus_params fit_gaus(TH1* hist);
} // namespace Miscellaneous

} // namespace ForwardTools

#endif /* FORWARD_TOOLS_H */
