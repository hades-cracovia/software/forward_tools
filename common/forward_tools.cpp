#include "forward_tools.h"
#include "ft_config.h"

#include <hcategory.h>
#include <hcategorymanager.h>
#include <hforwardcand.h>
#include <hparticlecand.h>
#include <hphysicsconstants.h>

#include <TCanvas.h>
#include <TColor.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1I.h>
#include <TH2F.h>
#include <TH2I.h>
#include <TH3I.h>
#include <THStack.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TMatrixD.h>
#include <TVector3.h>

using namespace ConfigBeamtime;

//----------------------------------------------------------------------------------------------
// Checking if event is elastic - HADES-HADES case
bool ForwardTools::Elastics::check_elastics_hh(HCategory* pcat, Float_t phi_diff_min,
                                               Float_t phi_diff_max, Float_t thetap_diff_min,
                                               Float_t thetap_diff_max)
{
    // Checking if ParticleCand is open
    if (!pcat) return false;

    auto particle_cand_cnt = pcat->getEntries();

    // Checking for 2 tracks in HADES
    if (particle_cand_cnt != 2) return false;

    // Particle 1 - HADES particle
    HParticleCand* cand1 = HCategoryManager::getObject(cand1, pcat, 0);

    auto t_phi_P1 = cand1->getPhi();     // [deg]
    auto t_theta_P1 = cand1->getTheta(); // [deg]

    // Particle 2 - HADES particle
    HParticleCand* cand2 = HCategoryManager::getObject(cand2, pcat, 1);

    auto t_phi_P2 = cand2->getPhi();     // [deg]
    auto t_theta_P2 = cand2->getTheta(); // [deg]

    // Two Partices Variables
    auto t_phi_diff = TMath::Abs(t_phi_P1 - t_phi_P2); // [deg]
    auto t_tan_theta_product = TMath::Tan(TMath::DegToRad() * t_theta_P1) *
                               TMath::Tan(TMath::DegToRad() * t_theta_P2); // [ ]

    return (t_phi_diff >= phi_diff_min && t_phi_diff <= phi_diff_max) &&
           (t_tan_theta_product >= thetap_diff_min && t_tan_theta_product <= thetap_diff_max);
}

//----------------------------------------------------------------------------------------------
// Checking if event is elastic - HADES-FwDet case
bool ForwardTools::Elastics::check_elastics_hf(HCategory* pcat, HCategory* fcat,
                                               Float_t phi_diff_min, Float_t phi_diff_max,
                                               Float_t thetap_diff_min, Float_t thetap_diff_max)
{
    // Checking if ParticleCand and ForwardCand are open
    if (!pcat or !fcat) return false;

    auto particle_cand_cnt = pcat->getEntries();
    auto forward_cand_cnt = fcat->getEntries();

    // Checking for 1 track in HADES and 1 track in FwDet
    if (particle_cand_cnt != 1 or forward_cand_cnt != 1) return false;

    // Particle 1 - HADES Particle
    HParticleCand* cand1 = HCategoryManager::getObject(cand1, pcat, 0);

    auto t_phi_P1 = cand1->getPhi();     // [deg]
    auto t_theta_P1 = cand1->getTheta(); // [deg]

    // Particle 2 - FwDet Particle
    HForwardCand* cand2 = HCategoryManager::getObject(cand2, fcat, 0);

    //                 if (cand2->getTofRec() <= 0)
    //                     t_ToF_Rec = 0;
    //                 else if (cand2->getTofRec() > 0)
    //                     t_ToF_Rec = 1;

    // Correction for track start position (Z_START)
    auto t_z_start_P2 = cand1->getZ();          // [mm]
    cand2->setStartXYZ(0.0, 0.0, t_z_start_P2); // We take the track start position from the
                                                // reconstructed HADES track paired with FwDet track
    cand2->calcPoints(); // Recalculating ToF and Distance to account for correction
    cand2->calc4vectorProperties(HPhysicsConstants::mass(14)); // Assume proton mass

    auto t_phi_P2 = cand2->getPhi();     // [deg]
    auto t_theta_P2 = cand2->getTheta(); // [deg]

    // Two Particles Variables
    auto t_phi_diff = TMath::Abs(t_phi_P1 - t_phi_P2); // [deg]
    auto t_tan_theta_product = TMath::Tan(TMath::DegToRad() * t_theta_P1) *
                               TMath::Tan(TMath::DegToRad() * t_theta_P2); // [ ]

    return (t_phi_diff >= phi_diff_min && t_phi_diff <= phi_diff_max) &&
           (t_tan_theta_product >= thetap_diff_min && t_tan_theta_product <= thetap_diff_max);
}

//----------------------------------------------------------------------------------------------
// Alternate ToF calculation scheme to bypass lack of T0 measurement
Float_t ForwardTools::Elastics::alt_ToF_calc(Float_t h_exp_ToF, Float_t h_theor_ToF,
                                             Float_t f_exp_ToF)
{
    return f_exp_ToF - h_exp_ToF + h_theor_ToF;
}

//----------------------------------------------------------------------------------------------
// Theoretical momentum of elasticaly scattered proton
Float_t ForwardTools::Elastics::calc_theor_el_mom(
    Float_t theta) // !!! IMPORTANT - Theta has to be input in radians !!!
{
    return beam_p01 / ((TMath::Cos(theta)) *
                       (1 + TMath::Power(TMath::Tan(theta) * beam_gamma_cm,
                                         2))); // Momentum of scattered proton (Lab) [MeV/c]
}

//----------------------------------------------------------------------------------------------
// Theoretical energy of elasticaly scattered proton
Float_t ForwardTools::Elastics::calc_theor_el_energy(
    Float_t theta) // !!! IMPORTANT - Theta has to be input in radians !!!
{
    Float_t mom = ForwardTools::Elastics::calc_theor_el_mom(
        theta); // Momentum of scattered proton (Lab) [MeV/c]
    return TMath::Sqrt(TMath::Power(mom, 2) +
                       TMath::Power(proton_mass, 2)); // Energy of scattered proton [MeV/c^2]
}

//----------------------------------------------------------------------------------------------
// Theoretical beta of elasticaly scattered proton
Float_t ForwardTools::Elastics::calc_theor_el_beta(
    Float_t theta) // !!! IMPORTANT - Theta has to be input in radians !!!
{
    Float_t energy =
        ForwardTools::Elastics::calc_theor_el_energy(theta); // Energy of scattered proton [MeV/c^2]
    Float_t gamma = energy / proton_mass;                    // Gamma of scattered proton [_]
    return TMath::Sqrt(1 - 1 / (pow(gamma, 2)));             // Beta of scattered proton [_]
}

//----------------------------------------------------------------------------------------------
// Theoretical ToF of elasticaly scattered proton
Float_t ForwardTools::Elastics::calc_theor_el_ToF(
    Float_t dist, Float_t theta) // !!! IMPORTANT - Theta has to be input in radians !!!
{
    Float_t beta =
        ForwardTools::Elastics::calc_theor_el_beta(theta); // Beta of scattered proton [_]
    return (dist / (beta * c_value));                      // ToF for elastic proton scattering [ns]
}

//----------------------------------------------------------------------------------------------
// Calculate hit position (X or Y) in fRPC from intersection of track with fRPC plane
Float_t ForwardTools::Miscellaneous::calc_hit_pos(Float_t x1, Float_t x2, Float_t z1, Float_t z2,
                                                  Float_t z_strip)
{
    Float_t slope = (x2 - x1) / (z2 - z1); // Track slope parameter
    Float_t intercept = x2 - slope * z2;   // Track intercept parameter
    return slope * z_strip + intercept;    // Hit position
}

// Fitting gaussian distribution to histogram and extracting fit parameters
ForwardTools::Miscellaneous::gaus_params ForwardTools::Miscellaneous::fit_gaus(TH1* hist)
{
    // Definign output parameters
    gaus_params PARAMETERS;

    int max_bin = hist->GetMaximumBin(); // Bin with max value in histogram
    float_t max_bin_x = hist->GetXaxis()->GetBinCenter(max_bin);
    float_t x_min = max_bin_x - 7;
    float_t x_max = max_bin_x + 7;

    float_t max_val = hist->GetBinContent(max_bin); // Maximum value in histogram
    if (max_val == 0.0)
    {
        // Extracting fit parameters values
        PARAMETERS.gaus_amp = 0.0;
        PARAMETERS.gaus_mean = 0.0;
        PARAMETERS.gaus_sigma = 0.0;

        // Extracting fit parameters error values
        PARAMETERS.gaus_amp_error = 0.0;
        PARAMETERS.gaus_mean_error = 0.0;
        PARAMETERS.gaus_sigma_error = 0.0;
    }

    // If histogram empty, no correction

    // Fitted function definition
    TF1* fun_gaus = new TF1("fit_gaus", "gaus", x_min, x_max);

    // Loop over bins - finding half width value
    int i = max_bin;
    Float_t prev_val = max_val; // Previous value
    for (; i > 0; --i)
    {
        Float_t t_val = hist->GetBinContent(i);

        if (t_val < max_val / 2. && prev_val > max_val / 2.) break; // Half width found
        prev_val = t_val;
    }

    // Setting starting fit parameters
    fun_gaus->SetParameters(0, max_val);
    fun_gaus->SetParameters(1, max_bin_x);
    fun_gaus->SetParameters(2, 2 * TMath::Abs(hist->GetBinCenter(i) - hist->GetBinCenter(max_bin)));

    // Fitting gaussian peak
    hist->Fit(fun_gaus, "R Q");
    // gStyle->SetOptFit(1111);

    fun_gaus->SetLineColor(kGreen);
    fun_gaus->Draw("same");

    // Extracting fit parameters values
    PARAMETERS.gaus_amp = fun_gaus->GetParameter(0);
    PARAMETERS.gaus_mean = fun_gaus->GetParameter(1);
    PARAMETERS.gaus_sigma = fun_gaus->GetParameter(2);

    // Extracting fit parameters error values
    PARAMETERS.gaus_amp_error = fun_gaus->GetParError(0);
    PARAMETERS.gaus_mean_error = fun_gaus->GetParError(1);
    PARAMETERS.gaus_sigma_error = fun_gaus->GetParError(2);

    return PARAMETERS;
}

//----------------------------------------------------------------------------------------------
// Drawing functions
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Drawing a red vertical line
void ForwardTools::Drawing::draw_vertical_line(TVirtualPad* can, TH1* hist, Float_t line_position)
{
    int max_bin = hist->GetMaximumBin();
    float_t max_val = hist->GetBinContent(max_bin);

    TLine line;
    line.SetLineColor(kRed);
    line.SetLineStyle(9);
    line.SetLineWidth(2);

    can->cd();
    line.DrawLine(line_position, 0, line_position, max_val);
}

//----------------------------------------------------------------------------------------------
// Drawing vertical line of chosen color
void ForwardTools::Drawing::draw_vertical_line_color(TVirtualPad* can, TH1* hist,
                                                     Float_t line_position, Int_t color)
{
    int max_bin = hist->GetMaximumBin();
    float_t max_val = hist->GetBinContent(max_bin);

    TLine line;
    line.SetLineColor(color);
    line.SetLineStyle(9);
    line.SetLineWidth(2);

    can->cd();
    line.DrawLine(line_position, 0, line_position, max_val);
}

//----------------------------------------------------------------------------------------------
// Draw histogram (on canvas) and write to directory in root file
void ForwardTools::Drawing::draw_and_write(TCanvas* can, TH1* hist, TDirectory* dir,
                                           Float_t line_pos)
{
    // Entering directory "dir" in root file
    if (dir) dir->cd();

    // Drawing histogram "hist" to canvas "can"
    can->cd();
    hist->Draw("colz");
    if (line_pos != -999.9)
        draw_vertical_line(gPad, hist, line_pos); // Drawing a vertical line at specified position

    // Writing histogram and canvas to root file
    can->Write();
    hist->Write();
}

//----------------------------------------------------------------------------------------------
// Draw histogram (on canvas), write to directory in root file and save them as pictures
void ForwardTools::Drawing::draw_write_and_save_picture(TCanvas* can, TH1* hist, TDirectory* dir,
                                                        Float_t line_pos, TString name)
{
    draw_and_write(can, hist, dir, line_pos);

    // Saving canvas as picture
    if (!name.IsNull()) { can->SaveAs(name); }
}

//----------------------------------------------------------------------------------------------
// Draw 2 histograms (on canvas) and write to directory in root file
void ForwardTools::Drawing::draw_and_write_x2(TCanvas* can, TH1* hist1, TH1* hist2, TDirectory* dir,
                                              Float_t line_pos)
{
    // Entering directory "dir" in root file
    if (dir) dir->cd();

    // Drawing histogram "hist1" to canvas "can"
    can->cd();
    hist1->Draw("colz");
    hist2->Draw("same colz");
    if (line_pos != -999.9)
        draw_vertical_line(gPad, hist1, line_pos); // Drawing a vertical line at specified position

    // Writing histogram and canvas to root file
    can->Write();
    hist1->Write();
    hist2->Write();
}

//----------------------------------------------------------------------------------------------
// Draw 2 histograms (on canvas), write to directory in root file and save them as pictures
void ForwardTools::Drawing::draw_write_and_save_picture_x2(TCanvas* can, TH1* hist1, TH1* hist2,
                                                           TDirectory* dir, Float_t line_pos,
                                                           TString name)
{
    draw_and_write_x2(can, hist1, hist2, dir, line_pos);

    // Saving canvas as picture
    if (!name.IsNull()) { can->SaveAs(name); }
}

//----------------------------------------------------------------------------------------------
// Draw histogram (on canvas) and write to directory in root file (With math function drawing)
void ForwardTools::Drawing::draw_and_write(TCanvas* can, TH1* hist, TDirectory* dir,
                                           Float_t line_pos, TF1* fun)
{
    // Entering directory "dir" in root file
    if (dir) dir->cd();

    // Drawing histogram "hist" to canvas "can"
    can->cd();
    hist->Draw("colz");
    if (line_pos != -999.9)
        draw_vertical_line(gPad, hist, line_pos); // Drawing a vertical line at specified position

    // Drawing function
    fun->Draw("same");

    // Writing histogram and canvas to root file
    can->Write();
    hist->Write();
}

//----------------------------------------------------------------------------------------------
// Draw histogram (on canvas), write to directory in root file and save them as pictures
void ForwardTools::Drawing::draw_write_and_save_picture(TCanvas* can, TH1* hist, TDirectory* dir,
                                                        Float_t line_pos, TF1* fun, TString name)
{
    draw_and_write(can, hist, dir, line_pos, fun);

    // Saving canvas as picture
    if (!name.IsNull()) { can->SaveAs(name); }
}

//----------------------------------------------------------------------------------------------
// Draw 2D histogram (on canvas) and write to directory in root file
void ForwardTools::Drawing::draw_and_write_2D(TCanvas* can, TH2* hist, TDirectory* dir,
                                              Float_t line_pos)
{
    // Entering directory "dir" in root file
    if (dir) dir->cd();

    // Drawing histogram "hist" to canvas "can"
    can->cd();
    hist->Draw("colz");
    if (line_pos != -999.9)
        draw_vertical_line(gPad, hist, line_pos); // Drawing a vertical line at specified position

    // Writing histogram and canvas to root file
    can->Write();
    hist->Write();
}

//----------------------------------------------------------------------------------------------
// Draw 2D histogram (on canvas), write to directory in root file and save them as pictures
void ForwardTools::Drawing::draw_write_and_save_picture_2D(TCanvas* can, TH2* hist, TDirectory* dir,
                                                           Float_t line_pos, TString name)
{
    draw_and_write(can, hist, dir, line_pos);

    // Saving canvas as picture
    if (!name.IsNull()) { can->SaveAs(name); }
}

//----------------------------------------------------------------------------------------------
// Draw 2D histogram (on canvas) and write to directory in root file (With math function drawing)
void ForwardTools::Drawing::draw_and_write_2D(TCanvas* can, TH2* hist, TDirectory* dir,
                                              Float_t line_pos, TF1* fun)
{
    // Entering directory "dir" in root file
    if (dir) dir->cd();

    // Drawing histogram "hist" to canvas "can"
    can->cd();
    hist->Draw("colz");
    if (line_pos != -999.9)
        draw_vertical_line(gPad, hist, line_pos); // Drawing a vertical line at specified position

    // Drawing function
    fun->Draw("same");

    // Writing histogram and canvas to root file
    can->Write();
    hist->Write();
}

//----------------------------------------------------------------------------------------------
// Draw 2D histogram (on canvas), write to directory in root file and save them as pictures
void ForwardTools::Drawing::draw_write_and_save_picture_2D(TCanvas* can, TH2* hist, TDirectory* dir,
                                                           Float_t line_pos, TF1* fun, TString name)
{
    draw_and_write(can, hist, dir, line_pos, fun);

    // Saving canvas as picture
    if (!name.IsNull()) { can->SaveAs(name); }
}
